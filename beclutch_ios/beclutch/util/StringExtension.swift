//
//  StringExtension.swift
//  beclutch
//
//  Created by SM on 2/18/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: self)
    }
    
    func intValue() -> Int {
        return Int(self) ?? 0
    }
    
    func boolValueFromServer() -> Bool {
        return self == "1"
    }

    static func moneyFormat(amount: Float?) -> String {
        guard let amount = amount else {
            return "$0"
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        let preciseAmount = String(format: "%.2f", amount)
//        let preciseAmount = formatter.string(from: NSNumber(value: amount))
        return "$\(preciseAmount ?? "0")"
    }
    
    static func isNilOrBlank(text: String?) -> Bool {
        return text == nil || text?.count == 0
    }
}
