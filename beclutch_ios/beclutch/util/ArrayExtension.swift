//
//  ArrayExtension.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension Array {
    static func toJsonString(array: [Any]?) -> String? {
        if array?.count == 0 {
            return nil
        }

        do {
            let data = try JSONSerialization.data(withJSONObject: array ?? [], options: .prettyPrinted)

            if let rosterStr = String(data: data, encoding: .utf8) {
                return rosterStr
            }
        } catch {
            print(error)
        }

        return nil
    }

    static func toCommaString(array: [Any]?) -> String {
        guard let array = array, array.count > 0 else {
            return ""
        }

        let result = array.map { (element) -> String in
            do {
                let elementData = try JSONSerialization.data(withJSONObject: element, options: .prettyPrinted)
                if let elementStr = String(data: elementData, encoding: .utf8) {
                    return elementStr
                }
            } catch {
                print(error)
            }

            return ""
        }.joined(separator: ",")

        return result
    }

    static func fromCommaString(str: String) -> [String] {
        var result = str

        let extraCharacters = ["[", "]", "\n", " "]
        extraCharacters.forEach {
            result = result.replacingOccurrences(of: $0, with: "")
        }
        return result.components(separatedBy: ",")
    }
}
