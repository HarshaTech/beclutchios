//
//  BLButton.swift
//  beclutch
//
//  Created by SM on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class BLButton: UIButton {
    var radius: CGFloat = 6.0

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpCorner(radius: radius)
        contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCorner(radius: radius)
        contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    }

    // MARK: - Public
    func setUpCorner(radius: CGFloat) {
        self.radius = radius
        layer.cornerRadius = self.radius
    }
}
