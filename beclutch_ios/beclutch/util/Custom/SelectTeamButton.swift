//
//  SelectTeamButton.swift
//  beclutch
//
//  Created by Vu Trinh on 27/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit

class SelectTeamButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()

        refreshUI()
    }

    func refreshUI() {
        semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)

        makeStrokeOnlyButton()
    }
}
