//
//  CircleImageView.swift
//  beclutch
//
//  Created by SM on 2/23/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class CircleImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = self.width/2.0
    }

}
