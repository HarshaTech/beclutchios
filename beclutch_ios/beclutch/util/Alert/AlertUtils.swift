//
//  AlertUtils.swift
//  videobridge
//
//  Created by Zeus on 2019/5/31.
//  Copyright © 2019 Zeus. All rights reserved.
//

import UIKit
import PopupDialog

public class AlertUtils {
    static func showWarningAlert(title: String, message: String, parent: UIViewController) {
        parent.alert(title: title, message: message, okTitle: "OK")
    }

    static func showWarningAlertWithHandler(title: String, message: String, parent: UIViewController, handler:@escaping () -> Void) {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
            handler()
        }
        popup.addButton(buttonTwo)
        parent.present(popup, animated: true, completion: nil)
    }

    static func showConfirmAlertWithHandler(title: String, message: String, confirmTitle: String? = nil, parent: UIViewController, handler:@escaping () -> Void) {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonTwo = DefaultButton(title: confirmTitle ?? "OK", dismissOnTap: true) {
            handler()
        }
        let buttonCancel = CancelButton(title: "No", dismissOnTap: true) {

        }
        popup.addButtons([buttonTwo, buttonCancel])
        parent.present(popup, animated: true, completion: nil)
    }

    static func showSaveAlertWithHandler(title: String, message: String, parent: UIViewController, handler_cancel:@escaping () -> Void, handler_save:@escaping () -> Void) {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonTwo = DefaultButton(title: "Save", dismissOnTap: true) {
            handler_save()
        }
        let buttonCancel = CancelButton(title: "Cancel", dismissOnTap: true) {
            handler_cancel()
        }
        popup.addButtons([buttonTwo, buttonCancel])
        parent.present(popup, animated: true, completion: nil)
    }

    static func setPopupStyle() {
        let dialogAppearance = PopupDialogDefaultView.appearance()
        dialogAppearance.titleFont            = .boldSystemFont(ofSize: 28)
        dialogAppearance.messageFont          = .systemFont(ofSize: 24)

    }

    static func showGeneralError(message: String?, parent: UIViewController?) {
        DispatchQueue.main.async {
            let messageStr = message ?? "Something go wrong"
            // Create the dialog
            let popup = PopupDialog(title: "Error", message: messageStr, image: nil)
            // This button will not the dismiss the dialog
            let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {

            }
            // Add buttons to dialog
            // Alternatively, you can use popup.addButton(buttonOne)
            // to add a single button
            popup.addButton(buttonTwo)

            // Present dialog
            if let viewController = parent {
                viewController.present(popup, animated: true, completion: nil)
            } else {
                NavUtil.topViewController()?.present(popup, animated: true, completion: nil)
            }
            
        }
    }
}
