//
//  CalendarPickerCommon.swift
//  beclutch
//
//  Created by Vu Trinh on 10/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class CalendarPickerCommon {
    static func picker() -> WWCalendarTimeSelector {
        let selector = WWCalendarTimeSelector.instantiate()
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        selector.optionButtonShowCancel = true

        selector.optionSelectorPanelBackgroundColor = .COLOR_PRIMARY_BECLUTCH
        selector.optionTopPanelBackgroundColor = .COLOR_PRIMARY_DARK_BECLUTCH

        selector.optionCalendarBackgroundColorTodayFlash = .COLOR_PRIMARY_BECLUTCH
        selector.optionCalendarBackgroundColorTodayHighlight = .COLOR_PRIMARY_BECLUTCH
        
        selector.optionCalendarBackgroundColorFutureDatesHighlight = .COLOR_PRIMARY_BECLUTCH

        selector.optionButtonFontColorCancel = .COLOR_PRIMARY_BECLUTCH
        selector.optionButtonFontColorDone = .COLOR_PRIMARY_BECLUTCH
        
        return selector
    }
}
