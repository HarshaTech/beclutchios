//
//  CustomLabelManager.swift
//  beclutch
//
//  Created by Vu Trinh on 23/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct CustomLabel: Equatable, ExpressibleByStringLiteral {
    let key: String
    let defaultValue: String

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key
    }

    init(stringLiteral value: String) {
        let components = value.components(separatedBy: ",")
        if components.count == 2 {
            self.key = components[0]
            self.defaultValue = components[1]
        } else {
            self.key = ""
            self.defaultValue = ""
        }
    }
}

enum CustomLableEnum: CustomLabel {
    case txt_team           = "txt_team,Team"
    case txt_add_team       = "txt_add_team,Add Team"
    case txt_choose_sport       = "txt_choose_sport,Choose Sport"
    case txt_arrive_early_game       = "txt_arrive_early_game,Arrive Early (Game)"
    case txt_practice_duration       = "txt_practice_duration,Practice Duration"
    case txt_team_information       = "txt_team_information,Team Information"
    case txt_team_setting       = "txt_team_setting,Team Settings"
    case txt_team_management_s       = "txt_team_management_s,Team Management (%s)"
    case txt_team_plans       = "txt_team_plans,Team Plans"
    case txt_team_chat       = "txt_team_chat,Team Chat"
    case txt_start_team_chat       = "txt_start_team_chat,Start Team Chat"
    case txt_last_team_chat_message       = "txt_last_team_chat_message,Last Team Chat Message"
    case txt_create_a_team       = "txt_create_a_team,Create a Team"
    case txt_email_team       = "txt_email_team,Email Team"
    case txt_player_number       = "txt_player_number,Player Number #"
    
    case ctl_calendar_btPractice       = "ctl_calendar_btPractice,1"
    case ctl_calendar_btGame       = "ctl_calendar_btGame,1"
    case ctl_addtoroster_chRepresentingParent       = "ctl_addtoroster_chRepresentingParent,1"
    case ctl_addtoroster_chRepresentingPlayer       = "ctl_addtoroster_chRepresentingPlayer,1"
    case ctl_addtoroster_txtRepresentingQuestion       = "ctl_addtoroster_txtRepresentingQuestion,1"
    case ctl_roster_profiledetail_chRepresenting       = "ctl_roster_profiledetail_chRepresenting,1"
    case ctl_roster_profiledetail_btnHelp       = "ctl_roster_profiledetail_btnHelp,1"
    case ctl_roster_profileedit_chRepresenting       = "ctl_roster_profileedit_chRepresenting,1"

    case ctl_roster_profileedit_btnHelp       = "ctl_roster_profileedit_btnHelp,1"
    case ctl_settings_btnOpponent       = "ctl_settings_btnOpponent,1"
    case ctl_teamsettings_btnUniformAway       = "ctl_teamsettings_btnUniformAway,1"
    case ctl_teamsettings_btnUniformHome       = "ctl_teamsettings_btnUniformHome,1"
    case ctl_teamsettings_txtHome       = "ctl_teamsettings_txtHome,1"
    case ctl_teamsettings_txtAway       = "ctl_teamsettings_txtAway,1"
    case ctl_teamsettings_hdrUniformColors       = "ctl_teamsettings_hdrUniformColors,1"
    case ctl_accountinfo_btnSplitAccount       = "ctl_accountinfo_btnSplitAccount,1"
    case ctl_accountinfo_btnHelp       = "ctl_accountinfo_btnHelp,1"
    case ctl_notifications_txtScoreReporting       = "ctl_notifications_txtScoreReporting,1"
    case ctl_notifications_chkAppScore       = "ctl_notifications_chkAppScore,1"
    case ctl_notifications_chkEmailScore       = "ctl_notifications_chkEmailScore,1"
}

class CustomLabelManager {
    static let shared = CustomLabelManager()

    private var labelDict: [String: String] = [:]

    func updateLabelDict(dict: [String: String]?) {
        guard let dict = dict else {
            return
        }

        labelDict = dict
    }

    func getCutomLabel(label: CustomLabel) -> String {
        return labelDict[label.key] ?? label.defaultValue
    }

    func getCutomLabel(label: CustomLabel, replacedString: String) -> String {
        var result = labelDict[label.key] ?? label.defaultValue
        result = result.replacingOccurrences(of: "%s", with: replacedString)

        if let range = result.range(of: "%s") {
            result = result.replacingCharacters(in: range, with: replacedString)
        }

        return result
    }
}
