//
//  AccountCommon.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class AccountCommon {

    static func checkRightError() -> String? {
        guard let _ = AppDataInstance.instance.currentSelectedTeam else {
            return "You don't belong any team yet."
        }

        if (Int(AppDataInstance.instance.currentSelectedTeam!.ROLE_INFO.ID)!) > Int(UserRoles.TEAM_MANAGER)! && (Int(AppDataInstance.instance.currentSelectedTeam!.ROLE_INFO.ID)!) < Int(UserRoles.TEAM_TREASURE)! {
            return "You don't have right to add."
        }

        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            return "You need to accept invitation"
        }

        return nil
    }
    
    static func isMe(rosterId: String?) -> Bool {
        if let rosterId = rosterId {
            return AppDataInstance.instance.me?.ID == rosterId
        }
        
        return false
    }

    static func isCoachOrManager(roster: RosterInfo? = AppDataInstance.instance.currentSelectedTeam) -> Bool {
        if let levelId = roster?.ROLE_INFO.ID {
            if levelId == UserRoles.TEAM_COACH || levelId == UserRoles.CLUB_OWNER || levelId == UserRoles.TEAM_MANAGER {
                return true
            }
        }

        return false
    }
    
    static func isTreasure(roster: RosterInfo? = AppDataInstance.instance.currentSelectedTeam) -> Bool {
        if let levelId = roster?.ROLE_INFO.ID {
            if levelId == UserRoles.TEAM_TREASURE || levelId == UserRoles.CLUB_TREASURER {
                return true
            }
        }

        return false
    }
    
    static func isPlayer(roster: RosterInfo? = AppDataInstance.instance.currentSelectedTeam) -> Bool {
        if let levelId = roster?.ROLE_INFO.ID {
            if levelId == UserRoles.PLAYER {
                return true
            }
        }

        return false
    }
    
    static func isGuest(roster: RosterInfo? = AppDataInstance.instance.currentSelectedTeam) -> Bool {
        if let levelId = roster?.ROLE_INFO.ID {
            if levelId == UserRoles.GUEST {
                return true
            }
        }

        return false
    }

    static func getRoleString(roster: RosterInfo) -> String {
        switch roster.LEVEL {
        case UserRoles.TEAM_COACH:
            return "Coach"
        case UserRoles.TEAM_MANAGER:
            return "Manager"
        case UserRoles.PLAYER:
            return "Player"
        case UserRoles.TEAM_TREASURE:
            return "Treasure"
        default:
            return "Other"
        }
    }

    static func isCoachManagerOrTreasure(roster: RosterInfo? = AppDataInstance.instance.currentSelectedTeam) -> Bool {
        return isCoachOrManager() || isTreasure()
    }
}
