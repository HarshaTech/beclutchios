//
//  ColorUtils.swift
//  videobridge
//
//  Created by Zeus on 2019/7/9.
//  Copyright © 2019 Zeus. All rights reserved.
//

import Foundation
import UIKit
public class ColorUtils {
    static func getDarkBlueColor() -> UIColor {
        return UIColor(red: 0.04, green: 0.11, blue: 0.25, alpha: 1.0)
    }

    static func getUIColorFromRGB(val: Int32) -> UIColor {
        let red =   CGFloat((val >> 16) & 0x000000FF) / 0xFF
        let green = CGFloat((val >> 8 ) & 0x000000FF) / 0xFF
        let blue =  CGFloat(val & 0x000000FF) / 0xFF
        let alpha = CGFloat(1.0)

        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}

extension UIColor {
    convenience public init(hex: String, alpha: CGFloat = 1) {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: alpha)
    }

    static var primary: UIColor {
//      return UIColor(hex: "#6D9B1C")
        return getColor("primary")
    }

    static var colorBlue: UIColor {
        return getColor("colorBlue")
    }

    static var colorRed: UIColor {
        return getColor("colorAccent")
    }

    static var messagePrimary: UIColor {
        return primary
//      return UIColor(red: 1 / 255, green: 93 / 255, blue: 48 / 255, alpha: 1)
    }

    static var incomingMessage: UIColor {
      return UIColor(red: 230 / 255, green: 230 / 255, blue: 230 / 255, alpha: 1)
    }

    static private func getColor(_ name: String) -> UIColor {
        let bundle = Bundle(identifier: "com.rock.beclutch")
        return UIColor(named: name, in: bundle, compatibleWith: nil) ?? COLOR_PRIMARY
    }

    static var COLOR_PRIMARY: UIColor {
        var themeValue: String
        themeValue = PreferenceMgr.readThemeValue()
        if themeValue.isEmpty {
            return getColor("colorPrimary")
        } else {
            return getColor("colorPrimary_" + themeValue)
        }
    }
    static var COLOR_PRIMARY_DARK: UIColor {
        var themeValue: String
        themeValue = PreferenceMgr.readThemeValue()
        if themeValue.isEmpty {
            return getColor("colorPrimaryDark")
        } else {
            return getColor("colorPrimaryDark_" + themeValue)
        }
    }
    static var COLOR_BLUE: UIColor {
        var themeValue: String
        themeValue = PreferenceMgr.readThemeValue()
        if themeValue.isEmpty || themeValue == "beclutch" {
            return getColor("colorBlue")
        } else {
            return getColor("colorPrimaryDark_" + themeValue)
        }
    }
    static var COLOR_ACCENT: UIColor {
        return getColor("colorAccent")
    }
    static var COLOR_PRIMARY_BECLUTCH: UIColor {
        return getColor("colorPrimary_beclutch")
    }
    static var COLOR_PRIMARY_DARK_BECLUTCH: UIColor {
        return getColor("colorPrimaryDark_beclutch")
    }
    static var COLOR_PRIMARY_BLUE: UIColor {
        return getColor("colorPrimary_blue")
    }
    static var COLOR_PRIMARY_DARK_BLUE: UIColor {
        return getColor("colorPrimaryDark_blue")
    }
    static var COLOR_PRIMARY_DARKBLUE: UIColor {
        return getColor("colorPrimary_darkblue")
    }
    static var COLOR_PRIMARY_DARK_DARKBLUE: UIColor {
        return getColor("colorPrimaryDark_darkblue")
    }
    static var COLOR_PRIMARY_LIGHTBLUE: UIColor {
        return getColor("colorPrimary_lightblue")
    }
    static var COLOR_PRIMARY_DARK_LIGHTBLUE: UIColor {
        return getColor("colorPrimaryDark_lightblue")
    }
    static var COLOR_PRIMARY_LIME: UIColor {
        return getColor("colorPrimary_lime")
    }
    static var COLOR_PRIMARY_DARK_LIME: UIColor {
        return getColor("colorPrimaryDark_lime")
    }
    static var COLOR_PRIMARY_GREEN: UIColor {
        return getColor("colorPrimary_green")
    }
    static var COLOR_PRIMARY_DARK_GREEN: UIColor {
        return getColor("colorPrimaryDark_green")
    }
    static var COLOR_PRIMARY_ORANGE: UIColor {
        return getColor("colorPrimary_orange")
    }
    static var COLOR_PRIMARY_DARK_ORANGE: UIColor {
        return getColor("colorPrimaryDark_orange")
    }
    static var COLOR_PRIMARY_BLACK: UIColor {
        return getColor("colorPrimary_black")
    }
    static var COLOR_PRIMARY_DARK_BLACK: UIColor {
        return getColor("colorPrimaryDark_black")
    }
    static var COLOR_PRIMARY_DARKRED: UIColor {
        return getColor("colorPrimary_darkred")
    }
    static var COLOR_PRIMARY_DARK_DARKRED: UIColor {
        return getColor("colorPrimaryDark_darkred")
    }
    static var COLOR_PRIMARY_RED: UIColor {
        return getColor("colorPrimary_red")
    }
    static var COLOR_PRIMARY_DARK_RED: UIColor {
        return getColor("colorPrimaryDark_red")
    }
    static var COLOR_PRIMARY_MAROON: UIColor {
        return getColor("colorPrimary_maroon")
    }
    static var COLOR_PRIMARY_DARK_MAROON: UIColor {
        return getColor("colorPrimaryDark_maroon")
    }
    static var COLOR_PRIMARY_GOLD: UIColor {
        return getColor("colorPrimary_gold")
    }
    static var COLOR_PRIMARY_DARK_GOLD: UIColor {
        return getColor("colorPrimaryDark_gold")
    }
    static var COLOR_PRIMARY_YELLOW: UIColor {
        return getColor("colorPrimary_yellow")
    }
    static var COLOR_PRIMARY_DARK_YELLOW: UIColor {
        return getColor("colorPrimaryDark_yellow")
    }
    static var COLOR_PRIMARY_DARKGREEN: UIColor {
        return getColor("colorPrimary_darkgreen")
    }
    static var COLOR_PRIMARY_DARK_DARKGREEN: UIColor {
        return getColor("colorPrimaryDark_darkgreen")
    }
    static var COLOR_PRIMARY_PURPLE: UIColor {
        return getColor("colorPrimary_purple")
    }
    static var COLOR_PRIMARY_DARK_PURPLE: UIColor {
        return getColor("colorPrimaryDark_purple")
    }
    static var COLOR_PRIMARY_GREY: UIColor {
        return getColor("colorPrimary_grey")
    }
    static var COLOR_PRIMARY_DARK_GREY: UIColor {
        return getColor("colorPrimaryDark_grey")
    }
    static var COLOR_PRIMARY_DARKGREY: UIColor {
        return getColor("colorPrimary_darkgrey")
    }
    static var COLOR_PRIMARY_DARK_DARKGREY: UIColor {
        return getColor("colorPrimaryDark_darkgrey")
    }
    static var COLOR_PRIMARY_MIDNIGHT: UIColor {
        return getColor("colorPrimary_midnight")
    }
    static var COLOR_PRIMARY_DARK_MIDNIGHT: UIColor {
        return getColor("colorPrimaryDark_midnight")
    }
    static var COLOR_PRIMARY_FUSCIA: UIColor {
        return getColor("colorPrimary_fuscia")
    }
    static var COLOR_PRIMARY_DARK_FUSCIA: UIColor {
        return getColor("colorPrimaryDark_fuscia")
    }
    static var COLOR_PRIMARY_BROWN: UIColor {
        return getColor("colorPrimary_brown")
    }
    static var COLOR_PRIMARY_DARK_BROWN: UIColor {
        return getColor("colorPrimaryDark_brown")
    }
    static var COLOR_ATTENDING_YES: UIColor {
        return getColor("colorAttendingYes")
    }
    static var COLOR_ATTENDING_NO: UIColor {
        return getColor("colorAttendingNo")
    }
    static var COLOR_ATTENDING_MAYBE: UIColor {
        return getColor("colorAttendingMaybe")
    }

}
