//
//  UITableViewCell.swift
//  beclutch
//
//  Created by SM on 2/22/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    public static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}
