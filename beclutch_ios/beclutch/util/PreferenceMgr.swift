//
//  PreferenceMgr.swift
//  videobridge
//
//  Created by Zeus on 2019/5/20.
//  Copyright © 2019 Zeus. All rights reserved.
//

import Foundation
import UIKit
public class PreferenceMgr {
    public static let TOKEN = "token"
    public static let FIREBASE_TOKEN = "fb_token"
    public static let EMAIL = "email"
    public static let REGISTER_STEP = "reg_step"
    public static let IS_AUTO_LOGIN = "auto_login"
    private static let THEME = "theme"

    public static func saveString(keyName key: String, stringValue value: String) {
        UserDefaults.standard.set(value, forKey: key)
    }

    public static func readString(keyName key: String) -> String {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }

    public static func saveInteger(keyName key: String, intValue value: Int) {
        UserDefaults.standard.set(value, forKey: key)
    }

    public static func readInteger(keyName key: String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }

    public static func saveBoolean(keyName key: String, boolValue value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
    }

    public static func readBoolean(keyName key: String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }

    public static func keyAlreadyExist(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    public static func deleteString(keyName keys: [String]) {
        for key in keys {
            UserDefaults.standard.removeObject(forKey: key)
        }

        UserDefaults.standard.synchronize()
    }

    public static func saveTheme(themeValue: String) {
        UserDefaults.standard.set(themeValue, forKey: self.THEME)
    }

    public static func readThemeValue() -> String {
        if UserDefaults.standard.string(forKey: self.THEME) == nil || UserDefaults.standard.string(forKey: self.THEME)!.isEmpty {
            return "beclutch"
        } else {
            return UserDefaults.standard.string(forKey: self.THEME)!
        }

    }

    public static func readTheme() -> ThemeColorModel {

        if UserDefaults.standard.string(forKey: self.THEME)!.isEmpty {
            return ThemeColorModel(isSelect: true, colorValue: "beclutch", colorName: "BeClutch", colorResourcePrimary: UIColor.COLOR_PRIMARY_BECLUTCH, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BECLUTCH, themeResource: 0)
        } else {
            var themeColorModel: ThemeColorModel
            switch UserDefaults.standard.string(forKey: self.THEME) {
            case "beclutch":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "beclutch", colorName: "BeClutch", colorResourcePrimary: UIColor.COLOR_PRIMARY_BECLUTCH, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BECLUTCH, themeResource: 0)
                break
            case "green":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "green", colorName: "Green", colorResourcePrimary: UIColor.COLOR_PRIMARY_GREEN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GREEN, themeResource: 0)
                break
            case "darkgreen":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "darkgreen", colorName: "Dark Green", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKGREEN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKGREEN, themeResource: 0)
                break
            case "lime":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "lime", colorName: "Lime", colorResourcePrimary: UIColor.COLOR_PRIMARY_LIME, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_LIME, themeResource: 0)
                break
            case "darkblue":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "darkblue", colorName: "Dark Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKBLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKBLUE, themeResource: 0)
                break
            case "blue":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "blue", colorName: "Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_BLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BLUE, themeResource: 0)
                break
            case "midnight":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "midnight", colorName: "Midnight", colorResourcePrimary: UIColor.COLOR_PRIMARY_MIDNIGHT, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_MIDNIGHT, themeResource: 0)
                break
            case "lightblue":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "lightblue", colorName: "Light Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_LIGHTBLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_LIGHTBLUE, themeResource: 0)
                break
            case "brown":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "brown", colorName: "Brown", colorResourcePrimary: UIColor.COLOR_PRIMARY_BROWN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BROWN, themeResource: 0)
                break
            case "orange":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "orange", colorName: "Orange", colorResourcePrimary: UIColor.COLOR_PRIMARY_ORANGE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_ORANGE, themeResource: 0)
                break
            case "gold":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "gold", colorName: "Gold", colorResourcePrimary: UIColor.COLOR_PRIMARY_GOLD, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GOLD, themeResource: 0)
                break
            case "yellow":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "yellow", colorName: "Yellow", colorResourcePrimary: UIColor.COLOR_PRIMARY_YELLOW, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_YELLOW, themeResource: 0)
                break
            case "red":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "red", colorName: "Red", colorResourcePrimary: UIColor.COLOR_PRIMARY_RED, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_RED, themeResource: 0)
                break
            case "darkred":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "darkred", colorName: "Dark Red", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKRED, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKRED, themeResource: 0)
                break
            case "maroon":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "maroon", colorName: "Maroon", colorResourcePrimary: UIColor.COLOR_PRIMARY_MAROON, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_MAROON, themeResource: 0)
                break
            case "fuscia":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "fuscia", colorName: "Fuscia", colorResourcePrimary: UIColor.COLOR_PRIMARY_FUSCIA, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_FUSCIA, themeResource: 0)
                break
            case "purple":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "purple", colorName: "Purple", colorResourcePrimary: UIColor.COLOR_PRIMARY_PURPLE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_PURPLE, themeResource: 0)
                break
            case "black":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "black", colorName: "Black", colorResourcePrimary: UIColor.COLOR_PRIMARY_BLACK, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BLACK, themeResource: 0)
                break
            case "grey":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "grey", colorName: "Grey", colorResourcePrimary: UIColor.COLOR_PRIMARY_GREY, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GREY, themeResource: 0)
                break
            case "darkgrey":
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "darkgrey", colorName: "Dark Grey", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKGREY, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKGREY, themeResource: 0)
                break
            default:
                themeColorModel = ThemeColorModel(isSelect: true, colorValue: "beclutch", colorName: "BeClutch", colorResourcePrimary: UIColor.COLOR_PRIMARY_BECLUTCH, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BECLUTCH, themeResource: 0)
                break
            }
            return themeColorModel
        }
    }
}
