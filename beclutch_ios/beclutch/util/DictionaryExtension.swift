//
//  DictionaryExtension.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension Dictionary {
    func printAsJson() {
        guard let dict = self as? [String: String] else {
            print("Dictionary should be in String: String")
            return
        }

        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(dict) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print("Json: " + jsonString)
            }
        }
    }
}
