//
//  DateExtension.swift
//  beclutch
//
//  Created by Trinh Vu on 2/13/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import EventKit

extension Date {
    static func makeDate(yearStr: String, monthStr: String, dayStr: String, hourStr: String, minStr: String, secStr: String) -> Date? {
        guard let year = Int(yearStr), let month = Int(monthStr), let day = Int(dayStr),
        let hour = Int(hourStr), let min = Int(minStr), let sec = Int(secStr) else {
            return nil
        }

        let calendar = Calendar(identifier: .gregorian)
        // calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = DateComponents(year: year, month: month, day: day, hour: hour, minute: min, second: sec)
        return calendar.date(from: components)!
    }

    static func makeDate(yearStr: String? = "2020", monthStr: String? = "02", dayStr: String? = "02", hr: Int = 0, min: Int = 0, sec: Int = 0) -> Date? {
        guard let year = Int(yearStr ?? ""), let month = Int(monthStr ?? ""), let day = Int(dayStr ?? "") else {
            return nil
        }

        let calendar = Calendar(identifier: .gregorian)
        // calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min, second: sec)
        return calendar.date(from: components)!
    }

    static func makeDateFromInt(year: Int? = 2020, month: Int? = 2, day: Int? = 2, hr: Int = 0, min: Int = 0, sec: Int = 0) -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        // calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min, second: sec)
        return calendar.date(from: components)!
    }

    // MARK: - Date format
    func dateChatString() -> String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM d h:mm a"

        return formatter2.string(from: self)
    }

    func praceDateString() -> String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM-d-yyyy"

        return formatter2.string(from: self)
    }

    func volunteerDateString() -> String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM-dd-yyyy"

        return formatter2.string(from: self)
    }

    func dateCommonString() -> String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM d, yyyy"

        return formatter2.string(from: self)
    }

    func dateSeverFormat() -> String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy/MM/d"

        return formatter2.string(from: self)
    }

    var fullDateInt: Int {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "YYYYMMdd"

        return Int(formatter2.string(from: self)) ?? 0
    }

    var hourIn12Format: Int {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:a"

        let str = formatter.string(from: self)
        let components = str.components(separatedBy: ":")

        if let hour = components.first, let result = Int(hour) {
            return result
        }

        return 0
    }
    
    static func dateServerFromString(dateStr: String) -> Date? {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-d"

        return formatter2.date(from: dateStr)
    }
    
    static func dateFromFullString(dateStr: String?) -> Date? {
        guard let text = dateStr else {
            return nil
        }
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return formatter2.date(from: text)
    }

    static func dateWith12PM() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        return formatter.date(from: "12:00") ?? Date()
    }

    // MARK: - Date converse
    var dayOfWeek: EKWeekday {
        let calendar = Calendar(identifier: .gregorian)
        let myComponents = calendar.dateComponents([.weekday], from: self)
        let weekDay = myComponents.weekday

        switch weekDay {
        case 1:
            return .sunday
        case 2:
            return .monday
        case 3:
            return .tuesday
        case 4:
            return .wednesday
        case 5:
            return .thursday
        case 6:
            return .friday
        case 7:
            return .saturday
        default:
            return .sunday
        }
    }

    var dayOfMonth: NSNumber {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "d"

        return NSNumber(pointer: formatter2.string(from: self))
    }

    var dayOfYear: NSNumber {
        return NSNumber(value: Calendar.current.ordinality(of: .day, in: .year, for: self)!)
    }

    var ampmStr: String {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "a"

        return formatter2.string(from: self)
    }

    // MARK: - Day adjust
    func adjustDate(hour: Int, minute: Int, ampm: String) -> Date? {
        var offsetHour = hour
        if offsetHour > 12 {
            offsetHour = offsetHour - 12
        }
        let dateStr = self.praceDateString() + " " + ampm + " \(offsetHour):\(minute)"

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM-d-yyyy a h:mm"

        return formatter2.date(from: dateStr)
    }
    func adjustAMPM(ampm: String) -> Date? {
        let dateStr = self.praceDateString() + " " + ampm

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM-d-yyyy a"

        return formatter2.date(from: dateStr)
    }
}
