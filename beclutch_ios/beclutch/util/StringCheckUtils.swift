//
//  StringCheckUtils.swift
//  beclutch
//
//  Created by zeus on 2019/11/16.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class StringCheckUtils {
    static func checkValidEmailAddress(email value: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: value)
    }

    static func getAddressFromLocation(model: TeamLocation) -> String {
        var addr: String = ""
        if !model.street!.isEmpty {
            addr = model.street!
        }

        if !model.city!.isEmpty {
            addr += ", " + model.city!
        }

        if !model.state!.isEmpty {
            addr += ", " + model.state!
        }

        if !model.zip!.isEmpty {
            addr += ", " + model.zip!
        }

        if addr.starts(with: ",") {
            if let index = addr.firstIndex(of: " ") {
                let nextIndex = addr.index(after: index)

                addr = String(addr[nextIndex...])
            }
        }

        return addr
    }
}
