//
//  Bool.swift
//  beclutch
//
//  Created by NamViet on 5/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension Bool {
    func toServerString() -> String {
        return self ? "1" : "0"
    }

    static func boolFromString(str: String?) -> Bool {
        if let str = str, let intValue = Int(str) {
            return intValue > 0
        }

        return false
    }
}
