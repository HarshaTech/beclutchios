//
//  UIView.swift
//  beclutch
//
//  Created by SM on 2/22/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func applyShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 1.0
    }

    func makeShadow() {
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 1.0
    }

    var width: CGFloat {
        return self.bounds.size.width
    }

    var height: CGFloat {
        return self.bounds.size.height
    }
}
