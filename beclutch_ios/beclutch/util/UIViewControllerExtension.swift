//
//  UIViewControllerExtension.swift
//  beclutch
//
//  Created by SM on 2/16/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    static func initiateFromStoryboard() -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: self))
    }

    // MARK: - Transition
    func presentViewControllerWithNavigation(viewController: UIViewController, isNavigationBarHidden: Bool = false) {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = .white
        navigationController.navigationBar.barTintColor = .primary
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        navigationController.isNavigationBarHidden = isNavigationBarHidden
        navigationController.modalPresentationStyle = .fullScreen

        self.present(navigationController, animated: true, completion: nil)
    }

    // MARK: - Alert

    func error(_ message: String?, okHandler: ((UIAlertAction) -> Swift.Void)?) {
        let message = message ??  "general_error_description"
        alert(title: "", message: message, okHandler: okHandler)
    }

    func alert(title: String = "", message: String, okTitle: String = "OK", cancelTitle: String? = nil, okHandler: ((UIAlertAction) -> Swift.Void)? = nil, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        var actions: [UIAlertAction] = []
        if !(okHandler == nil && cancelHandler != nil) {
            actions.append(UIAlertAction(title: okTitle.localized(), style: (cancelHandler != nil ? .default : .cancel), handler: okHandler))
        }
        if cancelHandler != nil {
            let text = cancelTitle ?? "No"
            actions.append(UIAlertAction(title: text, style: .cancel, handler: cancelHandler))
        }

        DispatchQueue.main.async {
            self.alert(title: title, message: message, actions: actions)
        }
    }

    func alert(title: String, message: String, actions: [UIAlertAction]) {
        DispatchQueue.main.async { [weak self] in
            let alertController = UIAlertController(title: title.localized(), message: message.localized(), preferredStyle: .alert)
            actions.forEach({ (action) in
                alertController.addAction(action)
            })
            self?.present(alertController, animated: true, completion: { [weak self] in
                guard let self = self else {
                    return
                }

                alertController.view.superview?.isUserInteractionEnabled = true
                alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
            })
        }
    }

    @objc private func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
