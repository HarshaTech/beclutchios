//
//  UILabel.swift
//  beclutch
//
//  Created by SM on 3/6/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func setUpDefaultFont() {
        self.font = UIFont(name: "Cambria", size: self.font.pointSize)
    }
}
