//
//  AdsProtocol.swift
//  beclutch
//
//  Created by Vu Trinh on 31/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import GoogleMobileAds

protocol AdsProtocol {

}

extension AdsProtocol {
    func configAdView(adView: GADBannerView, from: UIViewController) {
        #if DEBUG
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["933248f12ac15a173f3c10a765fc5720"]
        #endif

        adView.adUnitID = AdsConfig.bannerAppId

        adView.rootViewController = from
//        adView.delegate = self
        adView.load(GADRequest())
    }
}
