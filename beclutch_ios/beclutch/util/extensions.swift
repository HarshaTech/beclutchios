//
//  extensions.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Alamofire

var loadingView: UIView = UIView()
var loadingAcitivity: NVActivityIndicatorView?
//var curviewcontroller: UIViewController?
let KEYWINDOW = UIApplication.shared.keyWindow

extension UIViewController: NVActivityIndicatorViewable {

    func UIColorFromHex(_ rgbValue: UInt32, alpha: Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0

        return UIColor(red: red, green: green, blue: blue, alpha: CGFloat(alpha))
    }

    func showIndicator() {
        let curviewcontroller = UIApplication.topViewController()
        let curframe = curviewcontroller?.view.frame

        if loadingView.superview == nil {
            loadingView = UIView(frame: (curviewcontroller?.view.frame)!)
            loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
            loadingAcitivity = NVActivityIndicatorView(frame: CGRect(x: (curframe?.width)!/2 - 18, y: (curframe?.height)!/2 - 18, width: 36, height: 36), type: .ballRotateChase, color: self.UIColorFromHex(0xEC644B), padding: CGFloat(0))
            loadingAcitivity!.startAnimating()
            loadingView.addSubview(loadingAcitivity!)

//            KEYWINDOW?.isUserInteractionEnabled = false

            self.view.addSubview(loadingView)
        }
    }

    func hideIndicator() {
        if loadingView.superview != nil {
            loadingAcitivity!.stopAnimating()
//            KEYWINDOW?.isUserInteractionEnabled = true
            loadingView.removeFromSuperview()
        }
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension UIButton {
    func makeRadius() {
        self.layer.cornerRadius = 5
    }

    func makeCheckedSegment() {
         self.tintColor = UIColor.COLOR_PRIMARY
        self.backgroundColor = .white
        self.alpha = 1
    }

    func makeUncheckSegment() {
        self.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        self.alpha = 0.7
        self.tintColor = .white
    }

    func makeGrayRadius() {
        self.layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
    }
    func makeGreenButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.COLOR_PRIMARY.cgColor
        
        setTitleColor(.white, for: .normal)
    }
    func makeRedButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.COLOR_ACCENT.cgColor
        
        setTitleColor(.white, for: .normal)
    }
    func makeBlueButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.COLOR_BLUE.cgColor
        
        setTitleColor(.white, for: .normal)
    }
    func makeWhiteButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.white.cgColor
        self.setTitleColor(UIColor.COLOR_PRIMARY_DARK, for: .normal)
    }
    func makeStrokeButton() {
        self.layer.borderColor = UIColor.COLOR_PRIMARY.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.COLOR_PRIMARY_DARK.cgColor
    }
    func makeStrokeOnlyButton() {
        self.layer.borderColor = UIColor.COLOR_PRIMARY.cgColor
        self.layer.borderWidth = 1
        self.layer.backgroundColor = UIColor.COLOR_PRIMARY_DARK.cgColor
    }

    func makeOptionButton() {
        layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 5
    }
}

extension UIColor {

    func rgb() -> Int32 {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            var colInt: Int32 = 0
            let int32Alpha: Int32 = Int32(fAlpha * 255)
            let int32Red: Int32 = Int32(fRed * 255)
            let int32Green: Int32 = Int32(fGreen * 255)
            let int32Blue: Int32 = Int32(fBlue * 255)

            colInt |= (int32Alpha << 24)
            colInt |= (int32Red << 16)
            colInt |= (int32Green << 8)
            colInt |= (int32Blue)

            return colInt
        } else {
            // Could not extract RGBA components:
            return 0
        }
    }
}
