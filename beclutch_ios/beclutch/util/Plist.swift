//
//  Plist.swift
//  SportBooks
//
//  Created by Ron Magno on 1/8/20.
//  Copyright © 2020 Spiral. All rights reserved.
//

import Foundation

struct Plist {
    private init() {}

    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    static func value<T>(for key: String,
                         bundle: Bundle = Bundle.main) throws -> T where T: LosslessStringConvertible {
        guard let object = bundle.object(forInfoDictionaryKey:key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            throw Error.invalidValue
        }
    }
}
