//
//  Rx+IgnoreNil.swift
//  SportBooks
//
//  Created by Ron on 10/29/19.
//  Copyright © 2019 Spiral. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: - Rx+Optional
public protocol OptionalType {
    associatedtype Wrapped

    var optional: Wrapped? { get }
}

extension Optional: OptionalType {
    public var optional: Wrapped? { return self }
}

// Unfortunately the extra type annotations are required,
// otherwise the compiler gives an incomprehensible error.
extension Observable where Element: OptionalType {
    /**
    Unwraps and filters out `nil` elements.
    - returns: `Observable` of source `Observable`'s elements, with `nil` elements filtered out.
    */
    func ignoreNil() -> Observable<Element.Wrapped> {
        return flatMap { value in
            value.optional.map { Observable<Element.Wrapped>.just($0) } ?? Observable<Element.Wrapped>.empty()
        }
    }
}

extension SharedSequenceConvertibleType where Element: OptionalType {
    /**
     Unwraps and filters out `nil` elements.
     - returns: `Driver` of source `Driver`'s elements, with `nil` elements filtered out.
     */

    func ignoreNil() -> SharedSequence<SharingStrategy, Element.Wrapped> {
        return flatMap { element -> SharedSequence<SharingStrategy, Element.Wrapped> in
            guard let value = element.optional else {
                return SharedSequence<SharingStrategy, Element.Wrapped>.empty()
            }
            return SharedSequence<SharingStrategy, Element.Wrapped>.just(value)
        }
    }
}

// MARK: - Rx+Result
protocol ResultConvertible {
    associatedtype ResultSuccess
    associatedtype ResultFailure: Error

    var result: Result<ResultSuccess, ResultFailure> { get }
}

extension Swift.Result: ResultConvertible {
    typealias ResultSuccess = Success
    typealias ResultFailure = Failure

    var result: Result<Success, Failure> { return self }
}

extension Observable where Element: ResultConvertible {
    /// Get the associated success value
    /// - Returns: Associated success value of the Result
    func successValue() -> Observable<Element.ResultSuccess> {
        compactMap { (element) -> Element.ResultSuccess? in
            switch element.result {
            case .success(let success):
                return success
            case .failure:
                return nil
            }
        }
    }

    /// Get the associated error value
    /// - Returns: Associated error value of the Result
    func failureValue() -> Observable<Element.ResultFailure>  {
        compactMap { element -> Element.ResultFailure? in
            switch element.result {
            case .success:
                return nil
            case let .failure(error):
                return error
            }
        }
    }
}

extension SharedSequenceConvertibleType where Element: ResultConvertible {
    /// Get the associated success value
    /// - Returns: Associated success value of the Result
    func successValue() -> SharedSequence<SharingStrategy, Element.ResultSuccess> {
        map { (element) -> Element.ResultSuccess? in
            switch element.result {
            case .success(let success):
                return success
            case .failure:
                return nil
            }
        }
        .ignoreNil()
    }

    /// Get the associated error value
    /// - Returns: Associated error value of the Result
    func failureValue() -> SharedSequence<SharingStrategy, Element.ResultFailure>  {
        map { element -> Element.ResultFailure? in
            switch element.result {
            case .success:
                return nil
            case let .failure(error):
                return error
            }
        }
        .ignoreNil()
    }
}
