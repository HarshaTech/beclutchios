//
//  NavUtil.swift
//  videobridge
//
//  Created by Zeus on 2019/5/19.
//  Copyright © 2019 Zeus. All rights reserved.
//

import Foundation
import UIKit
public class NavUtil {
    public static var intendedScreenType: Int?
    
    public static func setRootViewController(storyboardId root: String) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: root) as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    public static func setRootNavigationController(stroyboardId root: String) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: root) as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    public static func showHomeViewController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeController") as UIViewController
        let rootNavigationController = UINavigationController(rootViewController: viewController)
        rootNavigationController.navigationBar.tintColor = .white
        rootNavigationController.navigationBar.barTintColor = .primary
        rootNavigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        UIApplication.shared.keyWindow?.rootViewController = rootNavigationController
    }
    
    public static func showLoginViewController() {
        if let viewController = ViewControllerFactory.makeLoginNavigationVC() {
            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
    }
    
    public static func showRegisterViewController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let regNav = mainStoryboard.instantiateViewController(withIdentifier: "RegisterNavigationController") as? RegisterNavigationController {
            UIApplication.shared.keyWindow?.rootViewController = regNav
        }
    }

    public static func showRosterViewController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeController")
        if let tabBarController = viewController as? UITabBarController {
            tabBarController.selectedIndex = 2
            let rootNavigationController = UINavigationController(rootViewController: viewController)
            rootNavigationController.navigationBar.tintColor = .white
            rootNavigationController.navigationBar.barTintColor = .primary
            rootNavigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

            UIApplication.shared.keyWindow?.rootViewController = rootNavigationController
        }
    }
    
    public static func showChatViewController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeController")
        if let tabBarController = viewController as? UITabBarController {
            tabBarController.selectedIndex = 3
            let rootNavigationController = UINavigationController(rootViewController: viewController)
            rootNavigationController.navigationBar.tintColor = .white
            rootNavigationController.navigationBar.barTintColor = .primary
            rootNavigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

            UIApplication.shared.keyWindow?.rootViewController = rootNavigationController
        }
    }
    
    public static func topViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}
