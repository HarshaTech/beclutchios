//
//  ViewControllerFactory.swift
//  beclutch
//
//  Created by Vu Trinh on 05/11/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

struct ViewControllerFactory {
    static let noteBookStoryboard = UIStoryboard(name: "Notebook", bundle: nil)

    static func makeRosterInviteVC() -> InviteRosterVC? {
        let rosterStoryboard = UIStoryboard(name: "Roster", bundle: nil)
        if let navNext = rosterStoryboard.instantiateViewController(withIdentifier: "InviteRosterVC") as? InviteRosterVC {
            navNext.modalPresentationStyle = .fullScreen
            return navNext
        }
        
        return nil
    }
    
    static func makeLoginNavigationVC() -> LoginNavigationController? {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginNavigationController") as? LoginNavigationController {
            viewController.navigationBar.tintColor = .white
            viewController.navigationBar.barTintColor = .primary
            viewController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            
            return viewController
        }
        
        return nil
    }
    
    static func makeTeamManageVC() -> TeamManageViewController? {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "TeamManage", bundle: nil)
        guard let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TeamManageViewController") as? TeamManageViewController else {
            return nil
        }
        
        return viewController
    }
    
    static func makeCreateTeamVC() -> CreateNewMyTeamViewController? {
        let storyboard: UIStoryboard = UIStoryboard(name: "CreateNewMyTeam", bundle: nil)
        guard let nextViewController = storyboard.instantiateViewController(withIdentifier: "CreateNewMyTeamViewController") as? CreateNewMyTeamViewController else {
            return nil
        }
        
        return nextViewController
    }
    
    static func competitionListTableVC() -> CometitionListVC? {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CometitionListVC") as? CometitionListVC else {
            return nil
        }
        
        return viewController
    }
    
    static func addCompetitionVC() -> AddCompetitionVC? {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AddCompetitionVC") as? AddCompetitionVC else {
            return nil
        }
        
        return viewController
    }
    
    static func makeSelectTeamVC() -> SelectTeamVC? {
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectTeam", bundle: nil)
        guard let nextViewController = storyboard.instantiateViewController(withIdentifier: "SelectTeamVC") as? SelectTeamVC else {
            return nil
        }
        
        nextViewController.modalPresentationStyle = .fullScreen
        
        return nextViewController
    }
    
    static func makeAcceptInvitationVC() -> AcceptInvitationVC? {
        let storyboard: UIStoryboard = UIStoryboard(name: "AcceptInvitation", bundle: nil)
        guard let nextViewController = storyboard.instantiateViewController(withIdentifier: "AcceptInvitationVC") as? AcceptInvitationVC else {
            return nil
        }
        
        nextViewController.modalPresentationStyle = .fullScreen
        
        return nextViewController
    }
}

extension ViewControllerFactory {
    static func noteBookViewController() -> NotebookViewController? {
        return noteBookStoryboard.instantiateViewController(withIdentifier: "NotebookViewController") as? NotebookViewController
    }
    static func newNoteViewController(note: Note? = nil) -> NewNoteViewController? {
        let newNoteVC = noteBookStoryboard.instantiateViewController(withIdentifier: "NewNoteViewController") as? NewNoteViewController

        newNoteVC?.currentNote = note

        return newNoteVC
    }
    static func noteListViewController() -> NoteListViewController? {
        return noteBookStoryboard.instantiateViewController(withIdentifier: "NoteListViewController") as? NoteListViewController
    }
}
