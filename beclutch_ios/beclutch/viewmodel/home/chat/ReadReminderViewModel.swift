//
//  ReadReminderViewModel.swift
//  beclutch
//
//  Created by Chung Le on 3/30/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class ReadReminderViewModel {
    var origin: ReminderEntryDTO?

    func doUpdateReminderRead() {
        let req = UpdateReadStatusReq()
        req.reminderId = origin?.id
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.token = AppDataInstance.instance.getToken()

        ChatService.instance.doUpdateReminderRead(req: req)
    }
}
