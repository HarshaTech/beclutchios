//
//  ChatListViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
import FirebaseFirestore

class ChatListViewModel {
    var loadingReminder = Observable(false)
    var loadingCarPool = Observable(false)
    var loadingTeamChat = Observable(false)
    var loadingConversation = Observable(false)

    var updatedConversationDetail = Observable(-1)

    var dp = Observable([ChatListViewItem]())

    var lastTeamChat = Observable(ChatEntryReadDTO())
    var lastCarPoolChat = Observable(ChatEntryReadDTO())

    var conversesListener: ListenerRegistration?
    var teamChatListener: ListenerRegistration?
    var carpoolListener: ListenerRegistration?

    var reminders: [ReminderEntryDTO] = []
    public private(set) var converse: [ConversationEntryDTO] = []

    private let db = Firestore.firestore()

    private var converseReference: CollectionReference {
      return db.collection("converse_chats")
    }

    private var teamChatReference: CollectionReference {
      return db.collection("team_chats")
    }

    private var carpoolReference: CollectionReference {
      return db.collection("carpool_chats")
    }

    func doLoadReminderList() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        self.loadingReminder.value = true
        let req = ReminderLoadReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = seletedTeam.ID

        ChatService.instance.doLoadAllReminder(req: req)
    }

    func doRemoveReminder(reminder: ReminderEntryDTO, completedClosure: @escaping (_ error: Error?) -> Void) {
        self.loadingReminder.value = true

        let req = ReminderDeleteReq()
        req.token = AppDataInstance.instance.getToken()
        req.reminderId = reminder.id
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        ChatService.instance.doDeleteReminder(req: req) { [weak self] (_, error) in
            self?.loadingReminder.value = false
            completedClosure(error)
        }
    }

    // MARK: - Add Conversation
    func updateConversationList(conversations: [ConversationEntryDTO]?) {
        guard let fullfillConversation = conversations else {
            return
        }

        for conversation in fullfillConversation {
            for oldConversation in self.converse {
                if conversation.id == oldConversation.id {
                    conversation.lastChatInfo = oldConversation.lastChatInfo

                    break
                }
            }
        }

        self.converse = fullfillConversation
    }

    func doLoadConversationList() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        self.loadingConversation.value = true
        let req =  ConversationEntryLoadReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = seletedTeam.ID

        ChatService.instance.doLoadAllConversationEntries(req: req)
    }

//    func doLoadConversationListDetail() {
//        if self.converse.count == 0 {
//            return
//        }
//
//        let converseIds = self.converse.prefix(10).map { (aConverse) -> String in
//            let id = aConverse.id ?? ""
//            return "converse_\(id)"
//        }
//
//        conversesListener = converseReference.whereField(FieldPath.documentID(), in: converseIds).addSnapshotListener(includeMetadataChanges: true) { (snapshot, _) in
//            if let documents = snapshot?.documents {
//                let teamChatOffset = self.lastTeamChat.value.lastSenderName == nil ? 0 : 1
//                let carpoolOffset = self.lastCarPoolChat.value.lastSenderName == nil ? 0 : 1
//                let offset = 3 + teamChatOffset + carpoolOffset
//
//                for i in 0 ..< documents.count {
//                    let document = documents[i]
//                    let index = self.indexOfConverse(converseId: document.documentID)
//                    self.converse[index].lastChatInfo = ChatEntryReadDTO(dict: document.data())
//
//                    self.updatedConversationDetail.value = index + offset
//                }
//            }
//        }
//    }

    func doLoadConversationListDetail() {
        for aConverse in self.converse {
            if let id = aConverse.id {
                let converseId = "converse_\(id)"

                converseReference.document(converseId).addSnapshotListener { (snapShot, _) in
                    if let document = snapShot,
                       let documentData = document.data() {
                        let index = self.indexOfConverse(converseId: document.documentID)
                        self.converse[index].lastChatInfo = ChatEntryReadDTO(dict: documentData)

                    }
                }
            }
        }
    }

    func doLoadConversation(indexPath: IndexPath) {
        let teamChatOffset = self.lastTeamChat.value.lastSenderName == nil ? 0 : 1
        let carpoolOffset = self.lastCarPoolChat.value.lastSenderName == nil ? 0 : 1
        let offset = 3 + teamChatOffset + carpoolOffset

        let index = indexPath.row - offset

        let converseId = self.converse[index].id

        converseReference.whereField(FieldPath.documentID(), in: [converseId]).addSnapshotListener(includeMetadataChanges: true) { (snapshot, _) in
            if let document = snapshot?.documents.first {
                self.converse[index].lastChatInfo = ChatEntryReadDTO(dict: document.data())

//                self.updatedConversationDetail.value = index + offset
            }
        }
    }

    func doAddConversation(groupSelection: GroupChatResult) {
        let req = NewConversationAddReq()
        req.token = AppDataInstance.instance.getToken()

        let dto = groupSelection.rosters
        req.rosterIds = dto.map({ (element) -> String in
            return element.rosterId ?? ""
        })

        req.users = groupSelection
        req.type = 1
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId

        ChatService.instance.doAddConversation(req: req)
    }

    func doRemoveConversation(converse: ConversationEntryDTO, completedClosure: @escaping (_ error: Error?) -> Void) {
        let req = ConversationThreadRemoveRes()
        req.token = AppDataInstance.instance.getToken()
        req.threadId = converse.id

        ChatService.instance.doDeleteConversation(req: req) { [weak self] (_, error) in
            self?.doRemoveConversationInFireBase(converse: converse)
            completedClosure(error)
        }
    }

    func doRemoveConversationInFireBase(converse: ConversationEntryDTO) {
        if let id = converse.id {
            let fullConverseId = "converse_\(id)"
            converseReference.document(fullConverseId).delete { (error) in
                if let error = error {
                    print("doRemoveConversationInFireBase error: \(error)")
                }
            }
        }
    }

    func doStartTeamChat(completedClosure: @escaping (_ error: Error?) -> Void) {
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            let teamId = "team_" + roster.TEAM_ID

            teamChatReference.document(teamId).setData(["lastSendMsg": "", "lastSenderName": "", "lastSendTime": "", "readEntries": ""]) { (error) in
                completedClosure(error)
            }
        }
    }
    func doLoadTeamChatInfo() {
        self.loadingTeamChat.value = true
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            let teamId = "team_" + roster.TEAM_ID
            print("Listen on team: " + teamId)

            teamChatListener = teamChatReference.whereField(FieldPath.documentID(), isEqualTo: teamId).addSnapshotListener(includeMetadataChanges: true) { (snapshot, _) in
                if let document = snapshot?.documents.first {
                    self.lastTeamChat.value = ChatEntryReadDTO(dict: document.data())
                    print("Getting update: " + document.data().description)
                } else {
                    self.lastTeamChat.value = ChatEntryReadDTO(dict: [:])
                    self.loadingTeamChat.value = false
                }
            }
        }
    }

    func doLoadCarPoolInfo() {
        self.loadingCarPool.value = true
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            let teamId = "team_" + roster.TEAM_ID
//            carpoolReference.whereField(FieldPath.documentID(), isEqualTo: teamId).getDocuments { (querySnapshot, error) in
//                if let document = querySnapshot?.documents.first {
//                    self.lastCarPoolChat.value = ChatEntryReadDTO(dict: document.data())
//                } else {
//                    self.lastCarPoolChat.value = ChatEntryReadDTO(dict: [:])
//                    self.loadingCarPool.value = false
//                }
//            }

            carpoolListener = carpoolReference.whereField(FieldPath.documentID(), isEqualTo: teamId).addSnapshotListener(includeMetadataChanges: true) { (snapshot, _) in
                if let document = snapshot?.documents.first {
                    self.lastCarPoolChat.value = ChatEntryReadDTO(dict: document.data())
                } else {
                    self.lastCarPoolChat.value = ChatEntryReadDTO(dict: [:])
                    self.loadingCarPool.value = false
                }
            }
        }
    }

    func buildDataProvider() {
        var dataProvider: [ChatListViewItem] = []
        dataProvider.append(ChatListViewItem(type: ChatListViewItem.HEADER_TEAM_CHAT))
        if lastTeamChat.value.lastSenderName != nil {
            dataProvider.append(ChatListViewItem(type: ChatListViewItem.ITEM_TEAM_CHAT))
        }
        dataProvider.append(ChatListViewItem(type: ChatListViewItem.HEADER_CARPOOL_CHAT))
        if lastCarPoolChat.value.lastSenderName != nil {
            dataProvider.append(ChatListViewItem(type: ChatListViewItem.ITEM_CARPOOL_CHAT))
        }
        dataProvider.append(ChatListViewItem(type: ChatListViewItem.HEADER_CONVERSATION_CHAT))
        for item in 0..<self.converse.count {
            dataProvider.append(ChatListViewItem(type: ChatListViewItem.ITEM_CONVERSATION, idx: item))
        }

        dataProvider.append(ChatListViewItem(type: ChatListViewItem.HEADER_REMINDER_CHAT))
        for item in 0..<self.reminders.count {
            dataProvider.append(ChatListViewItem(type: ChatListViewItem.ITEM_REMINDER, idx: item))
        }
        self.dp.value = dataProvider
    }

    func removeAllFirebaseListener() {
        conversesListener?.remove()
        teamChatListener?.remove()
        carpoolListener?.remove()
    }

    // Table View Index
    func indexPathsFor(section: Int) -> [IndexPath] {
        if self.dp.value.count == 0 {
            return []
        }

        var firstIndex = 0
        var lastIndex = self.dp.value.count - 1

        for i in 0 ..< self.dp.value.count {
            if section == self.dp.value[i].itemType {
                firstIndex = i
                break
            }
        }
        for i in 0 ..< self.dp.value.count {
            if section + 1 == self.dp.value[i].itemType {
                lastIndex = i
                break
            }
        }

        var result: [IndexPath] = []
        for i in firstIndex ... lastIndex {
            result.append(IndexPath(row: i, section: 0))
        }

        return result
    }

    func indexOfConverse(converseId: String?) -> Int {
        for i in 0 ..< self.converse.count {
            let shortId = converseId?.replacingOccurrences(of: "converse_", with: "")
            if self.converse[i].id == converseId || self.converse[i].id == shortId {
                return i
            }
        }

        return 0
    }
}

class ChatListViewItem {
    static let HEADER_TEAM_CHAT = 0
    static let HEADER_CARPOOL_CHAT = 1
    static let HEADER_CONVERSATION_CHAT = 2
    static let HEADER_REMINDER_CHAT = 3
    static let ITEM_TEAM_CHAT = 4
    static let ITEM_CARPOOL_CHAT = 5
    static let ITEM_CONVERSATION = 6
    static let ITEM_REMINDER = 7
    var itemType: Int?
    var idxInOriginArr: Int?
    init(type: Int) {
        self.itemType = type
        self.idxInOriginArr = 0
    }

    init(type: Int, idx: Int) {
        self.itemType = type
        self.idxInOriginArr = idx
    }
}
