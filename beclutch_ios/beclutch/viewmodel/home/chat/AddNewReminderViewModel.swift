//
//  AddNewReminderVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class AddNewReminderViewModel {
    var isBusy = Observable(false)
    var curDate = Observable(Date())
    var selResult = Observable(GroupSelectionResult(val: nil))

    func doUploadNewReminder(subject: String, content: String) {
        let arr_ReadStatus = [Int(AppDataInstance.instance.currentSelectedTeam!.ID)!]
        self.isBusy.value = true
        let req = SendReminderReq()
        req.content = content
        req.expirationDate = self.curDate.value
        req.groupSelectionResult = self.selResult.value
        req.readStatus = arr_ReadStatus
        req.senderFirstName = AppDataInstance.instance.currentSelectedTeam?.ROSTER_FIRST_NAME
        req.senderLastName = AppDataInstance.instance.currentSelectedTeam?.ROSTER_LAST_NAME
        req.senderRosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.senderTeamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.subject = subject
        req.token = AppDataInstance.instance.getToken()

        ChatService.instance.doSendNewReminder(req: req)
    }
}
