//
//  ChooseRosterForChatViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class ChooseRosterForChatViewModel {
    var isBusy = Observable(false)
    var dp: [RosterSelectionChatDTO] = [RosterSelectionChatDTO]()
    var initial: GroupSelectionResult?
    var editPossible: Bool = true
    func doLoadRosters() {
        self.isBusy.value = true
        let req = LoadMyRosterReq()
        req.token = AppDataInstance.instance.getToken()
        req.currentTeamClubMemberId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.targetLevel = "0"

        HomeService.instance.doLoadMyRosters(req: req)
    }

    func isContainRosterInInitial(roster: RosterInfo) -> Bool {
        if let initial = self.initial {
            if initial.isSelelctAll! && initial.dto!.count == 0 {
                return true
            }

            for groupResultEntity in initial.dto! {
                if groupResultEntity.rosterId == roster.ID {
                    return true
                }
            }
        }

        return false
    }
}
