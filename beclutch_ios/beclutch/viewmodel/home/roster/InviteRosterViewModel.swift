import Foundation
import Observable
class InviteRosterViewModel {
    var isBusy = Observable(false)
    var curSelRole = Observable(-1)

    var roles: [Role] = [Role]()

    func doInviteRosterToThisTeam(email: String, fname: String, lname: String, sendMail: Bool, isPlayer:Bool) {
        self.isBusy.value = true
        let req = CreateInviteRosterToThisTeamReq()
        req.clubId = AppDataInstance.instance.currentSelectedTeam?.CLUB_ID
        req.email = email
        req.fname = fname
        req.lname = lname
        req.roleId = self.roles[self.curSelRole.value].ID
        req.roleTitle = self.roles[self.curSelRole.value].ROLE_NAME
        req.sendMail = sendMail
        req.sportId = AppDataInstance.instance.currentSelectedTeam?.SPORT_INFO.ID
        req.spotTitle = AppDataInstance.instance.currentSelectedTeam?.SPORT_INFO.NAME
        req.token = AppDataInstance.instance.getToken()
        req.teamName = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.TEAM_ID
        req.isPlayer = isPlayer

        CreateService.instance.doInviteRosterToTeam(req: req)
    }

    func doInviteTeamToThisClub(email: String, fname: String, lname: String, sendMail: Bool) {
        self.isBusy.value = true
        let req = CreateInviteTeamToClubReq()
        req.clubId = AppDataInstance.instance.currentSelectedTeam?.CLUB_ID
        req.email = email
        req.fname = fname
        req.lname = lname
        req.roleId = self.roles[self.curSelRole.value].ID
        req.roleTitle = self.roles[self.curSelRole.value].ROLE_NAME
        req.sendEMail = sendMail
        req.sportId = AppDataInstance.instance.currentSelectedTeam?.SPORT_INFO.ID
        req.sportTitle = AppDataInstance.instance.currentSelectedTeam?.SPORT_INFO.NAME
        req.teamName = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME
        req.token = AppDataInstance.instance.getToken()
        req.teamId = "0"

        CreateService.instance.doCreateInviteTeamManager(req: req)
    }

    func doBuildRole() {
        self.roles = [Role]()
        if AppDataInstance.instance.roles != nil {
            let roleId = AppDataInstance.instance.currentSelectedTeam!.ROLE_INFO.ID
            if roleId == UserRoles.CLUB_OWNER {
                // club owner
                // coach, manager, club treasure add
                addRoleWithId(targetId: UserRoles.TEAM_COACH)
                addRoleWithId(targetId: UserRoles.TEAM_MANAGER)
                addRoleWithId(targetId: UserRoles.CLUB_TREASURER)
            } else if roleId == UserRoles.TEAM_COACH {
                //coach
                //Manager, Father,Mother, Player, Team Treasurer
                addRoleWithId(targetId: UserRoles.TEAM_COACH)
                addRoleWithId(targetId: UserRoles.TEAM_MANAGER)
                addRoleWithId(targetId: UserRoles.PLAYER)
                addRoleWithId(targetId: UserRoles.TEAM_TREASURE)
            } else if roleId == UserRoles.TEAM_MANAGER {
                // manager
                // coach, Father,Mother, Player, Team Treasurer
                addRoleWithId(targetId: UserRoles.TEAM_COACH)
                addRoleWithId(targetId: UserRoles.PLAYER)
                addRoleWithId(targetId: UserRoles.TEAM_TREASURE)
            }
        }
    }

    private func addRoleWithId(targetId: String) {
        for tmpRole in AppDataInstance.instance.roles! {
            if tmpRole.ID == targetId {
                self.roles.append(tmpRole)
                break
            }
        }
    }
}
