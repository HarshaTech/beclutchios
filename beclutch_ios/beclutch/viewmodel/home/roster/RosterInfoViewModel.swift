//
//  RosterInfoViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class RosterInfoViewModel {
    var isBusy = Observable(false)
    var origin: RosterInfo?

    func doLoadRosterInfo() {
        self.isBusy.value = true
        let req = LoadRosterInfoReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = self.origin?.ID

        RosterService.instance.doLoadRosterInformation(req: req)
    }

    func doRemoveRosterFromTeam() {
           self.isBusy.value = true
           let req = RemoveRosterFromTeamReq()
           req.token = AppDataInstance.instance.getToken()
           req.rosterId = origin?.ID

           RosterService.instance.doRemoveRosterFromTeam(req: req)
       }
}
