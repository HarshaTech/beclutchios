//
//  RosterInfoEditViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class RosterInfoEditViewModel {
    var isBusy = Observable(false)
    var originPhotoURL: String?
    var newUploadPhoto: Data?

    func doUpdateRosterInfo(rosterId: String, email: String, fname: String, lname: String, cell: String, home: String, work: String) {
        self.isBusy.value = true
        let req = UpdateRosterProfileReq()
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = newUploadPhoto
        req.email = email
        req.fName = fname
        req.lName = lname
        req.phoneCell = cell
        req.phoneHome = home
        req.phoneWork = work
        req.rosterId = rosterId
        RosterService.instance.doUpdateRosterProfile(req: req)

    }
}
