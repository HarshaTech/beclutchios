//
//  AddContactViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class AddContactViewModel {
    var isBusy = Observable(false)
    var curKind = Observable("")
    var originPhotoURL: String?
    var newUploadPhoto: Data?
    var isUpdate = false
    var originPlayerId: String?

    func doCreateNewContact(email: String, f_name: String, l_name: String, phoneCell: String, phoneHome: String, phoneWork: String) {
        self.isBusy.value = true
        let req = AddPlayerContactReq()
        req.email = email
        req.fName = f_name
        req.lName = l_name
        req.phoneCell = phoneCell
        req.phoneHome = phoneHome
        req.phoneWork = phoneWork
        req.playerId = self.originPlayerId!
        req.role = self.curKind.value
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = self.newUploadPhoto

        RosterService.instance.doAddPlayerContact(req: req)
    }

    func doUpdateExistContact(email: String, fName: String, lName: String, phoneCell: String, phoneHome: String, phoneWork: String, originRosterId: String) {
        self.isBusy.value = true
        let req = UpdatePlayerContactReq()
        req.email = email
        req.fName = fName
        req.lName = lName
        req.phoneCell = phoneCell
        req.phoneHome = phoneHome
        req.phoneWork = phoneWork
        req.role = self.curKind.value
        req.rosterId = originRosterId
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = self.newUploadPhoto

        RosterService.instance.doUpdatePlayerContact(req: req)
    }
}
