//
//  PlayerInfoViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/5.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class PlayerInfoViewModel {
    var isBusy = Observable(false)
    var origin: RosterInfo?
    var dp: FullRosterPlayer?

    func doLoadPlayerInfo() {
        self.isBusy.value = true
        let req = PlayerProfileLoadReq()
        req.token = AppDataInstance.instance.getToken()
        req.playerId = self.origin?.ID

        RosterService.instance.doLoadPlayerInformation(req: req)
    }

    func doRemoveContactGuest(rosterId: String) {
        self.isBusy.value = true
        let req = RemoveContactGuestReq()
        req.token = AppDataInstance.instance.getToken()
        req.playerId = self.dp?.player?.ID
        req.rosterId = rosterId

        RosterService.instance.doRemoveContactGuest(req: req)
    }

    func doRemoveRosterFromTeam() {
        self.isBusy.value = true
        let req = RemoveRosterFromTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = origin?.ID

        RosterService.instance.doRemoveRosterFromTeam(req: req)
    }
    
    func doResendInviteEmail(roster: RosterInfo) {
        self.isBusy.value = true

        let req = ResendInviteRosterEmailReq()
        req.userId = roster.USER_ID
        req.memberId = roster.ID
        req.token = AppDataInstance.instance.getToken()

        CreateService.instance.doResendEmailInviteRosterToTeam(req: req)
    }
}
