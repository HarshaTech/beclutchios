//
//  RosterListViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class RosterListViewModel {
    var isBusy = Observable(false)
    var roles: [Role] = [Role]()
    var curSelectRolePos = Observable(-1)
    var dp: [RosterInfo] = [RosterInfo]()

    func buildRole() {
        var rolesTmp: [Role] = [Role]()
        rolesTmp.append(Role.createEveryoneDefault())
        if AppDataInstance.instance.roles != nil {
            if AppDataInstance.instance.currentSelectedTeam?.CLUB_OR_TEAM == "1" {
                // if it is club
                rolesTmp += AppDataInstance.instance.roles!
            } else {
                // if it is team
                for tmpRole in AppDataInstance.instance.roles! {
                    if tmpRole.ID != UserRoles.CLUB_OWNER && tmpRole.ID != UserRoles.CLUB_TREASURER {
                        if let tmpRoleInt = Int(tmpRole.ID), let treasureInt = Int(UserRoles.TEAM_TREASURE) {
                            if tmpRoleInt < treasureInt {
                                rolesTmp.append(tmpRole)
                            }
                        }
                    }
                }
            }
        }

        self.roles = rolesTmp
    }

    func doLoadMyRoster() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }
        self.isBusy.value = true
        let req  = LoadMyRosterReq()
        req.token = AppDataInstance.instance.getToken()
        req.targetLevel = self.roles[self.curSelectRolePos.value].ID
        req.currentTeamClubMemberId = seletedTeam.ID

        HomeService.instance.doLoadMyRosters(req: req)
    }

    func doStartIndividualConversation(rosterId: String) {
        guard let firstRoser = AppDataInstance.instance.currentSelectedTeam?.ID else {
            return
        }

        let req = IndividualConversationCheckReq()
        req.token = AppDataInstance.instance.getToken()
        req.firstRosterId = firstRoser
        req.secondRosterId = rosterId

        ChatService.instance.doCheckIndividualChat(req: req)
    }

    func doResendInviteEmail(index: Int) {
        self.isBusy.value = true

        let roster = dp[index]
        let req = ResendInviteRosterEmailReq()
        req.userId = roster.USER_ID
        req.memberId = roster.ID
        req.token = AppDataInstance.instance.getToken()

        CreateService.instance.doResendEmailInviteRosterToTeam(req: req)
    }

    func doSplashAction() {
        self.isBusy.value = true
        
       let token = PreferenceMgr.readString(keyName: PreferenceMgr.TOKEN)
       let fbToken = PreferenceMgr.readString(keyName: PreferenceMgr.FIREBASE_TOKEN)

       let req = SplashRequest(token: token, fb_token: fbToken)
       AuthService.instance.doSplashAction(req: req)
   }
}
