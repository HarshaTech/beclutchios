//
//  AddGuestViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class AddGuestViewModel {
    var isBusy = Observable(false)
    var originPhotoURL: String?
    var newUploadPhoto: Data?
    var isUpdate = false
    var originPlayerId: String?

    func doCreateNewGuest(email: String, f_name: String, l_name: String, cell: String, home: String, work: String) {
        self.isBusy.value = true
        let req = AddPlayerContactReq()
        req.email = email
        req.fName = f_name
        req.lName = l_name
        req.phoneCell = cell
        req.phoneHome = home
        req.phoneWork = work
        req.playerId = self.originPlayerId
        req.role = UserRoles.GUEST
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = self.newUploadPhoto

        RosterService.instance.doAddPlayerContact(req: req)
    }

    func doUpdateGuest(email: String, f_name: String, l_name: String, cell: String, home: String, work: String, rosterId: String) {
        self.isBusy.value = true
        let req = UpdateRosterProfileReq()
        req.email = email
        req.fName = f_name
        req.lName = l_name
        req.phoneCell = cell
        req.phoneHome  = home
        req.phoneWork = work
        req.rosterId = rosterId
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = self.newUploadPhoto

        RosterService.instance.doUpdateRosterProfile(req: req)
    }
}
