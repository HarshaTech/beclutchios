//
//  PlayerEditViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/6.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class PlayerEditViewModel {
    var isBusy = Observable(false)
    var originPhotoURL: String?
    var newUploadPhoto: Data?

    func doUpdatePlayerInfo(rosterId: String, playerNo: String, email: String, fname: String, lname: String, cell: String, home: String, work: String) {
        self.isBusy.value = true
        let req = UpdatePlayerProfileReq()
        req.token = AppDataInstance.instance.getToken()
        req.uploadFile = newUploadPhoto
        req.email = email
        req.fName = fname
        req.lName = lname
        req.phoneCell = cell
        req.phoneHome = home
        req.phoneWork = work
        req.rosterId = rosterId
        req.playerNo = playerNo

        RosterService.instance.doUpdatePlayerProfile(req: req)
    }

}
