//
//  SelectTeamViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class SelectTeamViewModel {
    var isBusy = Observable(false)
    var dp: [RosterInfo]?
    func loadClubTree() {
        let req = ClubTreeLoadReq()
        req.token = AppDataInstance.instance.getToken()
        self.isBusy.value = true
        HomeService.instance.doLoadClubTree(req: req)
    }

    func doUpdateTeam(pos: Int) {
        let req = UpdateDefaultTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = self.dp![pos].ID
        HomeService.instance.doUpdateDefaultTeam(req: req)
    }
}
