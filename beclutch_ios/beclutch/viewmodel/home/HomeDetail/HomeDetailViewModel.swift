//
//  HomeDetailViewModel.swift
//  beclutch
//
//  Created by SM on 2/20/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
import Firebase
import FirebaseFirestore

class HomeDetailViewModel {
    var isBusy = Observable(false)
    var homeDashboardResponse: LoadHomeDashboardRes?
    var teamChatInfo = Observable(ChatEntryReadDTO())

    var teamChatListener: ListenerRegistration?

    var eventHeight: CGFloat = 128

    private let db = Firestore.firestore()
    private var carpoolChatReference: CollectionReference {
      return db.collection("carpool_chats")
    }

    private var teamChatReference: CollectionReference {
      return db.collection("team_chats")
    }

    let countryList = ["US"]
    let stateList = ["", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA",
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
        "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
    
    var eventModel: EventModel? {
        return homeDashboardResponse?.practice ?? homeDashboardResponse?.game
    }
    
    var eventType: CALENDAR_ITEM_TYPE {
        if homeDashboardResponse?.practice != nil {
            return .PRACTICE_MODEL
        }
        
        return .GAME_MODEL
    }

    func doUpdateHealCheckForMultipleRoster(rosters: [String], status: String, calendarItemId: String, calendarItemType: String, completedClosure: @escaping (_ error: String?) -> Void) {
           self.isBusy.value = true

           let req = UpdateAttendanceMultipleReq()
           req.token = AppDataInstance.instance.getToken()
           req.rosters = rosters
           req.calendarItemId = calendarItemId
           req.calendarItemKind = calendarItemType
           req.status = status

           CalendarService.instance.doUpdateHealthCheckMultipleRoster(req: req) { [weak self] (_, error) in
               self?.isBusy.value = false
               completedClosure(error)
           }
       }

    func loadHomeDashBoard() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        let req: LoadHomeDashboardReq = LoadHomeDashboardReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = seletedTeam.ID

        isBusy.value = true
        HomeService.instance.doLoadHomeDashBoard(req: req)
    }

    func doGetResponsible(memberId: String) {
           self.isBusy.value = true
           let req = GetResponsibleReq()
           req.token = AppDataInstance.instance.getToken()
           req.memberId = memberId

           CalendarService.instance.doGetResponsible(req: req)
       }

    func doCarpool(game: GameModel?, completion: @escaping (_ error: Error?) -> Void ) {
        if let roster = AppDataInstance.instance.currentSelectedTeam, let game = game {
            let id = "team_" + roster.TEAM_ID
            let reference = db.collection(["carpool_chats", id, "messages"].joined(separator: "/"))

            let gameName = game.NAME ?? ""
            let oponentName = game.opponent_detail?.OPPONENT_NAME ?? ""
            let content = "\(roster.displayName) requested a carpool for \(gameName) vs \(oponentName) at " + game.startTimeDisplay + " on " + game.slashesDateDisplay
            let message = Message(roster: roster, content: content, type: 2)

            reference.addDocument(data: message.representation) { error in
                    if let e = error {
                      print("Error sending carpool: \(e.localizedDescription)")
                      return
                    }

                    completion(error)

                    self.carpoolChatReference.document(id).setData(ChatEntryReadDTO(message: message).firebaseDict()) { (error) in
                        print(error ?? "")
                    }
                  }
        }
    }

    func doCarpool(practice: PracticeModel?, completion: @escaping (_ error: Error?) -> Void ) {
        if let roster = AppDataInstance.instance.currentSelectedTeam, let practice = practice {
                let id = "team_" + roster.TEAM_ID
                let reference = db.collection(["carpool_chats", id, "messages"].joined(separator: "/"))

                let practiceName = practice.NAME ?? ""
                let content = "\(roster.displayName) requested a carpool for \(practiceName) at " + practice.startTimeDisplay + " on " + practice.slashesDateDisplay
                let message = Message(roster: roster, content: content, type: 2)

                reference.addDocument(data: message.representation) { error in
                    if let e = error {
                      print("Error sending carpool: \(e.localizedDescription)")
                      return
                    }

                    completion(error)

                    self.carpoolChatReference.document(id).setData(ChatEntryReadDTO(message: message).firebaseDict()) { (error) in
                        print(error ?? "")
                    }
              }
        }
    }

    func doLoadTeamChatInfo() {
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            let teamId = "team_" + roster.TEAM_ID
            teamChatListener = teamChatReference.whereField(FieldPath.documentID(), isEqualTo: teamId).addSnapshotListener(includeMetadataChanges: true) { (snapshot, _) in
                if let document = snapshot?.documents.first {
                    self.teamChatInfo.value = ChatEntryReadDTO(dict: document.data())
                } else {
                    self.teamChatInfo.value = ChatEntryReadDTO(dict: [:])
                }
            }
        }
    }

    func doStartTeamChat(completedClosure: @escaping (_ error: Error?) -> Void) {
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            let teamId = "team_" + roster.TEAM_ID

            teamChatReference.document(teamId).setData(["lastSendMsg": "", "lastSenderName": "", "lastSendTime": "", "readEntries": ""]) { (error) in
                completedClosure(error)
            }
        }
    }

    func doUpdateAttendanceForMultipleRoster(rosters: [String], status: String, calendarItemId: String, calendarItemType: String, completedClosure: @escaping (_ error: String?) -> Void) {
        self.isBusy.value = true

        let req = UpdateAttendanceMultipleReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosters = rosters
        req.calendarItemId = calendarItemId
        req.calendarItemKind = calendarItemType
        req.status = status

        CalendarService.instance.doUpdateAttendanceForMultipleRoster(req: req) { [weak self] (_, error) in
            self?.isBusy.value = false
            completedClosure(error)
        }
    }
}

// MARK: - Event
extension HomeDetailViewModel {
    func doCreateAttendForEvent(eventId: String, targetStatus: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = CreateAttendaceReq()
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.eventId = eventId
        req.kind = calendarItemType == CALENDAR_ITEM_TYPE.GAME_MODEL ? "1" : "0"
        req.status = targetStatus
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        CalendarService.instance.doCreateAttendanceForEvent(req: req)
    }

    func doCancelGame(gameId: String, pos: Int, notify: Bool) {
        self.isBusy.value = true
        let req = CancelGameReq()
        req.token = AppDataInstance.instance.getToken()
        req.gameId = gameId
        req.pos = pos
        req.notifyTeam = notify

        CalendarService.instance.doCancelGame(req: req)
    }

    func doDeleteGamePractice(eventId: String, kind: String, pos: Int) {
        self.isBusy.value = true
        let req = DeleteCalendarItemReq()
        req.token = AppDataInstance.instance.getToken()
        req.Id = eventId
        req.kind = kind
        req.pos = pos

        CalendarService.instance.doDeletePracticeGame(req: req)
    }

    func doUpdateAttendForEvent(attendanceID: String, status: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = UpdateAttendanceReq()
        req.attendanceId = attendanceID
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.status = status
        req.token = AppDataInstance.instance.getToken()

        CalendarService.instance.doUpdateAttendanceForEvent(req: req)
    }

    func doCancelPractice(practiceId: String, pos: Int, notifyTeam: Bool) {
        self.isBusy.value = true

        let req = CancelPracticeReq()
        req.token = AppDataInstance.instance.getToken()
        req.practiceId = practiceId
        req.pos = pos
        req.notify = notifyTeam

        CalendarService.instance.doCancelPractice(req: req)
    }

    func removeAllFirebaseListener() {
        teamChatListener?.remove()
    }
}
