//
//  AcceptInviteViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class AcceptInviteViewModel {
    var isBusy = Observable(false)

    func doSubmitInviteCode(code: String) {
        self.isBusy.value = true
        let req = InviteAcceptReq()
        req.token  = AppDataInstance.instance.getToken()
        req.code = code
        HomeService.instance.doSubmitInvitationCode(req: req)
    }
    
    func loadClubTree() {
        let req = ClubTreeLoadReq()
        req.token = AppDataInstance.instance.getToken()
        self.isBusy.value = true
        HomeService.instance.doLoadClubTree(req: req)
    }
    
    func doUpdateTeam(rosterID: String) {
        let req = UpdateDefaultTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = rosterID
        HomeService.instance.doUpdateDefaultTeam(req: req)
    }
}
