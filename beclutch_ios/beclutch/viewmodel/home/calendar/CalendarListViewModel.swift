//
//  CalendarViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
import Firebase
import FirebaseFirestore

class CalendarListViewModel {
    var isBusy = Observable(false)
    var dp: [CalendarListDTO] = []
    var historyDP = [CalendarListDTO]()

    let countryList = ["US"]
    let stateList = ["", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA",
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
        "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    private let db = Firestore.firestore()

    private var carpoolChatReference: CollectionReference {
      return db.collection("carpool_chats")
    }

    func doLoadCalendarItem() {
        self.isBusy.value = true

        let req = GetCalendarByTeamIdReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        CalendarService.instance.doGetCalendarContentsByTeamId(req: req)
    }

    func doUpdateAttendanceForMultipleRoster(rosters: [String], status: String, calendarItemId: String, calendarItemType: String, completedClosure: @escaping (_ error: String?) -> Void) {
        self.isBusy.value = true

        let req = UpdateAttendanceMultipleReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosters = rosters
        req.calendarItemId = calendarItemId
        req.calendarItemKind = calendarItemType
        req.status = status

        CalendarService.instance.doUpdateAttendanceForMultipleRoster(req: req) { [weak self] (_, error) in
            self?.isBusy.value = false
            completedClosure(error)
        }
    }

    func doUpdateHealCheckForMultipleRoster(rosters: [String], status: String, calendarItemId: String, calendarItemType: String, completedClosure: @escaping (_ error: String?) -> Void) {
         self.isBusy.value = true

         let req = UpdateAttendanceMultipleReq()
         req.token = AppDataInstance.instance.getToken()
         req.rosters = rosters
         req.calendarItemId = calendarItemId
         req.calendarItemKind = calendarItemType
         req.status = status

         CalendarService.instance.doUpdateHealthCheckMultipleRoster(req: req) { [weak self] (_, error) in
             self?.isBusy.value = false
             completedClosure(error)
         }
     }

    func doCancelGame(gameId: String, pos: Int, notify: Bool) {
        self.isBusy.value = true
        let req = CancelGameReq()
        req.token = AppDataInstance.instance.getToken()
        req.gameId = gameId
        req.pos = pos
        req.notifyTeam = notify

        CalendarService.instance.doCancelGame(req: req)
    }

    func doDeleteGamePractice(eventId: String, kind: String, pos: Int) {
        self.isBusy.value = true
        let req = DeleteCalendarItemReq()
        req.token = AppDataInstance.instance.getToken()
        req.Id = eventId
        req.kind = kind
        req.pos = pos

        CalendarService.instance.doDeletePracticeGame(req: req)
    }

    func doGetHistoryEvents(memberId: String, page: Int) {
        self.isBusy.value = true
        let req = GetHistoryEventsReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = memberId
        req.page = "\(page)"

        CalendarService.instance.doGetHistoryEvents(req: req)
    }

    func doUpdateAttendForEvent(attendanceID: String, status: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = UpdateAttendanceReq()
        req.attendanceId = attendanceID
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.status = status
        req.token = AppDataInstance.instance.getToken()

        CalendarService.instance.doUpdateAttendanceForEvent(req: req)
    }

    func doCreateAttendForEvent(eventId: String, targetStatus: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = CreateAttendaceReq()
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.eventId = eventId
        req.kind = calendarItemType == CALENDAR_ITEM_TYPE.GAME_MODEL ? "1" : "0"
        req.status = targetStatus
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        CalendarService.instance.doCreateAttendanceForEvent(req: req)
    }

    func doCancelPractice(practiceId: String, pos: Int, notifyTeam: Bool) {
        self.isBusy.value = true

        let req = CancelPracticeReq()
        req.token = AppDataInstance.instance.getToken()
        req.practiceId = practiceId
        req.pos = pos
        req.notify = notifyTeam

        CalendarService.instance.doCancelPractice(req: req)
    }

    func doGetResponsible(memberId: String) {
        self.isBusy.value = true
        let req = GetResponsibleReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = memberId

        CalendarService.instance.doGetResponsible(req: req)
    }

    func doCarpool(calendar: CalendarListDTO, completion: @escaping (_ error: Error?) -> Void ) {
        guard let roster = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        if let game = calendar.game {
            let id = "team_" + roster.TEAM_ID
            let reference = db.collection(["carpool_chats", id, "messages"].joined(separator: "/"))

            let gameName = game.NAME ?? ""
            let oponentName = game.opponent_detail?.OPPONENT_NAME ?? ""
            let content = "\(roster.displayName) requested a carpool for \(gameName) vs \(oponentName) at " + game.startTimeDisplay + " on " + game.slashesDateDisplay
            let message = Message(roster: roster, content: content, type: 2)

            reference.addDocument(data: message.representation) { error in
                    if let e = error {
                      print("Error sending carpool: \(e.localizedDescription)")
                      return
                    }

                    completion(error)

                    self.carpoolChatReference.document(id).setData(ChatEntryReadDTO(message: message).firebaseDict()) { (error) in
                        print(error ?? "")
                    }
                  }
        } else if let practice = calendar.practice {
            let id = "team_" + roster.TEAM_ID
            let reference = db.collection(["carpool_chats", id, "messages"].joined(separator: "/"))

            let practiceName = practice.NAME ?? ""

            let content = "\(roster.displayName) requested a carpool for \(practiceName) at " + practice.startTimeDisplay + " on " + practice.slashesDateDisplay
            let message = Message(roster: roster, content: content, type: 2)

            reference.addDocument(data: message.representation) { error in
                if let e = error {
                  print("Error sending carpool: \(e.localizedDescription)")
                  return
                }

                completion(error)

                self.carpoolChatReference.document(id).setData(ChatEntryReadDTO(message: message).firebaseDict()) { (error) in
                    print(error ?? "")
                }
          }
        }
    }
}
