//
//  AddGameViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/9.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class AddGameViewModel {
    var isBusy = Observable(false)
    var oppo = Observable(OpponentModel(val: nil))
    var uniformColor = Observable("")
    var isHomeTeam: Int = -1

    var startTime: Date?
    var startTimeAMPM: String = ""
    var startTimeHour: Int = 0
    var startTimeMin: Int = 0

    var arrivalTime: Date?
    var arrivalTimeAMPM: String  = ""
    var arrivalTimeHour: Int = 0
    var arrivalTimeMin: Int = 0

    var durationHour: Int = 0
    var durationMin: Int = 0

    var eventTime: Date?
    var year: Int = 0
    var month: Int = 0
    var dayOfMonth: Int = 0
    var dayOfWeek: String = ""

    var teamLocation = Observable(TeamLocation(val: nil))

    func doCreateNewGame(addr: String, memo: String, notify: Bool) {
        self.isBusy.value = true
        let req = CreateNewGameReq()
        req.addr = addr
        req.arrivalTimeAmPm = self.arrivalTimeAMPM
        req.arrivalTimeHour = "\(self.arrivalTimeHour)"
        req.arrivalTimeMin = "\(self.arrivalTimeMin)"
        req.dayOfMonth = "\(self.dayOfMonth)"
        req.dayOfWeek = self.dayOfWeek
        req.durationHour = "\(self.durationHour)"
        req.durationMin = "\(self.durationMin)"
        req.isHomeTeam = "\(self.isHomeTeam)"
        req.memo = memo
        req.month = "\(self.month)"
        req.needNotify = notify
        req.opponentId = self.oppo.value.ID
        req.startTimeAmPm = self.startTimeAMPM
        req.startTimeHour = "\(self.startTimeHour)"
        req.startTimeMin = "\(self.startTimeMin)"
        req.teamId = AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.ID
        req.teamLocationId = self.teamLocation.value.id
        req.token = AppDataInstance.instance.getToken()
        req.wearColor = self.uniformColor.value
        req.year = "\(self.year)"

        CalendarService.instance.doCreateGame(req: req)
    }

    func doUpdateGame(name: String, memo: String, addr: String, originId: String, notify: Bool) {
        self.isBusy.value = true

        let req = UpdateGameReq()
        req.addr = addr
        req.arrivalTimeAmPm = self.arrivalTimeAMPM
        req.arrivalTimeHour = "\(self.arrivalTimeHour)"
        req.arrivalTimeMin = "\(self.arrivalTimeMin)"
        req.dayOfMonth = "\(self.dayOfMonth)"
        req.dayOfWeek = self.dayOfWeek
        req.durationHour = "\(self.durationHour)"
        req.durationMin = "\(self.durationMin)"
        req.isHomeTeam = "\(self.isHomeTeam)"
        req.memo = memo
        req.month = "\(self.month)"
        req.needNotify = notify
        req.opponentId = self.oppo.value.ID
        req.startTimeAmPm = self.startTimeAMPM
        req.startTimeHour = "\(self.startTimeHour)"
        req.startTimeMin = "\(self.startTimeMin)"
        req.teamId = AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.ID
        req.teamLocationId = self.teamLocation.value.id
        req.token = AppDataInstance.instance.getToken()
        req.wearColor = self.uniformColor.value
        req.year = "\(self.year)"
        req.name = name
        req.id = originId

        CalendarService.instance.doUpdateGame(req: req)
    }
}
