//
//  PracticeUpdateDialogViewModel.swift
//  beclutch
//
//  Created by Vu Trinh on 21/10/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class PracticeUpdateDialogViewModel {
    var isBusy = Observable(false)
    
    func doCancelPractice(practiceId: String?, notifyTeam: Bool = true, isApplyAll: Bool) {
        if let practiceId = practiceId {
            self.isBusy.value = true

            let req = CancelPracticeReq()
            req.token = AppDataInstance.instance.getToken()
            req.practiceId = practiceId
            req.notify = notifyTeam
            req.isApplyAll = isApplyAll

            CalendarService.instance.doCancelPractice(req: req)
        }
    }
    
    func doDeletePractice(practiceId: String?, kind: String = "0", isApplyAll: Bool) {
        if let practiceId = practiceId {
            self.isBusy.value = true
            let req = DeleteCalendarItemReq()
            req.token = AppDataInstance.instance.getToken()
            req.Id = practiceId
            req.kind = kind
            req.isApplyAll = isApplyAll

            CalendarService.instance.doDeletePracticeGame(req: req)
        }
    }
}
