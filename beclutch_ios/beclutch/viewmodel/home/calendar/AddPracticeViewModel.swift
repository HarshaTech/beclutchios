//
//  AddPracticeViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
import EventKit

class AddPracticeViewModel {
    var isBusy = Observable(false)
    var repeatModel = Observable(RepeatModel(val: nil))

    var startTime: Date?
    var startTimeAMPM: String = ""
    var startTimeHour: Int = 0
    var startTimeMin: Int = 0

    var arrivalTime: Date?
    var arrivalTimeAMPM: String  = ""
    var arrivalTimeHour: Int = 0
    var arrivalTimeMin: Int = 0

    var durationHour: Int = 0
    var durationMin: Int = 0

    var eventTime: Date?
    var year: Int = 0
    var month: Int = 0
    var dayOfMonth: Int = 0
    var dayOfWeek: String = ""

    var screenType: PracticeScreenType = .practice

    var teamLocation = Observable(TeamLocation(val: nil))

    func doCreatePractice(name: String = "Practice", addr: String, memo: String, needNotify: Bool) {
        self.isBusy.value = true
        let req = CreatePracticeReq()
        req.token = AppDataInstance.instance.getToken()
        req.addr = addr
        req.arrivalTimeAmPm = self.arrivalTimeAMPM
        req.arrivalTimeMin = "\(self.arrivalTimeMin)"
        req.arrivalTimeHour = "\(self.arrivalTimeHour)"
        req.dayOfMonth = "\(self.dayOfMonth)"
        req.dayofWeek = self.dayOfWeek
        req.durationHour = "\(self.durationHour)"
        req.durationMin = "\(self.durationMin)"
        req.locationId = self.teamLocation.value.id
        req.memo = memo
        req.month = "\(self.month)"
        req.needNotify = needNotify
        req.repeatModel = self.repeatModel.value.toJson()
        req.startTimeAmPm = self.startTimeAMPM
        req.startTimeHour = "\(self.startTimeHour)"
        req.startTimeMin = "\(self.startTimeMin)"
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.token = AppDataInstance.instance.getToken()
        req.year = "\(self.year)"
        req.name = screenType == .other ? name : "Practice"

        CalendarService.instance.doCreatePractice(req: req)
    }

    func doUpdatePractice(originId: String, name: String, addr: String, memo: String, needNotify: Bool, isApplyAll: Bool) {
        self.isBusy.value = true
        let req = UpdatePracticeReq()
        req.token = AppDataInstance.instance.getToken()
        req.addr = addr
        req.arrivalTimeAmPm = self.arrivalTimeAMPM
        req.arrivalTimeMin = "\(self.arrivalTimeMin)"
        req.arrivalTimeHour = "\(self.arrivalTimeHour)"
        req.dayOfMonth = "\(self.dayOfMonth)"
        req.dayofWeek = self.dayOfWeek
        req.durationHour = "\(self.durationHour)"
        req.durationMin = "\(self.durationMin)"
        req.locationId = self.teamLocation.value.id
        req.memo = memo
        req.month = "\(self.month)"
        req.needNotify = needNotify
        req.repeatModel = self.repeatModel.value.toJson()
        req.startTimeAmPm = self.startTimeAMPM
        req.startTimeHour = "\(self.startTimeHour)"
        req.startTimeMin = "\(self.startTimeMin)"
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.token = AppDataInstance.instance.getToken()
        req.year = "\(self.year)"
        req.originId = originId
        req.name = name
        
        req.isApplyAll = isApplyAll

        CalendarService.instance.doUpdatePractice(req: req)
    }
}
