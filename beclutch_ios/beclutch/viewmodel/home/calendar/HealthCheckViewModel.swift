//
//  HealthCheckViewModel.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/20/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class HealthCheckViewModel {
    var isBusy = Observable(false)

    func doSubmitHealthCheck(eventId: String, targetStatus: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = CreateHealthCheckReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.eventId = eventId
        req.eventKind = calendarItemType == CALENDAR_ITEM_TYPE.GAME_MODEL ? "1" : "0"
        req.status = targetStatus

        CalendarService.instance.doCreateHealthCheck(req: req)
    }
}
