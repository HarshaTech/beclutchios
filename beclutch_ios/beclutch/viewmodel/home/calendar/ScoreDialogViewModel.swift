//
//  File.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/21/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class ScoreDialogViewModel {
    var isBusy = Observable(false)

    func doGetScoreGame(id: String) {
        self.isBusy.value = true

        let req = GetGameScoreReq()
        req.token =  AppDataInstance.instance.getToken()
        req.gameId = id

        CalendarService.instance.doGetGameScore(req: req)
    }

    func doUpdateScoreGame(id: String,
                           usScore: String,
                           themScore: String,
                           memo: String,
                           flag: Bool) {
        self.isBusy.value = true

        let req = UpdateGameScoreReq()
        req.token = AppDataInstance.instance.getToken()
        req.gameId = id
        req.usScore = usScore
        req.themScore = themScore
        req.memo = memo
        req.flag = flag

        CalendarService.instance.doUpdateGameScore(req: req)
    }

}
