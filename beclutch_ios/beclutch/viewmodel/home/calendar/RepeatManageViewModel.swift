//
//  RepeatManageViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class RepeatManageViewModel {
    var isBusy = Observable(false)
    var repeatType = ""
    var chooseDate: Date?

    var tillDailyDate = Date()
    var tillDaily = Observable("")

    var tillWeeklyDate = Date()
    var tillWeekly = Observable("")

    var tillMonthlyDate = Date()
    var tillMonthly = Observable("")

    var tillYearlyDate = Date()
    var tillYearly = Observable("")

    func makeRetVal(daily: [Int]?) -> RepeatModel {
        let retVal: RepeatModel = RepeatModel(val: nil)
        retVal.type = self.repeatType
        retVal.chooseDate = self.chooseDate
        if retVal.type == RepeatModel.REPEAT_TYPE_NO_REPEAT {
            retVal.tillDate = self.tillDaily.value
        } else if retVal.type == RepeatModel.REPEAT_TYPE_DAILY_REPEAT {
            retVal.tillDate = self.tillDaily.value

            if let dailyArr = daily {
                retVal.dailyInt = dailyArr
            }
        } else if retVal.type == RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT {
            retVal.tillDate = self.tillWeekly.value
        } else if retVal.type == RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT {
            retVal.tillDate = self.tillMonthly.value
        } else if retVal.type == RepeatModel.REPEAT_TYPE_YEARLY_REPEAT {
            retVal.tillDate = self.tillYearly.value
        }
        return retVal
    }
}
