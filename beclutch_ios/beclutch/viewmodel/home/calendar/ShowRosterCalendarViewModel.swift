//
//  ShowRosterCalendarViewModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/11.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class ShowRosterCalendarViewModel {

    var isBusy = Observable(false)
    var dp: [CalendarRosterDTO] = []
    var eventId: String?
    var kind: String?

    var expandsTrack: [Bool] = []

    func updateExpandTrack() {
        if expandsTrack.count > dp.count {
            expandsTrack.removeAll()
        }

        for _ in expandsTrack.count..<dp.count {
            expandsTrack.append(false)
        }
    }

    func resetExpandTrack() {
        for i in 0 ..< expandsTrack.count {
            expandsTrack[i] = false
        }
    }

    func doLoadRoster() {
        self.isBusy.value = true
        let req = LoadRosterInCalendarReq()
        req.token = AppDataInstance.instance.getToken()
        req.kind = self.kind!
        req.eventId = self.eventId!
        req.curRosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        resetExpandTrack()

        CalendarService.instance.doLoadRosterForEvent(req: req)
    }

    func doCreateAttendForEvent(eventId: String, targetStatus: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = CreateAttendaceReq()
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.eventId = eventId
        req.kind = calendarItemType == CALENDAR_ITEM_TYPE.GAME_MODEL ? "1" : "0"
        req.status = targetStatus
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        CalendarService.instance.doCreateAttendanceForEvent(req: req)
    }

    func doUpdateAttendForEvent(attendanceID: String, status: String, calendarItemId: String, calendarItemType: CALENDAR_ITEM_TYPE) {
        self.isBusy.value = true

        let req = UpdateAttendanceReq()
        req.attendanceId = attendanceID
        req.calendarItemId = calendarItemId
        req.calendarItemType = calendarItemType
        req.status = status
        req.token = AppDataInstance.instance.getToken()

        CalendarService.instance.doUpdateAttendanceForEvent(req: req)
    }

    func doUpdateAttendForMultiple(rosters: [String], status: String, calendarItemId: String, calendarItemType: String, completedClosure: @escaping (_ error: String?) -> Void) {
        self.isBusy.value = true

        let req = UpdateAttendanceMultipleReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosters = rosters
        req.calendarItemId = calendarItemId
        req.calendarItemKind = calendarItemType
        req.status = status

        CalendarService.instance.doUpdateAttendanceForMultipleRoster(req: req) { [weak self] (_, error) in
            self?.isBusy.value = false
            completedClosure(error)
        }
    }
}
