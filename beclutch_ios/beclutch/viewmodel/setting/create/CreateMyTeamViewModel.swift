//
//  CreateMyTeamViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Observable

class CreateMyTeamViewModel {
    var isBusy = Observable(false)
    
    var selectSportItem = BehaviorRelay<Production>(value: Production())
    var selectGroupItem = BehaviorRelay<Production>(value: Production())
    var selectRoleItem = BehaviorRelay<Production>(value: Production())
    
    var isAddTeamToClub: Bool = false
    var originClubId: String = ""
    
    let service = CreateNewMyTeamService()
    let activityIndicator = ActivityIndicator()

    func doCreateMyTeam(teamName: String) {
        self.isBusy.value = true
        let req = CreateTeamInSettingReq()
        req.token = AppDataInstance.instance.getToken()
        req.team_name = teamName
        req.sport_id = self.selectSportItem.value.ID
        req.role = selectRoleItem.value.ID
        req.club_id = "0"

        CreateService.instance.doCreateMyTeamInSetting(req: req)
    }

    func loadClubTree() {
       let req = ClubTreeLoadReq()
       req.token = AppDataInstance.instance.getToken()
       self.isBusy.value = true
       HomeService.instance.doLoadClubTree(req: req)
   }

    func doAddMyTeamToClub(teamName: String) {
        self.isBusy.value = true
        let req = CreateTeamInSettingReq()
        req.token = AppDataInstance.instance.getToken()
        req.team_name = teamName
        req.sport_id = self.selectSportItem.value.ID
        req.role = selectRoleItem.value.ID
        req.club_id = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId

        CreateService.instance.doCreateMyTeamInSetting(req: req)
    }

    func doUpdateTeam(rosterID: String) {
        let req = UpdateDefaultTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = rosterID
        HomeService.instance.doUpdateDefaultTeam(req: req)
    }
}
