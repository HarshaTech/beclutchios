//
//  CreateClubViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class CreateClubViewModel {
    var isBusy = Observable(false)
    var selectSportItem = Observable(Production())

    func doCreateMyClub(clubName: String) {
        self.isBusy.value = true
        let req = CreateClubInSettingReq()
        req.token = AppDataInstance.instance.getToken()
        req.club_name = clubName
        req.sport_id = self.selectSportItem.value.ID

        CreateService.instance.doCreateMyClubInSetting(req: req)
    }
    
    func loadClubTree() {
       let req = ClubTreeLoadReq()
       req.token = AppDataInstance.instance.getToken()
       self.isBusy.value = true
       HomeService.instance.doLoadClubTree(req: req)
   }
    
    func doUpdateTeam(rosterID: String) {
        let req = UpdateDefaultTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = rosterID
        HomeService.instance.doUpdateDefaultTeam(req: req)
    }
}
