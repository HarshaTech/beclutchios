//
//  CreateInviteTeamManagerViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/26.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class CreateInviteTeamManagerViewModel {
    var isBusy = Observable(false)
    var selectSportItem = Observable(Production())
    var clubInfo: RosterInfo?
    func InviteCreateTeamManager(teamName: String, f_name: String, l_name: String, email: String, sendMail: Bool) {
        self.isBusy.value = true
        let req = CreateInviteTeamToClubReq()
        req.token = AppDataInstance.instance.getToken()
        req.clubId = self.clubInfo?.CLUB_ID
        req.email = email
        req.fname = f_name
        req.lname = l_name
        req.roleId = "2"
        req.roleTitle = "Coach"
        req.sendEMail = sendMail
        req.sportId = self.selectSportItem.value.ID
        req.sportTitle = self.selectSportItem.value.NAME
        req.teamId = "0"
        req.teamName = teamName

        CreateService.instance.doCreateInviteTeamManager(req: req)
    }
}
