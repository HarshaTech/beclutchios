//
//  NotificationViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

class NotificationViewModel {
    var isBusy = Observable(false)
    var originSettingId: String?
    func doLoadNotificationSetting() {
        self.isBusy.value = true
        let req = NotificationLoadReq()
        req.token = AppDataInstance.instance.getToken()
        SettingService.instance.doLoadNotificationSetting(req: req)
    }

    func doSaveNotificationSetting(sch_app: String, sch_mail: String, chat_app: String, chat_mail: String, score_app: String, score_mail: String, reminder_app: String, reminder_mail: String, tour_app: String, tour_mail: String) {
        self.isBusy.value = true
        let req = NotificationUpdateReq()
        req.token = AppDataInstance.instance.getToken()
        req.sch_app = sch_app
        req.sch_mail = sch_mail
        req.chat_app = chat_app
        req.chat_mail = chat_mail
        req.score_app = score_app
        req.score_mail = score_mail
        req.reminder_app = reminder_app
        req.reminder_mail  = reminder_mail
        req.tour_app = tour_app
        req.tour_mail = tour_mail
        req.id = self.originSettingId == nil ? "-1" : self.originSettingId
        SettingService.instance.doUpdateNotificationSetting(req: req)
    }
}
