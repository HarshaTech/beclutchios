//
//  TeamPlanViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/18.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class TeamPlanViewModel {
    var isBusy = Observable(false)
    var curPlan = Observable(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL)

    func updatePlan(plan: String) {
        self.isBusy.value = true
        let req = TeamPlanUpdateReq()
        req.plan = plan
        req.token = AppDataInstance.instance.getToken()
        req.team_id = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        SettingService.instance.doUpdateTeamPlan(req: req)
    }
}
