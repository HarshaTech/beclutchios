//
//  TeamManageViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class TeamManageViewModel {
    var isBusy = Observable(false)
    var productInfo = Observable(Production(val: nil))
    var timeZone = Observable(0)
    var practiceHour = Observable("0")
    var practiceMin = Observable("0")
    var arriveHour = Observable("0")
    var arriveMin = Observable("0")

    var homeColor = Observable("")
    var awayColor = Observable("")
    var alter1Color = Observable("")
    var alter2Color = Observable("")
    //------------------ theme --------------------//
    var isSelect = Observable("0")
    var colorValue = Observable("")
    var colorName = Observable("")
    var colorResourcePrimary = Observable("0")
    var colorResourceDark = Observable("0")
    var themeResource = Observable("0")
    //------------------ theme --------------------//
    var timezones: [TimeZoneInfo] = []
    var originPhotoURL: String?
    var newUploadPhoto: Data?

    var isImageEditted = false
    
    let service: CalendarRxService
    
    init(service: CalendarRxService) {
        self.service = service
    }

    func doLoadTimezone() {
        self.isBusy.value = true
        let req = LoadTimeZoneReq()
        req.token = AppDataInstance.instance.getToken()
        SettingService.instance.doLoadTimezone(req: req)
    }

    func doSaveTeamSetting(name: String) {
        self.isBusy.value = true
        let req = TeamInfoUpdateReq()
        req.token = AppDataInstance.instance.getToken()
        req.alterColor1 = self.alter1Color.value
        req.alterColor2 = self.alter2Color.value
        req.arriveHour = self.arriveHour.value
        req.arriveMin = self.arriveMin.value
        req.awayColor = self.awayColor.value
        req.homeColor = self.homeColor.value
        req.durationHour = self.practiceHour.value
        req.durationMin = self.practiceMin.value
        req.fileData = self.newUploadPhoto
        req.memberId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.name = name
        req.sport = self.productInfo.value.ID
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.timezone = String(self.timeZone.value)

        req.isSelect = self.isSelect.value
        req.colorValue = self.colorValue.value
        req.colorName = self.colorName.value
        req.colorResourcePrimary = self.colorResourcePrimary.value
        req.colorResourceDark = self.colorResourceDark.value
        req.themeResource = self.themeResource.value

        SettingService.instance.doUpdateTeamInfo(req: req)
    }

    func doSplashAction() {
        let token = PreferenceMgr.readString(keyName: PreferenceMgr.TOKEN)
        let fbToken = PreferenceMgr.readString(keyName: PreferenceMgr.FIREBASE_TOKEN)

        let req = SplashRequest(token: token, fb_token: fbToken)
        AuthService.instance.doSplashAction(req: req)
    }
}
