//
//  TeamSupportViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/18.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class TeamSupportViewModel {
    var isBusy = Observable(false)
    var curSupportMail = Observable("")

    func doAddSupportHistory(subject: String, content: String) {
        self.isBusy.value = true
        let req = TeamSupportReq()
        req.token  = AppDataInstance.instance.getToken()
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.fName = AppDataInstance.instance.me?.FNAME
        req.lName = AppDataInstance.instance.me?.LNAME
        req.subject = subject
        req.content = content
        req.version = CommonUtils.appVersion

        SettingService.instance.doCreateTeamSupport(req: req)
    }
}
