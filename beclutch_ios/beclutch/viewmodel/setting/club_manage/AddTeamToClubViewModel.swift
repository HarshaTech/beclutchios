//
//  AddTeamToClubViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/27.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class AddTeamToClubViewModel {
    var isBusy = Observable(false)
    var dp: [RosterInfo]?
    var chooseStatus: [Bool]?
    var originClubId: String = "0"
    var originClubSportId: String = "0"

    func loadIndependentTeam() {
        self.isBusy.value = true
        let req = LoadMyClubReq()
        req.token = AppDataInstance.instance.getToken()
        req.sport = self.originClubSportId

        CreateService.instance.doLoadMyTeamToAddClub(req: req)
    }
}
