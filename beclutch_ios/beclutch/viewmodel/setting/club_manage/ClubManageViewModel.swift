//
//  ClubManageViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class ClubManageViewModel {
    var isBusy = Observable(false)
    var originPhotoURL: String?
    var newUploadPhoto: Data?
    var selectSportItem = Observable(Production())

    var isImageEditted = false

    func doUpdateClubInfo(name: String) {
        self.isBusy.value = true
        let req = ClubInfoUpdateReq()
        req.token  = AppDataInstance.instance.getToken()
        req.club_name = name
        req.clubId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID
        req.memberId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.sport = selectSportItem.value.ID
        req.fileData = newUploadPhoto
        SettingService.instance.doUpdateClubInfo(req: req)
    }
}
