//
//  TeamsInClubViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class TeamsInClubViewModel {
    var isBusy = Observable(false)
    var dp: [RosterInfo]?

    func loadTeamsInClub() {
        guard let currentTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        self.isBusy.value = true
        let req = LoadTeamsInClubReq()
        req.token = AppDataInstance.instance.getToken()
        req.clubId = currentTeam.CLUB_TEAM_INFO.ID

        SettingService.instance.doLoadTeamsInClub(req: req)
    }
}
