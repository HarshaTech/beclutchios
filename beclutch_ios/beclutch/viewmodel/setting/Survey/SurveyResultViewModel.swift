//
//  SurveyResultViewModel.swift
//  beclutch
//
//  Created by NamViet on 4/30/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Observable

class SurveyResultViewModel {
    var isBusy = Observable(false)

    var tableData: [TeamSurvey] = []

    var expandsTrack: [Bool] = []

    func updateExpandTrack() {
        if expandsTrack.count > tableData.count {
            expandsTrack.removeAll()
        }

        for _ in expandsTrack.count..<tableData.count {
            expandsTrack.append(false)
        }
    }

    func resetExpandTrack() {
        for i in 0 ..< expandsTrack.count {
            expandsTrack[i] = false
        }
    }

    func doLoadSurveyDetailResult(surveyId: String) {
        isBusy.value = true

        let req = SurveyGetResponseDetailReq()
        req.token = AppDataInstance.instance.getToken()
        req.surveyId = surveyId

        SurveyService.instance.doGetSurveyDetailResponse(req: req)
    }
}
