//
//  AddSurveyViewModel.swift
//  beclutch
//
//  Created by NamViet on 4/30/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Observable

//var isMultiple: Bool?
//var isAnonymous: Bool?
//var reminderFreq: Int?
//var startDate:Date?
//var endDate:Date?

class AddSurveyViewModel {
    var isBusy = Observable(false)
    var selectedFrequencyIndex = Observable(0)

    var expiredDate = Observable(Date())
    var answers = ["", ""]
    var selResult = Observable(GroupSelectionResult(val: nil))

    func doAddNewSurvey(name: String?, question: String?, isMultiple: Bool, isAnonymous: Bool, frequency: Int) {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        isBusy.value = true

        let req = SurveyAddReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID

        req.name = name
        req.desc = "desc"
        req.question = question
        req.answers = answers.filter { $0.count > 0 }

        req.isMultiple = isMultiple
        req.isAnonymous = isAnonymous
        req.reminderFreq = frequency

        req.startDate = Date()
        req.endDate = expiredDate.value

        req.isAllPlayers = selResult.value.isSelelctPlayers
        req.groupSelectionResult = selResult.value

        SurveyService.instance.doCreateNewSurvey(req: req)
    }
}
