//
//  SurveysViewModel.swift
//  beclutch
//
//  Created by NamViet on 4/30/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class SurveysViewModel {
    var isBusy = Observable(false)

    var tableData: [TeamSurvey] = []

    var expandsTrack: [Bool] = []
    
    var currentSurvey: TeamSurvey?
    var currentAnswers: [SurveyAnswerDetail]?

    func updateExpandTrack() {
        if expandsTrack.count > tableData.count {
            expandsTrack.removeAll()
        }

        for _ in expandsTrack.count..<tableData.count {
            expandsTrack.append(false)
        }
    }

    func resetExpandTrack() {
        for i in 0 ..< expandsTrack.count {
            expandsTrack[i] = false
        }
    }

    func doLoadTeamSurveys() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        isBusy.value = true
        resetExpandTrack()

        let req = SurveyTeamGetReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        SurveyService.instance.doGetTeamSurvey(req: req)
    }

    func doDeleteSurveyItem(surveyId: String?) {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }
        isBusy.value = true

        let req = SurveyDeleteReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.surveyId = surveyId
        SurveyService.instance.doDeleteSurvey(req: req)
    }

    func doSumbitAnswer() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        isBusy.value = true

        let req = SurveySubmitReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        req.surveyId = currentSurvey?.id
        req.questionId = currentSurvey?.questionId
        req.vote_answers = currentAnswers

        SurveyService.instance.doSubmitRosterResponse(req: req)
    }
    
    func doSumbitAnswerForMultipleRoster(rosters: [String]) {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        isBusy.value = true

        let req = SurveyMultipleSubmitReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID
        req.rosters = rosters

        req.surveyId = currentSurvey?.id
        req.questionId = currentSurvey?.questionId
        req.vote_answers = currentAnswers

        SurveyService.instance.doSubmitMultipleRosterResponse(req: req)
    }
    
    func doGetResponsible() {
        self.isBusy.value = true
        let req = GetResponsibleReq()
        req.token = AppDataInstance.instance.getToken()
        req.memberId = AppDataInstance.instance.currentSelectedTeam?.ID

        CalendarService.instance.doGetResponsible(req: req)
    }
}
