//
//  LocationListViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class LocationListViewModel {
    var isBusy = Observable(false)
    var isForChoose: Bool = false
    var dataProvider: [TeamLocation] = []

    func doLoadLocations() {
        self.isBusy.value = true
        let req = LocationLoadReq()
        req.token = AppDataInstance.instance.getToken()
        SettingService.instance.doLoadLocations(req: req)
    }
}
