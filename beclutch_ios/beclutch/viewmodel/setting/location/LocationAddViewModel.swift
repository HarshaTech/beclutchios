//
//  LocationAddViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
public class LocationAddViewModel {
    var isBusy = Observable(false)
    var originLocation: TeamLocation?
    var country = Observable("")
    var countryIdx: Int = 0
    var state = Observable("")
    var stateIdx: Int = -1

    func doLocationAdd(name: String, city: String, street: String, zip: String, link: String, note: String) {
        self.isBusy.value = true
        let req = LocationAddReq()
        req.token = AppDataInstance.instance.getToken()
        req.name = name
        req.country = self.country.value
        req.country_idx = "\(self.countryIdx)"
        req.state = self.state.value
        req.state_idx = "\(self.stateIdx)"
        req.city = city
        req.street = street
        req.zip = zip
        req.link = link
        req.note = note
        req.lat = "0"
        req.lot = "0"
        SettingService.instance.doAddLocation(req: req)
    }

    func doLocationUpdate(name: String, city: String, street: String, zip: String, link: String, note: String) {
        self.isBusy.value = true
        let req = LocationUpdateReq()
        req.token = AppDataInstance.instance.getToken()
        req.name = name
        req.country = self.country.value
        req.country_idx = "\(self.countryIdx)"
        req.state = self.state.value
        req.state_idx = "\(self.stateIdx)"
        req.city = city
        req.street = street
        req.zip = zip
        req.link = link
        req.id = originLocation?.id
        req.note = note
        req.lat = "0"
        req.lot = "0"
        SettingService.instance.doUpdateLocation(req: req)
    }
}
