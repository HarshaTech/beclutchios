//
//  OpponentAddViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class OpponentAddViewModel {
    var isBusy = Observable(false)
    var originOppo: OpponentModel?
    func doAddOpponent(name: String, contactName: String, phone: String, email: String, note: String) {
        self.isBusy.value = true
        let req = AddOpponentReq()
        req.token = AppDataInstance.instance.getToken()
        req.name = name
        req.contactPhone = phone
        req.contactName = contactName
        req.contactEmail = email
        req.contactNote = note

        SettingService.instance.doAddOpponent(req: req)
    }

    func doEditOpponent(name: String, contactName: String, phone: String, email: String, note: String) {
        self.isBusy.value = true
        let req = UpdateOpponentReq()
        req.token = AppDataInstance.instance.getToken()
        req.name = name
        req.contactPhone = phone
        req.contactName = contactName
        req.contactEmail = email
        req.contactNote = note
        req.id = self.originOppo?.ID

        SettingService.instance.doUpdateOpponent(req: req)
    }
}
