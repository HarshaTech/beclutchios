//
//  OpponentListViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
public class OpponentListViewModel {
    var isBusy = Observable(false)
    var isForChoose: Bool = false
    var dataProvider: [OpponentModel] = []

    func doLoadOpponentList() {
        self.isBusy.value = true
        let req = OpponentLoadReq()
        req.token = AppDataInstance.instance.getToken()
        SettingService.instance.doLoadOpponentList(req: req)
    }
}
