//
//  AccountInfoViewModel.swift
//  beclutch
//
//  Created by FAT_MAC on 7/31/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class AccountInfoViewModel {

    var isBusy = Observable(false)

    func doSaveAccount(fname: String, lname: String, phone: String, addr: String, city: String, state: String, zip: String) {
        self.isBusy.value = true
        let req = AccountInfoReq()
        req.token = AppDataInstance.instance.getToken()
        req.fname = fname
        req.lname = lname
        req.phone = phone
        req.addr = addr
        req.city = city
        req.state = state
        req.zip = zip

        AuthService.instance.doUpdateAccountInfomation(req: req)
    }
}
