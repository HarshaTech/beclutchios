//
//  FinanceViewModel.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class FinanceViewModel {
    var isBusy = Observable(false)

    var tableData: [(String, [FeeInfo])] = []

    var fees: [FeeInfo] = []

    var expandsTrack: [Bool] = []

    func updateExpandTrack() {
        if expandsTrack.count > tableData.count {
            expandsTrack.removeAll()
        }

        for _ in expandsTrack.count..<tableData.count {
            expandsTrack.append(false)
        }
    }

    func resetExpandTrack() {
        for i in 0 ..< expandsTrack.count {
            expandsTrack[i] = false
        }
    }

    func doLoadFinanceTeam() {
        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        isBusy.value = true
        resetExpandTrack()

        let req = FinanceTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = seletedTeam.CLUB_TEAM_INFO.ID
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        FinanceService.instance.doGetFinanceTeam(req: req)
    }

    func doAddPay(dueId: String, amount: Float) {
        isBusy.value = true

        let req = FinancePayAddReq()
        req.token = AppDataInstance.instance.getToken()
        req.dueId = dueId
        req.amount = String(amount)

        FinanceService.instance.doApplyPaid(req: req)
    }
}

class CompetitionViewModel {
    
    var isBusy = Observable(false)

    var fees: [LeaderBoardInfo] = []
    var teamList: [LeaderBoardTeamListInfo] = []
    func doLoadLeaderList() {
//        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
//            return
//        }
        isBusy.value = true

        let req = LeaderListReq()
        req.token = AppDataInstance.instance.getToken()
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId

        AddCompetitionService.instance.doGetLeaderList(req: req)
    }
    
    func doRemoveCompetition(leaderboardId: String, completedClosure: @escaping (_ error: String?) -> Void) {
        isBusy.value = true

        let req = CompetitionRemoveReq()
        req.token = AppDataInstance.instance.getToken()
        req.leaderboardId = leaderboardId

        AddCompetitionService.instance.doRemoveCompetitionList(req: req) { (_, error) in
            completedClosure(error)
        }
    }
    
    func doLoadTeamList(leaderboardId: String) {
//        guard let seletedTeam = AppDataInstance.instance.currentSelectedTeam else {
//            return
//        }
        isBusy.value = true

        let req = CompetitionTeamReq()
        req.token = AppDataInstance.instance.getToken()
        req.leaderboardId = leaderboardId

        AddCompetitionService.instance.doGetTeamList(req: req)
    }
    
    func doUpdateScore(score: String?, roasterid: String?, leaderboardId: String?) {
        isBusy.value = true

        let req = UpdateScoreReq()
        req.token = AppDataInstance.instance.getToken()
        req.roasterid = roasterid
        req.score = score
        req.leaderboardid = leaderboardId
//        FinanceService.instance.doAddNewFee(req: req)
        AddCompetitionService.instance.doUpdateScore(req: req)
    }
}

