//
//  ManageFundViewModel.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Observable

class ManageFundViewModel {
    var isBusy = Observable(false)
    var tableData: [(String, [FeeInfo])] = []

    var fees: [FeeInfo] = [] {
        didSet {
            let sortedFee = Dictionary(grouping: fees, by: { $0.description ?? "" }).map { $0 }.sorted(by: { (element1, element2) -> Bool in
                return element1.0 < element2.0
            })

            tableData = sortedFee
        }
    }

    var expandsTrack: [Bool] = []

    func updateExpandTrack() {
        if expandsTrack.count > fees.count {
            expandsTrack.removeAll()
        }

        for _ in expandsTrack.count..<fees.count {
            expandsTrack.append(false)
        }
    }

    func doRemoveFee(feeId: String, completedClosure: @escaping (_ error: String?) -> Void) {
        isBusy.value = true

        let req = FinanceFeeRemovesReq()
        req.token = AppDataInstance.instance.getToken()
        req.feeId = feeId

        FinanceService.instance.doRemoveFee(req: req) { (_, error) in
            completedClosure(error)
        }
    }

    func doRemoveFeeDetail(feeId: String, completedClosure: @escaping (_ error: String?) -> Void) {
        isBusy.value = true

        let req = FinanceFeeDetailRemoveReq()
        req.token = AppDataInstance.instance.getToken()
        req.feeId = feeId

        FinanceService.instance.doRemoveFeeDetail(req: req) { (_, error) in
            completedClosure(error)
        }
    }
}
