//
//  AddFeeViewModel.swift
//  beclutch
//
//  Created by SM on 2/28/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class AddFeeViewModel {
    var isBusy = Observable(false)
    var curDate = Observable(Date())
    var selResult = Observable(GroupSelectionResult(val: nil))

    func doAddFee(amount: String?, description: String?, notify: Bool?) {
        isBusy.value = true

        let req = FeeAddReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId
        req.amount = amount
        req.description = description
        req.dueDate = curDate.value
        req.isAllPlayers = selResult.value.isSelelctPlayers
        req.groupSelectionResult = selResult.value
        req.needNotify = notify

        FinanceService.instance.doAddNewFee(req: req)
    }
}

class AddCompetitionViewModel {
    var isBusy = Observable(false)
//    var startDate = Observable(Date())
//    var endDate = Observable(Date())
    var selResult = Observable(GroupSelectionResult(val: nil))

    func doAddCompetition(activityName: String?, activityType: String?, notify: Bool?, title: String?, frequency: String?, startDate: String?, endDate: String?) {
        isBusy.value = true

        let req = LeaderAddReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId
        req.activityName = activityName
        req.activityType = activityType
        req.startDate = startDate
        req.endDate = endDate
        req.isAllPlayers = selResult.value.isSelelctPlayers
        req.groupSelectionResult = selResult.value
        req.needNotify = notify
        req.title = title
        req.frequency = frequency
//        FinanceService.instance.doAddNewFee(req: req)
        AddCompetitionService.instance.doAddCompetition(req: req)
    }
    
    
}
