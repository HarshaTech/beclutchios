//
//  PaymentHistoryViewModel.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable

class PaymentHistoryViewModel {
    var isBusy = Observable(false)
    var tableData: [FeeInfo] = []

    func doLoadFinanceRoster(rosterId: String) {
        isBusy.value = true

        let req = FinanceRosterReq()
        req.token = AppDataInstance.instance.getToken()
        req.rosterId = rosterId
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID

        FinanceService.instance.doGetFinanceRoster(req: req)
    }

//    func doRemoveFee(feeId: String) {
//        isBusy.value = true
//
//        let req = FinanceFeeRemovesReq()
//        req.token = AppDataInstance.instance.getToken()
//        req.feeId = feeId
//
//        FinanceService.instance.doRemoveFee(req: req)
//    }

    func doRemoveFeeDetail(feeId: String, completedClosure: @escaping (_ error: String?) -> Void) {
        isBusy.value = true

        let req = FinanceFeeDetailRemoveReq()
        req.token = AppDataInstance.instance.getToken()
        req.feeId = feeId

        FinanceService.instance.doRemoveFeeDetail(req: req) { [weak self] (_, error) in
            self?.isBusy.value = false
            completedClosure(error)
        }
    }
}
