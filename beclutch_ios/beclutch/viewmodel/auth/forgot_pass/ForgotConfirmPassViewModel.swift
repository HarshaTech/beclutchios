//
//  ForgotConfirmPassViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class ForgotConfirmPassViewModel {
    var isBusy = Observable(false)

    func confirmPass(pass: String, token: String) {
        self.isBusy.value = true
        let req = ForgotConfirmReq()
        req.pass = pass
        req.token = token
        AuthService.instance.doRequestNewPasswordSubmit(req: req)
    }
}
