//
//  ForgotVerifyCodeSubmitViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class ForgotVerifyCodeSubmitViewModel {
    var isBusy = Observable(false)

    func doSubmitVerificationCode(val: String) {
        self.isBusy.value = true
        let req: PasswordResetVerificationCodeReq = PasswordResetVerificationCodeReq()
        req.code = val
        AuthService.instance.doRequestPasswordResetVerifyCodeSubmit(req: req)
    }
}
