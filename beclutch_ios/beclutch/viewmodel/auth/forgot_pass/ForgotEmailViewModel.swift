//
//  ForgotEmailViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/27.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

public class ForgotEmailViewModel {
    var isBusy = Observable(false)
    var confirmToken = Observable("")

    func doSendVerificationEmail(email: String) {
        self.isBusy.value = true
        let req = PasswordResetEmailVerifyReq()
        req.email = email
        AuthService.instance.doRequestPasswordEmailVerify(req: req)
    }
}
