//
//  SplashViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class SplashViewModel {

    func doSplashAction() {
        let token = PreferenceMgr.readString(keyName: PreferenceMgr.TOKEN)
        let fbToken = PreferenceMgr.readString(keyName: PreferenceMgr.FIREBASE_TOKEN)

        let req = SplashRequest(token: token, fb_token: fbToken)
        AuthService.instance.doSplashAction(req: req)
    }
}
