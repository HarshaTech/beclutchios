//
//  RegisterVerificationCodeViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class RegisterVerificationCodeViewModel {
    var isBusy = Observable(false)

    func doSubmitVerificationCode(code: String) {
        self.isBusy.value = true
        let req = RegisterSubmitCodeReq()
        req.code = code
        AuthService.instance.doRegisterSubmitCode(req: req)
    }
}
