//
//  CreateClubTeamInRegisterViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

class CreateClubTeamInRegisterViewModel {
    var isBusy = Observable(false)
    var selectSportItem = Observable(Production())

    func doCreateClubTeam(name: String, isClubCreate: String) {
        self.isBusy.value = true
        let req = CreateClubTeamReq()
        req.name = name
        req.sportId = self.selectSportItem.value.ID
        req.token = AppDataInstance.instance.getToken()
        req.isTeam = isClubCreate
        AuthService.instance.doCreateClubTeam(val: req)
    }
}
