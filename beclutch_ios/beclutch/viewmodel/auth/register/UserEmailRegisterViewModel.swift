//
//  UserEmailRegisterViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

class UserEmailRegisterViewModel {
    var isBusy = Observable(false)
    var confirmToken = Observable("")

    func doRegisterEmail(email: String, pwd: String, firebaseToken: String) {
        isBusy.value = true
        let req = RegisterEmailRequest()
        req.email = email
        req.pwd = pwd
        req.firebaseToken = firebaseToken
        AuthService.instance.doRegisterEmailInfo(req: req)
    }

    func doResendEmail(email: String, token: String) {
        isBusy.value = true
        let req = RegisterEmailResendReq()
        req.email = email
        req.token = token
        AuthService.instance.doRegisterResendEmail(req: req)
    }
}
