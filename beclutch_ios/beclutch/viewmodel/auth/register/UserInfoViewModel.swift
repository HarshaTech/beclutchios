//
//  UserInfoViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

class UserInfoViewModel {
    var selectedState: String! = ""
    var isBusy = Observable(false)

    func doSubmitUserInformation(fname: String, lname: String, phone: String, addr: String, city: String, state: String, zip: String) {
        let req = RegisterUserInfoReq()
        req.addr = addr
        req.city = city
        req.fName = fname
        req.lName = lname
        req.phone = phone
        req.state = self.selectedState
        req.token = AppDataInstance.instance.getToken()

        isBusy.value = true
        AuthService.instance.doRegisterSubmitUserInfo(req: req)
    }
}
