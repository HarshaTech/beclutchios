//
//  RegisterAcceptInvitationViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/3.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable
class RegisterAcceptInvitationViewModel {
    var isBusy = Observable(false)

    func doSubmitInviteCode(inviteCode: String) {
        self.isBusy.value = true
        let req = InviteAcceptReq()
        req.token  = AppDataInstance.instance.getToken()
        req.code = inviteCode
        AuthService.instance.doSubmitInvitationCode(val: req)
    }
}
