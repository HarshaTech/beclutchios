//
//  LoginViewModel.swift
//  beclutch
//
//  Created by zeus on 2019/11/16.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Observable

class LoginViewModel {
    var isBusy = Observable(false)

    func doLogin(email: String, pwd: String) {
        isBusy.value = true
        let req = LoginReq()
        req.email = email
        req.pwd = pwd
        req.fbToken = PreferenceMgr.readString(keyName: PreferenceMgr.FIREBASE_TOKEN)
        AuthService.instance.doLogin(req: req)
    }
}
