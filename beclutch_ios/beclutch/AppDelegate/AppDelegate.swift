//
//  AppDelegate.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import FirebaseCore
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any] {
            
            if let screenType = userInfo["type"] as? String {
                NavUtil.intendedScreenType = Int(screenType)
            }
        }
        
        IQKeyboardManager.shared.enable = true
        
        // Use Firebase library to configure APIs.
        FirebaseApp.configure()
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.sharedInstance().start(completionHandler: nil)

        Messaging.messaging().delegate = self

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        ChatStatusManager.shared.saveDataToUserDefault()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken

        let tokenParts = deviceToken.map { data in
               return String(format: "%02.2hhx", data)
           }

           let token = tokenParts.joined()
           print("Device token: \(token)")
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")

        let dataDict: [String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)

        AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let screenType = userInfo["type"] as? String {//}, let _ = userInfo["conversation_id"] {
            print(screenType)
            
            let state = UIApplication.shared.applicationState
            if state == .inactive && !AppDataInstance.instance.getToken().isEmpty {
//                NavUtil.intendedScreenType = Int(screenType)
//                NavUtil.showHomeViewController()
                if let rootNavigationController = window?.rootViewController as? UINavigationController,
                   let homeController = rootNavigationController.viewControllers.first as? HomeController,
                   let tabbarIndex = Int(screenType) {
                    if homeController.presentedViewController != nil {
                        homeController.dismiss(animated: false) {
                            homeController.selectedIndex = tabbarIndex
                        }
                    } else {
                        homeController.selectedIndex = tabbarIndex
                    }
                }
            }
            
//            NavUtil.topViewController()?.error("Get push notification \(state.rawValue)", okHandler: nil)
        }

        debugPrint(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}
