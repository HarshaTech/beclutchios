//
//  ForgotVerifyCodeViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class ForgotVerifyCodeViewController: UIViewController {

    @IBOutlet weak var txtError: UILabel!
    @IBOutlet weak var txtVerifyCode: UITextField!
    @IBOutlet weak var btBack: UIButton!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet var backgroundView: UIView!

    private var disposal = Disposal()
    var viewModel: ForgotVerifyCodeSubmitViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ForgotVerifyCodeSubmitViewModel()
        // Do any additional setup after loading the view.
        configUI()

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "ForgotPasswordVerifyCodeRes", handler: {
            res in
            // go to main home screen here
            let res = res?.object as! ForgotPasswordVerifyCodeRes
            self.viewModel?.isBusy.value = false
            if (res.result ?? false) == true {
                self.txtError.isHidden = true

                if let nav = self.navigationController as? ForgotNavController {
                    nav.verifyToken = res.msg

                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotConfirmPassViewController") as UIViewController
                    nav.pushViewController(viewController, animated: true)
                }
            } else {
                self.txtError.isHidden = false
            }
        })
    }

    func configUI() {
        btBack.makeGreenButton()
        btNext.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        txtVerifyCode.backgroundColor = .white
    }

    @IBAction func onClickNext(_ sender: Any) {
        if txtVerifyCode.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warnging", message: "Enter the verification we sent to your email", parent: self)
            return
        }

        viewModel?.doSubmitVerificationCode(val: self.txtVerifyCode.text!)
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
