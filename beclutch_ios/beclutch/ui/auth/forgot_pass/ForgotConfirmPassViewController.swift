//
//  ForgotConfirmPassViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import PopupDialog
class ForgotConfirmPassViewController: UIViewController {

    @IBOutlet weak var txtConfirm: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet weak var btBack: UIButton!
    @IBOutlet var backgroundView: UIView!

    let btnShowPass = UIButton(type: .system)
    let btnConfirmShowPass = UIButton(type: .system)
    private var disposal = Disposal()
    var viewModel: ForgotConfirmPassViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        viewModel = ForgotConfirmPassViewModel()
        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "ForgotConfirmRes", handler: {
            res in
            // go to main home screen here
            let res = res?.object as! ForgotConfirmRes
            self.viewModel?.isBusy.value = false
            if (res.result ?? false) == true {
                let popup = PopupDialog(title: "Info", message: "Password has been changed.  Please log in with new password.", image: nil)
                let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                    NavUtil.showLoginViewController()
                }
                popup.addButton(buttonTwo)
                self.present(popup, animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: res.msg!, parent: self)
            }
        })

    }

    func configUI() {
        btBack.makeGreenButton()
        btNext.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        btnShowPass.tintColor = .lightGray
        btnShowPass.setImage(UIImage(named: "openEye"), for: .normal)
        btnShowPass.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        btnShowPass.frame = CGRect(x: CGFloat(txtPass.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(15))
        btnShowPass.addTarget(self, action: #selector(self.showHidePasswordPressed), for: .touchUpInside)

        txtPass.rightView = btnShowPass
        txtPass.rightViewMode = .always

        btnConfirmShowPass.tintColor = .lightGray
        btnConfirmShowPass.setImage(UIImage(named: "openEye"), for: .normal)
        btnConfirmShowPass.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        btnConfirmShowPass.frame = CGRect(x: CGFloat(txtConfirm.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(15))
        btnConfirmShowPass.addTarget(self, action: #selector(self.showHideConfirmPasswordPressed), for: .touchUpInside)

        txtConfirm.rightView = btnConfirmShowPass
        txtConfirm.rightViewMode = .always

        txtPass.backgroundColor = .white
        txtConfirm.backgroundColor = .white
    }

    func checkValidation() -> Bool {
        if txtPass.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter new password", parent: self)
            return false
        }

        if txtConfirm.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter confirm password", parent: self)
            return false
        }

        if txtPass.text! != txtConfirm.text! {
            AlertUtils.showWarningAlert(title: "Warning", message: "Password is not matched", parent: self)
            return false
        }

        if txtPass.text!.count < 8 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter 8 characters at least", parent: self)
            return false
        }

        return true
    }

    // MARK: - Action
    @objc func showHidePasswordPressed() {
        txtPass.isSecureTextEntry = !txtPass.isSecureTextEntry

        let tintColor: UIColor = txtPass.isSecureTextEntry ? .lightGray : .black
        btnShowPass.tintColor = tintColor

        if txtPass.isSecureTextEntry {
            btnShowPass.setImage( UIImage(named: "openEye"), for: .normal)
        } else {
            btnShowPass.setImage( UIImage(named: "closeEye"), for: .normal)
        }

    }

    @objc func showHideConfirmPasswordPressed() {
        txtConfirm.isSecureTextEntry = !txtConfirm.isSecureTextEntry

        let tintColor: UIColor = txtConfirm.isSecureTextEntry ? .lightGray : .black
        btnConfirmShowPass.tintColor = tintColor

        if txtConfirm.isSecureTextEntry {
            btnConfirmShowPass.setImage( UIImage(named: "openEye"), for: .normal)
        } else {
            btnConfirmShowPass.setImage( UIImage(named: "closeEye"), for: .normal)
        }

    }

    @IBAction func onClickNext(_ sender: Any) {
        if self.checkValidation() {
            let nav = self.navigationController as! ForgotNavController
            viewModel?.confirmPass(pass: txtPass.text!, token: nav.verifyToken!)
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
