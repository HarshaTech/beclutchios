//
//  ForgotEmailViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import PopupDialog
import Firebase
import FirebaseMessaging

class ForgotEmailViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet weak var btBack: UIButton!
    @IBOutlet weak var btResend: UIButton!
    @IBOutlet var backgroundView: UIView!

    @IBOutlet weak var txtError: UILabel!
    var viewModel: ForgotEmailViewModel?
    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        let fcmToken = Messaging.messaging().fcmToken
        AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
        print("Fcm Token:" + fcmToken!)

        viewModel = ForgotEmailViewModel()

        let nav = self.navigationController as! ForgotNavController
        if nav.verifyToken == nil {
            self.viewModel?.confirmToken.value = ""
        } else {
            self.viewModel?.confirmToken.value = nav.verifyToken!
        }

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel?.confirmToken.observe {
            newConfirm, _ in
            if newConfirm.isEmpty {
                self.btResend.isHidden = true
            } else {
                self.btResend.isHidden = false
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "ForgotPasswordResetEmailVerifyResult", handler: {
            res in
            // go to main home screen here
            let res = res?.object as! ForgotPasswordResetEmailVerifyResult
            self.viewModel?.isBusy.value = false
            if (res.result ?? false) == true {
                self.txtError.isHidden = true

                let popup = PopupDialog(title: "Info", message: res.msg, image: nil)
                let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotVerifyCodeViewController") as UIViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                popup.addButton(buttonTwo)
                self.present(popup, animated: true, completion: nil)

            } else {
                self.txtError.text = res.msg
                self.txtError.isHidden = false
            }
        })
    }

    func configUI() {
        btBack.makeGreenButton()
        btNext.makeWhiteButton()
        btResend.makeStrokeButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        
        txtEmail.backgroundColor = .white
    }

    override func viewDidLayoutSubviews() {
        let nav = self.navigationController as! ForgotNavController
        self.viewModel!.confirmToken.value = nav.verifyToken ?? ""
    }

    func checkValidation() -> Bool {
        if txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter Email address", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: txtEmail.text!) {
            txtError.isHidden = false
            return false
        }

        return true
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickNext(_ sender: Any) {
        self.txtError.isHidden = true
        if checkValidation() {
            viewModel?.doSendVerificationEmail(email: txtEmail.text!)
        }
    }

    @IBAction func onClickResendCode(_ sender: Any) {
        self.txtError.isHidden = true
        if checkValidation() {
            viewModel?.doSendVerificationEmail(email: txtEmail.text!)
        }
    }
}
