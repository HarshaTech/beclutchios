//
//  RegisterNavigationController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging

class RegisterNavigationController: UINavigationController {

    var email: String = ""
    var userInfoReq: RegisterUserInfoReq?
    var isInvitedUser: Bool  = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let fcmToken = Messaging.messaging().fcmToken {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken)
            print("Fcm Token:" + fcmToken)
        }

        self.email = AppDataInstance.instance.getEmail()
        let curStep = PreferenceMgr.readInteger(keyName: PreferenceMgr.REGISTER_STEP)

        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        if curStep == RegisterConstants.REGISTER_SETP_START || curStep == RegisterConstants.REGISTER_COMPLETE || curStep == RegisterConstants.REGISTER_NOT_START_YET {
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserEmailRegisterViewController") as! UserEmailRegisterViewController
            onNavigate(next: viewController)
        } else {
            // do initial with cur position
            if curStep == RegisterConstants.REGISTER_EMAIL_CODE_VERIFY {
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserEmailVerificationViewController") as! UserEmailVerificationViewController
                onNavigate(next: viewController)
            } else if curStep == RegisterConstants.REGISTER_ROLE_CHOOSE {
                let chooseRoleStoryboard: UIStoryboard = UIStoryboard(name: "ChooseRole", bundle: nil)
                let viewController = chooseRoleStoryboard.instantiateViewController(withIdentifier: "ChooseRoleViewController") as! ChooseRoleViewController
                onNavigate(next: viewController)
            } else if curStep == RegisterConstants.REGISTER_CLUB_INFO {
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CreateClubTeamInRegisterViewController") as! CreateClubTeamInRegisterViewController
                viewController.isForTeam = false
                onNavigate(next: viewController)
            } else if curStep == RegisterConstants.REGISTER_TEAM_INFO {
                if let viewController = ViewControllerFactory.makeCreateTeamVC() {
                    onNavigate(next: viewController)
                }
            } else if curStep == RegisterConstants.REGISTER_INVITATION_CODE {
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "EnterInviteCodeViewController") as! EnterInviteCodeViewController
                onNavigate(next: viewController)
            } else if curStep == RegisterConstants.REGISTER_REG_USER_INFO {
                let userInfoStoryboard = UIStoryboard(name: "UserInfoRegister", bundle: nil)
                let viewController = userInfoStoryboard.instantiateViewController(withIdentifier: "UserInfoRegisterViewController") as! UserInfoRegisterViewController
                onNavigate(next: viewController)
            }
        }
    }

    func onNavigate(next: UIViewController) {
        self.setViewControllers([next], animated: true)
    }
}
