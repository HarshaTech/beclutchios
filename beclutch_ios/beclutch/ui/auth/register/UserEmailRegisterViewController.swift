//
//  UserEmailRegisterViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import Firebase
import FirebaseMessaging

class UserEmailRegisterViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBOutlet weak var btResend: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    private var disposal = Disposal()

    let passwordButton = UIButton(type: .system)
    let rePasswordButton = UIButton(type: .system)

    var viewModel: UserEmailRegisterViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        let fcmToken = Messaging.messaging().fcmToken
        if fcmToken != nil {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
            print("Fcm Token:" + fcmToken!)
        }

        viewModel = UserEmailRegisterViewModel()

        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_SETP_START)
        let nav = self.navigationController as! RegisterNavigationController
        self.txtEmail.text = nav.email
        
        let token = AppDataInstance.instance.getToken()
        if token != "" {
            viewModel?.confirmToken.value = token
        }

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel?.confirmToken.observe {
            newConfirm, _ in
            if !nav.isInvitedUser {
                self.btResend.isHidden = newConfirm.isEmpty
            } else {
                self.btResend.isHidden = true
            }
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "RegisterEmailRes", handler: {
            res in
            // go to main home screen here
            let regRes = res?.object as! RegisterEmailRes
            self.viewModel?.isBusy.value = false
            if (regRes.result ?? false) == true {
                AppDataInstance.instance.setEmail(val: self.txtEmail.text!)
                AppDataInstance.instance.setToken(token: regRes.msg!)
                self.viewModel?.confirmToken.value = regRes.msg!

                nav.email = self.txtEmail.text!

                if regRes.isInvited == "0" {
                    nav.isInvitedUser = false
                    AlertUtils.showWarningAlertWithHandler(title: "Verification Information",
                                                           message: "We have sent a verification email to your email address. You will need the Verification Code in that email to complete your registration.",
                                                           parent: self,
                                                           handler: {
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserEmailVerificationViewController") as! UserEmailVerificationViewController
                            nav.pushViewController(viewController, animated: false)
                        })
                } else {
                    nav.isInvitedUser = true
                    //move to user information page
                    let userInfoStoryboard: UIStoryboard = UIStoryboard(name: "UserInfoRegister", bundle: nil)
                    let viewController = userInfoStoryboard.instantiateViewController(withIdentifier: "UserInfoRegisterViewController") as! UserInfoRegisterViewController
                    nav.pushViewController(viewController, animated: false)
                }
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: regRes.msg!, parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "RegisterEmailResendRes", handler: {
            res in
            // go to main home screen here
            let regRes = res?.object as! RegisterEmailResendRes
            self.viewModel?.isBusy.value = false
            if (regRes.result ?? false) == true {
                AlertUtils.showWarningAlertWithHandler(title: "Success", message: "We have sent a verification email to your email address", parent: self,
                                                       handler: {
                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserEmailVerificationViewController") as! UserEmailVerificationViewController
                                                        nav.pushViewController(viewController, animated: false)
                })

            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "E-mail is invalid. Try again.", parent: self)
            }
        })
    }

    // MARK: - UI
    func configUI() {
        addShowPassToTextField(textField: txtPassword, button: passwordButton, action: #selector(showHidePasswordPressed))
        addShowPassToTextField(textField: txtConfirmPass, button: rePasswordButton, action: #selector(showHideRePasswordPressed))

        backBtn.makeGreenButton()
        btResend.makeStrokeButton()
        nextBtn.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        
        txtEmail.backgroundColor = .white
        txtPassword.backgroundColor = .white
        txtConfirmPass.backgroundColor = .white
    }

    func addShowPassToTextField(textField: UITextField, button: UIButton, action: Selector) {
        button.tintColor = .lightGray
        button.setImage(UIImage(named: "openEye"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        button.addTarget(self, action: action, for: .touchUpInside)

        textField.rightView = button
        textField.rightViewMode = .always
    }

    func checkValid() -> Bool {
        if txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email address is required", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: txtEmail.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invalid Email address entered", parent: self)
            return false
        }

        if txtPassword.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Password is required", parent: self)
            return false
        }

        if txtPassword.text!.count < 8 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Password should be 8 characters at least", parent: self)
            return false
        }

        if txtConfirmPass.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Confirm password required", parent: self)
            return false
        }

        if txtPassword.text! != txtConfirmPass.text! {
            AlertUtils.showWarningAlert(title: "Warning", message: "Password doesn't match", parent: self)
            return false
        }
        return true
    }

    // MARK: - Action
    @objc func showHidePasswordPressed() {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry

        let tintColor: UIColor = txtPassword.isSecureTextEntry ? .lightGray : .black
        passwordButton.tintColor = tintColor
    }

    @objc func showHideRePasswordPressed() {
        txtConfirmPass.isSecureTextEntry = !txtConfirmPass.isSecureTextEntry

        let tintColor: UIColor = txtConfirmPass.isSecureTextEntry ? .lightGray : .black
        rePasswordButton.tintColor = tintColor
    }

    @IBAction func onClickResend(_ sender: Any) {
        if self.txtEmail.text!.lowercased() == AppDataInstance.instance.getEmail().lowercased() {
            self.viewModel?.doResendEmail(email: txtEmail.text!, token: (viewModel?.confirmToken.value)!)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter original email address.", parent: self)
        }
    }

    @IBAction func onClickNext(_ sender: Any) {
        if checkValid() {
            let nav = self.navigationController as! RegisterNavigationController
            if nav.isInvitedUser {
                // go to user information enter page
                nav.isInvitedUser = false
                let userInfoStoryboard: UIStoryboard = UIStoryboard(name: "UserInfoRegister", bundle: nil)
                let viewController = userInfoStoryboard.instantiateViewController(withIdentifier: "UserInfoRegisterViewController") as! UserInfoRegisterViewController
                nav.pushViewController(viewController, animated: true)
            } else {
                viewModel?.doRegisterEmail(email: txtEmail.text!, pwd: txtPassword.text!, firebaseToken: "")
            }
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        NavUtil.showLoginViewController()
    }
}
