//
//  ChooseRoleViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class ChooseRoleViewController: UIViewController {

    @IBOutlet var backButton: UIButton!
    @IBOutlet var createClubBtn: UIButton!
    @IBOutlet var createTeamBtn: UIButton!
    @IBOutlet var acceptIntivationBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()

        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_ROLE_CHOOSE)
    }

    func configUI() {
        backButton.makeGreenButton()
        createClubBtn.makeStrokeButton()
        createTeamBtn.makeStrokeButton()
        acceptIntivationBtn.makeStrokeButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    @IBAction func onClickCreateClub(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CreateClubTeamInRegisterViewController") as! CreateClubTeamInRegisterViewController
        viewController.isForTeam = false
        self.show(viewController, sender: self)
    }

    @IBAction func onClickCreateTeam(_ sender: Any) {
        if let viewController = ViewControllerFactory.makeCreateTeamVC() {
            self.show(viewController, sender: self)
        }
    }

    @IBAction func onClickAcceptInvitation(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "EnterInviteCodeViewController") as! EnterInviteCodeViewController
        self.show(viewController, sender: self)
    }

    @IBAction func onClickBack(_ sender: Any) {
        if self.navigationController?.viewControllers.count == 1 {
            NavUtil.showLoginViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
