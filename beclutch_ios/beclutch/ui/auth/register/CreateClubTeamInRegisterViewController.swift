//
//  CreateClubTeamInRegisterViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

protocol ISportChooser {
    func onChooseSport(sport: Production)
    func onChooseSport(sport: Production, type: ScreenSelectOptionType)
}

extension ISportChooser {
    func onChooseSport(sport: Production) {
        
    }
    
    func onChooseSport(sport: Production, type: ScreenSelectOptionType) {
        
    }
}

class CreateClubTeamInRegisterViewController: UIViewController, ISportChooser {
    @IBOutlet weak var btSport: UIButton!
    private var disposal = Disposal()
    @IBOutlet weak var txtPageTitle: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    var isForTeam: Bool = true
    var viewModel: CreateClubTeamInRegisterViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        if isForTeam {
            PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_TEAM_INFO)
        } else {
            PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_CLUB_INFO)
        }

        viewModel = CreateClubTeamInRegisterViewModel()

        if self.isForTeam {
            txtPageTitle.text = "Team Information"
        } else {
            txtPageTitle.text = "Club Information"
        }

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel?.selectSportItem.observe {
            newSport, _ in

            if newSport.ID == "" {
                self.btSport.setTitle("Choose Sport", for: .normal)
            } else {
                self.btSport.setTitle(newSport.NAME, for: .normal)
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateClubTeamRes", handler: {
            res in
            let regRes = res?.object as! CreateClubTeamRes
            self.viewModel!.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.setToken(token: regRes.msg)
                AppDataInstance.instance.setEmail(val: regRes.userInfo.EMAIL)
                AppDataInstance.instance.me = regRes.userInfo
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: regRes.clubDtos)
                AppDataInstance.instance.currentSelectedTeam = regRes.defaultLevel
                AppDataInstance.instance.roles = regRes.roles
                
                PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_COMPLETE)
                NavUtil.showHomeViewController()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to create " + (self.isForTeam ? "club" : "team") + ". Try again.", parent: self)
            }
        })
    }

    func configUI() {
        btSport.makeStrokeButton()
        backBtn.makeGreenButton()
        nextBtn.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    @IBAction func onClickSport(_ sender: Any) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = self.viewModel?.selectSportItem.value

        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseSport(sport: Production) {
        self.viewModel?.selectSportItem.value = sport
    }

    func checkValidation() -> Bool {
        if self.viewModel?.selectSportItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose sport", parent: self)
            return false
        }

        if self.txtName.text!.isEmpty {
            if self.isForTeam {
                AlertUtils.showWarningAlert(title: "Warning", message: "Enter the name of team", parent: self)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: "Enter the name of club", parent: self)
            }

            return false
        }

        return true
    }

    @IBAction func onClickNext(_ sender: Any) {
        if self.checkValidation() {
            viewModel?.doCreateClubTeam(name: self.txtName!.text!, isClubCreate: isForTeam ? "1" : "0")
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        if self.navigationController?.viewControllers.count == 1 {
            NavUtil.showLoginViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
