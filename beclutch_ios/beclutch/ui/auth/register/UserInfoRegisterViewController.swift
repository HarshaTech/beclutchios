//
//  UserInfoRegisterViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import SwiftEventBus
import Observable
import InputMask
protocol IStateChooser {
    func onChooseState(state: String, idx: Int)
}

 class UserInfoRegisterViewController: UIViewController, IStateChooser {

    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtAddr: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var btState: UIButton!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    private var disposal = Disposal()
    var viewModel = UserInfoViewModel()

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = txtPhone.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        txtPhone.text = output
    }

    override  func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.addTarget(self, action: #selector(UserInfoRegisterViewController.textFieldDidChange(_:)), for: .editingChanged)
        configUI()
        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_REG_USER_INFO)
        btState.layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        btState.layer.borderWidth = 1
        btState.layer.cornerRadius = 5
        btState.setTitle("Choose State", for: .normal)

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "RegisterUserInfoRes", handler: {
            res in
            let regRes = res?.object as! RegisterUserInfoRes
            self.viewModel.isBusy.value = false
            if (regRes.result ?? false) == true {
                let chooseRoleStoryboard: UIStoryboard = UIStoryboard(name: "ChooseRole", bundle: nil)
                let viewController = chooseRoleStoryboard.instantiateViewController(withIdentifier: "ChooseRoleViewController") as! ChooseRoleViewController
                self.navigationController?.pushViewController(viewController, animated: false)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save user information. Try again.", parent: self)
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        hideIndicator()
    }
    
    func configUI() {
        backBtn.makeGreenButton()
        nextBtn.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        
        txtFName.backgroundColor = .white
        txtLName.backgroundColor = .white
        txtPhone.backgroundColor = .white
        txtAddr.backgroundColor = .white
        txtCity.backgroundColor = .white
        txtZip.backgroundColor = .white
    }

    @IBAction func onClickStateButton(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ChooseStateViewController") as! ChooseStateViewController
        viewController.parentListener = self
        viewController.selectionString = self.viewModel.selectedState
        self.present(viewController, animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if txtFName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required", parent: self)
            return false
        }

        if txtLName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required", parent: self)
            return false
        }

        if txtPhone.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Phone Number is required", parent: self)
            return false
        }

        return true
    }

    func onChooseState(state: String, idx: Int) {
        self.viewModel.selectedState = state

        btState.setTitle(state, for: .normal)
    }

    @IBAction func onClickNext(_ sender: Any) {
        if checkValidation() {
            self.viewModel.doSubmitUserInformation(fname: txtFName.text!, lname: txtLName.text!, phone: txtPhone.text!, addr: txtAddr.text!, city: txtCity.text!, state: viewModel.selectedState, zip: txtZip.text!)
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        if self.navigationController?.viewControllers.count == 1 {
            NavUtil.showLoginViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
