//
//  UserEmailVerificationViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class UserEmailVerificationViewController: UIViewController {

    private var disposal = Disposal()
    var viewModel: RegisterVerificationCodeViewModel?
    @IBOutlet weak var txtVerifyCode: UITextField!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()

        self.viewModel = RegisterVerificationCodeViewModel()
        let nav = self.navigationController as! RegisterNavigationController
        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_EMAIL_CODE_VERIFY)
//        if AppDataInstance.instance.getToken().isEmpty {
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserEmailRegisterViewController") as! UserEmailRegisterViewController
//            nav.setViewControllers([viewController], animated: true)
//        }

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)
        SwiftEventBus.onMainThread(self, name: "RegisterSubmitRes", handler: {
            res in

            let regRes = res?.object as! RegisterSubmitRes
            self.viewModel?.isBusy.value = false
            if (regRes.result ?? false) == true {
                AlertUtils.showWarningAlertWithHandler(title: "Success", message: regRes.msg!, parent: self, handler: {
                    AppDataInstance.instance.setToken(token: regRes.token)
                    
                    let userInfoStoryboard: UIStoryboard = UIStoryboard(name: "UserInfoRegister", bundle: nil)
                    let viewController = userInfoStoryboard.instantiateViewController(withIdentifier: "UserInfoRegisterViewController") as! UserInfoRegisterViewController
                    nav.pushViewController(viewController, animated: false)
                })
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: regRes.msg!, parent: self)
            }
        })
    }

    func configUI() {
        backBtn.makeGreenButton()
        nextBtn.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        
        txtVerifyCode.backgroundColor = .white
    }

    @IBAction func onClickNext(_ sender: Any) {
        if txtVerifyCode.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter verification code", parent: self)
            return
        }

        self.viewModel?.doSubmitVerificationCode(code: self.txtVerifyCode.text!)
    }

    @IBAction func onClickBack(_ sender: Any) {
        if self.navigationController?.viewControllers.count == 1 {
            NavUtil.showLoginViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
