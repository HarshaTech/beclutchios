//
//  EnterInviteCodeViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/14.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class EnterInviteCodeViewController: UIViewController {

    @IBOutlet weak var txtInviteCode: UITextField!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var doneBtn: UIButton!
    @IBOutlet var backgroundView: UIView!

    var viewModel: RegisterAcceptInvitationViewModel?
    var disposal = Disposal()
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_INVITATION_CODE)
        self.viewModel = RegisterAcceptInvitationViewModel()

        self.viewModel?.isBusy.observe {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "InviteAccpetRes", handler: {
            res in
            let regRes = res?.object as! InviteAccpetRes
            self.viewModel!.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.setToken(token: regRes.msg)
                AppDataInstance.instance.setEmail(val: regRes.userInfo.EMAIL)
                AppDataInstance.instance.me = regRes.userInfo
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: regRes.clubDtos)
                AppDataInstance.instance.currentSelectedTeam = regRes.defaultLevel
                AppDataInstance.instance.roles = regRes.roles
                PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_COMPLETE)

                NavUtil.showHomeViewController()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to accept invitation", parent: self)
            }
        })
    }

    func configUI() {
        backBtn.makeGreenButton()
        doneBtn.makeWhiteButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    @IBAction func onClickNext(_ sender: Any) {
        if txtInviteCode.text!.count < 6 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invitation Code is Invalid. Make sure it is your Team Invitation Code not your Verification Code", parent: self)
            return
        }

        self.viewModel!.doSubmitInviteCode(inviteCode: txtInviteCode.text!)
    }

    @IBAction func onClickBack(_ sender: Any) {
        if self.navigationController?.viewControllers.count == 1 {
            NavUtil.showLoginViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
