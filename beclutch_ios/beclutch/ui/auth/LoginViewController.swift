//
//  LoginViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import Firebase
import FirebaseMessaging
import InputMask
import MessageUI

class LoginViewController: BaseViewController {

    @IBOutlet weak var txtError: UILabel!
    @IBOutlet weak var btRegister: UIButton!
    @IBOutlet weak var btLogin: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet var backgroundView: UIView!

    @IBOutlet weak var privacyLabel: UILabel!

    let button = UIButton(type: .system)

    var viewModel: LoginViewModel? = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        let fcmToken = Messaging.messaging().fcmToken
        if fcmToken != nil {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
            print("Fcm Token:" + fcmToken!)
        }

        btRegister.makeRadius()
        btLogin.makeRadius()
        // Do any additional setup after loading the view.
        self.viewModel?.isBusy.observe { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoginRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            // go to main home screen here
            let loginRes = res?.object as! LoginRes
            self.viewModel?.isBusy.value = false
            if (loginRes.result ?? false) == true {
                if !PreferenceMgr.keyAlreadyExist(key: PreferenceMgr.IS_AUTO_LOGIN) {
                    PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: true)
                }

                AppDataInstance.instance.setToken(token: loginRes.msg!)
                AppDataInstance.instance.setEmail(val: loginRes.user!.EMAIL)
                AppDataInstance.instance.me = loginRes.user
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: loginRes.clubs)
                AppDataInstance.instance.currentSelectedTeam = loginRes.default_member

                CustomLabelManager.shared.updateLabelDict(dict: loginRes.labels)

                if let colorValue = loginRes.default_member?.CLUB_TEAM_INFO.COLOR_VALUE {
                    PreferenceMgr.saveTheme(themeValue: colorValue)
                }

                AppDataInstance.instance.roles = loginRes.roles

                NavUtil.showHomeViewController()
            } else {
                if let isActive = loginRes.isActive, isActive == false {
                    PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_EMAIL_CODE_VERIFY)
                    NavUtil.showRegisterViewController()
                } else {
                    self.txtError.text = loginRes.msg
                    self.txtError.isHidden = false
                    AppDataInstance.instance.logoutAndResetDatas()
                }
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NavUtil.intendedScreenType = nil
    }

    // MARK: - UI
    func configUI() {
        button.tintColor = .lightGray
        button.setImage(UIImage(named: "openEye"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(txtPassword.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(15))
        button.addTarget(self, action: #selector(self.showHidePasswordPressed), for: .touchUpInside)

        txtPassword.rightView = button
        txtPassword.rightViewMode = .always
        
        txtEmail.backgroundColor = .white
        txtPassword.backgroundColor = .white

        btLogin.makeGreenButton()
        btRegister.makeGreenButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

//        let tofText = "Terms of Use"
//        let privacyText = "Privacy Policy"
//        let fullPrivacyText = "By continuing, you agree to the \nBeClutch " + tofText + " and " + privacyText
//        let attributedText = NSMutableAttributedString(string: fullPrivacyText)
//
//        let tofRange = fullPrivacyText.range(of: tofText)
//        let privacyRange = fullPrivacyText.range(of: fullPrivacyText)
//
//        attributedText.addAttribute(NSAttributedString.Key.underlineColor, value: NSUnderlineStyle.single.rawValue, range: tofRange)
//        NSMutableAttributedString().bold

        let gesture = UITapGestureRecognizer(target: self, action: #selector(privacyTapped))
        privacyLabel.isUserInteractionEnabled = true
        privacyLabel.addGestureRecognizer(gesture)
    }

    // MARK: - Action
    @objc func showHidePasswordPressed() {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry

        let tintColor: UIColor = txtPassword.isSecureTextEntry ? .lightGray : .black
        button.tintColor = tintColor

        if txtPassword.isSecureTextEntry {
            button.setImage( UIImage(named: "openEye"), for: .normal)
        } else {
            button.setImage( UIImage(named: "closeEye"), for: .normal)
        }

    }

    @IBAction func privacyTapped(gesture: UITapGestureRecognizer) {
        if gesture.didTapAttributedTextInLabel(label: privacyLabel, inRange: NSRange(location: 8, length: 15)) {
            UIApplication.shared.open(URL(string: "https://beclutch.club/termsofuse.html")!, options: [:], completionHandler: nil)
        } else if gesture.didTapAttributedTextInLabel(label: privacyLabel, inRange: NSRange(location: 28, length: 15)) {
            UIApplication.shared.open(URL(string: "https://beclutch.club/privacy.html")!, options: [:], completionHandler: nil)
        }
    }

    @IBAction func onRegister(_ sender: Any) {
//        PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_SETP_START)

        NavUtil.showRegisterViewController()
    }

    @IBAction func doLogin(_ sender: Any) {
        if checkValidation() {
            self.viewModel!.doLogin(email: txtEmail.text!, pwd: txtPassword.text!)
        }
    }

    @IBAction func onForgotPassword(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ForgotNavController") as! UINavigationController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    @IBAction func supportAction(_ sender: Any) {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients(["support@beclutch.club"])
//        mailVC.setSubject("Subject for email")
//        mailVC.setMessageBody("Email message string", isHTML: false)

        present(mailVC, animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter E-mail address", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: txtEmail.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "E-mail address is invalid.", parent: self)
            return false
        }

        if txtPassword.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter the password", parent: self)
            return false
        }

        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension LoginViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let error = error {
            alert(message: error.localizedDescription)
        } else {
            controller.dismiss(animated: true, completion: nil)
        }
    }
}
