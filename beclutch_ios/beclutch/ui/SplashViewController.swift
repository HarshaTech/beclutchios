//
//  ViewController.swift
//  beclutch
//
//  Created by zeus on 2019/11/13.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import SwiftEventBus
import Firebase
import FirebaseMessaging

class SplashViewController: UIViewController {

    var viewModel = SplashViewModel()
    @IBOutlet var backgroundView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        let fcmToken = Messaging.messaging().fcmToken
        if fcmToken != nil {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
            print("Fcm Token:" + fcmToken!)
        }

        SwiftEventBus.onMainThread(self, name: "SplashCompleteEvent", handler: {
            _ in
                //go to log in screen here
            NavUtil.showLoginViewController()
        })

        SwiftEventBus.onMainThread(self, name: "LoginRes", handler: {
            res in
                // go to main home screen here

            let loginRes = res?.object as! LoginRes
            if loginRes.result == false {
                NavUtil.showLoginViewController()
//                 AppDataInstance.instance.logoutAndResetDatas()
            } else {
                if loginRes.default_member == nil || (loginRes.default_member?.ID.isEmpty)! {
                    loginRes.default_member = loginRes.clubs?.first
                    PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: true)
                }

                AppDataInstance.instance.setToken(token: loginRes.msg!)
                AppDataInstance.instance.setEmail(val: loginRes.user!.EMAIL)
                AppDataInstance.instance.me = loginRes.user
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: loginRes.clubs)
                AppDataInstance.instance.currentSelectedTeam = loginRes.default_member
                AppDataInstance.instance.roles = loginRes.roles

                CustomLabelManager.shared.updateLabelDict(dict: loginRes.labels)

                if let color = loginRes.default_member?.CLUB_TEAM_INFO.COLOR_VALUE {
                    PreferenceMgr.saveTheme(themeValue: color)
                }

                if PreferenceMgr.readBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN) {
                    NavUtil.showHomeViewController()
                } else {
                    NavUtil.showLoginViewController()
                }
            }
        })
        if !PreferenceMgr.readBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN) {
//            PreferenceMgr.saveString(keyName: PreferenceMgr.TOKEN, stringValue: "")
//            PreferenceMgr.saveString(keyName: PreferenceMgr.FIREBASE_TOKEN, stringValue: "")
        }
        
        viewModel.doSplashAction()
    }

    override func viewWillDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self)
    }
}
