//
//  NotificationViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import Observable
import SwiftEventBus
class NotificationViewController: UIViewController {

    @IBOutlet weak var chkSchduleApp: BEMCheckBox!
    @IBOutlet weak var chkScheduleMail: BEMCheckBox!
    @IBOutlet weak var chkChatApp: BEMCheckBox!
    @IBOutlet weak var chkChatMail: BEMCheckBox!
//    @IBOutlet weak var chkScoreApp: BEMCheckBox!
//    @IBOutlet weak var chkScoreMail: BEMCheckBox!
    @IBOutlet weak var chkReminderApp: BEMCheckBox!
    @IBOutlet weak var chkReminderMail: BEMCheckBox!
    @IBOutlet weak var chkTourApp: BEMCheckBox!
    @IBOutlet weak var chkTourMail: BEMCheckBox!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var txtSchedule: UILabel!
    @IBOutlet weak var txtChat: UILabel!
    @IBOutlet weak var txtReminder: UILabel!
    @IBOutlet weak var txtTournament: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    var viewModel: NotificationViewModel = NotificationViewModel()
    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()

        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        txtSchedule.layer.borderWidth = 1
        txtSchedule.layer.borderColor = UIColor.COLOR_PRIMARY_DARK.cgColor
        txtChat.layer.borderWidth = 1
        txtChat.layer.borderColor = UIColor.COLOR_PRIMARY_DARK.cgColor
        txtReminder.layer.borderWidth = 1
        txtReminder.layer.borderColor = UIColor.COLOR_PRIMARY_DARK.cgColor
        txtTournament.layer.borderWidth = 1
        txtTournament.layer.borderColor = UIColor.COLOR_PRIMARY_DARK.cgColor

        self.chkSchduleApp.boxType = .square
        self.chkScheduleMail.boxType = .square
        self.chkChatApp.boxType = .square
        self.chkChatMail.boxType = .square
//        self.chkScoreApp.boxType = .square
//        self.chkScoreMail.boxType = .square
        self.chkReminderApp.boxType = .square
        self.chkReminderMail.boxType = .square
        self.chkTourApp.boxType = .square
        self.chkTourMail.boxType = .square

        self.chkChatMail.isEnabled = false

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "NotificationLoadRes", handler: {
            res in
            let resAPI = res?.object as! NotificationLoadRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                self.chkSchduleApp.setOn(resAPI.setting?.schApp == "1", animated: true)
                self.chkScheduleMail.setOn(resAPI.setting?.schMail == "1", animated: true)
                self.chkChatApp.setOn(resAPI.setting?.chatApp == "1", animated: true)
                self.chkChatMail.setOn(resAPI.setting?.chatMail == "1", animated: true)
                self.chkReminderApp.setOn(resAPI.setting?.reminderApp == "1", animated: true)
                self.chkReminderMail.setOn(resAPI.setting?.reminderMail == "1", animated: true)
                self.chkTourApp.setOn(resAPI.setting?.tourApp == "1", animated: true)
                self.chkTourMail.setOn(resAPI.setting?.tourMail == "1", animated: true)
//                self.chkScoreApp.setOn(resAPI.setting?.scoreApp == "1", animated: true)
//                self.chkScoreMail.setOn(resAPI.setting?.scoreMail == "1", animated: true)
                self.viewModel.originSettingId = resAPI.setting?.id ?? "0"
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save notification setting. Try again.", parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "NotificationUpdateRes", handler: {
            res in
            let resAPI = res?.object as! NotificationUpdateRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                AlertUtils.showWarningAlertWithHandler(title: "Success", message: "Notification settings have been updated", parent: self, handler: {
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save notification setting. Try again.", parent: self)
            }
        })

        self.viewModel.doLoadNotificationSetting()
    }

    @IBAction func onClickSave(_ sender: Any) {
        self.viewModel.doSaveNotificationSetting(sch_app: self.chkSchduleApp.on ? "1":"0", sch_mail: self.chkScheduleMail.on ? "1":"0", chat_app: self.chkChatApp.on ? "1" : "0", chat_mail: self.chkChatMail.on ? "1" : "0", score_app: "0", score_mail: "0", reminder_app: self.chkReminderApp.on ? "1" : "0", reminder_mail: self.chkReminderMail.on ? "1" : "0", tour_app: self.chkTourApp.on ? "1" : "0", tour_mail: self.chkTourMail.on ? "1" : "0")
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
