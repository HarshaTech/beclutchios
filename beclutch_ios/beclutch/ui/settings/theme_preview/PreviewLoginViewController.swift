//
//  PreviewLoginViewController.swift
//  beclutch
//
//  Created by FAT_MAC on 8/4/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class PreviewLoginViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    var selectedTheme: ThemeColorModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = selectedTheme.colorResourceDark
        loginBtn.makeGreenButton()
        loginBtn.backgroundColor = selectedTheme.colorResourcePrimary
        registerBtn.makeGreenButton()
        registerBtn.backgroundColor = selectedTheme.colorResourcePrimary
    }

}
