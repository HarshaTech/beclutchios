//
//  ThemePreviewController.swift
//  beclutch
//
//  Created by FAT_MAC on 8/3/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import PageMaster

class ThemePreviewController: UIViewController {

    var selectedTheme: ThemeColorModel!
    private let pageMaster = PageMaster([])
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageFrameView: UIView! {
        willSet {
            self.addChild(self.pageMaster)
            newValue.addSubview(self.pageMaster.view)
            self.pageMaster.didMove(toParent: self)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageViewController()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        footerView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        pageControl.addTarget(self, action: #selector(self.pageControlSelectionAction(_:)), for: .touchUpInside)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        self.pageMaster.view.frame = pageFrameView.bounds
    }

    @IBAction func onBackBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func pageControlSelectionAction(_ sender: UIPageControl) {
        //move page to wanted page
        let page: Int? = sender.currentPage
        self.pageMaster.setPage(page!)
    }

}

extension ThemePreviewController {
    private func setupPageViewController() {
        self.pageMaster.pageDelegate = self

        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let pre_slpashView = mainStoryboard.instantiateViewController(withIdentifier: "PreviewSplashViewController") as! PreviewSplashViewController
        let pre_loginView = mainStoryboard.instantiateViewController(withIdentifier: "PreviewLoginViewController") as! PreviewLoginViewController
        pre_slpashView.selectedTheme = self.selectedTheme
        pre_loginView.selectedTheme = self.selectedTheme

        let vcList = [pre_slpashView, pre_loginView]

        self.pageControl.numberOfPages = vcList.count
        self.pageMaster.setup(vcList)
    }

    private func generateViewController(color: UIColor) -> UIViewController {
        let vc = UIViewController()
        vc.view.backgroundColor = color
        return vc
    }
}
// MARK: - PageMasterDelegate
extension ThemePreviewController: PageMasterDelegate {

    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        self.pageControl.currentPage = page
    }
}
