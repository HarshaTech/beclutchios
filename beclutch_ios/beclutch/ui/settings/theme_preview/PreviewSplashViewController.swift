//
//  PreviewSplashViewController.swift
//  beclutch
//
//  Created by FAT_MAC on 8/4/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class PreviewSplashViewController: UIViewController {

    var selectedTheme: ThemeColorModel!
    @IBOutlet weak var backgroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = selectedTheme.colorResourceDark
        // Do any additional setup after loading the view.
    }

}
