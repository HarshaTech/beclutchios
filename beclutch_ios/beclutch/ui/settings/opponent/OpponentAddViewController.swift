//
//  OpponentAddViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import InputMask
class OpponentAddViewController: UIViewController {

    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btSave: UIButton!
    @IBOutlet weak var txtOpponentName: UITextField!
    @IBOutlet weak var txtPageTitle: UILabel!
    @IBOutlet weak var txtContactName: UITextField!
    @IBOutlet weak var txtContactPhone: UITextField!
    @IBOutlet weak var txtContactEmail: UITextField!
    @IBOutlet weak var txtNote: UITextField!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var backBtn: UIButton!

    private var disposal = Disposal()
    public var isEidt: Bool = false
    public var isShowing: Bool = false
    var viewModel: OpponentAddViewModel = OpponentAddViewModel()

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = textField.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        textField.text = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtContactPhone.addTarget(self, action: #selector(OpponentAddViewController.textFieldDidChange(_:)), for: .editingChanged)
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btSave.makeGreenButton()
        btCancel.makeRedButton()

        if isEidt {
            txtPageTitle.text = "Detail of Opponent"

            txtOpponentName.text = self.viewModel.originOppo?.OPPONENT_NAME
            txtContactName.text = self.viewModel.originOppo?.CONTACT_NAME
            txtContactEmail.text = self.viewModel.originOppo?.CONTACT_EMAIL
            txtContactPhone.text = self.viewModel.originOppo?.CONTACT_PHONE
            txtNote.text = self.viewModel.originOppo?.CONTACT_NOTE
            if self.isShowing {
                self.txtNote.isEnabled = false
                self.txtContactPhone.isEnabled = false
                self.txtContactEmail.isEnabled = false
                self.txtContactName.isEnabled = false
                self.txtOpponentName.isEnabled = false

                self.btSave.isHidden = true
                self.btCancel.setTitle("Back", for: .normal)
            }
        } else {
            txtPageTitle.text = "Add New Opponent"
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "AddOpponentRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! AddOpponentRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save opponent information. Try again.", parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateOpponentRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! UpdateOpponentRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save opponent information. Try again.", parent: self)
            }
        })
    }

    func checkValidation() -> Bool {
        if txtOpponentName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Opponent name is required", parent: self)
            return false
        }

        if !txtContactEmail.text!.isEmpty {
            if !StringCheckUtils.checkValidEmailAddress(email: txtContactEmail.text!) {
                AlertUtils.showWarningAlert(title: "Warning", message: "Enter the valid email address", parent: self)
                return false
            }
        }
        return true
    }

    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {

            if self.isEidt {
                self.viewModel.doEditOpponent(name: txtOpponentName.text!, contactName: txtContactName.text!, phone: txtContactPhone.text!, email: txtContactEmail.text!, note: txtNote.text!)
            } else {
                self.viewModel.doAddOpponent(name: txtOpponentName.text!, contactName: txtContactName.text!, phone: txtContactPhone.text!, email: txtContactEmail.text!, note: txtNote.text!)
            }
        }
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickBAck(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
