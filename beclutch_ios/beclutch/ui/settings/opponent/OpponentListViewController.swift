//
//  OpponentListViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

protocol IOpponentChooser: class {
    func chooseOpponent(val: OpponentModel)
}

class OpponentListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var viewModel: OpponentListViewModel = OpponentListViewModel()
    weak var parentDelegate: IOpponentChooser?

    @IBOutlet weak var btAddRound: UIButton!
    @IBOutlet weak var txtToolbarTitle: UILabel!
    @IBOutlet weak var tb: UITableView!
    private var disposal = Disposal()

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataProvider.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OpponentListCell") as! OpponentListCell
        cell.imgOppo.layer.cornerRadius = cell.imgOppo.frame.width / 2

        cell.txtOppoName.text = self.viewModel.dataProvider[indexPath.row].OPPONENT_NAME
        cell.txtOppoContact.text = self.viewModel.dataProvider[indexPath.row].CONTACT_NAME!.isEmpty ? "No Contact" : self.viewModel.dataProvider[indexPath.row].CONTACT_NAME

        return cell
    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btAddRound.backgroundColor = UIColor.COLOR_ACCENT
        btAddRound.layer.cornerRadius = btAddRound.frame.width / 2
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "OpponentLoadRes", handler: {
            res in
            let regRes = res?.object as! OpponentLoadRes
            self.viewModel.isBusy.value = false
            if (regRes.result ?? false) == true {
                self.viewModel.dataProvider = regRes.opponentList!
                self.tb.reloadData()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to get opponent list. Try again.", parent: self)
            }
        })
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewModel.isForChoose == false {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextCont = mainStoryboard.instantiateViewController(withIdentifier: "OpponentAddViewController") as! OpponentAddViewController
            nextCont.modalPresentationStyle = .fullScreen
            nextCont.isEidt = true
            nextCont.viewModel.originOppo = self.viewModel.dataProvider[indexPath.row]
            self.present(nextCont, animated: true, completion: nil)
        } else {
            self.parentDelegate?.chooseOpponent(val: self.viewModel.dataProvider[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.doLoadOpponentList()
    }

    @IBAction func onClickAddToolbar(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextCont = mainStoryboard.instantiateViewController(withIdentifier: "OpponentAddViewController") as! OpponentAddViewController
        nextCont.isEidt = false
        nextCont.modalPresentationStyle = .fullScreen
        self.present(nextCont, animated: true, completion: nil)
    }
}
