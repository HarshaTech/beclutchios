//
//  OpponentListCell.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class OpponentListCell: UITableViewCell {

    @IBOutlet weak var imgOppo: UIImageView!
    @IBOutlet weak var txtOppoName: UILabel!
    @IBOutlet weak var txtOppoContact: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
