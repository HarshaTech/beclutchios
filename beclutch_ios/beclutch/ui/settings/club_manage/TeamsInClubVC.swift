//
//  TeamsInClubVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class TeamsInClubVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var txtToobarTitle: UILabel!
    @IBOutlet weak var tb: UITableView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var addBtn: UIButton!

    private var disposal = Disposal()
    var viewModel: TeamsInClubViewModel = TeamsInClubViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBtn.backgroundColor = UIColor.COLOR_ACCENT
        self.backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        self.appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.txtToobarTitle.text = "Manage Teams in \(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME ?? "Club")"
        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadTeamsInClubRes", handler: { [weak self]
            res in
            let regRes = res?.object as! LoadTeamsInClubRes
            self?.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                self?.viewModel.dp = regRes.teams
                self?.tb.reloadData()
            }
        })

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.viewModel.loadTeamsInClub()
    }

    @IBAction func onClickToobarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickAddMoreTeam(_ sender: Any) {
        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //sheet.view.tintColor = .primary

        let action2 = UIAlertAction(title: "Create Team in the Club", style: .default) { _ in
            let storyboard = UIStoryboard(name: "CreateNewMyTeam", bundle: nil)
            let navNext = storyboard.instantiateViewController(withIdentifier: "CreateNewMyTeamViewController") as! CreateNewMyTeamViewController
            navNext.viewModel.isAddTeamToClub = true
            navNext.viewModel.originClubId = (AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ID)!
            navNext.viewModel.selectSportItem.accept(AppDataInstance.instance.currentSelectedTeam!.SPORT_INFO)
            self.present(navNext, animated: true, completion: nil)
        }
        action2.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(action2)

        let action3 = UIAlertAction(title: "Invite Team to the Club", style: .default) { _ in
            let navNext = self.storyboard?.instantiateViewController(withIdentifier: "CreateInviteTeamManagerVC") as! CreateInviteTeamManagerVC
            navNext.viewModel.clubInfo = AppDataInstance.instance.currentSelectedTeam
            self.present(navNext, animated: true, completion: nil)
        }
        action3.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(action3)

        let action4 = UIAlertAction(title: "Add Existing Team to the Club", style: .default) { _ in
            let navNext = self.storyboard?.instantiateViewController(withIdentifier: "AddMyTeamToClubVC") as! AddMyTeamToClubVC
            navNext.viewModel.originClubSportId = AppDataInstance.instance.currentSelectedTeam!.SPORT_INFO.ID
            navNext.viewModel.originClubId = AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.ID
            self.present(navNext, animated: true, completion: nil)
        }
        action4.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(action4)

        let action1 = UIAlertAction(title: "Cancel", style: .destructive) { _ in

        }
        action1.setValue(UIColor.COLOR_ACCENT, forKey: "titleTextColor")
        sheet.addAction(action1)

        self.present(sheet, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamInClubCell") as! TeamInClubCell

        if self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL.isEmpty {
            cell.img.image = UIImage(named: "logo")
        } else {
            cell.img.kf.setImage(with: URL(string: self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL))
        }

        cell.txtTeamName.text = self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.NAME
        cell.txtSport.text = "Coach: " + self.viewModel.dp![indexPath.row].USER_INFO.FNAME + " " + self.viewModel.dp![indexPath.row].USER_INFO.LNAME

        if self.viewModel.dp![indexPath.row].IS_ACTIVE == "1" {
            cell.txtStatus.isHidden = true
        } else {
            cell.txtStatus.isHidden = false
            cell.txtStatus.text = "Status: Pending"
        }
        cell.removeBtn.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        cell.removeBtn.layer.cornerRadius = cell.removeBtn.width/2
        cell.actionRemove = {
            //do nothing yet.
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}
