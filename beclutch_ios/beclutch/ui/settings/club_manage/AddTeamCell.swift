//
//  AddTeamCell.swift
//  beclutch
//
//  Created by zeus on 2019/12/27.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox

class AddTeamCell: UITableViewCell {

    @IBOutlet weak var txtSportName: UILabel!
    @IBOutlet weak var txtRoleName: UILabel!
    @IBOutlet weak var txtTeamName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var chk: BEMCheckBox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onChoose(_ sender: BEMCheckBox) {
    }
}
