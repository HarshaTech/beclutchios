//
//  AddMyTeamToClubVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/27.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class AddMyTeamToClubVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tb: UITableView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var addBtn: UIButton!

    var viewModel: AddTeamToClubViewModel = AddTeamToClubViewModel()
    private var disposal = Disposal()
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        addBtn.makeGreenButton()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadMyClubRes", handler: {
            res in
            let regRes = res?.object as! LoadMyClubRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                self.viewModel.dp = regRes.clubTree
                self.viewModel.chooseStatus = []
                if regRes.clubTree!.count > 0 {
                    for _ in 0...(regRes.clubTree!.count - 1) {
                        self.viewModel.chooseStatus?.append(false)
                    }
                }

                self.tb.reloadData()
            }
        })

    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.loadIndependentTeam()
    }

    @IBAction func onClickAddTeam(_ sender: Any) {

    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTeamCell") as! AddTeamCell
        cell.chk.boxType = .square
        cell.chk.on = self.viewModel.chooseStatus![indexPath.row]
        if self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL.isEmpty {
            cell.img.image = UIImage(named: "logo")
        } else {
            cell.img.kf.setImage(with: URL(string: self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL))
        }
        cell.txtRoleName.text = self.viewModel.dp![indexPath.row].ROLE_INFO.ROLE_NAME
        cell.txtSportName.text = self.viewModel.dp![indexPath.row].SPORT_INFO.NAME

        return cell
    }
}
