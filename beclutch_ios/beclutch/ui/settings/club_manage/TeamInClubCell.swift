//
//  TeamInClubCell.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class TeamInClubCell: UITableViewCell {

    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtTeamName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var removeBtn: UIButton!

    var actionRemove: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClickRemove(_ sender: Any) {
        self.actionRemove!()
    }

}
