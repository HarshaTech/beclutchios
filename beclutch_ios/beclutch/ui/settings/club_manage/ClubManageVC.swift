//
//  ClubManageVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import PopupDialog
import Kingfisher
import SwiftEventBus
class ClubManageVC: UIViewController, ISportChooser, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var btSport: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var img: UIImageView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    private var disposal = Disposal()
    @IBOutlet weak var btSave: UIButton!
    var viewModel: ClubManageViewModel = ClubManageViewModel()
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let currentTeam = AppDataInstance.instance.currentSelectedTeam else {
            return
        }

        self.btSport.makeGrayRadius()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        editPhotoBtn.makeGreenButton()
        saveBtn.makeGreenButton()
        backBtn.makeRedButton()

        imagePicker.delegate  = self
        if currentTeam.CLUB_TEAM_INFO.OWNER_ID != AppDataInstance.instance.me?.ID {
            self.btSport.isEnabled = false
            self.txtName.isEnabled  = false
            self.btSave.isHidden = true
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.selectSportItem.observe {
            newSport, _ in

            if newSport.ID == "" {
                self.btSport.setTitle("Choose Sport", for: .normal)
            } else {
                self.btSport.setTitle(newSport.NAME, for: .normal)
            }
            }.add(to: &disposal)

        self.txtName.text = currentTeam.CLUB_TEAM_INFO.NAME
        self.viewModel.selectSportItem.value = currentTeam.SPORT_INFO

        self.viewModel.originPhotoURL = currentTeam.CLUB_TEAM_INFO.PIC_URL
        if currentTeam.CLUB_TEAM_INFO.PIC_URL.isEmpty {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            let url = URL(string: currentTeam.CLUB_TEAM_INFO.PIC_URL)
            self.img.kf.setImage(with: url)
        }

        SwiftEventBus.onMainThread(self, name: "TeamInfoUpdateRes", handler: {
            res in
            let regRes = res?.object as! TeamInfoUpdateRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.currentSelectedTeam = regRes.currentTeam
                self.dismiss(animated: true, completion: nil)
            }
        })
    }

    @IBAction func onClickAddPhoto(_ sender: Any) {
        if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.OWNER_ID == AppDataInstance.instance.me?.ID {
            let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
            let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
                self.openCamera()
            }

            let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
                self.openGallery()
            }

            popup.addButtons([camera, gallery])
            self.present(popup, animated: true, completion: nil)
        }
    }

    @IBAction func onClickRemovePhoto(_ sender: Any) {
        if self.viewModel.newUploadPhoto != nil {
            self.viewModel.newUploadPhoto = nil

            viewModel.isImageEditted = true
        }

        if self.viewModel.originPhotoURL!.isEmpty {
            img.image = UIImage(named: "img_photo_placeholder")
        } else {
            let url = URL(string: self.viewModel.originPhotoURL!)
            img.kf.setImage(with: url)
        }
    }

    @IBAction func onClickSport(_ sender: Any) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = self.viewModel.selectSportItem.value
        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseSport(sport: Production) {
        self.viewModel.selectSportItem.value = sport
    }

    func checkValidation() -> Bool {
        if self.txtName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Club name is required.", parent: self)
            return false
        }

        if self.viewModel.selectSportItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Sport is required", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickSave(_ sender: UIButton) {
        self.viewModel.doUpdateClubInfo(name: self.txtName.text!)
    }

    @IBAction func onClickBack(_ sender: Any) {
        if isInfomationChanged() {
            alert(title: "", message: "Do you want to save your changes?", okTitle: "Yes", okHandler: { (_) in
                self.onClickSave(self.btSave)
            }) { (_) in
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {
            self.viewModel.newUploadPhoto = pickedImage.jpegData(compressionQuality: 0.6)
            self.img.image = pickedImage

            viewModel.isImageEditted = true
        }

        picker.dismiss(animated: true, completion: nil)
    }

    // MARK: - Utils
    func isInfomationChanged() -> Bool {
        return viewModel.isImageEditted ||
            self.txtName.text != AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME ||
            (self.viewModel.selectSportItem.value.NAME != AppDataInstance.instance.currentSelectedTeam!.SPORT_INFO.NAME)
    }
}
