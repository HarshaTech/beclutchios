//
//  CreateNewClubViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class CreateNewClubViewController: UIViewController, ISportChooser {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btSportItem: UIButton!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    private var disposal = Disposal()
    var viewModel: CreateClubViewModel = CreateClubViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtName.backgroundColor = .white
        
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        btSportItem.makeGrayRadius()

        btSportItem.makeGrayRadius()
        self.viewModel.isBusy.observe { [weak self]
            newBusy, oldBusy in
            guard let strongSelf = self else {
                return
            }
            
            if newBusy == oldBusy {return}

            if newBusy {strongSelf.showIndicator()} else {strongSelf.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.selectSportItem.observe { [weak self]
            newSport, _ in
            guard let strongSelf = self else {
                return
            }

            if newSport.ID == "" {
                strongSelf.btSportItem.setTitle("Choose Sport", for: .normal)
            } else {
                strongSelf.btSportItem.setTitle(newSport.NAME, for: .normal)
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateClubInSettingRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let regRes = res?.object as! CreateClubInSettingRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                self.viewModel.loadClubTree()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to create club. Try again.", parent: self)
            }
        })
        
        SwiftEventBus.onMainThread(self, name: "ClubTreeLoadRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! ClubTreeLoadRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: resAPI.rosters)

                if let listTeam = AppDataInstance.instance.myClubTeamList {
                    if let index = listTeam.firstIndex(where: { $0.teamName == self.txtName.text ?? ""}) {
                        AppDataInstance.instance.currentSelectedTeam = listTeam[index]
                        self.viewModel.doUpdateTeam(rosterID: listTeam[index].ID)
                    }
                }
                NavUtil.showRosterViewController()
            }
        })
    }
    
    deinit {
        print("CreateNewClubViewController deinit")
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            self.viewModel.doCreateMyClub(clubName: txtName.text!)
        }
    }

    func checkValidation() -> Bool {
        if self.viewModel.selectSportItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose sport", parent: self)
            return false
        }

        if self.txtName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter the name of club", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickBtSport(_ sender: Any) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = self.viewModel.selectSportItem.value

        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseSport(sport: Production) {
        self.viewModel.selectSportItem.value = sport
    }

}
