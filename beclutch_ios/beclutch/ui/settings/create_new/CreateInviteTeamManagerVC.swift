//
//  CreateInviteTeamManagerVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/25.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import Observable
import SwiftEventBus
class CreateInviteTeamManagerVC: UIViewController, ISportChooser {

    @IBOutlet weak var txtTeamName: UITextField!
    @IBOutlet weak var btSport: UIButton!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btRole: UIButton!
    @IBOutlet weak var chkSendMail: BEMCheckBox!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var createBtn: UIButton!

    private var disposal = Disposal()
    var viewModel: CreateInviteTeamManagerViewModel = CreateInviteTeamManagerViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btRole.makeGrayRadius()
        self.btSport.makeGrayRadius()
        self.chkSendMail.boxType = .square
        self.backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        createBtn.makeGreenButton()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)
        self.viewModel.selectSportItem.observe(DispatchQueue.main) {
            newSport, _ in

            if newSport.ID == "" {
                self.btSport.setTitle("Choose Sport", for: .normal)
            } else {
                self.btSport.setTitle(newSport.NAME, for: .normal)
            }
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateInviteTeamToClubRes", handler: {
            res in
            let regRes = res?.object as! CreateInviteTeamToClubRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                if !regRes.sendmail! {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    AlertUtils.showWarningAlertWithHandler(title: "Invitation Code", message: regRes.msg!, parent: self, handler: {
                            self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to invite team to club. Try again.", parent: self)
            }
        })
    }

    @IBAction func onClickSport(_ sender: Any) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = self.viewModel.selectSportItem.value

        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseSport(sport: Production) {
        self.viewModel.selectSportItem.value = sport
    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if self.txtTeamName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Team name is required", parent: self)
            return false
        }

        if self.txtFName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required", parent: self)
            return false
        }

        if self.txtLName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required", parent: self)
            return false
        }

        if self.txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email is required", parent: self)
            return false
        }

        if self.viewModel.selectSportItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose sport", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.InviteCreateTeamManager(teamName: self.txtTeamName.text!, f_name: self.txtFName.text!, l_name: self.txtLName.text!, email: self.txtEmail.text!, sendMail: self.chkSendMail.on)
        }
    }
}
