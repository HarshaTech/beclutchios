//
//  CreateNewMyTeamViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import RxSwift
import RxCocoa

import BEMCheckBox

class CreateNewMyTeamViewController: UIViewController, ISportChooser {
    @IBOutlet weak var screenTitleLabel: UILabel!
    @IBOutlet weak var informationLabel: UILabel!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btSport: UIButton!
    
    @IBOutlet weak var groupTitleLabel: UILabel!
    @IBOutlet weak var roleTitleLabel: UILabel!
    
    @IBOutlet weak var groupButton: UIButton!
    @IBOutlet weak var roleButton: UIButton!
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!

    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var clubCheckBox: BEMCheckBox!
    @IBOutlet weak var addToClubLabel: UILabel!
    
    
    var teamName: String?

    private var disposal = Disposal()
    private let disposeBag = DisposeBag()
    
    var viewModel: CreateMyTeamViewModel = CreateMyTeamViewModel()
    weak var delegate: SelectTeamVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
       setupStyle()

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            guard let self = self else {
                return
            }
            
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateMyTeamInSettingRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let regRes = res?.object as! CreateMyTeamInSettingRes
            self.viewModel.isBusy.value = false
            if regRes.result == true {
                //update teamList
                self.viewModel.loadClubTree()
                CustomLabelManager.shared.updateLabelDict(dict: regRes.labels)
                
                PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_COMPLETE)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to create team. Try again.", parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "ClubTreeLoadRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! ClubTreeLoadRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: resAPI.rosters)

                if let listTeam = AppDataInstance.instance.myClubTeamList {
                    if let index = listTeam.firstIndex(where: { $0.teamName == self.teamName ?? ""}) {
                        AppDataInstance.instance.currentSelectedTeam = listTeam[index]
                        self.viewModel.doUpdateTeam(rosterID: listTeam[index].ID)
                        self.delegate?.didSelectTeam()
                    }
                }
                NavUtil.showRosterViewController()
            }
        })

        groupButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.showOptionSelection(type: .group)
            })
            .disposed(by: disposeBag)

        btSport.rx.tap
            .filter { [unowned self] in self.viewModel.selectGroupItem.value.ID.count > 0 }
            .subscribe(onNext: { [weak self] in
                if let groupId = self?.viewModel.selectGroupItem.value.ID, let groupIdInt = Int(groupId) {
                    self?.showOptionSelection(type: .sport(groupId: groupIdInt))
                }
            })
            .disposed(by: disposeBag)

        roleButton.rx.tap
            .filter { [unowned self] in self.viewModel.selectGroupItem.value.ID.count > 0 && self.viewModel.selectSportItem.value.ID.count > 0 }
            .subscribe(onNext: { [weak self] in
                if let sportId = self?.viewModel.selectSportItem.value.ID, let sportIdInt = Int(sportId) {
                    self?.showOptionSelection(type: .role(sportId: sportIdInt))
                }
            })
            .disposed(by: disposeBag)
        
        self.viewModel.selectGroupItem
            .map { $0.displayName ?? "Choose Group Type" }
            .bind(to: self.groupButton.rx.title())
            .disposed(by: disposeBag)
        
        self.viewModel.selectSportItem
            .map { $0.displayName ?? "Choose Group" }
            .bind(to: self.btSport.rx.title())
            .disposed(by: disposeBag)

        self.viewModel.selectRoleItem
            .map { $0.displayName ?? "Choose Role" }
            .bind(to: self.roleButton.rx.title())
            .disposed(by: disposeBag)
        
        self.viewModel.activityIndicator
            .debug("activityIndicator")
            .drive(onNext: { [weak self] in
                $0 ? self?.showIndicator() : self?.hideIndicator()
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        print("CreateNewMyTeamViewController deinit")
    }
    
    private func setupStyle() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()

        btSport.makeOptionButton()
        groupButton.makeOptionButton()
        roleButton.makeOptionButton()
        
        txtName.backgroundColor = .white

        if viewModel.isAddTeamToClub {
            self.btSport.isEnabled = false
        }
    }
    
    private func setupUI() {
        let isClub = AppDataInstance.instance.currentSelectedTeam?.getGroupType() == .club
        checkView.isHidden = !isClub
        clubCheckBox.tintColor = .white
        clubCheckBox.onCheckColor = .primary
        clubCheckBox.boxType = .square
        
        if let isClub = AppDataInstance.instance.currentSelectedTeam?.isClub, isClub,
           let clubName = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME {
            clubCheckBox.setOn(true, animated: false)
            addToClubLabel.text = "Add to " + clubName
        }
    }
    
    private func currentSeletedProduction(type: ScreenSelectOptionType) -> Production {
        switch type {
        case .group:
            return viewModel.selectGroupItem.value
        case .sport:
            return viewModel.selectSportItem.value
        case .role:
            return viewModel.selectRoleItem.value
        }
    }
    
    private func showOptionSelection(type: ScreenSelectOptionType) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = currentSeletedProduction(type: type)
        
        viewController.optionType = type

        self.present(viewController, animated: true, completion: nil)
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickBtSport(_ sender: Any) {
//        showOptionSelection(type: .sport)
    }

    func checkValidation() -> Bool {
        if self.txtName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter the name of team", parent: self)
            return false
        }
        
        if self.viewModel.selectGroupItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose group", parent: self)
            return false
        }
        
        if self.viewModel.selectSportItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose sport", parent: self)
            return false
        }

        if self.viewModel.selectRoleItem.value.ID == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose role", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            if self.viewModel.isAddTeamToClub || clubCheckBox.on {
                self.viewModel.doAddMyTeamToClub(teamName: self.txtName.text!)
            } else {
                self.viewModel.doCreateMyTeam(teamName: self.txtName.text!)
            }
            self.teamName = self.txtName.text!
        }
    }

    func onChooseSport(sport item: Production, type: ScreenSelectOptionType) {
        switch type {
        case .group:
            self.viewModel.selectGroupItem.accept(item)
            if let groupId = Int(item.ID) {
                self.viewModel.service.getProductionGroupItems(groupId: groupId) // Service 1
                    .trackActivity(self.viewModel.activityIndicator)
                    .successValue()
                    .filter { $0.msg.count == 1 }
                    .compactMap { $0.msg.first }
                    .do(onNext: { [weak self] sport in
                        self?.viewModel.selectSportItem.accept(sport)
                    })
                    .flatMap { self.viewModel.service.getSportRoles(sportId: Int($0.ID) ?? 0) } // Service 2
                    .successValue()
                    .filter { $0.msg.count == 1 }
                    .compactMap { $0.msg.first }
                    .subscribe(onNext: { [weak self] in
                        self?.viewModel.selectRoleItem.accept($0)
                    })
                    .disposed(by: disposeBag)
                
                if item.NAME == "Sports" {
                    screenTitleLabel.text = "Create a Team"
                    informationLabel.text = "Team Information"
                    saveBtn.setTitle("Create a Team", for: .normal)
                } else {
                    screenTitleLabel.text = "Create a Group"
                    informationLabel.text = "Group Information"
                    saveBtn.setTitle("Create a Group", for: .normal)
                }
                
                groupTitleLabel.text = "Choose \(item.displayName ?? "") Group"
            }

        case .sport:
            self.viewModel.selectSportItem.accept(item)

            if let sportId = Int(item.ID) {
                self.viewModel.service.getSportRoles(sportId: sportId)
                    .trackActivity(self.viewModel.activityIndicator)
                    .successValue()
                    .filter { $0.msg.count == 1 }
                    .compactMap { $0.msg.first }
                    .subscribe(onNext: { [weak self] in
                        self?.viewModel.selectRoleItem.accept($0)
                    })
                    .disposed(by: disposeBag)
            }
        case .role:
            self.viewModel.selectRoleItem.accept(item)
        }
    }
}
