//
//  ThemeColorDialogViewController.swift
//  beclutch
//
//  Created by FAT_MAC on 8/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol PopUpDelegate: class {
    func handleAction(selectedTheme: ThemeColorModel, action: Bool)
}

class ThemeColorDialogViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    weak var delegate: PopUpDelegate?
    var selectedTheme = PreferenceMgr.readTheme()
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var dlgBoxView: UIView!
    @IBOutlet weak var dlgTitleView: UIView!
    @IBOutlet weak var dlgOkBtn: UIButton!
    @IBOutlet weak var dlgCancelBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        initData()
    }

    func configUI() {
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        dlgBoxView.applyShadow()
        dlgTitleView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        dlgOkBtn.setTitleColor(UIColor.COLOR_PRIMARY_DARK, for: .normal)
        dlgCancelBtn.setTitleColor(UIColor.COLOR_ACCENT, for: .normal)
    }

    func initData() {
        for i in 0..<themeColorItems.count {
            themeColorItems[i].isSelect = false
        }
        if let index = themeColorItems.firstIndex(where: {$0.colorValue == self.selectedTheme.colorValue}) {
            themeColorItems[index].isSelect = true
        }
    }

    @IBAction func onOkBtnClicked(_ sender: Any) {
        if selectedTheme.isSelect! {
            self.delegate?.handleAction(selectedTheme: selectedTheme, action: true)
            self.dismiss(animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "Please select one theme", parent: self)
        }

    }
    @IBAction func onCancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    static func showPopup(parentVC: UIViewController) {
      //creating a reference for the dialogView controller
      if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ThemeColorDialog") as? ThemeColorDialogViewController {
      popupViewController.modalPresentationStyle = .custom
      popupViewController.modalTransitionStyle = .crossDissolve
      //setting the delegate of the dialog box to the parent viewController
      popupViewController.delegate = parentVC as? PopUpDelegate
      //presenting the pop up viewController from the parent viewController
      parentVC.present(popupViewController, animated: true)
      }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.themeColorItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemeColorItemCell", for: indexPath as IndexPath) as! ThemeColorItemCell
        cell.colorName.text = themeColorItems[indexPath.row].colorName
        cell.colorBack.backgroundColor = themeColorItems[indexPath.row].colorResourceDark
        cell.colorChk.isHidden = !themeColorItems[indexPath.row].isSelect!
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (collectionView.frame.size.width - 32) / 4
        return CGSize(width: width, height: width + 25)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let _tempVal = themeColorItems[indexPath.row].isSelect
        for i in 0..<themeColorItems.count {
              themeColorItems[i].isSelect = false
          }
        themeColorItems[indexPath.row].isSelect = !_tempVal!
        selectedTheme = themeColorItems[indexPath.row]
        collectionView.reloadData()

        print("You selected cell #\(indexPath.item)!")
    }

    var themeColorItems = [
    ThemeColorModel(isSelect: false, colorValue: "beclutch", colorName: "BeClutch", colorResourcePrimary: UIColor.COLOR_PRIMARY_BECLUTCH, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BECLUTCH, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "green", colorName: "Green", colorResourcePrimary: UIColor.COLOR_PRIMARY_GREEN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GREEN, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "darkgreen", colorName: "Dark Green", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKGREEN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKGREEN, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "lime", colorName: "Lime", colorResourcePrimary: UIColor.COLOR_PRIMARY_LIME, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_LIME, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "darkblue", colorName: "Dark Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKBLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKBLUE, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "blue", colorName: "Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_BLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BLUE, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "midnight", colorName: "Midnight", colorResourcePrimary: UIColor.COLOR_PRIMARY_MIDNIGHT, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_MIDNIGHT, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "lightblue", colorName: "Light Blue", colorResourcePrimary: UIColor.COLOR_PRIMARY_LIGHTBLUE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_LIGHTBLUE, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "brown", colorName: "Brown", colorResourcePrimary: UIColor.COLOR_PRIMARY_BROWN, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BROWN, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "orange", colorName: "Orange", colorResourcePrimary: UIColor.COLOR_PRIMARY_ORANGE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_ORANGE, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "gold", colorName: "Gold", colorResourcePrimary: UIColor.COLOR_PRIMARY_GOLD, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GOLD, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "yellow", colorName: "Yellow", colorResourcePrimary: UIColor.COLOR_PRIMARY_YELLOW, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_YELLOW, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "red", colorName: "Red", colorResourcePrimary: UIColor.COLOR_PRIMARY_RED, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_RED, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "darkred", colorName: "Dark Red", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKRED, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKRED, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "maroon", colorName: "Maroon", colorResourcePrimary: UIColor.COLOR_PRIMARY_MAROON, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_MAROON, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "fuscia", colorName: "Fuscia", colorResourcePrimary: UIColor.COLOR_PRIMARY_FUSCIA, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_FUSCIA, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "purple", colorName: "Purple", colorResourcePrimary: UIColor.COLOR_PRIMARY_PURPLE, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_PURPLE, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "black", colorName: "Black", colorResourcePrimary: UIColor.COLOR_PRIMARY_BLACK, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_BLACK, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "grey", colorName: "Grey", colorResourcePrimary: UIColor.COLOR_PRIMARY_GREY, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_GREY, themeResource: 0),
    ThemeColorModel(isSelect: false, colorValue: "darkgrey", colorName: "Dark Grey", colorResourcePrimary: UIColor.COLOR_PRIMARY_DARKGREY, colorResourceDark: UIColor.COLOR_PRIMARY_DARK_DARKGREY, themeResource: 0)
    ]

}

class ThemeColorItemCell: UICollectionViewCell {
    @IBOutlet weak var colorName: UILabel!
    @IBOutlet weak var colorBack: UIView!
    @IBOutlet weak var colorChk: UIImageView!
}
