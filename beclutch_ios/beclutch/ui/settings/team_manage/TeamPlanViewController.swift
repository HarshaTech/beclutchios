//
//  TeamPlanViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/18.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class TeamPlanViewController: UIViewController {

    var viewModel: TeamPlanViewModel = TeamPlanViewModel()
    @IBOutlet weak var txtCurrentPlan: UILabel!
    @IBOutlet var backgroundView: UIView!
    var tb: TeamPlansTableVC?
    private var disposal = Disposal()
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.curPlan.observe(DispatchQueue.main) {
            newPlan, oldPlan in
            if newPlan == oldPlan {return}

            if newPlan == "0" {
                self.txtCurrentPlan.text = "Your Current Plan : FREE"
            } else if newPlan == "1" {
                self.txtCurrentPlan.text = "Your Current Plan : CORE"
            } else if newPlan == "2"{
                self.txtCurrentPlan.text = "Your Current Plan : PLUS"
            }

            self.tb?.updatePlan()
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "TeamPlanUpdateRes", handler: {
            res in
            let regRes = res?.object as! TeamPlanUpdateRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL = regRes.plan!
                self.viewModel.curPlan.value = regRes.plan
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to update plan. Try again.", parent: self)
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show_table" {
            self.tb = segue.destination as? TeamPlansTableVC
            self.tb?.parentDelegate = self
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
