//
//  TeamSupportVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/18.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import MessageUI

class TeamSupportVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var btMailTo: UIButton!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtContent: UITextView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var sendBtn: UIButton!

    var viewModel: TeamSupportViewModel = TeamSupportViewModel()
    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btMailTo.makeGreenButton()
        sendBtn.makeGreenButton()

        self.txtContent.layer.borderWidth = 1
        self.txtContent.layer.borderColor = UIColor.gray.cgColor
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {
                self.showIndicator()
            } else {
                self.hideIndicator()

            } }.add(to: &disposal)

        self.viewModel.curSupportMail.observe {
            newMail, _ in
                self.btMailTo.setTitle("Mail to " + newMail, for: .normal)
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "TeamSupportRes", handler: {
            res in
            let regRes = res?.object as! TeamSupportRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                self.txtSubject.text = ""
                self.txtContent.text = ""
                AlertUtils.showWarningAlert(title: "Info", message: "Issue has been submitted successfully", parent: self)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to create support ticket. Try again.", parent: self)
            }
        })

        if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL == "0" {
            self.viewModel.curSupportMail.value = "support@beclutch.club"
        } else if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL == "1" {
            self.viewModel.curSupportMail.value = "support2@beclutch.club"
        } else if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL == "2" {
            self.viewModel.curSupportMail.value = "support3@beclutch.club"
        }
    }

    @IBAction func onClickBtMailTo(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([self.viewModel.curSupportMail.value])
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have mail app on this device", parent: self)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    func checkValidation() -> Bool {
        if self.txtSubject.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Subject is required", parent: self)
            return false
        }

        if self.txtContent.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Content is required", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickToSend(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.doAddSupportHistory(subject: self.txtSubject.text!, content: self.txtContent.text!)
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
