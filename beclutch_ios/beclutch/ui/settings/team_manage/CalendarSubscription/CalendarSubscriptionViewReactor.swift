//
//  CalendarSubscriptionViewReactor.swift
//  beclutch
//
//  Created by Vu Trinh on 12/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import ReactorKit

enum CalendarType: Int {
    case google = 1
    case outlook = 2
}

class CalendarSubscriptionViewReactor: Reactor {
    var initialState: State
    
    enum Action {
        case sendEmail
    }
    
    struct State {
        var calendarType: CalendarType
        var sendEmailResult = false
    }
    
    enum Mutation {
        case setSendEmailResult(Bool)
    }
    
    private let service: CalendarSubscriptionService = CalendarSubscriptionService()
    
    init(calendarType: CalendarType) {
        initialState = State(calendarType: calendarType)
    }
    
    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .sendEmail:
            let calendarType = self.currentState.calendarType.rawValue
            let rosterId = AppDataInstance.instance.currentRosterId
            return self.service.sendCalendarInstruction(calendarType: calendarType, rosterId: rosterId)
                .successValue()
                .filter { $0.result == true }
                .map { _ in Void() }
                .map { Mutation.setSendEmailResult(true) }
        }
    }
    
    func reduce(state: State, mutation: Mutation) -> State {
        print("Calling reduce")
        var state = state
        switch mutation {
        case let .setSendEmailResult(result):
            state.sendEmailResult = result
        }
        
        return state
    }
}
