//
//  CalendarSubscriptionService.swift
//  beclutch
//
//  Created by Vu Trinh on 12/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

import RxSwift

class CalendarSubscriptionService {
    func sendCalendarInstruction(calendarType: Int, rosterId: Int) -> Observable<Result<CommonResponse, Error>> {
        APIManager.shared.sendCalendarInstruction(calendarType: calendarType, rosterId: rosterId)
    }
}
