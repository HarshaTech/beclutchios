//
//  CalendarSubscriptionViewController.swift
//  beclutch
//
//  Created by Vu Trinh on 12/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import ReactorKit
import WebKit

class CalendarSubscriptionViewController: BaseViewController, ReactorKit.StoryboardView {
    var disposeBag: DisposeBag = DisposeBag()

    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var webView: WKWebView!
    
    var icalLink: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupStyle()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupStyle() {
        sendEmailButton.makeGreenButton()
        
        self.navigationController?.isNavigationBarHidden = false
        webView.backgroundColor = .white
    }

    func bind(reactor: CalendarSubscriptionViewReactor) {
        sendEmailButton.rx.tap
            .map { CalendarSubscriptionViewReactor.Action.sendEmail }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        reactor.state.map { $0.sendEmailResult }
            .subscribe(onNext: { [weak self] value in
                if value {
                    self?.alert(message: "Email has been successfully sent")
                }
            })
            .disposed(by: disposeBag)

        reactor.state.map { $0.calendarType }
            .map { $0 == .google ? "Google Calendar Sync" : "Outlook Calendar Sync" }
            .bind(to: self.rx.title )
            .disposed(by: disposeBag)
        
        if let googlePath = Bundle.main.url(forResource: "BeclutchCalendarSync_google", withExtension: "html"),
           let googleStr = try? String(contentsOf: googlePath),
           let outlookPath = Bundle.main.url(forResource: "BeclutchCalendarSync_outlook", withExtension: "html"),
           let outLookStr = try? String(contentsOf: outlookPath)        {
            let googleHtmlContent = googleStr.replacingOccurrences(of: "Link_Here", with: icalLink)
            let outlookHtmlContent = outLookStr.replacingOccurrences(of: "Link_Here", with: icalLink)
            
            reactor.state.map { $0.calendarType }
                .compactMap { $0 == .google ? googleHtmlContent : outlookHtmlContent }
                .subscribe(onNext: { [unowned self] in
                    self.webView.loadHTMLString($0, baseURL: nil)
                })
                .disposed(by: disposeBag)
        }
//        append("<p style='word-wrap: break-word; text-decoration: underline;'>" + calUrl + "</p>").append("\n");
        
        
        
    }
}
