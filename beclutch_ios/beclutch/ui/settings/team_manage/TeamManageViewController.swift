//
//  TeamManageViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import Kingfisher
import SwiftEventBus
import ActionSheetPicker_3_0
import PopupDialog

import RxSwift
import RxCocoa

class TeamManageViewController: UIViewController, ISportChooser, PopUpDelegate, IColorPickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var selectedTheme: ThemeColorModel!

    @IBOutlet weak var btAwayColor: UIButton!
    @IBOutlet weak var btHomeColor: UIButton!
    @IBOutlet weak var btArriveMin: UIButton!
    @IBOutlet weak var btArriveHour: UIButton!
    @IBOutlet weak var btPracticeMin: UIButton!
    @IBOutlet weak var btPracticeHour: UIButton!
    @IBOutlet weak var btHomeLocation: UIButton!
    @IBOutlet weak var btChooseSport: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var imgPic: UIImageView!

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var removePhotoBtn: UIButton!

    @IBOutlet weak var TeamInfoView: UIView!
    @IBOutlet weak var InfoTitleView: UIView!
    @IBOutlet weak var DefaultTimeView: UIView!
    @IBOutlet weak var DefaultTimeTitleView: UIView!
    @IBOutlet weak var uniformColorView: UIView!
    @IBOutlet weak var uniformColorTitleView: UIView!

    @IBOutlet weak var themeView: UIView!
    @IBOutlet weak var themeTitleView: UIView!
    @IBOutlet weak var btThemePreview: UIButton!
    @IBOutlet weak var btThemeColor: UIButton!
    
    @IBOutlet weak var calendarLinkTextField: UITextField!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var appleCalendarButton: UIButton!
    @IBOutlet weak var googleCalendarButton: UIButton!
    @IBOutlet weak var outlookCalendar: UIButton!

    @IBOutlet weak var screenTitleLabel: UILabel!
    @IBOutlet weak var teamInformationLabel: UILabel!
    @IBOutlet weak var practiceDurationLabel: UILabel!
    @IBOutlet weak var arriveEarlyLabel: UILabel!

    @IBOutlet weak var photoView: UIView!

    var viewModel: TeamManageViewModel?
    private var disposal = Disposal()
    
    private let disposeBag = DisposeBag()
    
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpStyle()

        self.viewModel = TeamManageViewModel(service: APIManager.shared)
        imagePicker.delegate = self

        setUpBinding()

        initAllUI()
        
        self.viewModel?.doLoadTimezone()
        self.viewModel?.doSplashAction()
    }
    
    deinit {
        print("TeamManageViewController deinit")
    }
    
    func setUpStyle() {
        setUpCustomLabel()

        selectedTheme = PreferenceMgr.readTheme()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveButton.makeGreenButton()
        cancelBtn.makeRedButton()
        editPhotoBtn.makeGreenButton()
        removePhotoBtn.backgroundColor = UIColor.COLOR_BLUE
        photoView.applyShadow()
        TeamInfoView.applyShadow()
        InfoTitleView.backgroundColor = UIColor.COLOR_BLUE
        DefaultTimeView.applyShadow()
        DefaultTimeTitleView.backgroundColor = UIColor.COLOR_BLUE
        uniformColorView.applyShadow()
        uniformColorTitleView.backgroundColor = UIColor.COLOR_BLUE
        themeView.applyShadow()
        themeTitleView.backgroundColor = UIColor.COLOR_BLUE

        self.btChooseSport.makeGrayRadius()
        self.btHomeLocation.makeGrayRadius()
        self.btPracticeHour.makeGrayRadius()
        self.btPracticeMin.makeGrayRadius()
        self.btArriveHour.makeGrayRadius()
        self.btArriveMin.makeGrayRadius()
        self.btHomeColor.makeGrayRadius()
        self.btAwayColor.makeGrayRadius()
        
        self.btThemePreview.makeGreenButton()
        self.btThemeColor.makeGrayRadius()
        self.btThemeColor.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        
        if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.OWNER_ID != AppDataInstance.instance.me?.ID {
            self.btChooseSport.isEnabled = false
            self.btHomeLocation.isEnabled = false
            self.btPracticeMin.isEnabled = false
            self.btPracticeHour.isEnabled = false
            self.btArriveMin.isEnabled = false
            self.btArriveHour.isEnabled = false
            self.btAwayColor.isEnabled  = false
            self.btHomeColor.isEnabled = false
            
            self.txtName.isEnabled = false

            btThemeColor.isEnabled = false
            btThemePreview.isEnabled = false
        }
        
        calendarLinkTextField.isEnabled = false
        calendarLinkTextField.isUserInteractionEnabled = false
        
        copyButton.makeBlueButton()
        
        [appleCalendarButton, googleCalendarButton, outlookCalendar].forEach { $0?.makeGreenButton() }
    }

    func setUpCustomLabel() {
        screenTitleLabel.text = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_team_setting.rawValue)
        teamInformationLabel.text = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_team_information.rawValue)
        practiceDurationLabel.text = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_practice_duration.rawValue)
        arriveEarlyLabel.text = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_arrive_early_game.rawValue)
    }
    
    func setUpBinding() {
        viewModel?.service.fetchCalendarIcalLink()
            .successValue()
            .map { $0.extra }
            .asDriver(onErrorJustReturn: "")
            .drive(calendarLinkTextField.rx.text)
            .disposed(by: disposeBag)

        copyButton.rx.controlEvent(.touchUpInside)
            .subscribe(onNext: { [unowned self] in
                UIPasteboard.general.string = self.calendarLinkTextField.text
            })
            .disposed(by: disposeBag)
            
        appleCalendarButton.rx.controlEvent(.touchUpInside)
            .compactMap { self.calendarLinkTextField.text }
            .compactMap { URL(string: $0) }
            .subscribe(onNext: { url in
                self.alert(title: "", message: "Would you like to Subscribe to your team Calendar using Apple Calendar?", okTitle: "Yes") {_ in
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } cancelHandler: { _ in }
            })
            .disposed(by: disposeBag)
        
        googleCalendarButton.rx.controlEvent(.touchUpInside)
            .subscribe(onNext: { [weak self] in
                self?.showCalendarInstruction(type: .google)
            })
            .disposed(by: disposeBag)
        
        outlookCalendar.rx.controlEvent(.touchUpInside)
            .subscribe(onNext: { [weak self] in
                self?.showCalendarInstruction(type: .outlook)
            })
            .disposed(by: disposeBag)
        
        self.viewModel?.isBusy.observe { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
        }.add(to: &disposal)

//        self.viewModel?.alter1Color.observe { [weak self]
//            newAlter, _ in
//            guard let strongSelf = self else { return }
//
//            if newAlter.isEmpty {
//                strongSelf.btAlternative1.setTitle("Choose", for: .normal)
//                strongSelf.btAlternative1.backgroundColor = UIColor.white
//            } else {
//                strongSelf.btAlternative1.setTitle("", for: .normal)
//                strongSelf.btAlternative1.backgroundColor = ColorUtils.getUIColorFromRGB(val: Int32(newAlter)!)
//            }
//        }.add(to: &disposal)
//
//        self.viewModel?.alter2Color.observe { [weak self]
//            newAlter, _ in
//            guard let strongSelf = self else { return }
//
//            if newAlter.isEmpty {
//                strongSelf.btAlternative2.setTitle("Choose", for: .normal)
//                strongSelf.btAlternative2.backgroundColor = UIColor.white
//            } else {
//                strongSelf.btAlternative2.setTitle("", for: .normal)
//                strongSelf.btAlternative2.backgroundColor = ColorUtils.getUIColorFromRGB(val: Int32(Int(newAlter)!))
//            }
//        }.add(to: &disposal)

        self.viewModel?.homeColor.observe { [weak self]
            newHome, _ in
            guard let strongSelf = self else { return }
            
            if newHome.isEmpty {
                strongSelf.btHomeColor.setTitle("Choose", for: .normal)
                strongSelf.btHomeColor.backgroundColor = UIColor.white
            } else {
                strongSelf.btHomeColor.setTitle("", for: .normal)
                strongSelf.btHomeColor.backgroundColor = ColorUtils.getUIColorFromRGB(val: Int32(Int(newHome)!))
            }
        }.add(to: &disposal)

        self.viewModel?.awayColor.observe { [weak self]
            newAway, _ in
            guard let strongSelf = self else { return }
            
            if newAway.isEmpty {
                strongSelf.btAwayColor.setTitle("Choose", for: .normal)
                strongSelf.btAwayColor.backgroundColor = UIColor.white
            } else {
                strongSelf.btAwayColor.setTitle("", for: .normal)
                strongSelf.btAwayColor.backgroundColor = ColorUtils.getUIColorFromRGB(val: Int32(Int(newAway)!))
            }
        }.add(to: &disposal)

        self.viewModel?.practiceHour.observe { [weak self]
            newPractice, _ in
            guard let strongSelf = self else { return }
            
            if newPractice.isEmpty {
                strongSelf.btPracticeHour.setTitle("0", for: .normal)
            } else {
                strongSelf.btPracticeHour.setTitle(newPractice, for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel?.practiceMin.observe { [weak self]
            newPractice, _ in
            guard let strongSelf = self else { return }
            
            if newPractice.isEmpty {
                strongSelf.btPracticeMin.setTitle("0", for: .normal)
            } else {
                strongSelf.btPracticeMin.setTitle(newPractice, for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel?.arriveHour.observe { [weak self]
            newArrive, _ in
            guard let strongSelf = self else { return }
            
            if newArrive.isEmpty {
                strongSelf.btArriveHour.setTitle("0", for: .normal)
            } else {
                strongSelf.btArriveHour.setTitle(newArrive, for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel?.arriveMin.observe { [weak self]
            newArrive, _ in
            guard let strongSelf = self else { return }
            
            if newArrive.isEmpty {
                strongSelf.btArriveMin.setTitle("0", for: .normal)
            } else {
                strongSelf.btArriveMin.setTitle(newArrive, for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel?.timeZone.observe { [weak self]
            newTime, _ in
            guard let strongSelf = self else { return }
            
            if newTime == 0 {
                strongSelf.btHomeLocation.setTitle("Choose timezone", for: .normal)
            } else {
                guard let timezones = strongSelf.viewModel?.timezones, timezones.count != 0 else {
                    return
                }

                let idx = newTime - 1

                if idx < timezones.count {
                    let timeZone = timezones[idx]
                    strongSelf.btHomeLocation.setTitle(timeZone.zoneName + " " + timeZone.zoneOffset, for: .normal)
                }
            }
        }.add(to: &disposal)

        self.viewModel?.productInfo.observe { [weak self]
            newProduct, _ in
            guard let strongSelf = self else { return }
            
            if newProduct.ID.isEmpty {
                strongSelf.btChooseSport.setTitle("Choose sport", for: .normal)
            } else {
                strongSelf.btChooseSport.setTitle(newProduct.NAME, for: .normal)
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadTimeZoneRes", handler: { [weak self]
            res in
            guard let strongSelf = self,
                  let regRes = res?.object as? LoadTimeZoneRes,
                  let viewModel = strongSelf.viewModel else { return }
            
            
            viewModel.isBusy.value = false
            if regRes.result == true,
               let timezones = regRes.timezones,
               let timeZoneValue = Int(AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.TIME_ZONE) {
                viewModel.timezones = timezones
                viewModel.timeZone.value = timeZoneValue
            }
        })

        SwiftEventBus.onMainThread(self, name: "TeamInfoUpdateRes", handler: { [weak self]
            res in
            guard let strongSelf = self,
                  let regRes = res?.object as? TeamInfoUpdateRes else { return }
            
            strongSelf.viewModel?.isBusy.value = false
            if regRes.result == true,
               let colorValue = strongSelf.selectedTheme.colorValue {
                AppDataInstance.instance.currentSelectedTeam = regRes.currentTeam
                PreferenceMgr.saveTheme(themeValue: colorValue)
                NavUtil.showHomeViewController()
            }
        })

        SwiftEventBus.onMainThread(self, name: "LoginRes", handler: { [weak self]
            res in
            guard let stongSelf = self,
                  let loginRes = res?.object as? LoginRes else { return }
            
            // go to main home screen here
            
            if loginRes.result == true {
                if loginRes.default_member == nil || (loginRes.default_member?.ID.isEmpty)! {
                    loginRes.default_member = loginRes.clubs?.first
                }

                if let msg = loginRes.msg {
                    AppDataInstance.instance.setToken(token: msg)
                }
                
                if let email = loginRes.user?.EMAIL {
                    AppDataInstance.instance.setEmail(val: email)
                }

                CustomLabelManager.shared.updateLabelDict(dict: loginRes.labels)
                
                AppDataInstance.instance.me = loginRes.user
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: loginRes.clubs)
                AppDataInstance.instance.currentSelectedTeam = loginRes.default_member
                AppDataInstance.instance.roles = loginRes.roles

                stongSelf.viewModel?.timeZone.value = Int(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.TIME_ZONE ?? "0") ?? 0
            }
        })
    }
    
    private func showCalendarInstruction(type: CalendarType) {
        guard let icalLink = self.calendarLinkTextField.text else {
            return
        }
        
        let calendarSubscribeVC = CalendarSubscriptionViewController()
        let reactor = CalendarSubscriptionViewReactor(calendarType: type)
        
        self.navigationController?.pushViewController(calendarSubscribeVC, animated: true)
        
        calendarSubscribeVC.icalLink = icalLink
        calendarSubscribeVC.reactor = reactor
    }

    func initAllUI() {
        if let teamInfo = AppDataInstance.instance.currentSelectedTeam {
            self.txtName.text = teamInfo.CLUB_TEAM_INFO.NAME
            
            guard let viewModel = self.viewModel else { return }
            
            viewModel.productInfo.value = teamInfo.SPORT_INFO
            //todo load timezone here and set the timezone value

            viewModel.practiceHour.value = teamInfo.CLUB_TEAM_INFO.PRACTICE_HOURS
            viewModel.practiceMin.value = teamInfo.CLUB_TEAM_INFO.PRACTICE_MINS
            viewModel.arriveHour.value = teamInfo.CLUB_TEAM_INFO.ARRIVE_HOURS
            viewModel.arriveMin.value = teamInfo.CLUB_TEAM_INFO.ARRIVE_MINS
            viewModel.homeColor.value = teamInfo.CLUB_TEAM_INFO.HOME_COLOR
            viewModel.awayColor.value = teamInfo.CLUB_TEAM_INFO.AWAY_COLOR
            viewModel.alter1Color.value = teamInfo.CLUB_TEAM_INFO.ADDITION_COLOR_1
            viewModel.alter2Color.value = teamInfo.CLUB_TEAM_INFO.ADDITION_COLOR_2
            viewModel.originPhotoURL = teamInfo.CLUB_TEAM_INFO.PIC_URL
            
            if teamInfo.CLUB_TEAM_INFO.PIC_URL.isEmpty {
                imgPic.image = UIImage(named: "img_photo_placeholder")
            } else {
                let url = URL(string: teamInfo.CLUB_TEAM_INFO.PIC_URL)
                imgPic.kf.setImage(with: url)
            }
        }
    }

    @IBAction func onClickRemovePhoto(_ sender: Any) {
        if self.viewModel?.newUploadPhoto != nil {
            self.viewModel?.newUploadPhoto = nil

            viewModel?.isImageEditted = true
        }
        if (self.viewModel?.originPhotoURL!.isEmpty)! {
            imgPic.image = UIImage(named: "img_photo_placeholder")
        } else {
            let url = URL(string: self.viewModel!.originPhotoURL!)
            imgPic.kf.setImage(with: url)
        }
    }

    @IBAction func onClickToolBack(_ sender: Any) {
        if isInfomationChanged() {
            alert(title: "", message: "Do you want to save your changes?", okTitle: "Yes", okHandler: { (_) in
                self.onClickSave(self.saveButton)
            }) { (_) in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func onClickEditPic(_ sender: Any) {
        if AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.OWNER_ID == AppDataInstance.instance.me?.ID {
            let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
            let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
                self.openCamera()
            }

            let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
                self.openGallery()
            }

            popup.addButtons([camera, gallery])
            self.present(popup, animated: true, completion: nil)
        }
    }

    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {
            self.viewModel?.newUploadPhoto = pickedImage.jpegData(compressionQuality: 0.6)
            self.imgPic.image = pickedImage

            viewModel?.isImageEditted = true
        }

        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickChooseSport(_ sender: Any) {
        let chooseSportStoryBoard = UIStoryboard(name: "ChooseSport", bundle: nil)
        let viewController = chooseSportStoryBoard.instantiateViewController(withIdentifier: "ChooseSportViewController") as! ChooseSportViewController
        viewController.parentListener = self
        viewController.selectedSportItem = self.viewModel?.productInfo.value

        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseSport(sport: Production) {
        self.viewModel?.productInfo.value = sport
    }

    @IBAction func onClickHomeLocation(_ sender: Any) {
        var arr = [String]()
        for zone in self.viewModel!.timezones {
            arr.append(zone.zoneName + " " + zone.zoneOffset)
        }

        if var timeZone = self.viewModel?.timeZone.value {
            if timeZone < 1 {
                timeZone = 1
            }
            ActionSheetStringPicker.show(withTitle: "Select Timezone", rows: arr, initialSelection: timeZone - 1, doneBlock: { (_, index, _) in
                self.viewModel?.timeZone.value = index + 1
            }, cancel: nil, origin: self.view)
        }
    }

    func handleDuration(isPraceticeDialog: Bool) {
        var arrHr = [String]()
        var arrMin = [String]()

        for hourMark in 0...24 {
            arrHr.append("\(hourMark)")
        }

        for tickMark in stride(from: 0, to: 60, by: 5) {
            arrMin.append("\(tickMark)")
        }

        let hourValue = isPraceticeDialog ? Int((self.viewModel?.practiceHour.value)!)! : Int((self.viewModel?.arriveHour.value)!)!
        let minValue = isPraceticeDialog ? Int((self.viewModel?.practiceMin.value)!)! / 5 : Int((self.viewModel?.arriveMin.value)!)! / 5
        let title = isPraceticeDialog ? "Choose Duration" : "Choose Arrive Early"

        ActionSheetMultipleStringPicker.show(withTitle: title, rows: [
            arrHr,
            arrMin
            ], initialSelection: [hourValue,
                                  minValue],
               doneBlock: {
                _, indexes, _ in
                if isPraceticeDialog {
                    self.viewModel?.practiceHour.value = arrHr[indexes![0] as! Int]
                    self.viewModel?.practiceMin.value = arrMin[indexes![1] as! Int]
                } else {
                    self.viewModel?.arriveHour.value = arrHr[indexes![0] as! Int]
                    self.viewModel?.arriveMin.value = arrMin[indexes![1] as! Int]
                }
                //                   self.btDuration.setTitle(arrHr[indexes![0] as! Int] + " hour(s) " + String(format: "%02d min(s)", Int(arrMin[indexes![1] as! Int])!), for: .normal)
                return
        }, cancel: { _ in return }, origin: self.view)
    }

    @IBAction func onClickPracticeHour(_ sender: Any) {
        handleDuration(isPraceticeDialog: true)
    }

    @IBAction func onClickPracticeMin(_ sender: Any) {
        handleDuration(isPraceticeDialog: true)
    }

    @IBAction func onClickArriveHour(_ sender: Any) {
        handleDuration(isPraceticeDialog: false)
    }

    @IBAction func onClickArriveMin(_ sender: Any) {
        handleDuration(isPraceticeDialog: false)
    }

    @IBAction func onClickHomeColor(_ sender: Any) {
        let colorPicker = mainStoryboard.instantiateViewController(withIdentifier: "ChooseColorViewController") as! ChooseColorViewController
        colorPicker.parentDelegate = self
        colorPicker.colorPickType = ColorPickerForType.HOME_COLOR
        colorPicker.selectedColor = (sender as! UIButton).backgroundColor!
        self.present(colorPicker, animated: true, completion: nil)

        print("A4")
    }

    @IBAction func onClickAwayColor(_ sender: Any) {
        let colorPicker = mainStoryboard.instantiateViewController(withIdentifier: "ChooseColorViewController") as! ChooseColorViewController
        colorPicker.parentDelegate = self
        colorPicker.colorPickType = ColorPickerForType.AWAY_COLOR
        colorPicker.selectedColor = (sender as! UIButton).backgroundColor!
        self.present(colorPicker, animated: true, completion: nil)

        print("A1")
    }

    @IBAction func onClickAlternative1(_ sender: Any) {
        let colorPicker = self.storyboard?.instantiateViewController(withIdentifier: "ChooseColorViewController") as! ChooseColorViewController
        colorPicker.parentDelegate = self
        colorPicker.colorPickType = ColorPickerForType.ALTER_1_COLOR
        colorPicker.selectedColor = (sender as! UIButton).backgroundColor!
        self.present(colorPicker, animated: true, completion: nil)

        print("A2")
    }

    @IBAction func onClickAlternative2(_ sender: Any) {
        let colorPicker = self.storyboard?.instantiateViewController(withIdentifier: "ChooseColorViewController") as! ChooseColorViewController
        colorPicker.parentDelegate = self
        colorPicker.colorPickType = ColorPickerForType.ALTER_2_COLOR
        colorPicker.selectedColor = (sender as! UIButton).backgroundColor!
        self.present(colorPicker, animated: true, completion: nil)

        print("A3")
    }

    func onSetColor(selCol: UIColor, type: ColorPickerForType) {
        switch type {
        case .HOME_COLOR:
            self.viewModel?.homeColor.value = "\(selCol.rgb())"
            break
        case .AWAY_COLOR:
            self.viewModel?.awayColor.value = "\(selCol.rgb())"
            break
        case .ALTER_1_COLOR:
            self.viewModel?.alter1Color.value = "\(selCol.rgb())"
            break
        case .ALTER_2_COLOR:
            self.viewModel?.alter2Color.value = "\(selCol.rgb())"
            break
        }
    }

    @IBAction func onClickSave(_ sender: UIButton) {
        if self.txtName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Name is required", parent: self)
            return
        }

        self.viewModel?.doSaveTeamSetting(name: txtName.text!)
    }

    @IBAction func onClickCancel(_ sender: Any) {
        onClickToolBack(sender)
    }

    // MARK: - Utils
    func isInfomationChanged() -> Bool {
        guard let viewModel = viewModel, let teamInfo = AppDataInstance.instance.currentSelectedTeam else {
            return false
        }

        return viewModel.isImageEditted ||
            self.txtName.text != teamInfo.CLUB_TEAM_INFO.NAME ||
            viewModel.productInfo.value.ID != teamInfo.SPORT_INFO.ID ||
            viewModel.practiceHour.value != teamInfo.CLUB_TEAM_INFO.PRACTICE_HOURS ||
            viewModel.practiceMin.value != teamInfo.CLUB_TEAM_INFO.PRACTICE_MINS ||
            viewModel.arriveHour.value != teamInfo.CLUB_TEAM_INFO.ARRIVE_HOURS ||
            viewModel.arriveMin.value != teamInfo.CLUB_TEAM_INFO.ARRIVE_MINS ||
            viewModel.homeColor.value != teamInfo.CLUB_TEAM_INFO.HOME_COLOR ||
            viewModel.awayColor.value != teamInfo.CLUB_TEAM_INFO.AWAY_COLOR ||
            viewModel.alter1Color.value != teamInfo.CLUB_TEAM_INFO.ADDITION_COLOR_1 ||
            viewModel.alter2Color.value != teamInfo.CLUB_TEAM_INFO.ADDITION_COLOR_2 ||
            viewModel.colorValue.value != teamInfo.CLUB_TEAM_INFO.COLOR_VALUE ||
            viewModel.timeZone.value != Int(AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.TIME_ZONE)
    }

    func handleAction(selectedTheme: ThemeColorModel, action: Bool) {
        btThemeColor.backgroundColor = selectedTheme.colorResourceDark
        viewModel?.colorValue.value = selectedTheme.colorValue!
        viewModel?.colorName.value = selectedTheme.colorName!
        viewModel?.colorResourceDark.value = "0"
        viewModel?.colorResourcePrimary.value = "0"
        viewModel?.themeResource.value = "0"
        if selectedTheme.isSelect! {
            viewModel?.isSelect.value = "1"
        } else {
            viewModel?.isSelect.value = "0"
        }

        self.selectedTheme = selectedTheme

    }
    @IBAction func onThemeColorBtnClicked(_ sender: Any) {
        if !AccountCommon.isCoachOrManager() {
            self.alert(message: "Only the team owner can change the team theme")
        } else {
            ThemeColorDialogViewController.showPopup(parentVC: self)
        }
    }
    @IBAction func onThemePreviewClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ThemePreviewController") as! ThemePreviewController
        viewController.selectedTheme = self.selectedTheme
        viewController.modalPresentationStyle = .fullScreen
        self.show(viewController, sender: self)
    }

}
