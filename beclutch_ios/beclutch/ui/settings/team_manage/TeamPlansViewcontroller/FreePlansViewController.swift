//
//  FreePlansViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/18/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class FreePlansViewController: UIViewController {
    var viewModel: TeamPlanViewModel = TeamPlanViewModel()
     private var disposal = Disposal()

    @IBOutlet weak var imvLine1: UIImageView!
    @IBOutlet weak var imvLine2: UIImageView!
    @IBOutlet weak var imvLine3: UIImageView!
    @IBOutlet weak var imvLine4: UIImageView!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var btnDowngrade: UIButton!

    private let plusMonthlyIdentifier = "com.rock.beclutch.plusPlanMonthly"
    private let plusYearlyIdentifier = "com.rock.beclutch.yearlyPlusPlan"
    private var inAppPurchaseHelper: IAPHelper?
    var curPlan: String?

    @objc fileprivate func updatePlan() {
        DispatchQueue.main.async {
            if self.curPlan == "Free" {
                       self.btnDowngrade.setTitle("Current Plan", for: .normal)
                       self.btnDowngrade.isEnabled = false
                   } else {
                       self.btnDowngrade.setTitle("Downgrade", for: .normal)
                       self.btnDowngrade.isEnabled = true
                   }

            self.btnDowngrade.isEnabled = AccountCommon.isCoachOrManager()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        inAppPurchaseHelper = IAPHelper(productIds: [self.plusMonthlyIdentifier, self.plusYearlyIdentifier])
                inAppPurchaseHelper?.requestProducts { [weak self] (_, products) in
                     for p in products! {
                                 if self?.inAppPurchaseHelper?.isPurchased(productId: p.productIdentifier) ?? false {
                                     if p.productIdentifier == self?.plusMonthlyIdentifier {
                                                       self?.curPlan = "Month"
                                                   } else if p.productIdentifier == self?.plusYearlyIdentifier {
                                                       self?.curPlan = "Year"
                                                   }
                                 } else {
                                                   self?.curPlan = "Free"
                                               }

                             }
                    self?.updatePlan()
                }

                NotificationCenter.default.addObserver(self, selector: #selector(updatePlan), name: .IAPHelperPurchaseNotification, object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .COLOR_PRIMARY_DARK
        self.headerView.backgroundColor = .COLOR_PRIMARY_DARK
        self.titleView.backgroundColor = .COLOR_PRIMARY

        self.btnDowngrade.makeGreenButton()
        self.btnDowngrade.tintColor = .white

        configIconWithTheme()
         updatePlan()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
                   newBusy, oldBusy in
                   if newBusy == oldBusy {return}

                   if newBusy {self.showIndicator()} else {self.hideIndicator()}
                   }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "TeamPlanUpdateRes", handler: {
            res in
            let regRes = res?.object as! TeamPlanUpdateRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL = regRes.plan!
                self.viewModel.curPlan.value = regRes.plan
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to update plan. Try again.", parent: self)
            }
        })

    }

    fileprivate func configIconWithTheme() {
        imvLine1.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine1.tintColor = .COLOR_PRIMARY

        imvLine2.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine2.tintColor = .COLOR_PRIMARY

        imvLine3.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine3.tintColor = .COLOR_PRIMARY

        imvLine4.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine4.tintColor = .COLOR_PRIMARY
    }

    @IBAction func handleBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func handleDowngrade(_ sender: Any) {
        //handle cancel subcription
    }
}
