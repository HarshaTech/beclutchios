//
//  PlusPlansViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/18/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class PlusPlansViewController: UIViewController {

    var viewModel: TeamPlanViewModel = TeamPlanViewModel()
    private var disposal = Disposal()

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!

    @IBOutlet weak var imvLine1: UIImageView!
    @IBOutlet weak var lbRule1: UILabel!
    @IBOutlet weak var imvLine2: UIImageView!
    @IBOutlet weak var imvLine3: UIImageView!
    @IBOutlet weak var imvLine4: UIImageView!
    @IBOutlet weak var imvLine5: UIImageView!
    @IBOutlet weak var imvLine6: UIImageView!
    @IBOutlet weak var imvLine7: UIImageView!
    @IBOutlet weak var imvLine8: UIImageView!
    @IBOutlet weak var imvLine9: UIImageView!
    @IBOutlet weak var imvLine10: UIImageView!
    @IBOutlet weak var imvLine11: UIImageView!
    @IBOutlet weak var imvLine12: UIImageView!
    @IBOutlet weak var imvLine13: UIImageView!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var btnDowngrade: UIButton!

    //segment control
    @IBOutlet weak var segmentContainer: UIView!
    @IBOutlet weak var btnMonthlyOption: UIButton!
    @IBOutlet weak var btnYearlyOption: UIButton!

    var isMonthlySelect = false //0: Monthly  1: Yearly
    var curPlan: String?

    private let plusMonthlyIdentifier = "com.rock.beclutch.plusPlanMonthly"
    private let plusYearlyIdentifier = "com.rock.beclutch.yearlyPlusPlan"
    private var inAppPurchaseHelper: IAPHelper?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .COLOR_PRIMARY_DARK
        self.headerView.backgroundColor = .COLOR_PRIMARY_DARK
        self.titleView.backgroundColor = .COLOR_PRIMARY

        self.btnDowngrade.makeGreenButton()
        self.btnDowngrade.tintColor = .white

        self.segmentContainer.layer.borderWidth = 1
        self.segmentContainer.layer.borderColor = UIColor.white.cgColor

        configIconWithTheme()
        handleSegmentState()
        updatePlan()

        btnMonthlyOption.makeUncheckSegment()
        btnYearlyOption.makeCheckedSegment()

        lbTitle.text = "$4.99"
        lbRule1.text = "$4.99 per month (billed $59.99 annually)"
        lbSubTitle.text = "Billed $59.99 annually"

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "TeamPlanUpdateRes", handler: {
            res in
            let regRes = res?.object as! TeamPlanUpdateRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL = regRes.plan!
                self.viewModel.curPlan.value = regRes.plan
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to update plan. Try again.", parent: self)
            }
        })

        inAppPurchaseHelper = IAPHelper(productIds: [self.plusMonthlyIdentifier, self.plusYearlyIdentifier])
        inAppPurchaseHelper?.requestProducts { [weak self] (_, products) in
            for p in products! {
                if self?.inAppPurchaseHelper?.isPurchased(productId: p.productIdentifier) ?? false {
                    if p.productIdentifier == self?.plusMonthlyIdentifier {
                                      self?.curPlan = "Month"
                                  } else if p.productIdentifier == self?.plusYearlyIdentifier {
                                      self?.curPlan = "Year"
                                  }
                } else {
                                  self?.curPlan = "Free"
                              }

            }
            self?.updatePlan()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(updatePlan), name: .IAPHelperPurchaseNotification, object: nil)
    }

    @objc fileprivate func updatePlan() {
        DispatchQueue.main.async {
            if self.curPlan == "Free" {
                self.btnDowngrade.setTitle( "Upgrade", for: .normal)
                self.btnDowngrade.isEnabled = true
            } else if self.curPlan == "Month" {
                if self.isMonthlySelect {
                    self.btnDowngrade.setTitle( "Current plan", for: .normal)
                    self.btnDowngrade.isEnabled = false
                } else {
                    self.btnDowngrade.setTitle( "Upgrade", for: .normal)
                    self.btnDowngrade.isEnabled = true
                }

            } else {
                if !self.isMonthlySelect {
                    self.btnDowngrade.setTitle( "Current plan", for: .normal)
                    self.btnDowngrade.isEnabled = false
                } else {
                    self.btnDowngrade.setTitle( "Downgrade", for: .normal)
                    self.btnDowngrade.isEnabled = true
                }

            }
            self.btnDowngrade.isEnabled = AccountCommon.isCoachOrManager()
        }

    }

    fileprivate func handleSegmentState() {
        if isMonthlySelect == false {
            btnMonthlyOption.makeCheckedSegment()
            btnYearlyOption.makeUncheckSegment()

            lbTitle.text = "$8.99"
            lbRule1.text = "$8.99 per month (billed monthly)"
            lbSubTitle.text = " Billed monthly"
        } else {
            btnMonthlyOption.makeUncheckSegment()
            btnYearlyOption.makeCheckedSegment()

            lbTitle.text = "$4.99"
            lbRule1.text = "$4.99 per month (billed $59.99 annually)"
            lbSubTitle.text = "Billed $59.99 annually"
        }

        updatePlan()
    }

    fileprivate func configIconWithTheme() {
        imvLine1.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine1.tintColor = .COLOR_PRIMARY

        imvLine2.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine2.tintColor = .COLOR_PRIMARY

        imvLine3.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine3.tintColor = .COLOR_PRIMARY

        imvLine4.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine4.tintColor = .COLOR_PRIMARY

        imvLine5.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine5.tintColor = .COLOR_PRIMARY

        imvLine6.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine6.tintColor = .COLOR_PRIMARY

        imvLine7.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine7.tintColor = .COLOR_PRIMARY

        imvLine8.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine8.tintColor = .COLOR_PRIMARY

        imvLine9.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine9.tintColor = .COLOR_PRIMARY

        imvLine10.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine10.tintColor = .COLOR_PRIMARY

        imvLine11.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine11.tintColor = .COLOR_PRIMARY

        imvLine12.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine12.tintColor = .COLOR_PRIMARY

        imvLine13.image = imvLine1.image?.withRenderingMode(.alwaysTemplate)
        imvLine13.tintColor = .COLOR_PRIMARY
    }

    @IBAction func handleMontlyClicked(_ sender: Any) {
        handleSegmentState()
        isMonthlySelect = !isMonthlySelect
    }

    @IBAction func handleYearlyClicked(_ sender: Any) {
        handleSegmentState()
        isMonthlySelect = !isMonthlySelect
    }

    @IBAction func handleBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func handleIAP(_ sender: Any) {

        if self.btnDowngrade.titleLabel?.text == "Upgrade" {
            if curPlan == "Free" {
                if isMonthlySelect {
                    let product = inAppPurchaseHelper?.getProduct()?.filter { $0.productIdentifier == self.plusMonthlyIdentifier}
                    inAppPurchaseHelper?.buyProduct((product?.first)!)
                } else {
                    let product = inAppPurchaseHelper?.getProduct()?.filter { $0.productIdentifier == self.plusYearlyIdentifier}
                    inAppPurchaseHelper?.buyProduct((product?.first)!)
                }
            } else if curPlan == "Month" {
                let product = inAppPurchaseHelper?.getProduct()?.filter { $0.productIdentifier == self.plusYearlyIdentifier}
                inAppPurchaseHelper?.buyProduct((product?.first)!)
            }
        } else if self.btnDowngrade.titleLabel?.text == "Downgrade" {
            //handle cancel subcription before buying new package
            let product = inAppPurchaseHelper?.getProduct()?.filter { $0.productIdentifier == self.plusMonthlyIdentifier}
            inAppPurchaseHelper?.buyProduct((product?.first)!)
        }
    }
}
