//
//  TeamPlansTableVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/18.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class TeamPlansTableVC: UITableViewController {

    @IBOutlet weak var btFree: UIButton!
    @IBOutlet weak var btCore: UIButton!

    private let plusMonthlyIdentifier = "com.rock.beclutch.plusPlanMonthly"
    private let plusYearlyIdentifier = "com.rock.beclutch.yearlyPlusPlan"

    private var inAppPurchaseHelper: IAPHelper?

    weak var parentDelegate: TeamPlanViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        updatePlan()

        inAppPurchaseHelper = IAPHelper(productIds: [self.plusMonthlyIdentifier, self.plusYearlyIdentifier])
        inAppPurchaseHelper?.requestProducts { [weak self] (_, products) in
            products
            self?.updatePlan()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(updatePlan), name: .IAPHelperPurchaseNotification, object: nil)
    }

    @objc func updatePlan() {
        DispatchQueue.main.async {
            let curPlan = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PLAN_LEVEL
            if curPlan == "0" {
                self.btFree.setTitle("CURRENT PLAN", for: .normal)
                self.btFree.isEnabled = false
                self.btCore.setTitle("UPGRADE", for: .normal)
                self.btCore.isEnabled = true
            } else if curPlan == "2" {
                self.btFree.setTitle("DOWNGRADE", for: .normal)
                self.btFree.isEnabled = true
                self.btCore.setTitle("CURRENT PLAN", for: .normal)
                self.btCore.isEnabled = false
            }
        }
    }

    @IBAction func onClickFree(_ sender: Any) {
        self.parentDelegate?.viewModel.updatePlan(plan: "0")
    }

    @IBAction func onClickCore(_ sender: Any) {
        self.parentDelegate?.viewModel.updatePlan(plan: "2")

//        inAppPurchaseHelper?.buyProduct()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

}
