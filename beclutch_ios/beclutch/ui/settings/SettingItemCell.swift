//
//  SettingItemCell.swift
//  beclutch
//
//  Created by FAT_MAC on 7/3/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SettingItemCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconBackView: UIView!
    @IBOutlet weak var titleLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.iconImage?.image = self.iconImage?.image!.withRenderingMode(.alwaysTemplate)
        self.iconImage?.tintColor = UIColor.white
        self.iconBackView?.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        self.iconBackView?.layer.cornerRadius = min((self.iconBackView?.frame.size.height)!, (self.iconBackView?.frame.size.width)!) / 2.0
        self.iconBackView?.layer.masksToBounds = true
        self.iconBackView?.clipsToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
