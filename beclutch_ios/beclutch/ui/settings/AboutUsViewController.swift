//
//  AboutUsViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var versionLabel: UILabel!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_about")!
        if indexPath.row == 0 {
            cell.textLabel?.text = "Privacy Policy"
        } else {
            cell.textLabel?.text = "Terms of Use"
        }
        cell.textLabel?.setUpDefaultFont()
        cell.backgroundView = nil
        cell.backgroundColor = UIColor.clear
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        cell.tintColor = UIColor.white
        let image = UIImage(named: "ic_next")?.withRenderingMode(.alwaysTemplate)
        let next_indicator = UIImageView(frame: CGRect(x: 0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!))
        next_indicator.image = image
        cell.accessoryView = next_indicator
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    @IBOutlet weak var tb: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        tb.separatorColor = UIColor.white
        tb.tableFooterView = UIView()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        versionLabel.text = "BeClutch \(CommonUtils.appVersion)"
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //privacy
            UIApplication.shared.open(URL(string: "https://beclutch.club/privacy.html")!, options: [:], completionHandler: nil)
        } else {
            //terms
            UIApplication.shared.open(URL(string: "https://beclutch.club/termsofuse.html")!, options: [:], completionHandler: nil)
        }
    }
}
