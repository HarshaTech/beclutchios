//
//  AccountInfoVC.swift
//  beclutch
//
//  Created by FAT_MAC on 7/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import InputMask
import SwiftEventBus
import Observable

class AccountInfoVC: UIViewController, IStateChooser {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var splitView: UIView!
    @IBOutlet weak var resetView: UIView!
    @IBOutlet weak var infoView: UIView!

    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var chkAutoLogin: BEMCheckBox!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var resetPwdBtn: UIButton!
    @IBOutlet weak var splitAccountBtn: UIButton!

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCell: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UIButton!
    @IBOutlet weak var txtZip: UITextField!

    var isAccountEdit = false

    private var disposal = Disposal()
    var viewModel: AccountInfoViewModel = AccountInfoViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
        initData()

        SwiftEventBus.onMainThread(self, name: "UpdateAccountInfoResult", handler: {
            res in
            let resAPI = res?.object as! AccountInfoRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                AppDataInstance.instance.me?.FNAME = self.txtFirstName.text!
                AppDataInstance.instance.me?.LNAME = self.txtLastName.text!
                AppDataInstance.instance.me?.EMAIL = self.txtEmail.text!
                AppDataInstance.instance.me?.PHONE = self.txtCell.text!
                AppDataInstance.instance.me?.ADDR = self.txtAddress.text!
                AppDataInstance.instance.me?.CITY = self.txtCity.text!
                AppDataInstance.instance.me?.STATE = self.txtState.currentTitle!
                AppDataInstance.instance.me?.ZIP = self.txtZip.text!

                if self.chkAutoLogin.on {
                    PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: true)
                } else {
                    PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: false)
                }
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save Account information. Try again.", parent: self)
                self.initData()
            }
        })
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = textField.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        textField.text = output
    }

    func configUI() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        infoView.applyShadow()
        resetView.applyShadow()
        splitView.applyShadow()
        helpBtn.contentHorizontalAlignment = .fill
        helpBtn.contentVerticalAlignment = .fill
        helpBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        helpBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        helpBtn.alpha = 0.7
        chkAutoLogin.boxType = .square
        resetPwdBtn.makeGreenButton()
        splitAccountBtn.makeGreenButton()
        splitAccountBtn.alpha = 0.7
        txtState.makeGrayRadius()
        txtCell.addTarget(self, action: #selector(AccountInfoVC.textFieldDidChange(_:)), for: .editingChanged)

        if PreferenceMgr.readBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN) {
            chkAutoLogin.setOn(true, animated: true)
        } else {
            chkAutoLogin.setOn(false, animated: true)
        }

        txtEmail.isUserInteractionEnabled = false
        setEditable(editable: false)
    }

    func initData() {
        txtFirstName.text = AppDataInstance.instance.me?.FNAME
        txtLastName.text = AppDataInstance.instance.me?.LNAME
        txtEmail.text = AppDataInstance.instance.me?.EMAIL
        txtCell.text = AppDataInstance.instance.me?.PHONE
        txtAddress.text = AppDataInstance.instance.me?.ADDR
        txtCity.text = AppDataInstance.instance.me?.CITY
        txtState.setTitle(AppDataInstance.instance.me?.STATE, for: .normal)
        txtZip.text = AppDataInstance.instance.me?.ZIP
    }

    func setEditable(editable: Bool) {
        txtFirstName.isEnabled = editable
        txtLastName.isEnabled = editable
        txtCell.isEnabled = editable
        txtAddress.isEnabled = editable
        txtCity.isEnabled = editable
        txtState.isEnabled = editable
        txtZip.isEnabled = editable
    }

    @IBAction func onEditSaveClicked(_ sender: Any) {
        if self.isAccountEdit {
            if checkValidate() {
                self.isAccountEdit = false
                editBtn.setImage(UIImage(named: "ic_edit.png"), for: .normal)
                setEditable(editable: false)

                AlertUtils.showSaveAlertWithHandler(title: "Save Account Info", message: "Do you want to update user info?", parent: self, handler_cancel: {
                    self.initData()
                }, handler_save: {
                    self.saveAccountInfo()
                })

            }
        } else {
            self.isAccountEdit = true
            editBtn.setImage(UIImage(named: "ic_save.png"), for: .normal)
            setEditable(editable: true)
        }
    }

    func checkValidate() -> Bool {
        if txtFirstName.text!.isEmpty {
            alert(title: "Warning", message: "Please enter first name")
            return false
        }
        if txtLastName.text!.isEmpty {
            alert(title: "Warning", message: "Please enter last name")
            return false
        }
        return true
    }
    @IBAction func onStateClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ChooseStateViewController") as! ChooseStateViewController
        viewController.parentListener = self
        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseState(state: String, idx: Int) {
        self.txtState.setTitle(state, for: .normal)
    }

    @IBAction func onAutoLoginChecked(_ sender: Any) {
        if self.chkAutoLogin.on {
            PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: true)
        } else {
            PreferenceMgr.saveBoolean(keyName: PreferenceMgr.IS_AUTO_LOGIN, boolValue: false)
        }
    }
    @IBAction func onResetPasswordClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ForgotNavController") as! UINavigationController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func saveAccountInfo() {
        self.viewModel.doSaveAccount(fname: txtFirstName.text!, lname: txtLastName.text!, phone: txtCell.text!, addr: txtAddress.text!, city: txtCity.text!, state: txtState.currentTitle!, zip: txtZip.text!)
    }
}
