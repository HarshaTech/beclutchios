//
//  LocationAddViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class LocationAddViewController: UIViewController, IStateChooser {

    @IBOutlet weak var txtPageTitle: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btCountry: UIButton!
    @IBOutlet weak var btState: UIButton!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtLink: UITextField!
    @IBOutlet weak var txtNote: UITextField!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    private var disposal = Disposal()
    var viewModel: LocationAddViewModel = LocationAddViewModel()
    var isEdit: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        self.btState.layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        self.btState.layer.borderWidth = 1
        self.btState.layer.cornerRadius = 5

        self.btCountry.layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        self.btCountry.layer.borderWidth = 1
        self.btCountry.layer.cornerRadius = 5

        if !self.isEdit {
            self.txtPageTitle.text = "Add New Location"
        } else {
            self.txtPageTitle.text = "Edit Location"

            self.viewModel.state.value = self.viewModel.originLocation!.state!
            self.viewModel.stateIdx = Int(self.viewModel.originLocation!.stateIdx!)!
            self.txtZip.text = self.viewModel.originLocation?.zip!
            self.txtName.text = self.viewModel.originLocation?.name
            self.txtCity.text = self.viewModel.originLocation?.city
            self.txtStreet.text = self.viewModel.originLocation?.street
            self.txtLink.text = self.viewModel.originLocation?.link
            self.txtNote.text = self.viewModel.originLocation?.note
        }

        self.viewModel.country.value = "United States"
        self.viewModel.countryIdx = 0

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.state.observe(DispatchQueue.main) {
            newVal, _ in
            if newVal.isEmpty {
                self.btState.setTitle("  Choose State", for: .normal)
            } else {
                self.btState.setTitle("  " + newVal, for: .normal)
            }
            }.add(to: &disposal)

        self.viewModel.country.observe(DispatchQueue.main) {
            _, _ in
                self.btCountry.setTitle("  United States", for: .normal)
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LocationAddRes", handler: {
            res in
            let resAPI = res?.object as! LocationAddRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save location information. Try again.", parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "LocationUpdateRes", handler: {
            res in
            let resAPI = res?.object as! LocationUpdateRes
            self.viewModel.isBusy.value = false
            if (resAPI.result ?? false) == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to save location information. Try again.", parent: self)
            }
        })
    }

    @IBAction func onClickState(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ChooseStateViewController") as! ChooseStateViewController
        viewController.parentListener = self
        viewController.selectionString = self.viewModel.state.value
        self.present(viewController, animated: true, completion: nil)
    }

    func onChooseState(state: String, idx: Int) {
        self.viewModel.state.value = state
        self.viewModel.stateIdx = idx
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if self.txtName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Location name is required", parent: self)
            return false
        }
        if self.viewModel.state.value.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "State is required", parent: self)
            return false
        }

        if self.txtZip.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Zip is required", parent: self)
            return false
        }
        return true
    }
    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            if self.isEdit {
                self.viewModel.doLocationUpdate(name: txtName.text!, city: txtCity.text!, street: txtStreet.text!, zip: txtZip.text!, link: txtLink.text!, note: txtNote.text!)
            } else {
                self.viewModel.doLocationAdd(name: self.txtName.text!, city: self.txtCity.text!, street: self.txtStreet.text!, zip: txtZip.text!, link: txtLink.text!, note: txtNote.text!)
            }
        }
    }
}
