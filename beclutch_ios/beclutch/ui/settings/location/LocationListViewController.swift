import UIKit
import Observable
import SwiftEventBus

protocol ILocationChooser: class {
    func onChooseLocation(loc: TeamLocation)
}

class LocationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btAddRound: UIButton!
    @IBOutlet weak var txtToolbarTitle: UILabel!
    @IBOutlet weak var tb: UITableView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    private var disposal = Disposal()
    weak var parentDelegate: ILocationChooser?
    var viewModel: LocationListViewModel = LocationListViewModel()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataProvider.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OpponentListCell") as! OpponentListCell

        cell.imgOppo.layer.cornerRadius = cell.imgOppo.frame.width / 2

        cell.txtOppoName.text = self.viewModel.dataProvider[indexPath.row].name
        cell.txtOppoContact.text = StringCheckUtils.getAddressFromLocation(model: self.viewModel.dataProvider[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewModel.isForChoose == false {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextCont = mainStoryboard.instantiateViewController(withIdentifier: "LocationAddViewController") as! LocationAddViewController
            nextCont.isEdit = true
            nextCont.viewModel.originLocation = self.viewModel.dataProvider[indexPath.row]
            nextCont.modalPresentationStyle = .fullScreen
            self.present(nextCont, animated: true, completion: nil)
        } else {
            self.parentDelegate?.onChooseLocation(loc: self.viewModel.dataProvider[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btAddRound.backgroundColor = UIColor.COLOR_ACCENT
        btAddRound.layer.cornerRadius = btAddRound.frame.width / 2
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LocationLoadRes", handler: {
            res in
            let regRes = res?.object as! LocationLoadRes
            self.viewModel.isBusy.value = false
            if (regRes.result ?? false) == true {
                self.viewModel.dataProvider = regRes.locationList!
                self.tb.reloadData()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to get location list. Try again.", parent: self)
            }
        })

    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.doLoadLocations()
    }

    @IBAction func onClickAddToolbar(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextCont = mainStoryboard.instantiateViewController(withIdentifier: "LocationAddViewController") as! LocationAddViewController
        nextCont.isEdit = false
        nextCont.modalPresentationStyle = .fullScreen
        self.present(nextCont, animated: true, completion: nil)
    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
