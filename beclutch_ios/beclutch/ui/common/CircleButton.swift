//
//  CircleButton.swift
//  beclutch
//
//  Created by NamViet on 4/30/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class CircleButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = self.width/2.0
        self.backgroundColor = .colorRed
    }
}
