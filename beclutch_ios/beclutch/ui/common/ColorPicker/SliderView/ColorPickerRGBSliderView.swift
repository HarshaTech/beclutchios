//
//  ColorPickerRGBSliderView.swift
//  ColorPicker
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 Jack Ily. All rights reserved.
//

import UIKit

class ColorPickerRGBSliderView: UIView, ColorPicker {

    // MARK: - IBOutlet
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var hexColorTF: UITextField!

    @IBOutlet weak var redSlider: ColorPickerSlider!
    @IBOutlet weak var redLbl: UILabel!

    @IBOutlet weak var greenSlider: ColorPickerSlider!
    @IBOutlet weak var greenLbl: UILabel!

    @IBOutlet weak var blueSlider: ColorPickerSlider!
    @IBOutlet weak var blueLbl: UILabel!

    @IBOutlet weak var opacitySlider: ColorPickerOpacitySlider!
    @IBOutlet weak var opacityLbl: UILabel!

    // MARK: - Properties
    weak var delegate: ColorPickerDelegate?

    var selectedColor: UIColor = .white {
        didSet {
            colorView.backgroundColor = selectedColor

            var r: CGFloat = 0.0
            var g: CGFloat = 0.0
            var b: CGFloat = 0.0
            var alpha: CGFloat = 0.0
            selectedColor.getRed(&r, green: &g, blue: &b, alpha: &alpha)

            r = r*255
            redLbl.text = "\(NSString(format: "%.0f", r))"
            redSlider.value = Float(r)

            g = g*255
            greenLbl.text = "\(NSString(format: "%.0f", g))"
            greenSlider.value = Float(g)

            b = b*255
            blueLbl.text = "\(NSString(format: "%.0f", b))"
            blueSlider.value = Float(b)

            alpha = alpha*100
            opacityLbl.text = "\(NSString(format: "%.0f", alpha))"
            opacitySlider.value = Float(alpha)

            hexColorTF.text = getHexStr(selectedColor).uppercased()
            setSliderColor(selectedColor)
        }
    }

    private let identifier = "ColorPickerRGBSliderView"
    private var previousTxt = ""

    // MARK: - Initialize
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNib()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        hexColorTF.addTarget(self, action: #selector(hexColorDidChange), for: .editingChanged)
        hexColorTF.delegate = self
    }

    @objc func hexColorDidChange(_ tf: UITextField) {
        guard let txt = tf.text else { return }

        let characterSet = CharacterSet(charactersIn: "0123456789abcdefABCDEF")
        let strCharacterSet = CharacterSet(charactersIn: txt)

        if !characterSet.isSuperset(of: strCharacterSet) {
            tf.text = previousTxt
            return
        }

        if txt.count != 6 { return }

        let alpha = NSString(string: opacityLbl.text!).floatValue/100
        let color = getHexColor(hexStr: txt, alpha: CGFloat(alpha))

        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        color.getRed(&r, green: &g, blue: &b, alpha: nil)

        r = r*255
        redLbl.text = "\(NSString(format: "%.0f", r))"
        redSlider.value = Float(r)

        g = g*255
        greenLbl.text = "\(NSString(format: "%.0f", g))"
        greenSlider.value = Float(g)

        b = b*255
        blueLbl.text = "\(NSString(format: "%.0f", b))"
        blueSlider.value = Float(b)
        didSelect(color)
    }

    // MARK: - IBAction
    @IBAction func changedRed(_ sender: UISlider) {
        redLbl.text = "\(NSString(format: "%.0f", sender.value))"
        hexColorTF.text = getHexStr(getColor()).uppercased()
        didSelect(getColor())
    }

    @IBAction func changedGreen(_ sender: UISlider) {
        greenLbl.text = "\(NSString(format: "%.0f", sender.value))"
        hexColorTF.text = getHexStr(getColor()).uppercased()
        didSelect(getColor())
    }

    @IBAction func changedBlue(_ sender: UISlider) {
        blueLbl.text = "\(NSString(format: "%.0f", sender.value))"
        hexColorTF.text = getHexStr(getColor()).uppercased()
        didSelect(getColor())
    }

    @IBAction func changedOpacity(_ sender: UISlider) {
        opacityLbl.text = "\(NSString(format: "%.0f", sender.value))"
        hexColorTF.text = getHexStr(getColor()).uppercased()
        didSelect(getColor())
    }
}

// MARK: - Configures

extension ColorPickerRGBSliderView {

    func loadNib() {
        let bundle = Bundle(for: ColorPickerRGBSliderView.self)
        let view = bundle.loadNibNamed(identifier, owner: self, options: nil)?.first as! UIView
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)

        colorView.clipsToBounds = true
        colorView.layer.cornerRadius = colorView.frame.height/2
    }

    func getColor() -> UIColor {
        let r = CGFloat(NSString(string: redLbl.text!).floatValue/255)
        let g = CGFloat(NSString(string: greenLbl.text!).floatValue/255)
        let b = CGFloat(NSString(string: blueLbl.text!).floatValue/255)
        let alpha = CGFloat(NSString(string: opacityLbl.text!).floatValue/100)
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }

    func getHexStr(_ color: UIColor) -> String {
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        color.getRed(&r, green: &g, blue: &b, alpha: &alpha)
        let rgb: Int = Int(r*255)<<16 | Int(g*255)<<8 | Int(b*255)<<0
        return "\(NSString(format: "%06x", rgb))"
    }

    func getHexColor(hexStr: String, alpha: CGFloat) -> UIColor {
        let hexString = hexStr.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: hexString)
        var color: UInt64 = 0

        if scanner.scanHexInt64(&color) {
            let r = CGFloat((color & 0xFF0000)>>16) / 255.0
            let g = CGFloat((color & 0xFF00)>>8) / 255.0
            let b = CGFloat((color & 0xFF)) / 255.0
            return UIColor(red: r, green: g, blue: b, alpha: alpha)

        } else {
            return .white
        }
    }

    func didSelect(_ color: UIColor) {
        colorView.backgroundColor = color
        setSliderColor(color)
        delegate?.colorPicker(self, didSelect: color)
    }

    func setSliderColor(_ color: UIColor) {
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        color.getRed(&r, green: &g, blue: &b, alpha: nil)

        let startR = UIColor(red: 0.0, green: g, blue: b, alpha: 1.0)
        let endR = UIColor(red: 1.0, green: g, blue: b, alpha: 1.0)
        redSlider.setGradient(startColor: startR, endColor: endR)

        let startG = UIColor(red: r, green: 0.0, blue: b, alpha: 1.0)
        let endG = UIColor(red: r, green: 1.0, blue: b, alpha: 1.0)
        greenSlider.setGradient(startColor: startG, endColor: endG)

        let startB = UIColor(red: r, green: g, blue: 0.0, alpha: 1.0)
        let endB = UIColor(red: r, green: g, blue: 1.0, alpha: 1.0)
        blueSlider.setGradient(startColor: startB, endColor: endB)

        let endO = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        opacitySlider.setGradient(startColor: .clear, endColor: endO)
    }

    func closeKeyboard() {
        hexColorTF.resignFirstResponder()
    }
}

// MARK: - ColorPickerRGBSliderView

extension ColorPickerRGBSliderView: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text!.uppercased()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentTxt = textField.text ?? ""
        let prospectiveTxt = NSString(string: currentTxt).replacingCharacters(in: range, with: string)
        let maxInputLength = 6
        if prospectiveTxt.count > maxInputLength { return false }

        previousTxt = currentTxt
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
