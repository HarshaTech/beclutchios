import UIKit

class ChooseStateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tb: UITableView!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var cancelBtn: UIBarButtonItem!

    let states = ["Alaska",
                  "Alabama",
                  "Arkansas",
                  "American Samoa",
                  "Arizona",
                  "California",
                  "Colorado",
                  "Connecticut",
                  "District of Columbia",
                  "Delaware",
                  "Florida",
                  "Georgia",
                  "Guam",
                  "Hawaii",
                  "Iowa",
                  "Idaho",
                  "Illinois",
                  "Indiana",
                  "Kansas",
                  "Kentucky",
                  "Louisiana",
                  "Massachusetts",
                  "Maryland",
                  "Maine",
                  "Michigan",
                  "Minnesota",
                  "Missouri",
                  "Mississippi",
                  "Montana",
                  "North Carolina",
                  " North Dakota",
                  "Nebraska",
                  "New Hampshire",
                  "New Jersey",
                  "New Mexico",
                  "Nevada",
                  "New York",
                  "Ohio",
                  "Oklahoma",
                  "Oregon",
                  "Pennsylvania",
                  "Puerto Rico",
                  "Rhode Island",
                  "South Carolina",
                  "South Dakota",
                  "Tennessee",
                  "Texas",
                  "Utah",
                  "Virginia",
                  "Virgin Islands",
                  "Vermont",
                  "Washington",
                  "Wisconsin",
                  "West Virginia",
                  "Wyoming"]

    var selectionString: String=""
    var selectionIndex: Int = -1
    var parentListener: IStateChooser?

    override func viewDidLoad() {
        super.viewDidLoad()

        doneBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        cancelBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return states.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "state_cell", for: indexPath)
        cell.textLabel?.text = states[indexPath.row]
        if self.states[indexPath.row] == self.selectionString {
            cell.accessoryType = .checkmark
            self.selectionIndex = indexPath.row
        } else {
            cell.accessoryType = .none
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectionString = states[indexPath.row]
        self.selectionIndex = indexPath.row
        self.tb!.reloadData()
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickDone(_ sender: Any) {
        if self.selectionString == "" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose state", parent: self)
            return
        }
        if self.parentListener != nil {
            self.parentListener?.onChooseState(state: self.selectionString, idx: self.selectionIndex)
        }
        self.dismiss(animated: true, completion: nil)
    }

}
