//
//  ChooseColorViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/10.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
//import IGColorPicker

protocol IColorPickerDelegate: class {
    func onSetColor(selCol: UIColor, type: ColorPickerForType)
}

enum ColorPickerForType {
    case HOME_COLOR
    case AWAY_COLOR
    case ALTER_1_COLOR
    case ALTER_2_COLOR
}

class ChooseColorViewController: UIViewController {

//    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
//        self.selIdx = indexPath.row
//    }
//
//
//    @IBOutlet weak var colorPicker: ColorPickerView!

    // MARK: - IBOutlet
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBOutlet weak var wheelView: ColorPickerWheelView!
    @IBOutlet weak var swatchesView: ColorPickerSwatchesView!
    @IBOutlet weak var sliderView: ColorPickerRGBSliderView!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var cancelBtn: UIBarButtonItem!

    // MARK: - Properties
    weak var parentDelegate: IColorPickerDelegate?
//    var selIdx:Int?
    var colorPickType: ColorPickerForType?

    enum SegmentIndex: Int {
        case picker = 0, table, slider
    }

    weak var delegate: ColorPickerDelegate?
    var selectedColor: UIColor = .white

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        self.colorPicker.delegate = self
        self.colorPicker.layoutDelegate = self
        self.colorPicker.style = .circle
        self.colorPicker.selectionStyle = .check
        self.colorPicker.isSelectedColorTappable = false
        self.colorPicker.scrollToPreselectedIndex = false
        
        var colors = colorPicker.colors
        
        let addedColors: [UIColor] = [.black, .red, .blue, .green]
        colors.insert(contentsOf: addedColors, at: 0)
        
        colorPicker.colors = colors
        */

        setupUI()
    }

    // MARK: - IBAction
    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

     @IBAction func onClickDone(_ sender: Any) {
//        if self.selIdx == nil {
//            return
//        }

        self.dismiss(animated: true, completion: {
            if self.parentDelegate != nil {
//                self.parentDelegate?.onSetColor(selCol: (self.colorPicker!.colors[self.selIdx!]), type:self.colorPickType!)
                self.parentDelegate?.onSetColor(selCol: (self.selectedColor), type: self.colorPickType!)
            }
        })
     }

    @IBAction func segmentAct(_ sender: UISegmentedControl) {
        sliderView.closeKeyboard()
        guard let index = SegmentIndex.init(rawValue: sender.selectedSegmentIndex) else { return }

        switch index {
        case .picker:
            wheelView.isHidden = false
            swatchesView.isHidden = true
            sliderView.isHidden = true
        case .table:
            swatchesView.reloadData()
            wheelView.isHidden = true
            swatchesView.isHidden = false
            sliderView.isHidden = true
        case .slider:
            wheelView.isHidden = true
            swatchesView.isHidden = true
            sliderView.isHidden = false
        }
    }

    /*
    func colorPickerView(_ colorPickerView: ColorPickerView, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // The size for each cell
        // 👉🏻 WIDTH AND HEIGHT MUST BE EQUALS!
        return CGSize(width: 68, height: 68)
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        // Space between cells
        return 24
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        // Space between rows
        return 24
    }
    
    func colorPickerView(_ colorPickerView: ColorPickerView, insetForSectionAt section: Int) -> UIEdgeInsets {
        // Inset used aroud the view
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    */
}

// MARK: - Configures

extension ChooseColorViewController {

    func setupUI() {

        segmentedControl.selectedSegmentIndex = 1

        wheelView.isHidden = true
        swatchesView.isHidden = false
        sliderView.isHidden = true

        wheelView.delegate = self
        wheelView.selectedColor = selectedColor

        swatchesView.delegate = self
        swatchesView.selectedColor = selectedColor

        sliderView.delegate = self
        sliderView.selectedColor = selectedColor

        doneBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        cancelBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
    }
}

// MARK: - ColorPicker

extension ChooseColorViewController: ColorPicker {}

// MARK: - ColorPickerDelegate

extension ChooseColorViewController: ColorPickerDelegate {

    func colorPicker(_ cp: ColorPicker, didSelect color: UIColor) {
        selectedColor = color

        if cp == wheelView {
            swatchesView.selectedColor = color
            sliderView.selectedColor = color

        } else if cp == swatchesView {
            wheelView.selectedColor = color
            sliderView.selectedColor = color

        } else if cp == sliderView {
            wheelView.selectedColor = color
            swatchesView.selectedColor = color
        }
    }
}
