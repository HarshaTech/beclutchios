//
//  UIColor+Ext.swift
//  beclutch
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

extension UIColor {

    convenience init(kHex: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((kHex & 0xFF0000)>>16) / 255.0
        let g = CGFloat((kHex & 0xFF00)>>8) / 255.0
        let b = CGFloat(kHex & 0xFF) / 255.0

        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
}
