//
//  ColorPickerSwatchesCVCell.swift
//  beclutch
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

struct CellInfo {

    var color = UIColor.clear

    init(color: UIColor) {
        self.color = color
    }
}

class ColorPickerSwatchesCVCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var colorView: UIView!

    // MARK: - Properties
    static let identifier = "ColorPickerSwatchesCVCell"

    var cellInfo: CellInfo! {
        didSet {
            colorView.backgroundColor = cellInfo.color
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let width = UIScreen.main.bounds.width
        let cellSize = (width-10*6)/5
        colorView.clipsToBounds = true
        colorView.layer.cornerRadius = cellSize/2
        colorView.layer.borderColor = UIColor.darkGray.cgColor
        colorView.layer.borderWidth = 0.5
    }
}
