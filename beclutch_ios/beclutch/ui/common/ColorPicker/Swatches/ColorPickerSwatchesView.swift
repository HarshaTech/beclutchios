//
//  ColorPickerSwatchesView.swift
//  ColorPicker
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 Jack Ily. All rights reserved.
//

import UIKit

class ColorPickerSwatchesView: UIView, ColorPicker {

    // MARK: - IBOutlet
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var opacitySlider: ColorPickerOpacitySlider!
    @IBOutlet weak var opacityLbl: UILabel!

    // MARK: - Properties
    weak var delegate: ColorPickerDelegate?
    var selectedColor: UIColor = .white {
        didSet {
            colorView.backgroundColor = selectedColor

            var alpha: CGFloat = 0.0
            selectedColor.getRed(nil, green: nil, blue: nil, alpha: &alpha)

            alpha = alpha*100
            opacityLbl.text = "\(NSString(format: "%.0f", alpha))"
            opacitySlider.value = Float(alpha)
        }
    }

    private let dataList = [
        CellInfo(color: UIColor.black),
        CellInfo(color: UIColor.white),
        CellInfo(color: UIColor.blue),
        CellInfo(color: UIColor.green),
        CellInfo(color: UIColor.red),
        CellInfo(color: UIColor.yellow),
        CellInfo(color: UIColor.purple),
        CellInfo(color: UIColor.orange),
        CellInfo(color: UIColor.cyan),
        CellInfo(color: UIColor(kHex: 0xFF8A80)),
        CellInfo(color: UIColor(kHex: 0xEA80FC)),
        CellInfo(color: UIColor(kHex: 0xB388FF)),
        CellInfo(color: UIColor(kHex: 0x8C9EFF)),
        CellInfo(color: UIColor(kHex: 0x80D8FF)),
        CellInfo(color: UIColor(kHex: 0x84FFFF)),
        CellInfo(color: UIColor(kHex: 0xFF1744)),
        CellInfo(color: UIColor(kHex: 0xD500F9)),
        CellInfo(color: UIColor(kHex: 0x651FFF)),
        CellInfo(color: UIColor(kHex: 0x00B0FF)),
        CellInfo(color: UIColor(kHex: 0x00B0FF)),
        CellInfo(color: UIColor(kHex: 0x00E5FF)),
        CellInfo(color: UIColor(kHex: 0xD50000)),
        CellInfo(color: UIColor(kHex: 0xAA00FF)),
        CellInfo(color: UIColor(kHex: 0x6200EA)),
        CellInfo(color: UIColor(kHex: 0x0091EA)),
        CellInfo(color: UIColor(kHex: 0x0091EA)),
        CellInfo(color: UIColor(kHex: 0x00B8D4)),
        CellInfo(color: UIColor(kHex: 0xB9F6CA)),
        CellInfo(color: UIColor(kHex: 0xFFFF8D)),
        CellInfo(color: UIColor(kHex: 0xFFD180)),
        CellInfo(color: UIColor(kHex: 0xFF9E80)),
        CellInfo(color: UIColor(kHex: 0xBCAAA4)),
        CellInfo(color: UIColor(kHex: 0xBDBDBD)),
        CellInfo(color: UIColor(kHex: 0x00E676)),
        CellInfo(color: UIColor(kHex: 0xFFEA00)),
        CellInfo(color: UIColor(kHex: 0xFF9100)),
        CellInfo(color: UIColor(kHex: 0xFF3D00)),
        CellInfo(color: UIColor(kHex: 0x795548)),
        CellInfo(color: UIColor(kHex: 0x616161)),
        CellInfo(color: UIColor(kHex: 0x00C853)),
        CellInfo(color: UIColor(kHex: 0xFFD600)),
        CellInfo(color: UIColor(kHex: 0xFF6D00)),
        CellInfo(color: UIColor(kHex: 0xDD2C00)),
        CellInfo(color: UIColor(kHex: 0x4E342E)),
        CellInfo(color: UIColor(kHex: 0x212121))
    ]

    private let identifier = "ColorPickerSwatchesView"

    // MARK: - Initialize
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNib()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
        collectionView.contentInsetAdjustmentBehavior = .never

        let nib = UINib(nibName: ColorPickerSwatchesCVCell.identifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: ColorPickerSwatchesCVCell.identifier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0

        let endO = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        opacitySlider.setGradient(startColor: .clear, endColor: endO)

        colorView.clipsToBounds = true
        colorView.layer.cornerRadius = colorView.frame.height/2
    }

    // MARK: - IBAction
    @IBAction func changedOpacity(_ sender: UISlider) {
        opacityLbl.text = "\(NSString(format: "%.0f", sender.value))"
        didSelect(colorView.backgroundColor!)
    }
}

// MARK: - Configures

extension ColorPickerSwatchesView {

    func loadNib() {
        let bundle = Bundle(for: ColorPickerSwatchesView.self)
        let view = bundle.loadNibNamed(identifier, owner: self, options: nil)?.first as! UIView
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }

    private func didSelect(_ color: UIColor) {
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        let alpha = NSString(string: opacityLbl.text!).floatValue/100
        color.getRed(&r, green: &g, blue: &b, alpha: nil)

        let selectColor = UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
        colorView.backgroundColor = selectColor
        delegate?.colorPicker(self, didSelect: selectColor)
    }

    func reloadData() {
        collectionView.reloadData()
        collectionView.setContentOffset(CGPoint.zero, animated: false)
    }
}

// MARK: - UICollectionViewDataSource

extension ColorPickerSwatchesView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ColorPickerSwatchesCVCell.identifier, for: indexPath) as! ColorPickerSwatchesCVCell
        cell.cellInfo = dataList[indexPath.row]
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension ColorPickerSwatchesView: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelect(dataList[indexPath.row].color)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ColorPickerSwatchesView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        let cellSize = (width-10*6)/5
        return CGSize(width: cellSize, height: cellSize)
    }
}
