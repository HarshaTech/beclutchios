//
//  ColorPickerOpacitySlider.swift
//  ColorPicker
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 Jack Ily. All rights reserved.
//

import UIKit

class ColorPickerOpacitySlider: UISlider {

    // MARK: - IBInspectable
    @IBInspectable var sliderTopSpace: CGFloat = 0.0
    @IBInspectable var sliderSideSpace: CGFloat = 0.0
    @IBInspectable var sliderColor = UIColor.clear

    // MARK: - Properties
    private let sliderLayer = CAGradientLayer()

    // MARK: - Initialiaze
    override var bounds: CGRect {
        didSet {
            drawSlider()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeView()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }

    override func draw(_ rect: CGRect) {
        drawSlider()
    }
}

// MARK: - Configures

extension ColorPickerOpacitySlider {

    func drawSlider() {
        sliderLayer.removeFromSuperlayer()

        let height = frame.height - sliderTopSpace*2
        let width = frame.width - sliderSideSpace*2
        sliderLayer.frame = CGRect(x: sliderSideSpace, y: sliderTopSpace, width: width, height: height)
        sliderLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        sliderLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        sliderLayer.cornerRadius = sliderLayer.frame.height/2
        sliderLayer.borderWidth = 0.5
        sliderLayer.borderColor = UIColor.gray.cgColor
        sliderLayer.backgroundColor = UIColor(patternImage: UIImage(named: "img-transparent.jpg")!).cgColor
        layer.insertSublayer(sliderLayer, at: 0)
    }

    private func initializeView() {
        minimumTrackTintColor = .clear
        maximumTrackTintColor = .clear

        let bundle = Bundle(for: ColorPickerOpacitySlider.self)
        if let path = bundle.path(forResource: "icon-slider-thumb@2x.png", ofType: nil) {
            setThumbImage(UIImage(contentsOfFile: path), for: .normal)
        }
    }

    func setGradient(startColor: UIColor, endColor: UIColor) {
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        sliderLayer.colors = [startColor.cgColor, endColor.cgColor]
        CATransaction.commit()
    }
}
