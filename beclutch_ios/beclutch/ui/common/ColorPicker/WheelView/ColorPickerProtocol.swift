//
//  PickerColorProtocol.swift
//  ColorPicker
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 Jack Ily. All rights reserved.
//

import UIKit

public protocol ColorPicker: NSObject {}

public protocol ColorPickerDelegate: class {
    func colorPicker(_ cp: ColorPicker, didSelect color: UIColor)
}
