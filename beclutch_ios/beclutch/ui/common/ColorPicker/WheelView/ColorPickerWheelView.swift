//
//  ColorPickerWheelView.swift
//  ColorPicker
//
//  Created by Jack Ily on 03/04/2020.
//  Copyright © 2020 Jack Ily. All rights reserved.
//

import UIKit

public let π = Double.pi
public let twoπ = 2*π

class ColorPickerWheelView: UIView, ColorPicker {

    // MARK: - IBOutlet
    @IBOutlet weak var colorView: UIView!

    @IBOutlet weak var wheelImgView: UIImageView!
    @IBOutlet weak var cursorImgView: UIImageView!

    @IBOutlet weak var brightnessLbl: UILabel!
    @IBOutlet weak var brightnessSlider: ColorPickerSlider!

    @IBOutlet weak var opacityLbl: UILabel!
    @IBOutlet weak var opacitySlider: ColorPickerOpacitySlider!

    // MARK: - Properties
    weak var delegate: ColorPickerDelegate?

    var selectedColor: UIColor = .white {
        didSet {
            colorView.backgroundColor = selectedColor

            let point = calculatePoint(selectedColor)
            cursorImgView.center = point

            var hue: CGFloat = 0.0
            var saturation: CGFloat = 0.0
            var brightness: CGFloat = 0.0
            var alpha: CGFloat = 0.0
            selectedColor.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)

            brightness = brightness*100
            brightnessLbl.text = "\(NSString(format: "%.0f", brightness))"
            brightnessSlider.value = Float(brightness)

            alpha = alpha*100
            opacityLbl.text = "\(NSString(format: "%.0f", alpha))"
            opacitySlider.value = Float(alpha)

            setSliderColor(selectedColor)
        }
    }

    private let identifier = "ColorPickerWheelView"

    // MARK: - Initialize
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNib()
    }

    override func draw(_ rect: CGRect) {
        let point = calculatePoint(selectedColor)
        cursorImgView.center = point
    }

    // MARK: - IBAction
    @IBAction func changedBrightness(_ sender: UISlider) {
        brightnessLbl.text = "\(NSString(format: "%.0f", sender.value))"
        didSelect(calculateColor(cursorImgView.center))
    }

    @IBAction func changedOpacity(_ sender: UISlider) {
        opacityLbl.text = "\(NSString(format: "%.0f", sender.value))"
        didSelect(calculateColor(cursorImgView.center))
    }
}

// MARK: - Configures

extension ColorPickerWheelView {

    func loadNib() {
        let bundle = Bundle(for: ColorPickerWheelView.self)
        let view = bundle.loadNibNamed(identifier, owner: self, options: nil)?.first as! UIView
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)

        wheelImgView.isUserInteractionEnabled = true

        let pan = UIPanGestureRecognizer(target: self, action: #selector(panAct))
        wheelImgView.addGestureRecognizer(pan)

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAct))
        wheelImgView.addGestureRecognizer(tap)

        colorView.clipsToBounds = true
        colorView.layer.cornerRadius = colorView.frame.height/2
    }

    @objc func panAct(_ sender: UIPanGestureRecognizer) {
        let point = sender.location(in: wheelImgView.superview)
        let path = UIBezierPath(ovalIn: wheelImgView.frame)
        if path.contains(point) {
            cursorImgView.center = point
            didSelect(calculateColor(point))
        }
    }

    @objc func tapAct(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: wheelImgView.superview)
        let path = UIBezierPath(ovalIn: wheelImgView.frame)
        if path.contains(point) {
            cursorImgView.center = point
            didSelect(calculateColor(point))
        }
    }

    private func setSliderColor(_ color: UIColor) {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        color.getHue(&hue, saturation: &saturation, brightness: nil, alpha: nil)

        let endColor = UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0)
        brightnessSlider.setGradient(startColor: .black, endColor: endColor)

        let endO = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        opacitySlider.setGradient(startColor: .clear, endColor: endO)
    }

    private func didSelect(_ color: UIColor) {
        setSliderColor(color)
        colorView.backgroundColor = color
        delegate?.colorPicker(self, didSelect: color)
    }
}

// MARK: - Calculate

extension ColorPickerWheelView {

    private func calculateColor(_ point: CGPoint) -> UIColor {
        let center = wheelImgView.center
        let radius = wheelImgView.frame.width/2
        let x = point.x - center.x
        let y = -(point.y - center.y)

        var radian = atan2(Float(y), Float(x))
        if radian < 0.0 { radian += Float(twoπ) }

        let distance = CGFloat(sqrtf(Float(pow(Double(x), 2) + pow(Double(y), 2))))
        let saturation = distance > radius ? 1.0 : distance/radius
        let brightness = CGFloat(brightnessSlider.value)/100
        let alpha = CGFloat(opacitySlider.value)/100
        let hue = CGFloat(radian/Float(twoπ))
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
    }

    private func calculatePoint(_ color: UIColor) -> CGPoint {
        var hue: CGFloat = 0.0
        var brightness: CGFloat = 0.0 //Độ sáng
        var alpha: CGFloat = 0.0
        var saturation: CGFloat = 0.0 //Bão hoà

        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        let center = wheelImgView.center
        let radius = wheelImgView.frame.width/2
        let angle = Float(hue) * Float(twoπ)
        let smallRadius = saturation*radius
        let x = center.x + smallRadius * CGFloat(cosf(angle))
        let y = center.y + smallRadius * CGFloat(sinf(angle)*(-1))
        let point = CGPoint(x: x, y: y)
        return point
    }
}
