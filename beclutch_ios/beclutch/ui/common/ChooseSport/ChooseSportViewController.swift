//
//  ChooseSportViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import SwiftEventBus
import RxSwift
import RxCocoa
import RxDataSources

enum ScreenSelectOptionType {
    case group
    case sport(groupId: Int)
    case role(sportId: Int)
    
    var title: String {
        switch self {
        case .sport:
            return "Choose Sport"
        case .group:
            return "Choose Group"
        case .role:
            return "Choose Role"
        }
    }
}

class ChooseSportViewController: UIViewController {
    @IBOutlet weak var tb: UITableView!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    @IBOutlet weak var titleLabel: UINavigationItem!
    
    var selectedSportItem: Production?
    var parentListener: ISportChooser?
    
    var optionType: ScreenSelectOptionType = .sport(groupId: 1)
    
    private let service: ChooseSportService = ChooseSportService()
    lazy var chooseSportDataSource = ChooseSportDataSource(production: self.selectedSportItem)
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStyle()
        
        tb.delegate = chooseSportDataSource
        
        dataObservalbe()
            .successValue()
            .map { [SectionOfProduction(header: "", items: $0.msg)] }
            .bind(to: tb.rx.items(dataSource: chooseSportDataSource.dataSource))
            .disposed(by: disposeBag)
    }
    
    deinit {
        print("ChooseSportViewController deinit")
    }
    
    private func dataObservalbe() -> Observable<Result<ProductionTypeResponse, Error>> {
        switch optionType {
        case let .sport(groupId: groupId):
            return service.getProductionGroupItems(groupId: groupId)
        case .group:
            return service.getProductionGroups()
        case let .role(sportId: sportId):
            return service.getSportRoles(sportId: sportId)
        }
    }
    
    private func setupStyle() {
        doneBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        cancelBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.COLOR_PRIMARY_DARK
        
        self.titleLabel.title = optionType.title
    }

    @IBAction func onClickDone(_ sender: Any) {
        guard let production = self.chooseSportDataSource.selectedProduction else {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Sport", parent: self)
            return
        }

        if self.parentListener != nil {
            self.parentListener?.onChooseSport(sport:production, type: optionType)
            
            self.parentListener?.onChooseSport(sport: production)
        }

        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
