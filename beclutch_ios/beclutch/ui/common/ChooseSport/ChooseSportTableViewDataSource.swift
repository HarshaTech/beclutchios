//
//  ChooseSportTableViewDataSource.swift
//  beclutch
//
//  Created by Vu Trinh on 14/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import UIKit
import RxDataSources
import RxSwift

struct SectionOfProduction {
    var header: String
    var items: [Item]
}

extension SectionOfProduction: SectionModelType {
    typealias Item = Production
    
    init(original: SectionOfProduction, items: [Item]) {
        self = original
        self.items = items
    }
}

class ChooseSportDataSource: NSObject {
    var selectedProduction: Production?
    
    init(production: Production?) {
        self.selectedProduction = production
    }
    
    lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfProduction>(
        configureCell: { [unowned self] _, tableView, indexPath, model -> UITableViewCell in
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "sport_cell", for: indexPath)
            cell.textLabel?.text = model.displayName
            cell.accessoryType = self.selectedProduction?.ID == model.ID ? .checkmark : .none
            
            return cell
        }
    )
}

extension ChooseSportDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProduction = self.dataSource[indexPath.section].items[indexPath.row]
        tableView.reloadData()
    }
}
