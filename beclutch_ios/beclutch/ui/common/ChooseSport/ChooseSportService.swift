//
//  ChooseSportService.swift
//  beclutch
//
//  Created by Vu Trinh on 13/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

import RxSwift

class ChooseSportService {
    func getProductionGroups() -> Observable<Result<ProductionTypeResponse, Error>> {
        APIManager.shared.getProductionTypes()
    }
    
    func getProductionGroupItems(groupId: Int) -> Observable<Result<ProductionTypeResponse, Error>> {
        APIManager.shared.getProductionGroupItems(groupId: groupId)
    }
    
    func getSportRoles(sportId: Int) -> Observable<Result<ProductionTypeResponse, Error>> {
        APIManager.shared.getSportRole(sportId: sportId)
    }
}
