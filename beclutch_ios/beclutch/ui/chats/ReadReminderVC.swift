//
//  ReadReminderVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/13.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ReadReminderVC: UIViewController {

    @IBOutlet weak var txtContent: UILabel!
    @IBOutlet weak var txtSubject: UILabel!
    @IBOutlet weak var txtExpirationDate: UILabel!
    @IBOutlet weak var txtToMembers: UILabel!
    @IBOutlet weak var txtSentDate: UILabel!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    var viewModel: ReadReminderViewModel = ReadReminderViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.txtContent.text  = viewModel.origin?.content
        self.txtSubject.text = viewModel.origin?.subject

        let date = Date(timeIntervalSince1970: Double(self.viewModel.origin!.expirationLong!))
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM d, YYYY"
        self.txtExpirationDate.text = formatter2.string(from: date)

        var strToMembers = ""
        if self.viewModel.origin?.strToRosters != nil {
            let data = self.viewModel.origin!.strToRosters!.data(using: .utf8)!
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                    let groupselectionRes = GroupSelectionResult(val: jsonArray as NSDictionary)
                    if groupselectionRes.isSelelctAll! {
                        strToMembers = "To All Members  "
                    } else {
                        for item in groupselectionRes.dto! {
                            strToMembers += (item.rosterName! + ", ")
                        }
                    }
                }
            } catch let error as NSError {
                print(error)
            }
        }
        if strToMembers.count > 0 {
            self.txtToMembers.text = String(strToMembers.dropLast(2))
        } else {
            self.txtToMembers.text = strToMembers
        }

        let dateCreatedAt = Date(timeIntervalSince1970: Double(self.viewModel.origin!.createdAt!))
        formatter2.dateFormat = "MMM d h:mm a"
        self.txtSentDate.text = formatter2.string(from: dateCreatedAt)
   }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        viewModel.doUpdateReminderRead()
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
