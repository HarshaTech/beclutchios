//
//  UrgentNotificationViewModel.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/28/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Observable
class UrgentNotificationViewModel {
    var isBusy = Observable(false)

    func doSendEmailToTeam(subject: String,
                           content: String) {
        let req = SendNotificationToTeamReq()
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId
        req.token = AppDataInstance.instance.getToken()
        req.subject = subject
        req.content = content

        ChatService.instance.doSendEmailToTeam(req: req)
    }

    func doSendNotificationToTeam(subject: String,
                                  content: String) {
        let req = SendNotificationToTeamReq()
        req.teamId = AppDataInstance.instance.currentSelectedTeam?.teamOrClubId
        req.token = AppDataInstance.instance.getToken()
        req.subject = subject
        req.content = content
        req.rosterId = AppDataInstance.instance.currentSelectedTeam?.ID

        ChatService.instance.doSendNotificationToTeam(req: req)
    }
}
