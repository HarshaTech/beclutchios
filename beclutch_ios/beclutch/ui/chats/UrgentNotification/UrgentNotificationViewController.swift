//
//  UrgentNotificationViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import SwiftEventBus

class UrgentNotificationViewController: UIViewController {
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var tfSubject: UITextField!
    @IBOutlet weak var tfContent: UITextField!

    var titleLabel: String?
    var viewModel = UrgentNotificationViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        SwiftEventBus.onMainThread(self, name: "DoSendEmailToTeamRes", handler: {
            res in
            let resAPI = res?.object as! GeneralRes

            if resAPI.result! {
                self.alert(title: "Notification", message: resAPI.msg ?? "", okHandler: { (_) in
                    self.dismiss(animated: true, completion: nil)
                })

            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "DoSendNotificationToTeamRes", handler: {
            res in
            let resAPI = res?.object as! GeneralRes

            if resAPI.result! {
                self.alert(title: "Notification", message: resAPI.msg ?? "", okHandler: { (_) in
                    self.dismiss(animated: true, completion: nil)
                })
            }
            self.viewModel.isBusy.value = false
        })
    }
    
    private func configUI() {
        lbTitle.text = titleLabel ?? ""

        self.view.backgroundColor = .COLOR_PRIMARY_DARK
        self.headerView.backgroundColor = .COLOR_PRIMARY_DARK

        btnSend.makeGreenButton()
        btnCancel.makeRedButton()
        
        let text = "Message"
        contentLabel.text = "\(text):"
        tfContent.placeholder = "Enter " + text
    }

    func validation() -> Bool {
        if self.tfSubject.text!.isEmpty || self.tfContent.text!.isEmpty {
            self.alert(message: "Please fill subject and content to submit.")
            return false
        }
        return true
    }
    
    private func isNotificationScreen() -> Bool {
        return titleLabel == "Urgent Notification"
    }

    @IBAction func onSubmit(_ sender: Any) {
        if !validation() {return}
        if isNotificationScreen() {
            self.viewModel.doSendNotificationToTeam(subject: self.tfSubject.text!,
                                                    content: self.tfContent.text!)
        } else {
            self.viewModel.doSendEmailToTeam(subject: self.tfSubject.text!,
                                             content: self.tfContent.text!)
        }
    }

    @IBAction func onDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
