//
//  ChooseRosterCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
class ChooseRosterCell: UITableViewCell {

    @IBOutlet weak var txtActive: UILabel!
    @IBOutlet weak var txtPos: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var chk: BEMCheckBox!
    var actionCheck: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.chk.boxType = .square
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onChangeChkVal(_ sender: Any) {
        self.actionCheck!()
    }
}
