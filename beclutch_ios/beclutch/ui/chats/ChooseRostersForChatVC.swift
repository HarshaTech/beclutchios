//
//  ChooseRostersForChatVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import Observable
import SwiftEventBus

protocol IChooseRosterForChat: class {
    func onChooseRosters(val: GroupSelectionResult)
}

class ChooseRostersForChatVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    enum ChooseRosterFor {
        case nomal
        case chat
    }

    var viewModel: ChooseRosterForChatViewModel = ChooseRosterForChatViewModel()
    weak var parentDelegate: IChooseRosterForChat?
    private var disposal = Disposal()
    @IBOutlet weak var tb: UITableView!

    @IBOutlet weak var appbarView: UIView!
    @IBOutlet var backgroundView: UIView!

    var chooseRosterFor: ChooseRosterFor = .nomal

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadMyRosterRes", handler: {
            res in
            let resAPI = res?.object as! LoadMyRosterRes

            if resAPI.result! {
                self.viewModel.dp = self.filterRosters(rosters: resAPI.rosters)

                self.tb.reloadData()
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: "Failed to create practice", parent: self)
            }
            self.viewModel.isBusy.value = false
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.viewModel.doLoadRosters()
        }
    }

    @IBAction func onClickDone(_ sender: Any) {
        let ret = GroupSelectionResult(val: nil)
        ret.isSelelctPlayers = false

        var entities = [GroupChatUserEntity]()

        for item in self.viewModel.dp {
            if item.isSel! {
                let entity = GroupChatUserEntity(rosterId: item.roster!.ID, rosterName: item.roster!.ROSTER_FIRST_NAME + " " + item.roster!.ROSTER_LAST_NAME)
                entities.append(entity)
            }
        }

        ret.isSelelctAll = entities.count - 1 == self.viewModel.dp.count
        ret.dto = entities

        self.parentDelegate?.onChooseRosters(val: ret)
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseRosterCell") as! ChooseRosterCell
        cell.chk.on = self.viewModel.dp[indexPath.row].isSel!
        if self.viewModel.dp[indexPath.row].roster!.ROSTER_PROFILE_PIC.isEmpty {
            cell.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            cell.img.kf.setImage(with: URL(string: self.viewModel.dp[indexPath.row].roster!.ROSTER_PROFILE_PIC))
        }
        cell.txtName.text = self.viewModel.dp[indexPath.row].roster!.ROSTER_FIRST_NAME + " " + self.viewModel.dp[indexPath.row].roster!.ROSTER_LAST_NAME
        cell.txtPos.text = self.viewModel.dp[indexPath.row].roster!.ROLE_INFO.ROLE_NAME

        cell.chk.isEnabled = self.viewModel.editPossible
        cell.actionCheck = {
            self.viewModel.dp[indexPath.row].isSel = cell.chk.on
        }
        cell.txtActive.layer.cornerRadius = 5
        cell.txtActive.backgroundColor = UIColor.COLOR_BLUE
        cell.txtActive.isHidden = self.viewModel.dp[indexPath.row].roster?.IS_ACTIVE != "0"
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension ChooseRostersForChatVC {
    private func filterRosters(rosters: [RosterInfo]?) -> [RosterSelectionChatDTO] {
        guard let rosters = rosters else {
            return []
        }
        
        var result: [RosterSelectionChatDTO] = []
        
        for roster in rosters {
            if let isActive = Int(roster.IS_ACTIVE), isActive > 0 {
                if !isRosterValid(roster: roster, currentRosters: result) {
                    continue
                }

                let item  = RosterSelectionChatDTO()
                item.roster = roster
                item.isSel = self.viewModel.isContainRosterInInitial(roster: roster)
                result.append(item)
            }
        }
        
        return result
    }
    
    private func isRosterValid(roster: RosterInfo, currentRosters: [RosterSelectionChatDTO]) -> Bool {
        if self.chooseRosterFor == .chat {
            if roster.ID == AppDataInstance.instance.currentSelectedTeam?.ID {
                return false
            }
            
            if roster.IS_PLAYER! == "1" {
                return false
            }
            
            if roster.LEVEL == UserRoles.GUEST {
                return false
            }
            
            for rosterChat in currentRosters {
                if roster.ROSTER_EMAIL == rosterChat.roster?.ROSTER_EMAIL {
                    return false
                }
            }
        }
        
        return true
    }
}
