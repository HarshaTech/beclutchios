//
//  AddNewReminderVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import ActionSheetPicker_3_0
import SwiftEventBus
class AddNewReminderVC: UIViewController, IChooseRosterForChat, WWCalendarTimeSelectorProtocol {

    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var btDate: UIButton!
    @IBOutlet weak var btTo: UIButton!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!

    var viewModel: AddNewReminderViewModel = AddNewReminderViewModel()
    private var disposal = Disposal()
    var expirationPicked = false

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btTo.makeGreenButton()
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        btDate.makeGrayRadius()
        self.txtContent.layer.borderColor = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        self.txtContent.layer.borderWidth = 1
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.curDate.observe(DispatchQueue.main) {
            newDate, _ in
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MMM d, yyyy"
            if self.expirationPicked {
                self.btDate.setTitle(formatter2.string(from: newDate), for: .normal)
                self.btDate.setTitleColor(.black, for: .normal)
            } else {
                self.btDate.setTitle("Enter Date", for: .normal)
                self.btDate.setTitleColor(.lightGray, for: .normal)
            }

        }.add(to: &disposal)
        self.viewModel.selResult.observe(DispatchQueue.main) {
            newSel, _ in
            if newSel.isSelelctAll! {
                self.btTo.setTitle("To all Members", for: .normal)
            } else {
                if newSel.dto?.count == 0 {
                    self.btTo.setTitle("Choose members", for: .normal)
                } else {
                    var strVal: String = ""
                    var disp: String = ""
                    for item in newSel.dto! {
                        strVal += (item.rosterName! + ", ")
                    }
                    if strVal.count > 0 {
                        disp = String(strVal.dropLast(2))
                    }
                    self.btTo.setTitle(disp, for: .normal)
                }
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "SendReminderRes", handler: {
            res in
            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! ChatServiceBaseRes
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.msg!, parent: self)
            }
        })
    }

    @IBAction func onClickToMembers(_ sender: Any) {
//        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRostersForChatVC") as! ChooseRostersForChatVC
//        nextNav.viewModel.initial = self.viewModel.selResult.value
//        nextNav.parentDelegate = self
//        self.present(nextNav, animated: true, completion: nil)
    }

    func onChooseRosters(val: GroupSelectionResult) {
        self.viewModel.selResult.value = val
    }

    @IBAction func onClickDate(_ sender: Any) {
        let selector = CalendarPickerCommon.picker()
        selector.delegate = self
        
        present(selector, animated: true, completion: nil)
    }

    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.doUploadNewReminder(subject: self.txtSubject.text!, content: self.txtContent.text!)
        }
    }

    func checkValidation() -> Bool {
        if !self.viewModel.selResult.value.isSelelctAll! && self.viewModel.selResult.value.dto?.count == 0 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Team Member", parent: self)
            return false
        }

        if self.txtSubject.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Subject is required", parent: self)
            return false
        }

        if self.txtContent.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Content is required", parent: self)
            return false
        } else if !expirationPicked {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter Expiration Date", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddNewReminderVC {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        self.viewModel.curDate.value = date
        self.expirationPicked = true
    }
}
