//
//  SelectTeamVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import Kingfisher

protocol SelectTeamVCDelegate: class {
    func didSelectTeam()
}

class SelectTeamVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tb: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var appbarView: UIView!

    var viewModel: SelectTeamViewModel = SelectTeamViewModel()
    private var disposal = Disposal()

    weak var delegate: SelectTeamVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        addBtn.backgroundColor = UIColor.COLOR_ACCENT
        addBtn.layer.cornerRadius = addBtn.width/2

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
            }.add(to: &disposal)
        self.viewModel.dp = AppDataInstance.instance.myClubTeamList

        SwiftEventBus.onMainThread(self, name: "ClubTreeLoadRes", handler: { [weak self]
            res in
            let resAPI = res?.object as! ClubTreeLoadRes
            self?.viewModel.isBusy.value = false
            if resAPI.result! {
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: resAPI.rosters)
                self?.viewModel.dp = resAPI.rosters
                self?.tb.reloadData()
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.loadClubTree()
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickRefresh(_ sender: Any) {
        self.viewModel.loadClubTree()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let rosterInfo = self.viewModel.dp?[indexPath.row] else {
            return UITableViewCell()
        }
        
        let isSubTeam = rosterInfo.TEAM_ID != "0" && rosterInfo.CLUB_ID != "0"
        let cellIdentifier = isSubTeam ? "ChildSelectTeamCell" : "SelectTeamCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SelectTeamCell

        if self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL.isEmpty {
            cell.img.image = UIImage(named: "logo")
        } else {
            cell.img.kf.setImage(with: URL(string: self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.PIC_URL))
        }

        cell.txtName.text = self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.NAME
        cell.txtPos.text = self.viewModel.dp![indexPath.row].ROLE_INFO.ROLE_NAME
        cell.txtProduction.text = self.viewModel.dp![indexPath.row].SPORT_INFO.NAME
        cell.txtClubOrTeam.text = self.viewModel.dp![indexPath.row].CLUB_OR_TEAM == "0" ? "Team" : "Club"
        if self.viewModel.dp![indexPath.row].CLUB_OR_TEAM == "0" {
            cell.txtClubOrTeam.backgroundColor = UIColor.COLOR_PRIMARY
        } else {
            cell.txtClubOrTeam.backgroundColor = UIColor.COLOR_BLUE
        }
        cell.txtClubOrTeam.layer.cornerRadius = 5
        cell.txtClubOrTeam.layer.masksToBounds = true
        cell.chatStatusView.isHidden = !CommonUtils.BLUE_DOT_LIST.contains(rosterInfo.teamIdInt)

        if self.viewModel.dp![indexPath.row].CLUB_OR_TEAM == "1" {
            cell.txtSubTeams.isHidden = false
            cell.txtSubTeams.text = self.viewModel.dp![indexPath.row].SUB_TEAMS! + " Team(s)"
        } else {
            cell.txtSubTeams.isHidden = true
            cell.txtSubTeams.text = ""
        }

        cell.txtStatus.backgroundColor = UIColor.COLOR_BLUE
        cell.txtStatus.layer.cornerRadius = 5
        cell.txtStatus.layer.masksToBounds = true

        if self.viewModel.dp![indexPath.row].IS_ACTIVE == "1" {
            cell.txtStatus.isHidden = true
        } else {
            cell.txtStatus.isHidden = false
            cell.txtStatus.text = "Status: Pending"
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AppDataInstance.instance.currentSelectedTeam?.ID != self.viewModel.dp![indexPath.row].ID {
            if self.viewModel.dp![indexPath.row].IS_ACTIVE == "0" {
                alert(title: "Warning", message: "You must accept your invitation to this team before proceeding. Please check your email for acceptance instructions. Would you like to enter the invitation code now?", okTitle: "Yes") { [weak self] _ in
                    self?.showAccpetInvitation()
                } cancelHandler: { _ in }

                return
            }
            
            if let rosterInfo = self.viewModel.dp?[indexPath.row] {
                let selectTeamId = rosterInfo.TEAM_ID.intValue()
                if CommonUtils.BLUE_DOT_LIST.contains(selectTeamId) {
                    CommonUtils.BLUE_DOT_LIST.removeAll { (teamId) -> Bool in
                        teamId == selectTeamId
                    }
                }
                
                ChatStatusManager.shared.doListenForRedBlueDot()
            }
            

            self.viewModel.doUpdateTeam(pos: indexPath.row)
            AppDataInstance.instance.currentSelectedTeam = self.viewModel.dp![indexPath.row]
            PreferenceMgr.saveTheme(themeValue: self.viewModel.dp![indexPath.row].CLUB_TEAM_INFO.COLOR_VALUE)
            self.delegate?.didSelectTeam()
        }

        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickAddMoreTeam(_ sender: Any) {
        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //sheet.view.tintColor = .primary

        let saveActionButton = UIAlertAction(title: "Create a Team", style: .default) { _ in
            self.showCreateNewTeam()
        }
        saveActionButton.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(saveActionButton)

        let deleteActionButton = UIAlertAction(title: "Join a Team", style: .default) { _ in
            self.showAccpetInvitation()
        }
        deleteActionButton.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(deleteActionButton)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .destructive) { _ in

        }
        cancelActionButton.setValue(UIColor.COLOR_ACCENT, forKey: "titleTextColor")
        sheet.addAction(cancelActionButton)

        self.present(sheet, animated: true, completion: nil)
    }

    func showCreateNewTeam() {
        let storyboard = UIStoryboard(name: "CreateNewMyTeam", bundle: nil)
        let createTeam = storyboard.instantiateViewController(withIdentifier: "CreateNewMyTeamViewController") as! CreateNewMyTeamViewController
        createTeam.modalPresentationStyle = .fullScreen
        self.present(createTeam, animated: true, completion: nil)
    }

    func showAccpetInvitation() {
        if let showVC = ViewControllerFactory.makeAcceptInvitationVC() {
            self.present(showVC, animated: true, completion: nil)
        }
    }
}
