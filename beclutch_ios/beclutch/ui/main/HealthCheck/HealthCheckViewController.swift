//
//  HealthCheckViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/20/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class HealthCheckViewController: UIViewController {
    var eventModel: EventModel?
    var eventType: CALENDAR_ITEM_TYPE = .PRACTICE_MODEL
    var status: Int?

    var submitSuccessClousure: ((Int) -> Void)?

    @IBOutlet weak var tbv: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!

    let questionsArr = [
        "Has any member of your household traveled outside the country in the past 14 days?",
        "Has any member of your household traveled to a state on your Governor's travel advisory list in the past 14 days?",
        "Has any member of your household had a fever, cough or other symptom of any illness in the past 14 days?",
        "Has any member of your household had a positive Covid test in the past 14 days?              ",
        "Has any member of your household been exposed to an individual with a presumed or known positive case of Covid 19 or any other illness in the past 14 days?"
    ]

    let colorArr: [UIColor] = [.COLOR_ATTENDING_YES, .yellow, .red, .yellow, .yellow]
    var answerArr = [-1, -1, -1, -1, -1]

    var viewModel: HealthCheckViewModel = HealthCheckViewModel()
    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateHealthCheckRes", handler: {
            res in
            let resAPI = res?.object as! GeneralRes

            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
                self.submitSuccessClousure!(self.status ?? 0)
            }
            self.viewModel.isBusy.value = false
        })
    }

    fileprivate func configUI() {
        btnCancel.makeRedButton()
        btnCancel.setTitleColor(.white, for: .normal)
        btnSubmit.makeGreenButton()
        btnSubmit.setTitleColor(.white, for: .normal)

        tbv.delegate = self
        tbv.dataSource = self

        tbv.separatorStyle = .none
        tbv.register(UINib(nibName: "AttendentCheckTableViewCell", bundle: nil), forCellReuseIdentifier: "AttendentCheckTableViewCell")
    }
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSubmit(_ sender: Any) {
        var countYES = 0
        var countNO = 0

        for i in 0..<answerArr.count {

            if answerArr[i] == 1 {
                countYES+=1
            } else if answerArr[i] == 0 {
                countNO+=1
            }

        }

        if countYES + countNO < 5 {
            self.alert(message: "Please select YES or NO")
            return
        } else {
            self.status = countNO == 5 ? 1 : 0
            self.dismiss(animated: true, completion: nil)
            self.submitSuccessClousure!(self.status ?? 0)
        }
    }

}

extension HealthCheckViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4 {
            return self.view.frame.width * CGFloat(self.questionsArr[indexPath.row].lengthOfBytes(using: .utf8)) / 360
        }
        return self.view.frame.width * CGFloat(self.questionsArr[indexPath.row].lengthOfBytes(using: .utf8)) / 280
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendentCheckTableViewCell", for: indexPath) as! AttendentCheckTableViewCell
        cell.lbContent.text = self.questionsArr[indexPath.row]
        cell.colorLineView.backgroundColor = self.colorArr[indexPath.row]
        cell.selectionStyle = .none

        if answerArr[indexPath.row] == -1 {
            cell.colorLineView.backgroundColor = .yellow
            cell.configAnttendance(attendance: "-1")
        } else if answerArr[indexPath.row] == 0 {
            cell.colorLineView.backgroundColor = .COLOR_ATTENDING_YES
            cell.configAnttendance(attendance: "0")
        } else {
            cell.colorLineView.backgroundColor = .red
            cell.configAnttendance(attendance: "1")
        }

        cell.updateAttendanceClosure = { [weak self] attendanceValue in
            guard let self = self else {
                return
            }
            self.answerArr[indexPath.row] = Int(attendanceValue) ?? -1

            if self.answerArr[indexPath.row] == -1 {
                      cell.colorLineView.backgroundColor = .yellow
            } else if self.answerArr[indexPath.row] == 0 {
                      cell.colorLineView.backgroundColor = .COLOR_ATTENDING_YES
                  } else {
                      cell.colorLineView.backgroundColor = .red
                  }

        }

        return cell
    }
}
