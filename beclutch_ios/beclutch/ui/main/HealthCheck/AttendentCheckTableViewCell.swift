//
//  AttendentCheckTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/20/20.
//  Csopyright © 2020 zeus. All rights reserved.
//

import UIKit

class AttendentCheckTableViewCell: UITableViewCell {
    @IBOutlet weak var colorLineView: UIView!
    @IBOutlet weak var lbContent: UILabel!

    @IBOutlet weak var btnYES: AttendButton!
    @IBOutlet weak var btnNO: AttendButton!

    var updateAttendanceClosure: ((_ attendance: String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()

        lbContent.numberOfLines = 0

        btnYES.setAttending(attending: Attendance.ATTENDING_YES)
        btnNO.setAttending(attending: Attendance.ATTENDING_NO)
    }

    func configAnttendance(attendance: String?) {
        if let attendance = attendance {
            btnYES.isChoose = false
            btnNO.isChoose = false

            if attendance == Attendance.ATTENDING_YES {
                btnYES.isChoose = true
            } else if attendance == Attendance.ATTENDING_NO {
                btnNO.isChoose = true
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func updateAttendancePressed(_ sender: AttendButton) {
        if sender.isChoose {
            return
        }
        configAnttendance(attendance: sender == btnYES ? Attendance.ATTENDING_YES : Attendance.ATTENDING_NO)

        updateAttendanceClosure!(sender == btnYES ? Attendance.ATTENDING_YES : Attendance.ATTENDING_NO)
    }

}
