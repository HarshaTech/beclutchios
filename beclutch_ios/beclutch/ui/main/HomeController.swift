//
//  HomeController.swift
//  beclutch
//
//  Created by zeus on 2019/11/17.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import SDVersion

class HomeController: UITabBarController {
    @IBOutlet weak var tabbar: UITabBar!

    private let blueBadgeViewTag = 1234

    enum HomeTabbarEnum: Int {
        case chat = 3
        case setting = 4
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        addBlueBadge()

        if let fcmToken = Messaging.messaging().fcmToken {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken)
            print("Fcm Token:" + fcmToken)
        }

        if SDiOSVersion.versionLessThan("13") {
            if let items = tabbar.items {
                for tabbarItem in items {
                    tabbarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
                }
            }
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabbar.tintColor = UIColor.COLOR_PRIMARY_DARK
        self.tabbar.unselectedItemTintColor = UIColor.COLOR_PRIMARY

        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(refreshChatStatus), name: .ChatStatusUpdateNotification, object: nil)
        
        if let screenIndex = NavUtil.intendedScreenType {
            self.selectedIndex = screenIndex
            NavUtil.intendedScreenType = nil
        }
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("tabBar didSelect")
        if item.tag != 5 {
            if let navigationController = self.viewControllers?.last as? UINavigationController {
                navigationController.popToRootViewController(animated: false)
            }
        }
    }

    @objc func refreshChatStatus() {
        let badge: String? = CommonUtils.IS_RED_DOT ? " " : nil
        tabbar.items?[HomeTabbarEnum.chat.rawValue].badgeValue = badge

        blueBadgeView()?.isHidden = CommonUtils.BLUE_DOT_LIST.count == 0
    }

}

extension HomeController {
    func addBlueBadge() {
//        for badgeView in tabBar.subviews[HomeTabbarEnum.chat.rawValue].subviews {
//            if NSStringFromClass(badgeView.classForCoder) == "_UIBadgeView" {
        let circleView = CircleView()
        circleView.backgroundColor = .blue
        circleView.tag = blueBadgeViewTag
        circleView.isHidden = true

        let x: CGFloat = tabbar.width/5.0 * 3.0 + 4.0
        let y: CGFloat = 4
        let height: CGFloat = 18
        let frame = CGRect(x: x, y: y, width: height, height: height)

        circleView.frame = frame
        tabbar.addSubview(circleView)
//            }
//        }
    }

    func blueBadgeView() -> UIView? {
        return tabBar.viewWithTag(blueBadgeViewTag)
    }
}
