//
//  SelectTeamCell.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class SelectTeamCell: UITableViewCell {

    @IBOutlet weak var txtSubTeams: UILabel!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtClubOrTeam: UILabel!
    @IBOutlet weak var txtPos: UILabel!
    @IBOutlet weak var txtProduction: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtName: UILabel!

    @IBOutlet weak var chatStatusView: CircleView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
