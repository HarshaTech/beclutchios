//
//  ChatHeaderCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ChatHeaderCell: UITableViewCell {
    @IBOutlet weak var topLine: UIView!

    @IBOutlet weak var txtTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
