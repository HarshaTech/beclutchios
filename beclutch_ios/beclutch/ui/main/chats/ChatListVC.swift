//
//  ChatListVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import Observable
import SwiftEventBus

import FirebaseFirestore
import GoogleMobileAds

class ChatListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AdsProtocol {

    var viewModel: ChatListViewModel = ChatListViewModel()
    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet weak var btToolAdd: UIButton!
    @IBOutlet weak var tb: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var adView: GADBannerView!

    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()

        configAdView(adView: adView, from: self)
        self.tb.separatorStyle = .none

        self.viewModel.loadingReminder.observe(DispatchQueue.main) {
            newBusy, _ in
            self.handleBusy(newBusy: newBusy)
        }.add(to: &disposal)
        self.viewModel.loadingConversation.observe(DispatchQueue.main) {
            newBusy, _ in
            self.handleBusy(newBusy: newBusy)
        }.add(to: &disposal)
        self.viewModel.loadingCarPool.observe(DispatchQueue.main) {
            newBusy, _ in
            self.handleBusy(newBusy: newBusy)
        }.add(to: &disposal)
        self.viewModel.loadingTeamChat.observe(DispatchQueue.main) {
            newBusy, _ in
            self.handleBusy(newBusy: newBusy)
        }.add(to: &disposal)

        self.viewModel.dp.observe(DispatchQueue.main) {
            _, _ in
            self.tb.reloadData()
        }.add(to: &disposal)

        self.viewModel.updatedConversationDetail.observe(DispatchQueue.main) { [weak self] (_, _) in
            guard let self = self else {
                return
            }
            
            let row = self.viewModel.updatedConversationDetail.value
            if row > 0 {
                let indexPath = IndexPath(row: row, section: 0)
                self.tb.reloadRows(at: [indexPath], with: .automatic)
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "ReminderLoadRes", handler: {
            res in
            let resAPI = res?.object as! ReminderLoadRes
            if resAPI.result! {
                self.viewModel.reminders = resAPI.reminders!
                self.viewModel.buildDataProvider()
            }
            self.viewModel.loadingReminder.value = false
        })

        SwiftEventBus.onMainThread(self, name: "ConversationEntryLoadRes", handler: {
            res in
            let resAPI = res?.object as! ConversationEntryLoadRes
            if resAPI.result! {
                let sortedConversation = resAPI.conversation_entries?.sorted(by: { (conver1, conver2) -> Bool in
                    guard let date1 = conver1.lastChatInfo?.lastSendDate, let date2 = conver2.lastChatInfo?.lastSendDate else {
                        return true
                    }
                    
                    return date1.compare(date2) == .orderedDescending
                })
                self.viewModel.updateConversationList(conversations: sortedConversation)
                self.viewModel.buildDataProvider()
            }
            self.viewModel.loadingConversation.value = false

            self.viewModel.doLoadConversationListDetail()
        })

        SwiftEventBus.onMainThread(self, name: "ConversationThreadRemoveRes", handler: {
            _ in

        })

        SwiftEventBus.onMainThread(self, name: "NewConversationAddReq", handler: {
            res in
            let resAPI = res?.object as! NewConversationAddRes
            if resAPI.result! {
                self.gotoConversationViewController(converse: resAPI.conversation)
            }
        })

        self.viewModel.lastTeamChat.observe(DispatchQueue.main) { (_, _) in
            //            self.tb.reloadRows(at: self.viewModel.indexPathsFor(section: ChatListViewItem.HEADER_TEAM_CHAT), with: .automatic)
            self.viewModel.buildDataProvider()
            self.viewModel.loadingTeamChat.value = false
        }.add(to: &disposal)

        self.viewModel.lastCarPoolChat.observe(DispatchQueue.main) { (_, _) in
            //            self.tb.reloadRows(at: self.viewModel.indexPathsFor(section: ChatListViewItem.HEADER_CARPOOL_CHAT), with: .automatic)
            self.viewModel.buildDataProvider()
            self.viewModel.loadingCarPool.value = false
        }.add(to: &disposal)

        self.viewModel.buildDataProvider()

        btToolAdd.layer.cornerRadius = btToolAdd.width/2.0
        btToolAdd.backgroundColor = UIColor.COLOR_ACCENT
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTheme()

        self.viewModel.doLoadConversationList()
        self.viewModel.doLoadTeamChatInfo()
        self.viewModel.doLoadCarPoolInfo()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.viewModel.doLoadReminderList()

        CommonUtils.IS_RED_DOT = false
        NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        viewModel.removeAllFirebaseListener()
    }
    
    // MARK: - UI
    func refreshTheme() {
        // changing work
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btSelectTeam.makeStrokeOnlyButton()
        
        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
    }

    private func handleBusy(newBusy: Bool) {
        if newBusy {
            if self.viewModel.loadingCarPool.value && self.viewModel.loadingTeamChat.value && self.viewModel.loadingConversation.value && self.viewModel.loadingReminder.value {
                self.showIndicator()
            }
        } else {
            if !self.viewModel.loadingCarPool.value && !self.viewModel.loadingTeamChat.value && !self.viewModel.loadingConversation.value && !self.viewModel.loadingReminder.value {
                self.hideIndicator()
            }
        }
    }

    @IBAction func onClickSelectTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            self.present(showVC, animated: true, completion: nil)
        }
    }

    @IBAction func onClickToolbarAdd(_ sender: Any) {
        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitation", parent: self)
        } else {
            let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            //sheet.view.tintColor = .primary

            let newConvers = UIAlertAction(title: "New Conversation", style: .default) { _ in
                self.addNewConversation()
            }
            newConvers.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
            sheet.addAction(newConvers)

            let newReminder = UIAlertAction(title: "New Reminder", style: .default) { _ in
                self.addNewReminder()
            }
            newReminder.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
            sheet.addAction(newReminder)

            let emailTeamTitle = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_email_team.rawValue)
            let emailTeam = UIAlertAction(title: emailTeamTitle, style: .default) { _ in
                self.addEmailTeam()
            }
            emailTeam.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
            sheet.addAction(emailTeam)

            let urgentNoti = UIAlertAction(title: "Urgent Notification", style: .default) { _ in
                self.addUrgentNoti()
            }
            urgentNoti.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
            sheet.addAction(urgentNoti)

            let cancelActionButton = UIAlertAction(title: "Cancel", style: .destructive) { _ in

            }
            cancelActionButton.setValue(UIColor.COLOR_ACCENT, forKey: "titleTextColor")
            sheet.addAction(cancelActionButton)

            self.present(sheet, animated: true, completion: nil)
        }
    }

    func addNewConversation() {
        if let rosterController = ChooseRostersForChatVC.initiateFromStoryboard() as? ChooseRostersForChatVC {
            rosterController.parentDelegate = self
            rosterController.chooseRosterFor = .chat
            self.present(rosterController, animated: true, completion: nil)
        }
    }

    func addNewReminder() {
        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewReminderVC") as! AddNewReminderVC
        nextNav.modalPresentationStyle = .fullScreen
        nextNav.viewModel.selResult.value.isSelelctAll = true

        self.present(nextNav, animated: true, completion: nil)
    }
//
    func addEmailTeam() {
        let nextNav = UrgentNotificationViewController()
        nextNav.modalPresentationStyle = .fullScreen
        let emailTeamTitle = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_email_team.rawValue)
        nextNav.titleLabel = emailTeamTitle
        self.present(nextNav, animated: true, completion: nil)
    }

    func addUrgentNoti() {
        let nextNav = UrgentNotificationViewController()
        nextNav.modalPresentationStyle = .fullScreen
        nextNav.titleLabel = "Urgent Notification"

        self.present(nextNav, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.viewModel.dp.value[indexPath.row]
        if model.itemType == ChatListViewItem.HEADER_TEAM_CHAT {
            let isTeamChatExist = self.viewModel.lastTeamChat.value.lastSenderName != nil
            if isTeamChatExist {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
                cell.txtTitle.text = "Team Chat"
                cell.contentView.backgroundColor = UIColor.COLOR_BLUE
                cell.topLine.isHidden = true
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    "ChatHeaderButtonCell") as! ChatHeaderButtonCell

                cell.cellContentView.backgroundColor = UIColor.COLOR_BLUE
                cell.contentView.backgroundColor = UIColor.COLOR_BLUE
                cell.btStart.makeGreenButton()

                cell.btStart.isHidden = isTeamChatExist

                cell.startClosure = {
                    self.viewModel.doStartTeamChat { (error) in
                        if error == nil {
                            self.gotoTeamChatViewController()
                        } else {
                            print("Start team chat failed: ", error.debugDescription)
                        }
                    }
                }

                return cell
            }
        } else if model.itemType == ChatListViewItem.HEADER_CARPOOL_CHAT {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
            cell.txtTitle.text = "Carpool"
            cell.topLine.isHidden = false
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            return cell
        } else if model.itemType == ChatListViewItem.HEADER_CONVERSATION_CHAT {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
            cell.txtTitle.text = "Conversations"
            cell.topLine.isHidden = false
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            return cell
        } else if model.itemType == ChatListViewItem.HEADER_REMINDER_CHAT {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
            cell.txtTitle.text = "Reminders"
            cell.topLine.isHidden = false
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            return cell
        } else if model.itemType == ChatListViewItem.ITEM_TEAM_CHAT {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell

            cell.config(chat: viewModel.lastTeamChat.value)
            cell.longPressClosure = nil

            return cell
        } else if model.itemType == ChatListViewItem.ITEM_CARPOOL_CHAT {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell

            cell.config(chat: viewModel.lastCarPoolChat.value)
            cell.longPressClosure = nil

            return cell
        } else if model.itemType == ChatListViewItem.ITEM_CONVERSATION {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell
            let conversation = self.viewModel.converse[model.idxInOriginArr!]

            cell.config(conversation: conversation)
            cell.longPressClosure = {
                AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: "Do you want to remove this conversation?", confirmTitle: "Yes", parent: self) {
                    let converse = self.viewModel.converse[model.idxInOriginArr!]
                    self.viewModel.doRemoveConversation(converse: converse) { (error) in
                        if error == nil, let index = model.idxInOriginArr {
                            var conversation = self.viewModel.converse
                            conversation.remove(at: index)

                            self.viewModel.updateConversationList(conversations: conversation)
                            self.viewModel.buildDataProvider()
                        }
                    }
                }
            }

            return cell
        } else {//if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.ITEM_REMINDER {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell
            let date = Date(timeIntervalSince1970: Double(self.viewModel.reminders[model.idxInOriginArr!].expirationLong!))
            let reminder = self.viewModel.reminders[model.idxInOriginArr!]

            cell.txtDate.text = date.dateChatString()
            cell.txtFrom.text = self.viewModel.reminders[model.idxInOriginArr!].senderFirstName! + " " + self.viewModel.reminders[model.idxInOriginArr!].senderLastName!
            cell.txtLastContent.text = self.viewModel.reminders[model.idxInOriginArr!].subject
            cell.viewUnread.isHidden = reminder.readStatusArr!.contains(Int(AppDataInstance.instance.currentSelectedTeam!.ID)!)

            cell.longPressClosure = {
                AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: "Do you want to remove this Reminder?", confirmTitle: "Yes", parent: self) {
                    self.viewModel.doRemoveReminder(reminder: reminder) { (error) in
                        if error == nil, let index = model.idxInOriginArr {
                            self.viewModel.reminders.remove(at: index)

                            self.viewModel.buildDataProvider()
                        }
                    }
                }
            }

            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.HEADER_TEAM_CHAT {
            let isTeamChatExist = self.viewModel.lastTeamChat.value.lastSenderName != nil
            if isTeamChatExist {
                return 45
            }
            return 93
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.HEADER_CARPOOL_CHAT {
            return 45
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.HEADER_CONVERSATION_CHAT {
            return 45
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.HEADER_REMINDER_CHAT {
            return 45
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.ITEM_TEAM_CHAT {
            return 80
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.ITEM_CARPOOL_CHAT {
            return 80
        } else if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.ITEM_CONVERSATION {
            return 80
        } else {//if self.viewModel.dp.value[indexPath.row].itemType == ChatListViewItem.ITEM_REMINDER {
            return 80
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.viewModel.dp.value[indexPath.row]
        if model.itemType == ChatListViewItem.ITEM_REMINDER {
            if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitation", parent: self)
            } else {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ReadReminderVC") as! ReadReminderVC
                nextNav.viewModel.origin = self.viewModel.reminders[model.idxInOriginArr!]
                nextNav.modalPresentationStyle = .fullScreen

                self.present(nextNav, animated: true, completion: nil)
            }
        } else if model.itemType == ChatListViewItem.ITEM_CONVERSATION {
            let converse = self.viewModel.converse[model.idxInOriginArr!]

            gotoConversationViewController(converse: converse)
        } else if model.itemType == ChatListViewItem.ITEM_CARPOOL_CHAT {
            if let roster = AppDataInstance.instance.currentSelectedTeam {
                let teamId = "team_" + roster.TEAM_ID
                let carpoolChatViewController = CarpoolChatViewController(teamId: teamId)
                self.navigationController?.pushViewController(carpoolChatViewController, animated: true)
            }
        } else if model.itemType == ChatListViewItem.ITEM_TEAM_CHAT {
            gotoTeamChatViewController()
        }
    }

    // MARK: - Chat Helpers

    func gotoTeamChatViewController() {
        DispatchQueue.main.async {
            if let roster = AppDataInstance.instance.currentSelectedTeam {
                let teamId = "team_" + roster.TEAM_ID
                let teamChatViewController = TeamChatViewController(teamId: teamId)
                self.navigationController?.pushViewController(teamChatViewController, animated: true)
            }
        }
    }

    func gotoConversationViewController(converse: ConversationEntryDTO?) {
        guard let converse = converse else {
            return
        }

        let converseViewController = ConversationChatViewController(converse: converse)

        self.navigationController?.pushViewController(converseViewController, animated: true)
    }
}

extension ChatListVC: IChooseRosterForChat {
    func onChooseRosters(val: GroupSelectionResult) {
        if let numberOfRoster = val.dto?.count, numberOfRoster < 1 {
            self.dismiss(animated: true) {
                AlertUtils.showWarningAlertWithHandler(title: "", message: "You must choose at least one roster member to start a conversation.", parent: self, handler: {
                    self.dismiss(animated: true, completion: nil)
                })
            }

            return
        }

        let me = GroupChatUserEntity(rosterId: AppDataInstance.instance.currentSelectedTeam?.ID, rosterName: AppDataInstance.instance.currentSelectedTeam?.displayName)
        val.dto?.append(me)
        
        self.viewModel.doAddConversation(groupSelection: GroupChatResult(selectionResult: val))
    }
}
