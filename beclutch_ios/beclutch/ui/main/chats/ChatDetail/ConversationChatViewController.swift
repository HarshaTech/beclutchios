//
//  ConversationChatViewController.swift
//  beclutch
//
//  Created by SM on 2/15/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Photos
import Firebase
import FirebaseStorage
import MessageKit
import FirebaseFirestore
import IQKeyboardManagerSwift

import Alamofire

final class ConversationChatViewController: BaseChatViewController {
    private var isSendingPhoto = false {
      didSet {
        DispatchQueue.main.async {
          self.messageInputBar.leftStackViewItems.forEach { item in
            item.isEnabled = !self.isSendingPhoto
          }
        }
      }
    }

    private let db = Firestore.firestore()
    private var reference: CollectionReference?
    private let storage = Storage.storage().reference()

    private var messageListener: ListenerRegistration?

    var converse: ConversationEntryDTO
    let roster = AppDataInstance.instance.currentSelectedTeam ?? RosterInfo(val: [:])

    private var converseId: String? {
        if let id = converse.id {
            return "converse_" + id
        }

        return nil
    }

    private var converseChatReference: CollectionReference {
      return db.collection("converse_chats")
    }

    deinit {
      messageListener?.remove()
    }

    init(converse: ConversationEntryDTO) {
        self.converse = converse

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let id = converseId else {
          navigationController?.popViewController(animated: true)
          return
        }

        let rightButton = UIBarButtonItem(title: "Members", style: .plain, target: self, action: #selector(memberPressed))

        self.navigationItem.setRightBarButton(rightButton, animated: false)

        reference = db.collection(["converse_chats", id, "messages"].joined(separator: "/"))

        messageListener = reference?.addSnapshotListener { querySnapshot, error in
          guard let snapshot = querySnapshot else {
            print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
            return
          }

          snapshot.documentChanges.forEach { change in
            self.handleDocumentChange(change)
          }
        }

        navigationItem.largeTitleDisplayMode = .never

        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = .messagePrimary
        messageInputBar.sendButton.setTitleColor(.messagePrimary, for: .normal)

        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self

        configUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.disabledToolbarClasses.append(ConversationChatViewController.self)
        IQKeyboardManager.shared.enable = false

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.COLOR_PRIMARY_DARK
           self.navigationItem.hidesBackButton = true
         let barButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backButton))
          barButton.image = UIImage(named: "baseline_arrow_back_ios_white_24dp")
          self.navigationItem.setLeftBarButton(barButton, animated: true)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Cambria", size: 20)!]

    }

    @objc func backButton() {
          self.navigationController?.popViewController(animated: true)
      }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        guard let id = self.converseId else {
            return
        }

        self.converseChatReference.document(id).getDocument { [weak self] (document, _) in
            if let document = document?.data() {
                guard let myId = Int(AppDataInstance.instance.currentSelectedTeam?.ID ?? "") else {
                    return
                }

                var readEntries: [Int] = []
                if let data = document["readEntries"] as? [Int] {
                    readEntries = data
                    if readEntries.contains(myId) {
                        return
                    }

                    readEntries.append(myId)
                } else {
                    readEntries = [myId]
                }

                self?.converseChatReference.document(id).updateData(["readEntries": readEntries])
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
    }

    func configUI() {
        if let roster = AppDataInstance.instance.currentSelectedTeam {
            guard let users = converse.groupSelection?.dto else {
                return
            }

            var groupStr = "No Members yet"
            if users.count > 2 {
                groupStr = "You and \(users.count - 1) members"
            } else if users.count >= 1 {
                groupStr = users.filter({ (element) -> Bool in
                    return element.rosterId != roster.ID && element.rosterName != nil
                }).map({ (element) -> String in
                    return element.rosterName!
                    }).joined(separator: " and ")

                groupStr = "You and " + groupStr
            }

            self.title = groupStr
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Actions
    @objc private func memberPressed() {
        if let rosterController = ChooseRostersForChatVC.initiateFromStoryboard() as? ChooseRostersForChatVC {
            rosterController.viewModel.initial = converse.groupSelection
            rosterController.viewModel.editPossible = false
            rosterController.chooseRosterFor = .chat

            self.present(rosterController, animated: true, completion: nil)
        }
    }

    // MARK: - Helpers

    private func save(_ message: Message, _ isSendingPhoto: Bool = false) {
      reference?.addDocument(data: message.representation) { error in
        if let e = error {
          print("Error sending message: \(e.localizedDescription)")
          return
        }

        if !isSendingPhoto {
            let req = SendTeamTextChatReq(message: message, converseId: self.converse.id)
            ChatService.instance.doSendConversationText(req: req)
        }

        guard let id = self.converseId else {
            return
        }

        if let users = self.converse.groupSelection?.dto?.map({ Int($0.rosterId ?? "0") ?? 0 }) {
            let msgDict = ChatEntryReadDTO(message: message, allIds: users).firebaseDict()
            
            self.converseChatReference.document(id).setData(msgDict) { (error) in
                print(error ?? "")
            }

            DispatchQueue.main.async {
                self.messagesCollectionView.scrollToBottom()
            }
        }
      }
    }

    private func insertNewMessage(_ message: Message) {
      guard !messages.contains(message) else {
        return
      }

      messages.append(message)
      messages.sort()

        let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
      let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage

      messagesCollectionView.reloadData()

      if shouldScrollToBottom {
        DispatchQueue.main.async {
          self.messagesCollectionView.scrollToBottom(animated: true)
        }
      }
    }

    private func handleDocumentChange(_ change: DocumentChange) {
      guard var message = Message(document: change.document) else {
        return
      }

      switch change.type {
      case .added:
        if message.type == 1 {
            if let url = URL(string: message.content) {
                downloadImage(at: url) { [weak self] image in
                  guard let `self` = self else {
                    return
                  }
                  guard let image = image else {
                    return
                  }

                    message.downloadURL = url
                  message.image = image

                    DispatchQueue.main.async {
                      self.insertNewMessage(message)
                    }
                }
            }
        } else {
          insertNewMessage(message)
        }

      default:
        break
      }
    }

    private func uploadImage(_ image: UIImage, completion: @escaping (URL?) -> Void) {
//      guard let channelID = teamId else {
//        completion(nil)
//        return
//      }

      guard let scaledImage = image.scaledToSafeUploadSize, let data = scaledImage.jpegData(compressionQuality: 0.4) else {
        completion(nil)
        return
      }

        let request = UploadImageMediaChatReq()
        request.token = AppDataInstance.instance.getToken()
        request.rosterId = self.roster.ID
        request.uploadFile = data

        ChatService.instance.doSendTeamImageMedia(req: request) { (response, _) in
            guard let response = response else {
                return
            }

            completion(URL(string: response.urlOnServer ?? ""))
        }
//      let metadata = StorageMetadata()
//      metadata.contentType = "image/jpeg"
//
//      let imageName = [UUID().uuidString, String(Date().timeIntervalSince1970)].joined()
//
//      storage.child(channelID).child(imageName).putData(data, metadata: metadata) { meta, error in
//        completion(meta?.downloadURL())
//      }
    }

    override func sendPhoto(_ image: UIImage) {
      isSendingPhoto = true

      uploadImage(image) { [weak self] url in
        guard let `self` = self else {
          return
        }
        self.isSendingPhoto = false

        guard let url = url else {
          return
        }

        var message = Message(roster: self.roster, image: image, downloadUrl: url.absoluteString)
        message.downloadURL = url

        self.save(message, true)
        self.messagesCollectionView.scrollToBottom()
      }
    }

    private func downloadImage(at url: URL, completion: @escaping (UIImage?) -> Void) {
        Alamofire.request(url, method: .get).responseData { (response) in
            guard let imageData = response.data else {
                print("Download image failed")
                completion(nil)
                return
            }

            completion(UIImage(data: imageData))
        }
    }
}

// MARK: - MessagesDisplayDelegate

extension ConversationChatViewController: MessagesDisplayDelegate {

  func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? .colorBlue : .incomingMessage
  }

  func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
    return true
  }

  func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
  }

}

// MARK: - MessagesDataSource

extension ConversationChatViewController: MessagesDataSource {

  func currentSender() -> Sender {
    return Sender(id: roster.ID, displayName: roster.displayName)
  }

  func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
    return messages.count
  }

  func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
    return messages[indexPath.section]
  }

  func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
    let name = message.sender.displayName
    return NSAttributedString(
      string: name,
      attributes: [
        .font: UIFont.preferredFont(forTextStyle: .caption1),
        .foregroundColor: UIColor(white: 0.3, alpha: 1)
      ]
    )
  }

}

// MARK: - MessageInputBarDelegate

extension ConversationChatViewController: MessageInputBarDelegate {

  func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
    let message = Message(roster: roster, content: text)

    save(message)
    inputBar.inputTextView.text = ""
  }

}


