//
//  BaseChatViewController.swift
//  beclutch
//
//  Created by SM on 2/16/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Photos
import MessageKit
import PopupDialog

class BaseChatViewController: MessagesViewController {
    private var imagePicker = UIImagePickerController()
    
    var messages: [Message] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false

        messagesCollectionView.messageCellDelegate = self
        
        let cameraItem = InputBarButtonItem(type: .system) // 1
        cameraItem.tintColor = .messagePrimary
        cameraItem.image = #imageLiteral(resourceName: "camera")
        cameraItem.addTarget(
          self,
          action: #selector(cameraButtonPressed), // 2
          for: .primaryActionTriggered
        )
        cameraItem.setSize(CGSize(width: 60, height: 30), animated: false)
        
        messageInputBar.leftStackView.alignment = .center
        messageInputBar.setLeftStackViewWidthConstant(to: 50, animated: false)
        messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        messagesCollectionView.reloadData()
        messagesCollectionView.scrollToBottom(animated: false)
    }
    
    // MARK: - Actions

    @objc private func cameraButtonPressed() {
        imagePicker.delegate  = self
        let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
        let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
            self.openCamera()
        }

        let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
            self.openGallery()
        }

        popup.addButtons([camera, gallery])
        self.present(popup, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func sendPhoto(_ image: UIImage) {
    }
}

// MARK: - MessagesLayoutDelegate

extension BaseChatViewController: MessagesLayoutDelegate {

  func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return .zero
  }

  func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
  }

  func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {

    return 0
  }

    func heightForMedia(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return self.view.bounds.size.width/2.0
    }

    func widthForMedia(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return self.view.bounds.size.width/2.0
    }

}

extension BaseChatViewController: MessageCellDelegate {
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("didTapMessage")

        if let indexPath = messagesCollectionView.indexPath(for: cell) {
            let message = messages[indexPath.section]
            guard message.type == 1 else {
                return
            }

            let configuration = ImageViewerConfiguration { config in
                config.image = message.image
            }

            let imageViewerController = ImageViewerController(configuration: configuration)
            present(imageViewerController, animated: true)
        }

    }
}

// MARK: - UIImagePickerControllerDelegate

extension BaseChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
    picker.dismiss(animated: true, completion: nil)

    if let asset = info[.phAsset] as? PHAsset { // 1
      let size = CGSize(width: 500, height: 500)
      PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: nil) { result, _ in
        guard let image = result else {
          return
        }

        self.sendPhoto(image)
      }
    } else if let image = info[.originalImage] as? UIImage { // 2
      sendPhoto(image)
    }
  }

  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }

}
