//
//  ChatContentCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ChatContentCell: UITableViewCell {
    @IBOutlet weak var txtLastContent: UILabel!

    @IBOutlet weak var viewUnread: UIView!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtFrom: UILabel!

    @IBOutlet weak var shadowView: UIView?

    var longPressClosure: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        self.addGestureRecognizer(longPressRecognizer)

        shadowView?.applyShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(chat: ChatEntryReadDTO?) {
        guard let chat = chat else {
            txtDate.text = ""
            txtFrom.text = ""
            txtLastContent.text = ""

            viewUnread.isHidden = false

            return
        }

        txtDate.text = chat.lastSendDate?.dateChatString()
        txtFrom.text = chat.lastSenderName
        txtLastContent.text = chat.lastSendMsg

        if let roster = AppDataInstance.instance.currentSelectedTeam, let rosterId = Int(roster.ID) {
            viewUnread.isHidden = chat.readEntries.contains(rosterId)
        }
    }

    func config(conversation: ConversationEntryDTO) {
        guard let chat = conversation.lastChatInfo else {
            txtDate.text = ""
            txtFrom.text = ""
            txtLastContent.text = ""

            viewUnread.isHidden = false

            return
        }

        txtDate.text = chat.lastSendDate?.dateChatString()
        let fromText = conversation.groupSelection?.dto?.map({ (element) -> String in
            return element.conversationDisplayName
            }).joined(separator: ", ")
        txtFrom.text = fromText
        txtLastContent.text = chat.conversationDisplayName + ": " + (chat.lastSendMsg ?? "")

        if let roster = AppDataInstance.instance.currentSelectedTeam, let rosterId = Int(roster.ID) {
            viewUnread.isHidden = chat.readEntries.contains(rosterId)
        }
    }

    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        print("longpressed")
        longPressClosure?()
    }
}
