//
//  ChatHeaderButtonCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ChatHeaderButtonCell: UITableViewCell {

    @IBOutlet weak var btStart: UIButton!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    
    var startClosure: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if AppDataInstance.instance.currentSelectedTeam?.getGroupType() == .club {
            btStart.setTitle("Start Club Chat", for: .normal)
            txtTitle.text = "Club Chat"
        } else {
            btStart.setTitle("Start Team Chat", for: .normal)
            txtTitle.text = "Team Chat"
        }
    }

    @IBAction func onClickStart(_ sender: Any) {
        startClosure?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
