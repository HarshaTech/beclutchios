//
//  AcceptInvitationVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class AcceptInvitationVC: UIViewController {

    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    var disposal = Disposal()
    var viewModel: AcceptInviteViewModel = AcceptInviteViewModel()
    
    weak var delegate: SelectTeamVCDelegate?
    
    var acceptTeamId: String?
    var isEnableBack = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        acceptBtn.makeGreenButton()
        cancelBtn.makeRedButton()
        cancelBtn.isEnabled = isEnableBack
        
        txtCode.backgroundColor = .white

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "InviteAccpetRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
                
            let regRes = res?.object as! InviteAccpetRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
//                self.dismiss(animated: true, completion: nil)
                //update teamList
                self.acceptTeamId = regRes.teamId
                self.viewModel.loadClubTree()
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to accept invitation", parent: self)
            }
        })
        
        SwiftEventBus.onMainThread(self, name: "ClubTreeLoadRes", handler: { [weak self]
            res in
            guard let self = self, let teamId = self.acceptTeamId else {
                return
            }
            
            let resAPI = res?.object as! ClubTreeLoadRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                AppDataInstance.instance.setMyClubTeamList(myClubTeamList: resAPI.rosters)

                if let listTeam = AppDataInstance.instance.myClubTeamList {
                    if let index = listTeam.firstIndex(where: { $0.teamOrClubId == teamId}) {
                        AppDataInstance.instance.currentSelectedTeam = listTeam[index]
                        self.viewModel.doUpdateTeam(rosterID: listTeam[index].ID)
                        self.delegate?.didSelectTeam()
                    }
                }
                NavUtil.showRosterViewController()
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        hideIndicator()
    }
    
    deinit {
        print("AcceptInvitationVC deinit")
    }

    @IBAction func onClickDone(_ sender: Any) {
        if self.txtCode.text!.count < 6 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Enter the correct invitaition code", parent: self)
            return
        }

        self.viewModel.doSubmitInviteCode(code: txtCode.text!)
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
