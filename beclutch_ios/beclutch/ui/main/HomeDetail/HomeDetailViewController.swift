//
//  HomeDetailViewController.swift
//  beclutch
//
//  Created by SM on 2/20/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import PopupDialog
import Kingfisher
import GoogleMobileAds

class HomeDetailViewController: UIViewController, CalendarCommon, AdsProtocol {
    @IBOutlet weak var tb: UITableView!
    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet weak var clubLogoImageView: UIImageView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var adView: GADBannerView!
    
    var selectPracticeModel: PracticeModel?
    var selectGameModel: GameModel?
    var selectStatus: String?
    var isGameCell: Bool?
    var selectedIndex: Int?

    var isHealCheck = false

    var viewModel: HomeDetailViewModel = HomeDetailViewModel()
    private var disposal = Disposal()

    enum HeaderEnum: Int, CaseIterable {
        case nextEvent = 0
        case lastTeamChat
        case lastReminder

        var title: String {
            switch self {
            case .nextEvent:
                return "Next Event"
            case .lastTeamChat:
                let teamChat = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_last_team_chat_message.rawValue)
                return AppDataInstance.instance.currentSelectedTeam?.getGroupType() == .team ? teamChat : "Last Club Chat Message"
            case .lastReminder:
                return "Last Reminder"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
        ChatStatusManager.shared.doListenForRedBlueDot()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let me = AppDataInstance.instance.me
        if String.isNilOrBlank(text: me?.FNAME) && String.isNilOrBlank(text: me?.LNAME) {
            PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_REG_USER_INFO)
            NavUtil.showRegisterViewController()
        } else if AppDataInstance.instance.isOnly1PendingTeam() || AppDataInstance.instance.currentSelectedTeam == nil {
            if let phoneNumber = me?.PHONE, phoneNumber.count > 0 {
                PreferenceMgr.saveInteger(keyName: PreferenceMgr.REGISTER_STEP, intValue: RegisterConstants.REGISTER_ROLE_CHOOSE)
            }
            
            NavUtil.showRegisterViewController()
        } else if AppDataInstance.instance.currentSelectedTeam?.ID.count == 0 {
            self.hideIndicator()
            if let showVC = ViewControllerFactory.makeAcceptInvitationVC() {
                showVC.isEnableBack = false
                self.present(showVC, animated: true, completion: nil)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTheme()
        
        // Do any additional setup after loading the view.
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, _ in
            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.teamChatInfo.observe(DispatchQueue.main) { [weak self] (_, _) in
            self?.reloadTableSection(section: HeaderEnum.lastTeamChat)
        }.add(to: &disposal)
        
        SwiftEventBus.onMainThread(self, name: "LoadHomeDashboardRes", handler: {
            res in
            let resAPI = res?.object as! LoadHomeDashboardRes

            if resAPI.result! {
                if let game = resAPI.game, let startTime = game.fullDate {
                    if startTime < Date() {
                        resAPI.game = nil
                    }
                }

                if let practice = resAPI.practice, let startTime = practice.fullDate {
                    if startTime < Date() {
                        resAPI.practice = nil
                    }

                    if let game = resAPI.game, let gameDate = game.fullDate {
                        if gameDate > startTime {
                            resAPI.game = nil
                        } else {
                            resAPI.practice = nil
                        }
                    }
                }

                self.viewModel.homeDashboardResponse = resAPI
                self.tb.reloadData()
                if let logo = self.viewModel.homeDashboardResponse?.logo {
                    self.clubLogoImageView.kf.setImage(with: URL(string: logo))
                }
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "UpdateAttendanceRes", handler: {
            res in
            let resAPI = res?.object as! UpdateAttendanceRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "CancelGameRes", handler: {
            res in
            let resAPI = res?.object as! CancelGameRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "DeleteCalendarItemRes", handler: {
            res in
            let resAPI = res?.object as! DeleteCalendarItemRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "CancelPracticeRes", handler: {
            res in
            let resAPI = res?.object as! CancelPracticeRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "UpdateGameRes", handler: {
            res in
            let resAPI = res?.object as! UpdateGameRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "GetResponsibleRes", handler: { [weak self]
                res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! GetResponsibleRes

            if resAPI.result! {
                if let roster = resAPI.rosters {
                    if roster.count > 1 {
                        let vc = GetResponsibleViewController()
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.playerList = resAPI.rosters
                        vc.delegate = self
                        self.present(vc, animated: true, completion: nil)
                        self.viewModel.isBusy.value = false
                    } else {
                        if self.isHealCheck {
                            let vc = HealthCheckViewController()
                            vc.eventModel = self.viewModel.eventModel
                            vc.eventType = self.viewModel.eventType
                            vc.modalPresentationStyle = .overFullScreen

                            vc.submitSuccessClousure = { status in
                                self.selectStatus = "\(status)"
                                self.updateAttendance(rosters: [AppDataInstance.instance.currentSelectedTeam?.ID ?? ""])
                            }

                            self.present(vc, animated: true, completion: nil)
                        } else {
                            self.updateAttendance(rosters: [AppDataInstance.instance.currentSelectedTeam?.ID ?? ""])
                        }
                    }
                }
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdatePracticeRes", handler: {
            res in
            let resAPI = res?.object as! UpdatePracticeRes

            if resAPI.result! {
                self.viewModel.loadHomeDashBoard()
            }

            self.viewModel.isBusy.value = false
        })

        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)

        self.viewModel.loadHomeDashBoard()
        self.viewModel.doLoadTeamChatInfo()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        viewModel.removeAllFirebaseListener()
        SwiftEventBus.unregister(self)
    }

    // MARK: - UI
    func configUI() {
        configAdView(adView: adView, from: self)
    }
    
    func refreshTheme() {
        btSelectTeam.makeStrokeOnlyButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    func reloadTableSection(section: HeaderEnum) {
        let indexSet = IndexSet(arrayLiteral: section.rawValue)
        self.tb.reloadSections(indexSet, with: .none)
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    @IBAction func onClickTeamToolbar(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            self.present(showVC, animated: true, completion: nil)
        }
    }
}

extension HomeDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == HeaderEnum.lastTeamChat.rawValue && self.viewModel.teamChatInfo.value.lastSenderName != nil {
            gotoTeamChatViewController()
        } else if indexPath.section == HeaderEnum.lastReminder.rawValue {
            gotoReminderViewController(reminder: self.viewModel.homeDashboardResponse?.reminder)
        }
    }
}

extension HomeDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return HeaderEnum.allCases.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HEIGHT_CONST.HEADER_HEIGHT
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = self.tb.dequeueReusableCell(withIdentifier: ContentHeaderCell.reuseIdentifier()) as? ContentHeaderCell {
            cell.topLineView.isHidden = section == 0
            cell.config(title: HeaderEnum.allCases[section].title)
            //changing work
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            return cell.contentView
        }

        return UIView()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == HeaderEnum.nextEvent.rawValue {
            if self.viewModel.homeDashboardResponse?.game != nil || self.viewModel.homeDashboardResponse?.practice != nil {
                return self.viewModel.eventHeight
            }
        } else if indexPath.section == HeaderEnum.lastReminder.rawValue {
            return 80
        } else if indexPath.section == HeaderEnum.lastTeamChat.rawValue {
            let teamChat = self.viewModel.teamChatInfo.value
            return teamChat.lastSenderName != nil ? 80 : 50
        }

        return 45
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == HeaderEnum.nextEvent.rawValue {
                return 1
        } else if section == HeaderEnum.lastReminder.rawValue {
            if self.viewModel.homeDashboardResponse?.reminder != nil {
                return 1
            }
        } else if section == HeaderEnum.lastTeamChat.rawValue {
            return 1
        }

        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == HeaderEnum.nextEvent.rawValue {
            if let game = self.viewModel.homeDashboardResponse?.game {
                return gameCell(indexPath: indexPath, model: game)
            } else if let practice = self.viewModel.homeDashboardResponse?.practice {
                return practiceCell(indexPath: indexPath, model: practice)
            } else {
                let cell = UITableViewCell()
                cell.textLabel?.text = "NO EVENTS CURRENTLY SCHEDULED"
                cell.textLabel?.textAlignment = .center
                cell.selectionStyle = .none

                return cell
            }
        } else if indexPath.section == HeaderEnum.lastReminder.rawValue {
            if let reminder = self.viewModel.homeDashboardResponse?.reminder {
                return reminderCell(indexPath: indexPath, reminder: reminder)
            }
        } else if indexPath.section == HeaderEnum.lastTeamChat.rawValue {
            let teamChat = self.viewModel.teamChatInfo.value
            if teamChat.lastSenderName != nil {
                return chatContentCell(indexPath: indexPath, teamChat: self.viewModel.teamChatInfo.value)
            } else {
                return chatHeaderButtonCell()
            }
        }

        return UITableViewCell()
    }

    func chatHeaderButtonCell() -> ChatHeaderButtonCell {
        let cell = tb.dequeueReusableCell(withIdentifier: "ChatHeaderButtonCell") as! ChatHeaderButtonCell
        //-------------changing work---------//
        cell.txtTitle.isHidden = true
        cell.contentView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        cell.btStart.makeGreenButton()
        //-------------changing work---------//

        let title = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_start_team_chat.rawValue)
        cell.btStart.setTitle(title, for: .normal)

        cell.startClosure = {
            self.viewModel.doStartTeamChat { (error) in
                if error == nil {
                    self.gotoTeamChatViewController()
                } else {
                    print("Start team chat failed: ", error.debugDescription)
                }
            }
        }

        return cell
    }

    func chatContentCell(indexPath: IndexPath, teamChat: ChatEntryReadDTO) -> ChatContentCell {
        let cell = tb.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell

        cell.config(chat: teamChat)

        return cell
    }

    func reminderCell(indexPath: IndexPath, reminder: ReminderEntryDTO) -> ChatContentCell {
        let cell = tb.dequeueReusableCell(withIdentifier: "ChatContentCell") as! ChatContentCell
        let date = Date(timeIntervalSince1970: Double(reminder.expirationLong!))

        cell.txtDate.text = date.dateChatString()
        cell.txtFrom.text = reminder.senderFirstName! + " " + reminder.senderLastName!
        cell.txtLastContent.text = reminder.subject
        cell.viewUnread.isHidden = reminder.readStatusArr!.contains(Int(AppDataInstance.instance.currentSelectedTeam!.ID)!)
        return cell
    }

    func practiceCell(indexPath: IndexPath, model: PracticeModel) -> PracticeCell {
        let cell = tb.dequeueReusableCell(withIdentifier: "PracticeCell", for: indexPath) as! PracticeCell

        cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
        cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
        cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)

        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        //------------------changing work---------------------//
        cell.txtOpenName.textColor = UIColor.COLOR_ACCENT
        cell.txtCloseName.textColor = UIColor.COLOR_BLUE
        cell.btExpand?.tintColor = UIColor.COLOR_BLUE
        cell.btCollapse?.tintColor = UIColor.COLOR_BLUE
        cell.btCancel.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btDelete.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btEdit.backgroundColor = UIColor.COLOR_PRIMARY
        
        let isShown = AccountCommon.isCoachManagerOrTreasure()
        cell.btDelete.isHidden = !isShown
        cell.btEdit.isHidden = !isShown
        cell.btCancel.isHidden = !isShown
        
        if Int(model.IS_ACTIVE!)! == PracticeModel.PRACTICE_CANCEL {
            cell.btEdit.isHidden = true
            cell.btCancel.isHidden = true
        }
        
        //----------------------------------------------//

        cell.txtCloseCancel.isHidden = Int(model.IS_ACTIVE!)! != PracticeModel.PRACTICE_CANCEL
        cell.txtOpenCancel.isHidden = Int(model.IS_ACTIVE!)! != PracticeModel.PRACTICE_CANCEL
        cell.txtCloseLocation.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtOpenLocation.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtCloseName.text = model.NAME
        cell.txtOpenName.text = model.NAME
        let startHour = Int(model.START_TIME_HOUR!)! > 12 ? Int(model.START_TIME_HOUR!)! - 12 : Int(model.realStartTimeHour!)!
        let startMin = Int(model.START_TIME_MIN!)!
        let strTime = String(format: "%d:%02d", startHour, startMin) + " " + model.START_TIME_AMPM!
        if model.DURATION_HOUR == nil {
            cell.txtCloseHour.text = strTime
            cell.txtOpenTime.text = strTime
        } else if model.DURATION_HOUR == "-1" || (model.DURATION_HOUR == "0" && model.DURATION_MIN == "0") {
            cell.txtCloseHour.text = strTime
            cell.txtOpenTime.text = strTime
        } else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "h:mm a"

            if var startDate: Date = formatter2.date(from: strTime) {
                startDate = Calendar.current.date(byAdding: .hour, value: Int(model.DURATION_HOUR!)!, to: startDate)!
                let date  = Calendar.current.date(byAdding: .minute, value: Int(model.DURATION_MIN!)!, to: startDate)!
                
                cell.txtCloseHour.text = strTime + " - " + formatter2.string(from: date)
                cell.txtOpenTime.text = strTime + " - " + formatter2.string(from: date)
            }
        }

        cell.btAttendMaybe.isChoose = false
        cell.btAttendYes.isChoose = false
        cell.btAttendNo.isChoose = false

        if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
            cell.btAttendYes.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
            cell.btAttendNo.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
            cell.btAttendMaybe.isChoose = true
        }

        cell.txtCloseDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtCloseDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenAddr.text = model.location_detail!.id!.isEmpty ? "No Location yet" : StringCheckUtils.getAddressFromLocation(model: model.location_detail!)
        cell.txtMemo.text = model.MEMO == nil ? "No Memo" : model.MEMO

        cell.config(model: model)

        cell.actionOpen = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.viewModel.eventHeight = HEIGHT_CONST.OPEN_PRACTICE_HEIGHT
                cell.unfold(true, animated: true, completion: nil)
                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    self.tb.beginUpdates()
                    self.tb.endUpdates()

                    if cell.frame.maxY > self.tb.frame.maxY {
                        self.tb.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }

                }, completion: nil)
            }
        }

        cell.actionClose = { [weak self] in
            guard let self = self else {
                return
            }

            if cell.isAnimating() {
                return
            }

            DispatchQueue.main.async {
                self.viewModel.eventHeight = HEIGHT_CONST.CLOSE_CELL_HEIGHT
                cell.unfold(false, animated: true, completion: nil)
                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    self.tb.beginUpdates()
                    self.tb.endUpdates()

                    if cell.frame.maxY > self.tb.frame.maxY {
                        self.tb.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: nil)
            }
        }

        cell.actionDirection = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if model.location_detail!.id!.isEmpty {
                    AlertUtils.showWarningAlert(title: "Warning", message: "No address yet", parent: self)
                } else {
                    let addr = StringCheckUtils.getAddressFromLocation(model: model.location_detail!).replacingOccurrences(of: " ", with: "+")
                    let targetAddr = "http://maps.apple.com/?daddr=" + addr
                    if let targetURL = URL(string: targetAddr) {
                        UIApplication.shared.open(targetURL, options: [:], completionHandler: nil)
                    }
                }
            }
        }

        cell.actionHealthCheck = { [weak self] in
            self?.volunterAction()
        }

        cell.actionAttendYes = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_YES
            self.selectedIndex = indexPath.row
            self.isHealCheck = false
            
            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendNo = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_NO
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendMaybe = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_MAYBE
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept inviation first.", parent: self)
            } else {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }            }

        cell.actionRosters = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ShowRosterListForEventVC") as! ShowRosterListForEventVC
                    nextNav.viewModel.kind = ShowRosterListForEventVC.KIND_EVENT_FOR_PRACTICE
                    nextNav.viewModel.eventId = model.ID
                    self.present(nextNav, animated: true, completion: nil)
                }
            }
        }

        cell.actionEdit = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPracticeVC") as! AddNewPracticeVC
                    nextNav.isUpdate = true
                    nextNav.origin = model
                    self.present(nextNav, animated: true, completion: nil)
                }
            }
        }

        cell.actionDelete = { [weak self] in
            self?.deleteEvent(eventId: model.ID, kind: "0", indexPath: indexPath)
        }

        cell.actionCancel = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    self.alert(title: "Confirm", message: "Notify Team?", okHandler: { (_) in
                        self.viewModel.doCancelPractice(practiceId: model.ID!, pos: indexPath.row, notifyTeam: true)
                    }) { (_) in
                        self.viewModel.doCancelPractice(practiceId: model.ID!, pos: indexPath.row, notifyTeam: false)
                    }
                }
            }
        }

        cell.actionAddCalendar = { [weak self] in
            guard let self = self else {
                return
            }

            SystemCalendarService.shared.addReminder(title: model.NAME, note: model.MEMO, eventTime: model.eventDate, duration: TimeInterval(model.duration)) {error in
                if error == nil {
                    self.alert(title: "", message: "Add practice to calendar Successfully.")
                }
            }
        }

        cell.actionCarPool = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.alert(message: "Are you sure you want to request a carpool?", okTitle: "Yes", okHandler: { _ in
                    self.viewModel.doCarpool(practice: model) { (error) in
                        if error == nil {
                            self.gotoCarPoolChatViewController()
                        }
                    }
                }, cancelHandler: { _ in})
            }
        }
        
        cell.actionShare = { [weak self] in
            self?.shareModel(model: model)
        }

        return cell
    }

    func gameCell(indexPath: IndexPath, model: GameModel) -> GameCell {
        let cell = tb.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath) as! GameCell
        
        cell.config(model: model)

        cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
        cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
        cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)

        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        /////////////////////////////////
        //------------------changing work---------------------//
        cell.txtOpenName.textColor = UIColor.COLOR_ACCENT
        cell.txtCloseName.textColor = UIColor.COLOR_BLUE
        cell.btUp.tintColor = UIColor.COLOR_BLUE
        cell.btDown.tintColor = UIColor.COLOR_BLUE
        cell.btCancel.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btDelete.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btEdit.backgroundColor = UIColor.COLOR_PRIMARY
        
        let isShown = AccountCommon.isCoachManagerOrTreasure()
        cell.btDelete.isHidden = !isShown
        cell.btEdit.isHidden = !isShown
        cell.btCancel.isHidden = !isShown
        
        if Int(model.IS_ACTIVE!)! == GameModel.GAME_CANCEL {
            cell.btEdit.isHidden = true
            cell.btCancel.isHidden = true
        }
        
        //----------------------------------------------//
//        cell.txtCloseCancel.isHidden = Int(model.IS_ACTIVE!)! != GameModel.GAME_CANCEL
        cell.txtOpenCancel.isHidden = Int(model.IS_ACTIVE!)! != GameModel.GAME_CANCEL

        cell.btAttendMaybe.isChoose = false
        cell.btAttendYes.isChoose = false
        cell.btAttendNo.isChoose = false

        if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
            cell.btAttendYes.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
            cell.btAttendNo.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
            cell.btAttendMaybe.isChoose = true
        }

        cell.txtCloseAddr.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtOpenAddr.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtCloseName.text = model.NAME
        cell.txtOpenName.text = model.NAME

        cell.txtCloseOppoName.text = "vs " + model.opponent_detail!.OPPONENT_NAME!
        cell.txtOpenOppoName.text = "vs " + model.opponent_detail!.OPPONENT_NAME!

        let strTime = String(format: "%d:%02d", Int(model.START_TIME_HOUR!)! > 12 ? Int(model.START_TIME_HOUR!)! - 12 : Int(model.realStartTimeHour!)!, Int(model.START_TIME_MIN!)!) + " " + model.START_TIME_AMPM!
        if model.DURATION_HOUR == nil {
            cell.txtCloseTime.text = strTime
            cell.txtOpenTime.text = strTime
        } else if model.DURATION_HOUR == "-1" || (model.DURATION_HOUR == "0" && model.DURATION_MIN == "0") {
            cell.txtCloseTime.text = strTime
            cell.txtOpenTime.text = strTime
        } else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "h:mm a"

            if var startDate: Date = formatter2.date(from: strTime) {
                startDate = Calendar.current.date(byAdding: .hour, value: Int(model.DURATION_HOUR!)!, to: startDate)!
                let date  = Calendar.current.date(byAdding: .minute, value: Int(model.DURATION_MIN!)!, to: startDate)!

                cell.txtCloseTime.text = strTime + " - " + formatter2.string(from: date)
                cell.txtOpenTime.text = strTime + " - " + formatter2.string(from: date)
            }
        }
        cell.txtCloseDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtCloseDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenLocation.text = model.location_detail!.id!.isEmpty ? "No Location yet" : StringCheckUtils.getAddressFromLocation(model: model.location_detail!)

        cell.btHome.isChoose = model.IS_HOME == "1"
        cell.btAway.isChoose = model.IS_HOME == "0"

        cell.viewWearColor.backgroundColor = ColorUtils.getUIColorFromRGB(val: model.UNIFORM_COLOR!)
        cell.txtMemo.text = model.MEMO == nil ? "No Memo" : model.MEMO

        if cell.txtCloseCancel.isHidden {
            cell.txtMonth.isHidden = false
            cell.txtMonthExpand.isHidden = false

            cell.txtMonth.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
            cell.txtMonthExpand.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
        }
        
        cell.actionOpen = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.viewModel.eventHeight = HEIGHT_CONST.OPEN_GAME_HEIGHT
                cell.unfold(true, animated: true, completion: nil)
                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    self.tb.beginUpdates()
                    self.tb.endUpdates()

                    if cell.frame.maxY > self.tb.frame.maxY {
                        self.tb.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: nil)
            }
        }

        cell.actionClose = { [weak self] in
            if cell.isAnimating() {
                return
            }

            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.viewModel.eventHeight = HEIGHT_CONST.CLOSE_CELL_HEIGHT
                cell.unfold(false, animated: true, completion: nil)
                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    self.tb.beginUpdates()
                    self.tb.endUpdates()

                    if cell.frame.maxY > self.tb.frame.maxY {
                        self.tb.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: nil)
            }
        }

        cell.actionDrive = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if model.location_detail!.id!.isEmpty {
                    AlertUtils.showWarningAlert(title: "Warning", message: "No address yet", parent: self)
                } else {
                    let addr = StringCheckUtils.getAddressFromLocation(model: model.location_detail!).replacingOccurrences(of: " ", with: "+")
                    let targetAddr = "http://maps.apple.com/?daddr=" + addr
                    if let targetURL = URL(string: targetAddr) {
                        UIApplication.shared.open(targetURL, options: [:], completionHandler: nil)
                    }
                }
            }
        }

        cell.actionCarPool = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.alert(message: "Are you sure you want to request a carpool?", okTitle: "Yes", okHandler: { _ in
                    self.viewModel.doCarpool(game: model) { (error) in
                        if error == nil {
                            self.gotoCarPoolChatViewController()
                        }
                    }
                }, cancelHandler: { _ in})
            }
        }

        cell.actionHealthCheck = { [weak self] in
            self?.volunterAction()
        }

        cell.actionAttendYes = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_YES
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            //game
            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendNo = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_NO
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendMaybe = { [weak self] in
            guard let self = self else {
                return
            }
            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_MAYBE
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionRosters = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ShowRosterListForEventVC") as! ShowRosterListForEventVC
                    nextNav.viewModel.kind = ShowRosterListForEventVC.KIND_EVENT_FOR_GAME
                    nextNav.viewModel.eventId = model.ID
                    self.present(nextNav, animated: true, completion: nil)
                }
            }
        }
        
        cell.actionScore = { [weak self] in
           guard let self = self else {
               return
           }
            
            let vc = ScoreDialogViewController()
            vc.scoreID = model.ID
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }

        cell.actionEdit = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGameVC") as! AddNewGameVC
                    nextNav.isUpdate = true
                    nextNav.origin = model
                    self.present(nextNav, animated: true, completion: nil)
                }
            }
        }

        cell.actionDelete = { [weak self] in
            self?.deleteEvent(eventId: model.ID!, kind: "1", indexPath: indexPath)
        }

        cell.actionCancel = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    self.alert(title: "Confirm", message: "Notify Team?", okHandler: { [weak self] (_) in
                        self?.viewModel.doCancelGame(gameId: model.ID!, pos: indexPath.row, notify: true)
                    }) { [weak self] (_) in
                        self?.viewModel.doCancelGame(gameId: model.ID!, pos: indexPath.row, notify: false)
                    }
                }
            }
        }

        cell.actionOppoInfo = { [weak self] in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                if self.validatePendingState() {
                    if model.opponent_detail!.ID!.isEmpty {
                        AlertUtils.showWarningAlert(title: "Warning", message: "You don't have opponent yet.", parent: self)
                    } else {
                        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "OpponentAddViewController") as! OpponentAddViewController
                        nextNav.isEidt = true
                        nextNav.isShowing = true
                        nextNav.viewModel.originOppo = model.opponent_detail
                        self.present(nextNav, animated: true, completion: nil)
                    }
                }
            }
        }
        
        cell.actionShare = { [weak self] in
            self?.shareModel(model: model)
        }
        
        return cell
    }
}

// MARK: - Utils
extension HomeDetailViewController {
    func openWeather(model: EventModel) {
        DispatchQueue.main.async {
            if model.location_detail!.id!.isEmpty {
                AlertUtils.showWarningAlert(title: "Warning", message: "No Location yet", parent: self)
            } else {
                let targetAddr = "https://www.wunderground.com/hourly/" + self.viewModel.countryList[Int(model.location_detail!.countryIdx!)!] + "/" +
                    self.viewModel.stateList[Int(model.location_detail!.stateIdx!)!] + "/" +
                    model.location_detail!.city! + "/" +
                    model.location_detail!.zip!
                if let targetURL = URL(string: targetAddr) {
                    UIApplication.shared.open(targetURL, options: [:], completionHandler: nil)
                }
            }
        }
    }

    func deleteEvent(eventId: String?, kind: String, indexPath: IndexPath) {
        guard let eventId = eventId else {
            return
        }

        DispatchQueue.main.async {
            if self.validatePendingState() {
                self.viewModel.doDeleteGamePractice(eventId: eventId, kind: kind, pos: indexPath.row)
            }
        }
    }

    func updateAttendance(model: EventModel, attending: String, type: CALENDAR_ITEM_TYPE) {
        DispatchQueue.main.async {
            if self.validatePendingState() {
                if model.attendance_detail == nil {
                    self.viewModel.doCreateAttendForEvent(eventId: model.ID!, targetStatus: attending, calendarItemId: model.ID!, calendarItemType: type)
                } else {
                    if model.attendance_detail!.ID!.isEmpty || model.attendance_detail!.ID! == "-1" {
                        self.viewModel.doCreateAttendForEvent(eventId: model.ID!, targetStatus: attending, calendarItemId: model.ID!, calendarItemType: type)
                    } else {
                        if model.attendance_detail?.ATTENDTING_DECISION != attending {
                            self.viewModel.doUpdateAttendForEvent(attendanceID: model.attendance_detail!.ID!, status: attending, calendarItemId: model.ID!, calendarItemType: type)
                        }
                    }
                }
            }
        }
    }
}

// MARK: - Utils
extension HomeDetailViewController {
    func validatePendingState() -> Bool {
        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Please accept the Invitation sent to your Email.", parent: self)
            return false
        }
        
        return true
    }
    
    func gotoCarPoolChatViewController() {
        DispatchQueue.main.async {
            if let roster = AppDataInstance.instance.currentSelectedTeam {
                let teamId = "team_" + roster.TEAM_ID
                let carpoolChatViewController = CarpoolChatViewController(teamId: teamId)
                self.navigationController?.pushViewController(carpoolChatViewController, animated: true)
            }
        }
    }

    func gotoTeamChatViewController() {
        DispatchQueue.main.async {
            if let roster = AppDataInstance.instance.currentSelectedTeam {
                let teamId = "team_" + roster.TEAM_ID
                let teamChatViewController = TeamChatViewController(teamId: teamId)
                self.navigationController?.pushViewController(teamChatViewController, animated: true)
            }
        }
    }

    func gotoReminderViewController(reminder: ReminderEntryDTO?) {
        DispatchQueue.main.async {
            guard let reminder = reminder else {
                return
            }

            if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitation", parent: self)
            } else {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ReadReminderVC") as! ReadReminderVC
                nextNav.viewModel.origin = reminder
                nextNav.modalPresentationStyle = .fullScreen
                self.present(nextNav, animated: true, completion: nil)
            }
        }
    }
}

extension HomeDetailViewController: GetResponsibleViewControllerDelegate {
    fileprivate func handleErrForGameCell(_ err: String?) {
        if let err = err {
            self.alert(message: err)
        } else {
            guard let model = self.selectGameModel else {return}
            model.attendance_detail!.ATTENDTING_DECISION! = self.selectStatus!

            let indexPath = IndexPath(row: self.selectedIndex!, section: 0)

            if let cell = self.tb.cellForRow(at: indexPath) as? GameCell {
                cell.btAttendMaybe.isChoose = false
                cell.btAttendYes.isChoose = false
                cell.btAttendNo.isChoose = false

                if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
                    cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
                    cell.btAttendYes.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
                    cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
                    cell.btAttendNo.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
                    cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
                    cell.btAttendMaybe.isChoose = true
                }
            }
            self.tb.reloadRows(at: [indexPath], with: .none)
        }
    }

    fileprivate func handleErrorForPracticeCell(_ err: String?) {
        if let err = err {
            self.alert(message: err)
        } else {
            guard let model = self.selectPracticeModel else {return}
            
            model.attendance_detail!.ATTENDTING_DECISION! = self.selectStatus!

            let indexPath = IndexPath(row: self.selectedIndex!, section: 0)

            if let cell = self.tb.cellForRow(at: indexPath) as? PracticeCell {
                cell.btAttendMaybe.isChoose = false
                cell.btAttendYes.isChoose = false
                cell.btAttendNo.isChoose = false

                //                                                                        if !model.attendance_detail!.ID!.isEmpty {
                if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
                    cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
                    cell.btAttendYes.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
                    cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
                    cell.btAttendNo.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
                    cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
                    cell.btAttendMaybe.isChoose = true
                }
                //                                                                        }
                self.tb.reloadRows(at: [indexPath], with: .none)
            }
        }
    }

    func updateAttendance(rosters: [String]) {
        guard let eventId = self.viewModel.eventModel?.ID else {
            return
        }
        
        if self.viewModel.eventType == .GAME_MODEL {
            if isHealCheck {
                self.viewModel.doUpdateHealCheckForMultipleRoster(rosters: rosters,
                                                                  status: self.selectStatus ?? "",
                                                                  calendarItemId: eventId,
                                                                  calendarItemType: CALENDAR_ITEM_TYPE.GAME_MODEL.itemKindString()) { [weak self] (err) in
                                                                    if let err = err {
                                                                        self?.alert(message: err)
                                                                    }
                }
            } else {
                self.viewModel.doUpdateAttendanceForMultipleRoster(rosters: rosters,
                                                                   status: self.selectStatus ?? "",
                                                                   calendarItemId: eventId,
                                                                   calendarItemType: CALENDAR_ITEM_TYPE.GAME_MODEL.itemKindString()) { (err) in
                                                                    self.handleErrForGameCell(err)
                }
            }

        } else {
            if isHealCheck {
                self.viewModel.doUpdateHealCheckForMultipleRoster(rosters: rosters,
                                                                  status: self.selectStatus ?? "",
                                                                  calendarItemId: eventId,
                                                                  calendarItemType: CALENDAR_ITEM_TYPE.PRACTICE_MODEL.itemKindString()) { [weak self] (err) in
                                                                    if let err = err {
                                                                        self?.alert(message: err)
                                                                    }

                }
            } else {
                self.viewModel.doUpdateAttendanceForMultipleRoster(rosters: rosters,
                                                                   status: self.selectStatus ?? "",
                                                                   calendarItemId: eventId,
                                                                   calendarItemType: CALENDAR_ITEM_TYPE.PRACTICE_MODEL.itemKindString()) { (err) in
                                                                    self.handleErrorForPracticeCell(err)

                }
            }

        }

    }

    func submitPressed(rosters: [String]) {
        guard rosters.count > 0 else {
            return
        }
        
        if self.isHealCheck {
            let vc = HealthCheckViewController()
            vc.eventModel = self.viewModel.eventModel
            vc.eventType = self.viewModel.eventType
            vc.modalPresentationStyle = .overFullScreen

            vc.submitSuccessClousure = { [weak self] status in
                self?.selectStatus = "\(status)"
                self?.updateAttendance(rosters: rosters)
            }

            self.present(vc, animated: true, completion: nil)
        } else {
            self.updateAttendance(rosters: rosters)
        }
    }
}

extension HomeDetailViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "body"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return "subject"
    }
}

extension HomeDetailViewController {
    private func healhCheckAction(model: EventModel, indexPath: IndexPath) {
        self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
        self.isHealCheck = true

        self.selectedIndex = indexPath.row
        self.isGameCell = true

        // Select per event
//        self.selectGameModel = model
    }

    private func volunterAction() {
        if let settingNavigationVC = self.tabBarController?.viewControllers?.last as? UINavigationController {
            settingNavigationVC.popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 4

            let volunteersVC = VolunteersViewController()
            settingNavigationVC.pushViewController(volunteersVC, animated: true)
        }
    }
}

extension HomeDetailViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
}
