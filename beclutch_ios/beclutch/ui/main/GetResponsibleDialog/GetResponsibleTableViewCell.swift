//
//  GetResponsibleTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class GetResponsibleTableViewCell: UITableViewCell {
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblName: UILabel!

    var checkedActionClousure: (() -> Void)?

    var isChecked: Bool = false {
        didSet {
            if isChecked {
                btnCheckBox.setImage(UIImage(named: "box-checked"), for: .normal)
            } else {
                btnCheckBox.setImage(UIImage(named: "box-unchecked"), for: .normal)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onPressedCheckBox(_ sender: Any) {
        checkedActionClousure?()
        isChecked = !isChecked
    }

}
