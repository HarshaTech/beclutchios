//
//  GetResponsibleViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

protocol GetResponsibleViewControllerDelegate: class {
    func submitPressed(rosters: [String])
}

class GetResponsibleViewController: UIViewController {
    @IBOutlet weak var tbv: UITableView!

    var playerList: [RosterInfo]?
    var selectedPlayerList = NSMutableArray()
    weak var delegate: GetResponsibleViewControllerDelegate?
    var isCheckAll = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        tbv.register(UINib(nibName: "GetResponsibleTableViewCell", bundle: nil), forCellReuseIdentifier: "GetResponsibleTableViewCell")
        tbv.delegate = self
        tbv.dataSource = self

        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none

    }

    @IBAction func onSubmit(_ sender: Any) {
        let list = selectedPlayerList.compactMap {($0 as! RosterInfo).ID}
        delegate?.submitPressed(rosters: list)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension GetResponsibleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (playerList?.count ?? 0) + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GetResponsibleTableViewCell", for: indexPath) as! GetResponsibleTableViewCell
        cell.selectionStyle = .none

        if indexPath.row == 0 {
            cell.lblName.text = "All"
        } else {
            let model = self.playerList![indexPath.row - 1]
            cell.lblName.text = model.ROSTER_FIRST_NAME + " " + model.ROSTER_LAST_NAME + "\(model.LEVEL.isEmpty ? "" : " (" + AccountCommon.getRoleString(roster: model) + ")")"
        }

        cell.checkedActionClousure = {
            if indexPath.row == 0 {
                if cell.isChecked {
                    self.isCheckAll = false
                    self.selectedPlayerList.removeAllObjects()

                } else {
                    self.isCheckAll = true
                    self.selectedPlayerList.removeAllObjects()
                    self.selectedPlayerList.addObjects(from: self.playerList ?? [RosterInfo]())

                }

                for i in 1...self.playerList!.count {
                    let indexPath = IndexPath(row: i, section: 0)
                    let tempCell = self.tbv.cellForRow(at: indexPath) as! GetResponsibleTableViewCell
                    tempCell.isChecked = !cell.isChecked
                }
            } else {
                if cell.isChecked {
                    if self.selectedPlayerList.count == self.playerList!.count {
                        let indexPath = IndexPath(row: 0, section: 0)
                                            let tempCell = self.tbv.cellForRow(at: indexPath) as! GetResponsibleTableViewCell
                                                               tempCell.isChecked = false
                    }
                    for i in 0..<self.selectedPlayerList.count {
                        if let item =  self.selectedPlayerList[i] as? RosterInfo {
                            if item.ID == self.playerList![indexPath.row - 1].ID {
                                self.selectedPlayerList.removeObject(at: i)
                                return
                            }
                        }
                    }
                } else {
                    self.selectedPlayerList.add(self.playerList![indexPath.row - 1])
                    if self.selectedPlayerList.count == self.playerList!.count {
                         let indexPath = IndexPath(row: 0, section: 0)
                        let tempCell = self.tbv.cellForRow(at: indexPath) as! GetResponsibleTableViewCell
                                           tempCell.isChecked = true
                    }
                }
            }
            print("\nList \n\n")
            self.selectedPlayerList.forEach { (item) in
                guard let item = item as? RosterInfo else {return}
                print(item.displayName)
            }
        }
        cell.backgroundColor = .clear
        return cell
    }

}
