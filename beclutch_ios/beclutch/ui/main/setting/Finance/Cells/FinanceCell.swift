//
//  FinanceCell.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class FinanceCell: UITableViewCell {
    @IBOutlet weak var feeTableView: UITableView!
    @IBOutlet weak var adjustHeightButton: UIButton!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    @IBOutlet weak var showPaymentBtn: BLButton!
    
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var showPaymentClosure: (() -> Void)?
    var adjustHeightClosure: (() -> Void)?
    var applyPaymentClosure: ((_ amount: Float, _ feeId: String) -> Void)?

    var isAnimatining = false

    var fees: [FeeInfo] = []

    private static var rowHeight: CGFloat = 110

    static func height(fees: [FeeInfo], isExpand: Bool) -> Int {
        var feeHeight = 0
        if fees.count > 0 {
            let isShow = fees.first?.canShowPaymentHistory ?? false
            let showPaymentHistoryHeight = isShow ? 50 : -5
            feeHeight = fees.count * Int(rowHeight) + showPaymentHistoryHeight
        }

        let baseHeight = 94
        return isExpand ? baseHeight + feeHeight : baseHeight
    }

    func config(fees: [FeeInfo], isExpand: Bool) {
        self.fees = fees

        let totalDue = FeeInfo.totalDueAmount(fees: fees)

        amountLabel.text = "Amount Due: " + String.moneyFormat(amount: totalDue)
        nameLabel.text = fees.first?.name

        feeTableView.reloadData()

        updateExpandImage(isExpand: isExpand)

        showPaymentBtn.makeGreenButton()
        
        let isShow = fees.first?.canShowPaymentHistory ?? false
        tableBottomConstraint.constant = isShow ? 45 : 0
    }

    func updateExpandImage(isExpand: Bool) {
        let imageName = isExpand ? "chevron_up_circle_fill" : "chevron_down_circle_fill"
        adjustHeightButton.setImage(UIImage(named: imageName), for: .normal)
        adjustHeightButton.tintColor = UIColor.COLOR_BLUE
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Action
    @IBAction func adjustHeightPressed(_ sender: Any) {
        if isAnimatining {
            return
        }

        adjustHeightClosure?()
    }

    @IBAction func showPaymentPressed(_ sender: Any) {
        showPaymentClosure?()
    }

}

extension FinanceCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FinanceCell.rowHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fees.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: PaymentOwnerCell = tableView.dequeueReusableCell(withIdentifier: PaymentOwnerCell.reuseIdentifier()) as? PaymentOwnerCell {
            let fee = fees[indexPath.row]
            cell.config(fee: fee)

            cell.applyPaymentClosure = { [weak self] amount in
                if let feeId = fee.id {
                    self?.applyPaymentClosure?(amount, feeId)
                }
            }

            return cell
        }

        return UITableViewCell()
    }
}
