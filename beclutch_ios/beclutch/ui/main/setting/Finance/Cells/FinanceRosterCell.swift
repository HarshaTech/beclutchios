//
//  FinanceRosterCell.swift
//  beclutch
//
//  Created by Chung Le on 4/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class FinanceRosterCell: UITableViewCell {
    @IBOutlet weak var rosterNameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var deleteFeeButton: UIButton!

    var deleteFeeClosure: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        deleteFeeButton.isHidden = !AccountCommon.isCoachManagerOrTreasure(roster: AppDataInstance.instance.currentSelectedTeam)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(fee: FeeInfo) {
        rosterNameLabel.text = fee.name

        if let amount = fee.amount, let paid = fee.paid {
            let balance = String.moneyFormat(amount: amount - paid)
            balanceLabel.text = "Balance: \(balance)"
        }
        dueDateLabel.text = "Due Date: " + (fee.dueDate?.dateCommonString() ?? "")
    }

    // MARK: - Action
    @IBAction func deleteFeePressed(_ sender: Any) {
        deleteFeeClosure?()
    }

}
