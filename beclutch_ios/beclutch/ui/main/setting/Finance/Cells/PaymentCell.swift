//
//  PaymentCell.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var dueValueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(fee: FeeInfo) {
        descriptionLabel.text = fee.description
        if let amount = fee.amount, let paid = fee.paid {
            let amount = amount - paid
//            amountLabel.text = "Amount Due: " + String.moneyFormat(amount: amount)
            dueValueLabel.text = String.moneyFormat(amount: amount)
        }

        dueDateLabel.text = fee.dueDate?.dateCommonString()
    }
}
