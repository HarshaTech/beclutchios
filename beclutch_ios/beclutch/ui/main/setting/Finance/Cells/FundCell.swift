//
//  FundCell.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class FundCell: UITableViewCell {
    @IBOutlet weak var feeTableView: UITableView!

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var collectedLabel: UILabel!

    @IBOutlet weak var adjustHeightButton: UIButton!
    @IBOutlet weak var deleteFeesButton: UIButton!

    var adjustHeightClosure: (() -> Void)?
    var deleteFeesClosure: (() -> Void)?
    var deleteFeeDetailClosure: ((_ financeDetailId: String, _ index: Int) -> Void)?

    var isAnimatining = false

    var fees: [FeeInfo] = []

    private static var rowHeight: CGFloat = 70

    static func height(fees: [FeeInfo], isExpand: Bool) -> Int {
        var feeHeight = 0
        if fees.count > 0 {
            feeHeight = fees.count * Int(rowHeight)
        }

        let baseHeight = 60
        return isExpand ? baseHeight + feeHeight : baseHeight
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        deleteFeesButton.isHidden = !AccountCommon.isCoachManagerOrTreasure(roster: AppDataInstance.instance.currentSelectedTeam)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(fees: [FeeInfo], isExpand: Bool) {
        self.fees = fees

        if let description = fees.first?.description {
            descriptionLabel.text = description
        }

        dueLabel.text = "Total Due\n" + String.moneyFormat(amount: FeeInfo.totalDueAmount(fees: fees))
        collectedLabel.text = "Total Collected\n" + String.moneyFormat(amount: FeeInfo.totalPaidAmount(fees: fees))

        feeTableView.reloadData()

        updateExpandImage(isExpand: isExpand)
    }

    func updateExpandImage(isExpand: Bool) {
        let imageName = isExpand ? "chevron_up_circle_fill" : "chevron_down_circle_fill"
        adjustHeightButton.setImage(UIImage(named: imageName), for: .normal)
        adjustHeightButton.tintColor = UIColor.COLOR_BLUE
    }

    // MARK: - Action
    @IBAction func adjustHeightPressed(_ sender: Any) {
        if isAnimatining {
            return
        }

        adjustHeightClosure?()
    }
    @IBAction func deleteFeesPressed(_ sender: Any) {
        deleteFeesClosure?()
    }
}

extension FundCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FundCell.rowHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fees.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: FinanceRosterCell = tableView.dequeueReusableCell(withIdentifier: FinanceRosterCell.reuseIdentifier()) as? FinanceRosterCell {
            let fee = fees[indexPath.row]
            cell.config(fee: fee)
            cell.deleteFeeClosure = { [weak self] in
                if let feeId = fee.id {
                    self?.deleteFeeDetailClosure?(feeId, indexPath.row)
                }
            }

            return cell
        }

        return UITableViewCell()
    }
}
