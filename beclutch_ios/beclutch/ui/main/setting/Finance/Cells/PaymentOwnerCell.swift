//
//  PaymentOwnerCell.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class PaymentOwnerCell: PaymentCell {
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var applyPaymentBtn: BLButton!

    var applyPaymentClosure: ((_ amount: Float) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        let currencyLabel = UILabel()
        currencyLabel.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        currencyLabel.text = "$  "
        currencyLabel.textAlignment = .center
        currencyLabel.textColor = .lightGray

        applyPaymentBtn.makeGreenButton()
        amountTextField.leftView = currencyLabel
        amountTextField.leftViewMode = .always
    }
    
    override func config(fee: FeeInfo) {
        super.config(fee: fee)
        
        applyPaymentBtn.isEnabled = AccountCommon.isCoachManagerOrTreasure()
        amountTextField.isEnabled = applyPaymentBtn.isEnabled
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func applyPaymentPressed(_ sender: Any) {
        if let amount = Float(self.amountTextField.text ?? "") {
            if amount > 0 {
                self.amountTextField.text = ""
                applyPaymentClosure?(amount)
            }
        }
    }
}
