//
//  PaymentHistoryCell.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {
    @IBOutlet weak var paidDateLabel: UILabel!
    @IBOutlet weak var dueDescription: UILabel!

    @IBOutlet weak var payAmount: UILabel!
    @IBOutlet weak var dueAmount: UILabel!
    @IBOutlet weak var balanceAmount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(fee: FeeInfo) {
        paidDateLabel.text = fee.createdDate?.dateCommonString()
        dueDescription.text = fee.description

        if let paid = fee.paid, let due = fee.amount {
            payAmount.text = String.moneyFormat(amount: paid)
            dueAmount.text = String.moneyFormat(amount: due)

            let balance = due - paid
            balanceAmount.text = String.moneyFormat(amount: balance)
        }

    }
}
