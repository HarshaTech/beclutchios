//
//  ManageFundViewController.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

protocol ManageFundViewControllerDelegate: NSObject {
    func didRemoveFees()
}

class ManageFundViewController: UIViewController {
    @IBOutlet weak var totalDueLabel: UILabel!
    @IBOutlet weak var totalCollectedLabel: UILabel!
    @IBOutlet weak var financeBtn: BLButton!

    @IBOutlet weak var tableView: UITableView!

    var viewModel = ManageFundViewModel()

    weak var deleagate: ManageFundViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configUI()
        viewModel.updateExpandTrack()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.COLOR_PRIMARY_DARK
        self.navigationItem.hidesBackButton = true
        let barButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backButton))
        barButton.image = UIImage(named: "baseline_arrow_back_ios_white_24dp")
        self.navigationItem.setLeftBarButton(barButton, animated: true)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Cambria", size: 20)!]
    }
    @objc func backButton() {

        self.navigationController?.popViewController(animated: true)
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    // MARK: - UI
    func configUI() {
        self.navigationController?.isNavigationBarHidden = false

        self.title = "Manage Funds"
        financeBtn.makeGreenButton()

        refreshTotalView()
    }

    func refreshTotalView() {
        var totalDue: Float = 0.0
        var totalPaid: Float = 0.0
        for fee in self.viewModel.fees {
            totalDue += fee.amount ?? 0
            totalPaid += fee.paid ?? 0
        }

        self.totalDueLabel.text = "Total due: " + String.moneyFormat(amount: totalDue - totalPaid)
        self.totalCollectedLabel.text = "Total collected: " + String.moneyFormat(amount: totalPaid)
    }

    // MARK: - Action
    @IBAction func financePressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ManageFundViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fees = viewModel.tableData[indexPath.row].1
        return CGFloat(FundCell.height(fees: fees, isExpand: viewModel.expandsTrack[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: FundCell = tableView.dequeueReusableCell(withIdentifier: FundCell.reuseIdentifier()) as? FundCell {
            let fees = viewModel.tableData[indexPath.row].1
            cell.config(fees: fees, isExpand: viewModel.expandsTrack[indexPath.row])

            cell.deleteFeesClosure = { [weak self] in
                DispatchQueue.main.async {
                    guard let self = self else {
                        return
                    }
                    
                    if let parentId = fees[0].parentId {
                        self.alert(title: "Confirm", message: "Are you sure you want to delete this fee?", okTitle: "Yes", okHandler: { (_) in
                            self.viewModel.doRemoveFee(feeId: parentId) { (error) in
                                if let error = error {
                                    AlertUtils.showGeneralError(message: error, parent: self)
                                } else {
                                    self.viewModel.tableData.remove(at: indexPath.row)
                                    self.tableView.reloadData()
                                    self.deleagate?.didRemoveFees()
                                }
                            }
                        }) { (_) in

                        }
                    }
                }
            }

            cell.deleteFeeDetailClosure = { [weak self] feeId, index in
                DispatchQueue.main.async {
                    guard let self = self else {
                        return
                    }
                    
                    self.alert(title: "Confirm", message: "Are you sure you want to delete this fee?", okTitle: "Yes", okHandler: { (_) in
                        self.viewModel.doRemoveFeeDetail(feeId: feeId, completedClosure: { (error) in
                            if let error = error {
                                AlertUtils.showGeneralError(message: error, parent: self)
                            } else {
                                if fees.count == 1 {
                                    self.viewModel.tableData.remove(at: indexPath.row)
                                } else {
                                    self.viewModel.tableData[indexPath.row].1.remove(at: index)
                                }

                                self.tableView.reloadData()
                                self.deleagate?.didRemoveFees()
                            }
                        })
                    }, cancelHandler: { _ in
                        
                    })
                }
            }

            cell.adjustHeightClosure = { [weak self] in
                guard let self = self else {
                    return
                }

                let newExpand = !self.viewModel.expandsTrack[indexPath.row]
                self.viewModel.expandsTrack[indexPath.row] = newExpand
                cell.updateExpandImage(isExpand: newExpand)

                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    tableView.beginUpdates()
                    tableView.endUpdates()

                    if cell.frame.maxY > tableView.frame.maxY {
                        tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: { _ in
                    cell.isAnimatining = false
                })
            }

            return cell
        }

        return UITableViewCell()
    }

}
