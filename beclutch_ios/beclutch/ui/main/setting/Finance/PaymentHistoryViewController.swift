//
//  PaymentHistoryViewController.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import SwiftEventBus

protocol PaymentHistoryViewControllerDelegate: class {
    func didRemoveFees()
}

class PaymentHistoryViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var deteleButton = UIBarButtonItem()

    var viewModel = PaymentHistoryViewModel()
    var rosterId: String?

    weak var delegate: PaymentHistoryViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addObser()
        configUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.COLOR_PRIMARY_DARK
           self.navigationItem.hidesBackButton = true
         let barButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backButton))
          barButton.image = UIImage(named: "baseline_arrow_back_ios_white_24dp")
          self.navigationItem.setLeftBarButton(barButton, animated: true)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Cambria", size: 20)!]
    }
    @objc func backButton() {

          self.navigationController?.popViewController(animated: true)
      }
    func configUI() {
        self.title = "Payment History"

        tableView.allowsMultipleSelection = false

        tableView.allowsSelection = false
        if AccountCommon.checkRightError() == nil {
//            deteleButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deletePressed))
            deteleButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deletePressed))

            refreshDeteleButton()
            self.navigationItem.setRightBarButton(deteleButton, animated: true)

            tableView.allowsSelection = true
        }

        if let rosterId = self.rosterId {
            self.viewModel.doLoadFinanceRoster(rosterId: rosterId)
        }
    }

    func addObser() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "FinanceRosterRes", handler: {
                   res in
            self.viewModel.isBusy.value = false

            let resAPI = res?.object as! FinanceRosterRes
            if resAPI.result! {
                self.viewModel.tableData = resAPI.fees ?? []
                self.tableView.reloadData()
            }
        })

//        SwiftEventBus.onMainThread(self, name: "FinanceFeeRemovesRes", handler: {
//                   res in
//            self.viewModel.isBusy.value = false
//            
//            let resAPI = res?.object as! FinanceFeeRemovesRes
//            if resAPI.result! {
//                if let rosterId = self.rosterId {
//                    self.viewModel.doLoadFinanceRoster(rosterId: rosterId)
//                }
//                
//                self.delegate?.didRemoveFees()
//            }
//        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UI
    func refreshDeteleButton() {
        let rows = tableView.indexPathsForSelectedRows ?? []
        deteleButton.isEnabled = rows.count > 0
    }

    // MARK: - Action
    @objc private func deletePressed() {
        if let indexPaths = tableView.indexPathsForSelectedRows, indexPaths.count > 0,
            let feeId = viewModel.tableData[0].id {
//            var feeIds: [String] = []
//            for indexPath in indexPaths {
//                if let feeId = viewModel.tableData[indexPath.row].id {
//                    feeIds.append(feeId)
//                }
//            }

//            self.viewModel.doRemoveFee(feeId: feeIds)
            self.viewModel.doRemoveFeeDetail(feeId: feeId) { [weak self] (error) in
                guard let self = self else {
                    return
                }

                if let error = error {
                    AlertUtils.showGeneralError(message: error, parent: self)
                } else {
                    if let rosterId = self.rosterId {
                        self.viewModel.doLoadFinanceRoster(rosterId: rosterId)
                        self.delegate?.didRemoveFees()
                    }
                }
            }
        }
    }
}

extension PaymentHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: PaymentHistoryCell = tableView.dequeueReusableCell(withIdentifier: PaymentHistoryCell.reuseIdentifier()) as? PaymentHistoryCell {
            let fee = self.viewModel.tableData[indexPath.row]
            cell.config(fee: fee)
            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        refreshDeteleButton()
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        refreshDeteleButton()
    }
}
