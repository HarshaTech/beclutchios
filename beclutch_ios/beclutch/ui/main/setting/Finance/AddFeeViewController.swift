//
//  AddFeeViewController.swift
//  beclutch
//
//  Created by Trinh Vu on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import ActionSheetPicker_3_0
import SwiftEventBus
import BEMCheckBox
import DropDown

protocol AddFeeViewControllerDelegate: class {
    func didAddFeeSuccess()
}

class AddFeeViewController: UIViewController, WWCalendarTimeSelectorProtocol {
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!

    @IBOutlet weak var btDate: UIButton!
    @IBOutlet weak var btTo: UIButton!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var chkReminders: BEMCheckBox!
    @IBOutlet weak var btToImage: UIImageView!
    let memberDropDown = DropDown()

    weak var delegate: AddFeeViewControllerDelegate?

    var viewModel = AddFeeViewModel()

    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        btDate.makeGrayRadius()
        addObserver()
    }

    // MARK: - UI
    func configUI() {
        let currencyLabel = UILabel()
        currencyLabel.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        currencyLabel.text = "$  "
        currencyLabel.textAlignment = .center
        currencyLabel.textColor = .lightGray

        amountTextField.leftView = currencyLabel
        amountTextField.leftViewMode = .always

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btTo.makeGreenButton()
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()
        chkReminders.boxType = .square
        chkReminders.on = true

        memberDropDown.anchorView = btTo
        memberDropDown.dataSource = ["To all Players", "To all Members", "Select from Roster"]
        memberDropDown.textFont = UIFont(name: "Cambria", size: 15)!
        memberDropDown.cellConfiguration = {(index, item) in return "\(item)"}
    }

    func showMemberDropDown() {
        btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        memberDropDown.selectionAction = {
            (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")

            self.btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.btTo.setTitle(self.memberDropDown.dataSource[index], for: .normal)
            if index == 0 {
                self.viewModel.selResult.value.isSelelctAll = false
                self.viewModel.selResult.value.isSelelctPlayers = true
            } else if index == 1 {
                self.viewModel.selResult.value.isSelelctAll = true
                self.viewModel.selResult.value.isSelelctPlayers = false
            } else if index == 2 {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRostersForChatVC") as! ChooseRostersForChatVC
                nextNav.viewModel.initial = self.viewModel.selResult.value
                nextNav.parentDelegate = self
                self.present(nextNav, animated: true, completion: nil)
            }
        }
        memberDropDown.cancelAction = {
            self.btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
        }
        memberDropDown.width = memberDropDown.anchorView?.plainView.width
        memberDropDown.bottomOffset = CGPoint(x: 0, y: ((memberDropDown.anchorView?.plainView.bounds.height)!))
        memberDropDown.show()
    }

    func addObserver() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.curDate.observe(DispatchQueue.main) {
            newDate, _ in
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "MMM d, yyyy"
                self.btDate.setTitle(formatter2.string(from: newDate), for: .normal)
            }.add(to: &disposal)
        self.viewModel.selResult.observe(DispatchQueue.main) {
            newSel, _ in
            if newSel.isSelelctAll! {
                self.btTo.setTitle("To all Members", for: .normal)
            } else if newSel.isSelelctPlayers {
                 self.btTo.setTitle("To all Players", for: .normal)
            } else {
                if newSel.dto?.count == 0 {
                    self.btTo.setTitle("Choose members", for: .normal)
                } else {
//                    var strVal: String = ""
//                    var disp: String = ""
//                    for item in newSel.dto! {
//                        strVal += (item.rosterName! + ", ")
//                    }
//                    if strVal.count > 0 {
//                        disp = String(strVal.dropLast(2))
//                    }
                    self.btTo.setTitle("Select from roster ", for: .normal)
                }
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "FeeAddRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! FeeAddRes
            if resAPI.result != nil {
                self.dismiss(animated: true, completion: nil)
                self.delegate?.didAddFeeSuccess()
            } else {
                AlertUtils.showGeneralError(message: resAPI.extraMsg, parent: self)
            }
        })
    }

    // MARK: - Action
    @IBAction func onClickToMembers(_ sender: Any) {
        showMemberDropDown()
    }

    @IBAction func onClickDate(_ sender: Any) {
        let selector = CalendarPickerCommon.picker()
        selector.delegate = self
        
        present(selector, animated: true, completion: nil)
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            self.viewModel.doAddFee(amount: self.amountTextField.text, description: self.descriptionTextField.text, notify: self.chkReminders.on)
        }
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    // MARK: - Validation
    func checkValidation() -> Bool {
        if !self.viewModel.selResult.value.isSelelctAll! && !self.viewModel.selResult.value.isSelelctPlayers && self.viewModel.selResult.value.dto?.count == 0 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Team Member", parent: self)
            return false
        }

        if self.descriptionTextField.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Description is required", parent: self)
            return false
        }

        if self.amountTextField.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Amount is required", parent: self)
            return false
        }

        return true
    }
}

extension AddFeeViewController: IChooseRosterForChat {
    func onChooseRosters(val: GroupSelectionResult) {
        self.viewModel.selResult.value = val
    }
}

extension AddFeeViewController {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        self.viewModel.curDate.value = date
    }
}
