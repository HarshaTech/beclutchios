//
//  FinanceViewController.swift
//  beclutch
//
//  Created by SM on 2/27/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import SDVersion
import SwiftEventBus

class FinanceViewController: BaseViewController {
    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet weak var btAddToolbar: UIButton!
    @IBOutlet weak var selectTeamLeadingConstraint: NSLayoutConstraint!

    @IBOutlet weak var totalDueLabel: UILabel!
//    @IBOutlet weak var totalCollectedLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var manageFundsBtn: BLButton!

    var viewModel = FinanceViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addObser()
        
        self.viewModel.doLoadFinanceTeam()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshUI()

        self.navigationController?.isNavigationBarHidden = true

        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Observer
    func addObser() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "FinanceTeamRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! FinanceTeamRes
            if resAPI.result! {
                if let array: [FeeInfo] = resAPI.fees {
                    self.viewModel.fees = array
                    self.viewModel.tableData = self.filterZeroBalance(fees: array)

                    self.viewModel.updateExpandTrack()

                    self.refreshTotalView()
                    self.tableView.reloadData()
                }
            }
        })

        SwiftEventBus.onMainThread(self, name: "FinancePayAddRes", handler: {
            res in
            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! FinancePayAddRes
            if resAPI.result != nil {
                AlertUtils.showWarningAlert(title: "Success", message: "Paid successfully", parent: self)
                self.viewModel.doLoadFinanceTeam()
            }
        })
    }

    // MARK: - UI

    func refreshUI() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btSelectTeam.makeStrokeOnlyButton()

        btAddToolbar.layer.cornerRadius = btAddToolbar.width/2.0
        btAddToolbar.backgroundColor = UIColor.COLOR_ACCENT

        btAddToolbar.isHidden = !AccountCommon.isCoachManagerOrTreasure(roster: AppDataInstance.instance.currentSelectedTeam)

        manageFundsBtn.isEnabled = AccountCommon.isCoachManagerOrTreasure()
        manageFundsBtn.makeGreenButton()
    }

    func refreshTotalView() {
//        var totalDue: Float = 0.0
//        for (_, fees) in self.viewModel.tableData {
//            totalDue += fees.map { Float($0.amount ?? 0.0) }.reduce(0, +)
//        }
        var totalDue: Float = 0.0
        var totalPaid: Float = 0.0
        for fee in self.viewModel.fees {
            totalDue += fee.amount ?? 0
            totalPaid += fee.paid ?? 0
        }

        self.totalDueLabel.text = "Total due: " + String.moneyFormat(amount: totalDue - totalPaid)
    }

    // MARK: - Action
    @IBAction func onClickSelectTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            showVC.delegate = self

            self.present(showVC, animated: true, completion: nil)
        }
    }

    @IBAction func onClickAddNew(_ sender: Any) {
        if let error = AccountCommon.checkRightError() {
            AlertUtils.showWarningAlert(title: "Warning", message: error, parent: self)
            return
        }

        if let addFeeVC = AddFeeViewController.initiateFromStoryboard() as? AddFeeViewController {
            addFeeVC.viewModel.selResult.value.isSelelctAll = false
            addFeeVC.viewModel.selResult.value.isSelelctPlayers = true
            addFeeVC.delegate = self

            self.present(addFeeVC, animated: true, completion: nil)
        }
    }

    @IBAction func manageFundPressed(_ sender: Any) {
        if let manageFundVC = ManageFundViewController.initiateFromStoryboard() as? ManageFundViewController {
            manageFundVC.deleagate = self
            manageFundVC.viewModel.fees = self.viewModel.fees

            navigationController?.pushViewController(manageFundVC, animated: true)
        }
    }

    @IBAction func backPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    // MARK: - Util
    private func filterZeroBalance(fees: [FeeInfo]) -> [(String, [FeeInfo])] {
        var sortedFee = Dictionary(grouping: fees, by: { $0.rosterId ?? "" }).map { $0 }.sorted(by: { (element1, element2) -> Bool in
            return element1.0 < element2.0
        }).filter({ (tupple) -> Bool in
            return  FeeInfo.totalDueAmount(fees: tupple.1) > 0
        })

        for i in 0 ..< sortedFee.count {
            let fee = sortedFee[i]

            var feeInfos = fee.1
            feeInfos.removeAll { $0.balance == 0 }
            sortedFee[i] = (fee.0, feeInfos)
        }

        return sortedFee
    }
}

extension FinanceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fees = self.viewModel.tableData[indexPath.row].1
        return CGFloat(FinanceCell.height(fees: fees, isExpand: self.viewModel.expandsTrack[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: FinanceCell = tableView.dequeueReusableCell(withIdentifier: FinanceCell.reuseIdentifier()) as? FinanceCell {
            let fees = self.viewModel.tableData[indexPath.row].1
            cell.config(fees: fees, isExpand: self.viewModel.expandsTrack[indexPath.row])

            cell.showPaymentClosure = { [weak self] in
                if let paymentHistoryVC = PaymentHistoryViewController.initiateFromStoryboard() as? PaymentHistoryViewController {
                    paymentHistoryVC.rosterId = fees.first?.rosterId
                    paymentHistoryVC.delegate = self

                    self?.navigationController?.pushViewController(paymentHistoryVC, animated: true)
                }
            }

            cell.adjustHeightClosure = { [weak self] in
                guard  let self = self else {
                    return
                }

                let newExpand = !self.viewModel.expandsTrack[indexPath.row]
                self.viewModel.expandsTrack[indexPath.row] = newExpand
                cell.updateExpandImage(isExpand: newExpand)

                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    tableView.beginUpdates()
                    tableView.endUpdates()

                    if cell.frame.maxY > tableView.frame.maxY {
                        tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: { _ in
                    cell.isAnimatining = false
                })
            }

            cell.applyPaymentClosure = { [weak self] (amount, feedId) in
                self?.viewModel.doAddPay(dueId: feedId, amount: amount)
            }

            return cell
        }

        return UITableViewCell()
    }
}

extension FinanceViewController: AddFeeViewControllerDelegate {
    func didAddFeeSuccess() {
        self.viewModel.doLoadFinanceTeam()
    }
}

extension FinanceViewController: SelectTeamVCDelegate {
    func didSelectTeam() {
        self.viewModel.doLoadFinanceTeam()
    }
}

extension FinanceViewController: PaymentHistoryViewControllerDelegate, ManageFundViewControllerDelegate {
    func didRemoveFees() {
        self.viewModel.doLoadFinanceTeam()
    }
}
