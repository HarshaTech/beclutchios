//
//  SettingViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

import Observable
import SwiftEventBus

class SettingViewController: UIViewController {

    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        SwiftEventBus.onMainThread(self, name: "UpdateDefaultTeamRes", handler: {
            res in

            if let resAPI = res?.object as? UpdateDefaultTeamRes, resAPI.result! { self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
                CustomLabelManager.shared.updateLabelDict(dict: resAPI.labels)
            }
        })

        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = .primary
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshTheme()
        self.navigationController?.navigationBar.isHidden = true

        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
    }

    func refreshTheme() {
        btSelectTeam.makeStrokeOnlyButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }
    
    @IBAction func onClickSelectTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            self.present(showVC, animated: true, completion: nil)
        }
    }

}
