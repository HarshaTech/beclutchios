//
//  SettingViewController.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class SettingTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    
    enum ItemType {
        case accountInfo
        case notification
        case location
        case opponents
        
        case finance
        case survey
        case volunteers
        case notebook
        case competitions
        
        case createClub
        case createTeam
        
        case teamSetting
        case teamPlan
        
        case support
        case about
        case logout
        
        func imageName() -> String {
            switch self {
            case .accountInfo:
                return "ic_setting_account"
            case .notification:
                return "baseline_notifications_black_24dp"
            case .location:
                return "ic_location"
            case .opponents:
                return "ic_setting_opponent"
                
            case .finance:
                return "ic_setting_finance"
            case .survey:
                return "ic_setting_survey"
            case .volunteers:
                return "ic_volunteer"
            case .notebook:
                return ""
            case .competitions:
                return ""
                
            
            case .createClub:
                return "baseline_group_add_black_24dp"
            case .createTeam:
                return "baseline_group_add_black_24dp"
                
            case .teamSetting:
                return "ic_setting_setting"
            case .teamPlan:
                return "baseline_schedule_black_24dp"
                
            case .support:
                return "baseline_live_help_black_24dp"
            case .about:
                return "baseline_info_black_24dp"
            case .logout:
                return "baseline_exit_to_app_black_24dp"
                
            default:
                return ""
            }
        }
    }
    
    var tableData: [(tittle: String?, identifier: String, type: ItemType?)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()

        setUpCustomLabel()

        let teamManagementCustom = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_team_management_s.rawValue, replacedString: AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME ?? "")
        if AccountCommon.isCoachOrManager() {
            tableData = [("General", "ContentHeaderCell_general", nil),
                         (nil, "cell_setting_account_info", .accountInfo), (nil, "cell_setting_notification", .notification), (nil, "cell_setting_location", .location), (nil, "cell_setting_opponents", .opponents),
                        ("Advanced Features", "ContentHeaderCell_advanced", nil),
                        (nil, "cell_setting_finances", .finance), (nil, "cell_setting_surveys", .survey), (nil, "cell_setting_volunteers", .volunteers), (nil, "cell_setting_notebook", .notebook),(nil, "cell_setting_competitions", .competitions),
                        ("Create New", "ContentHeaderCell_new", nil),
                        (nil, "cell_setting_create_club", .createClub), (nil, "cell_setting_create_team", .createTeam),
//                        ("Club Management (\(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME ?? ""))", "ContentHeaderCell_club", nil),
//                        (nil, "cell_setting_club", .teamSetting), (nil, "cell_setting_my_team", nil), (nil, "(nil, "cell_setting_team", nil),", nil),
                        (teamManagementCustom, "ContentHeaderCell_team", nil),
                        (nil, "cell_setting_team", .teamSetting),// (nil, "cell_setting_team_plan", .teamPlan),
                        ("App Management", "ContentHeaderCell_app", nil),
                        (nil, "cell_setting_support", .support), (nil, "cell_setting_about", .about), (nil, "cell_setting_logout", .logout)]
        } else if AccountCommon.isGuest() {
                tableData = [("General", "ContentHeaderCell_general", nil),
                            (nil, "cell_setting_account_info", .accountInfo), (nil, "cell_setting_notification", .notification),
                            ("Create New", "ContentHeaderCell_new", nil),
                            (nil, "cell_setting_create_club", .createClub), (nil, "cell_setting_create_team", .createTeam),
                            (teamManagementCustom, "ContentHeaderCell_team", nil),
                            (nil, "cell_setting_team", .teamSetting),
    //                        (nil, "cell_setting_team_plan", .teamPlan),
                            ("App Management", "ContentHeaderCell_app", nil),
                            (nil, "cell_setting_support", .support), (nil, "cell_setting_about", .about), (nil, "cell_setting_logout", .logout)]
        } else {
            tableData = [("General", "ContentHeaderCell_general", nil),
                        (nil, "cell_setting_account_info", .accountInfo), (nil, "cell_setting_notification", .notification),
                        ("Advanced Features", "ContentHeaderCell_advanced", nil),
                        (nil, "cell_setting_finances", .finance), (nil, "cell_setting_surveys", .survey), (nil, "cell_setting_volunteers", .volunteers), (nil, "cell_setting_notebook", .notebook),
                        ("Create New", "ContentHeaderCell_new", nil),
                        (nil, "cell_setting_create_club", .createClub), (nil, "cell_setting_create_team", .createTeam),
                        (teamManagementCustom, "ContentHeaderCell_team", nil),
                        (nil, "cell_setting_team", .teamSetting),
//                        (nil, "cell_setting_team_plan", .teamPlan),
                        ("App Management", "ContentHeaderCell_app", nil),
                        (nil, "cell_setting_support", .support), (nil, "cell_setting_about", .about), (nil, "cell_setting_logout", .logout)]
        }

        tableView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }

    private func setUpCustomLabel() {
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemData = tableData[indexPath.row]
        if let title = itemData.tittle {
            let cell = tableView.dequeueReusableCell(withIdentifier: itemData.identifier, for: indexPath) as! ContentHeaderCell
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            cell.config(title: title)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: itemData.identifier, for: indexPath) as! SettingItemCell
            cell.iconBackView?.backgroundColor = UIColor.COLOR_PRIMARY_DARK
            
//            if let imageName = itemData.type?.imageName() {
//                cell.iconImage.image = UIImage(named: imageName)
//                cell.iconImage.tintColor = UIColor.white
//            }
            
            if cell.titleLabel?.text == "Team Settings" || cell.titleLabel?.text == "Group Settings" {
                cell.titleLabel?.text = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_team_setting.rawValue)
            }

            return cell
        }
    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemData = tableData[indexPath.row]
        
        if let type = itemData.type {
            switch type {
            case .accountInfo:
                showAccountInfo()
            case .notification:
                showNotificationSetting()
            case .location:
                showLocationList()
            case .opponents:
                showOpponentList()
            case .finance:
                let financeVC = FinanceViewController.initiateFromStoryboard()
                self.navigationController?.pushViewController(financeVC, animated: true)
            case .survey:
                let surveyVC = SurveysViewController.initiateFromStoryboard()
                self.navigationController?.pushViewController(surveyVC, animated: true)
            case .volunteers:
                showVolunteer()
            case .notebook:
                if let notebookVC = ViewControllerFactory.noteBookViewController() {
                    self.navigationController?.pushViewController(notebookVC, animated: true)
                }
                
            case .competitions:
                self.goToCompetitionList()
                
            case .createClub:
                showCreateMyClub()
            case .createTeam:
                showCreateMyTeam()
            case .teamSetting:
                //team setting
                if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                    AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept the invitation first", parent: self)
                } else {
                    showTeamManage()
                }
            case .teamPlan:
                //team plan
                if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                    AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitiation first", parent: self)
                } else {
                    showTeamPlan()
                }
            case .support:
                if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
                    AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitiation first", parent: self)
                } else {
                    showTeamSupport()
                }
            case .about:
                showAboutUs()
            case .logout:
                //log out
                logoutAndBackToLogin()
            }
        }
    }
    
    func logoutAndBackToLogin() {
        alert(title: "Confirm", message: "Are you sure you want to Logout?", okTitle: "Yes", okHandler: { (_) in
            AppDataInstance.instance.logoutAndResetDatas()
            
            NavUtil.showLoginViewController()
        }, cancelHandler: { [weak self] (_) in
            self?.dismiss(animated: true, completion: nil)
        })
    }

    func showAccountInfo() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountInfoVC") as! AccountInfoVC
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }

    func showTeamSupport() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "TeamSupportVC") as! TeamSupportVC
        self.present(nextVC, animated: true, completion: nil)
    }

    func showTeamPlan() {
        let teamPlansVC = PlansPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        teamPlansVC.modalPresentationStyle = .fullScreen
        self.present(teamPlansVC, animated: true, completion: nil)
    }

    func showTeamManage() {
        if let nextViewController = ViewControllerFactory.makeTeamManageVC() {
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    func goToCompetitionList() {
        if let nextViewController = ViewControllerFactory.competitionListTableVC() {
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }

    func showCreateMyTeam() {
        if let nextViewController = ViewControllerFactory.makeCreateTeamVC() {
            self.present(nextViewController, animated: true, completion: nil)
        }
    }

    func showCreateMyClub() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "CreateNewClubViewController") as!  CreateNewClubViewController
        self.present(nextViewController, animated: true, completion: nil)
    }

    func showNotificationSetting() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as!  NotificationViewController
        self.present(nextViewController, animated: true, completion: nil)
    }

    func showLocationList() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewControlelr = mainStoryboard.instantiateViewController(withIdentifier: "LocationListViewController") as!  LocationListViewController
        nextViewControlelr.viewModel.isForChoose = false
        self.present(nextViewControlelr, animated: true, completion: nil)
    }

    func showOpponentList() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewControlelr = mainStoryboard.instantiateViewController(withIdentifier: "OpponentListViewController") as!  OpponentListViewController
        nextViewControlelr.viewModel.isForChoose = false
        self.present(nextViewControlelr, animated: true, completion: nil)
    }

    func showAboutUs() {
        let aboutUsStoryboard: UIStoryboard = UIStoryboard(name: "AboutUs", bundle: nil)
        let nextViewControlelr = aboutUsStoryboard.instantiateViewController(withIdentifier: "AboutUsViewController") as!  AboutUsViewController
        self.showDetailViewController(nextViewControlelr, sender: self)
    }

    func showVolunteer() {
        let volunteers = VolunteersViewController()
        self.navigationController?.pushViewController(volunteers, animated: true)
    }

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let itemData = tableData[indexPath.row]
        if itemData.tittle != nil {
            return 40
        }
        
        return 50
    }

     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
