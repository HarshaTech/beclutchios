//
//  LeaderBoardCell.swift
//  beclutch
//
//  Created by TechQuadra on 15/05/21.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {
    
    
    @IBOutlet weak var noL: UILabel!
    
    @IBOutlet weak var nameL: UILabel!
    
    @IBOutlet weak var scoreL: UILabel!
    @IBOutlet weak var scoreBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
