//
//  CompetitionListCell.swift
//  beclutch
//
//  Created by TechQuadra on 14/05/21.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit

class CompetitionListCell: UITableViewCell {

    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var titleL: UILabel!
    @IBOutlet weak var startDateL: UILabel!
    @IBOutlet weak var endDateL: UILabel!
    @IBOutlet weak var typeL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
