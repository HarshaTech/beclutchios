//
//  LeaderBoardVC.swift
//  beclutch
//
//  Created by TechQuadra on 15/05/21.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class LeaderBoardVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startDateL: UILabel!
    @IBOutlet weak var endDateL: UILabel!
    @IBOutlet weak var typeL: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popBGV: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var userTypeL: UILabel!
    @IBOutlet weak var teamNameL: UILabel!
    @IBOutlet weak var svoreTF: UITextField!
    @IBOutlet weak var updateBtn: UIButton!
    
    var viewModel = CompetitionViewModel()
    var disposal = Disposal()
    var data: LeaderBoardInfo?
    var roasterid = ""
    var leaderboardid = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        addObser()
        self.viewModel.doLoadTeamList(leaderboardId: data?.id ?? "0")
        self.titleLabel.text = data?.name
        self.startDateL.text = "Start Date: \(data?.startDate ?? "")"
        self.endDateL.text = "End Date: \(data?.endDate ?? "")"
        self.typeL.text = data?.colnName
        // Do any additional setup after loading the view.
    }
    
    @objc func backBtnClick() {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Observer
    func addObser() {
        
        self.backBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
        self.updateBtn.addTarget(self, action: #selector(updateBtnClicked), for: .touchUpInside)
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}
            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "TeamListRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! TeamListRes
            if resAPI.result! {
                if let array: [LeaderBoardTeamListInfo] = resAPI.fees {
                    self.viewModel.teamList = array
                    self.tableView.reloadData()
                }
            }
        })
        
        SwiftEventBus.onMainThread(self, name: "UpdateScoreRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            self.popBGV.isHidden = true
            self.popUpView.isHidden = true
            self.viewModel.isBusy.value = false
            self.viewModel.doLoadTeamList(leaderboardId: self.data?.id ?? "0")
        })

    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.popBGV.isHidden = true
        self.popUpView.isHidden = true
    }
    
    // MARK: Update Leaderboard Score
    @objc func updateBtnClicked() {
        if svoreTF.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Please enter score", parent: self)
        } else {
            self.viewModel.doUpdateScore(score: svoreTF.text!, roasterid: self.roasterid, leaderboardId:  data?.id ?? "0")
        }
    }
    
    //MARK: ScoreBoard btn Click Event
    @objc func scoreBtnClicked(_ sender: UIButton) {
        self.teamNameL.text = "\(self.viewModel.teamList[sender.tag].firstName ?? "") \(self.viewModel.teamList[sender.tag].lastName ?? "")"
        self.userTypeL.text = data?.name
        self.svoreTF.text = "\(self.viewModel.teamList[sender.tag].score ?? "0")"
        self.roasterid = "\(self.viewModel.teamList[sender.tag].roaster_id ?? "0")"
        self.leaderboardid = "\(self.viewModel.teamList[sender.tag].id ?? "0")"
        self.popBGV.isHidden = false
        self.popUpView.isHidden = false
    }
    
}

//MARK: UITableView Delegate D

extension LeaderBoardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.teamList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardCell") as! LeaderBoardCell
        cell.scoreL.text = "\(self.viewModel.teamList[indexPath.row].score ?? "0")"
        cell.nameL.text = "\(self.viewModel.teamList[indexPath.row].firstName ?? "") \(self.viewModel.teamList[indexPath.row].lastName ?? "")"
        cell.noL.text = "\(indexPath.row + 1)"
        cell.scoreBtn.tag = indexPath.row
        cell.scoreBtn.addTarget(self, action: #selector(scoreBtnClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
