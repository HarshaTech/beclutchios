//
//  AddCompetitionVC.swift
//  beclutch
//
//  Created by TechQuadra on 13/05/21.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import DropDown
import SwiftEventBus
import Observable
import ActionSheetPicker_3_0
import BEMCheckBox


protocol AddCompetitionVCDelegate: class {
    func didAddFeeSuccess()
}

class AddCompetitionVC: UIViewController {
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var topV: UIView!
    @IBOutlet weak var checkedBtn: BEMCheckBox!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var secondV: UIView!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var typeDownImage: UIImageView!
    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var endDateTF: UITextField!
    @IBOutlet weak var bottomV: UIView!
    @IBOutlet weak var frequencyTF: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var btnToImage: UIImageView!
    
    @IBOutlet weak var labelSV: UIStackView!
    @IBOutlet weak var typeL: UILabel!
    @IBOutlet weak var nameL: UILabel!
    @IBOutlet weak var tfStackV: UIStackView!
    
    
    let memberDropDown = DropDown()
    var viewModel = AddCompetitionViewModel()
    weak var delegate: AddCompetitionVCDelegate?
    private var disposal = Disposal()
    let reminderPicker = UIPickerView()
    let typePicker = UIPickerView()
    let reminderOption = ["Never", "24 Hours Prior"]
    let typeOption = ["Steps", "Distance", "kCal", "Workout", "Minutes", "Custom"]
    var startDateBool = true
    var startVal = ""
    var endVal = ""
    var startDate = String()
    var endDate = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setupPickerView()
        addObserver()
    }
    
    
    func setupPickerView() {
        reminderPicker.dataSource = self
        reminderPicker.delegate = self
        
        typePicker.dataSource = self
        typePicker.delegate = self
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelPicker))
        toolbar.setItems([cancelButton], animated: false)
        
        frequencyTF.text = reminderOption.first
        frequencyTF.inputAccessoryView = toolbar
        frequencyTF.delegate = self
        frequencyTF.inputView = reminderPicker
        
        typeTF.text = typeOption.first
        nameTF.text = typeOption.first
        typeTF.inputAccessoryView = toolbar
        typeTF.delegate = self
        typeTF.inputView = typePicker
    }
    
    @objc func cancelPicker() {
        self.frequencyTF.endEditing(true)
        self.typeTF.endEditing(true)
    }

    
    //MARK: UI
    func initialSetUp() {
        btnTo.layer.cornerRadius = 5
        topV.addShadow(offset: CGSize(width: 0.5, height: 0.5), color: .lightGray, radius: 2, opacity: 0.5)
        secondV.addShadow(offset: CGSize(width: 0.5, height: 0.5), color: .lightGray, radius: 2, opacity: 0.5)
        bottomV.addShadow(offset: CGSize(width: 0.5, height: 0.5), color: .lightGray, radius: 2, opacity: 0.5)
        btnTo.addTarget(self, action: #selector(openDrpoDown), for: .touchUpInside)
        checkedBtn.boxType = .square
        checkedBtn.on = false
        let drpdwn = #imageLiteral(resourceName: "down").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.typeDownImage.image = drpdwn
        self.typeDownImage.tintColor = UIColor.darkGray
        nameL.isHidden = true
        nameTF.isHidden = true
        
        backBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
        saveBtn.addTarget(self, action: #selector(saveBtnClicked), for: .touchUpInside)

        memberDropDown.anchorView = btnTo
        memberDropDown.dataSource = ["To all Players", "To all Members", "Select from Roster"]
        memberDropDown.textFont = UIFont(name: "Cambria", size: 15)!
        memberDropDown.cellConfiguration = {(index, item) in return "\(item)"}
        self.startDateTF.delegate = self
        self.endDateTF.delegate = self
    }
    
    @objc func openDrpoDown() {
        showMemberDropDown()
    }
    
    // MARK: - Validation
    func checkValidation() -> Bool {
        if !self.viewModel.selResult.value.isSelelctAll! && !self.viewModel.selResult.value.isSelelctPlayers && self.viewModel.selResult.value.dto?.count == 0 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Team Member", parent: self)
            return false
        }
        
        if self.titleTF.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Leaderboard title is required", parent: self)
            return false
        }
        
        if self.startDateTF.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Start date is required", parent: self)
            return false
        }
        
        if self.endDateTF.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "End date is required", parent: self)
            return false
        }
        
        if self.typeTF.text!.lowercased() == "custom" {
            if self.nameTF.text!.isEmpty {
                AlertUtils.showWarningAlert(title: "Warning", message: "Activity name is required", parent: self)
                return false
            }
        }
        
        return true
    }
    
    
    //MARK: DropDown Menu
    func showMemberDropDown() {
        btnToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        memberDropDown.selectionAction = {
            (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")

            self.btnToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.btnTo.setTitle(self.memberDropDown.dataSource[index], for: .normal)
            if index == 0 {
                self.viewModel.selResult.value.isSelelctAll = false
                self.viewModel.selResult.value.isSelelctPlayers = true
            } else if index == 1 {
                self.viewModel.selResult.value.isSelelctAll = true
                self.viewModel.selResult.value.isSelelctPlayers = false
            } else if index == 2 {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRostersForChatVC") as! ChooseRostersForChatVC
                nextNav.viewModel.initial = self.viewModel.selResult.value
                nextNav.parentDelegate = self
                self.present(nextNav, animated: true, completion: nil)
            }
        }
        memberDropDown.cancelAction = {
            self.btnToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
        }
        memberDropDown.width = memberDropDown.anchorView?.plainView.width
        memberDropDown.bottomOffset = CGPoint(x: 0, y: ((memberDropDown.anchorView?.plainView.bounds.height)!))
        memberDropDown.show()
    }
    
    //MARK: Back button
    @objc func backBtnClick() {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Observer
    func addObserver() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.selResult.observe(DispatchQueue.main) {
            newSel, _ in
            if newSel.isSelelctAll! {
                self.btnTo.setTitle("To all Members", for: .normal)
            } else if newSel.isSelelctPlayers {
                 self.btnTo.setTitle("To all Players", for: .normal)
            } else {
                if newSel.dto?.count == 0 {
                    self.btnTo.setTitle("Choose members", for: .normal)
                } else {
                    self.btnTo.setTitle("Select from roster ", for: .normal)
                }
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "AddCompetitionRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! AddCompetitionRes
            if resAPI.result != nil {
                DispatchQueue.main.async {
                    SwiftEventBus.post("RefreshCompetitionList", sender: nil)
                }
                self.delegate?.didAddFeeSuccess()
                self.dismiss(animated: true, completion: nil)
                
                
            } else {
                AlertUtils.showGeneralError(message: resAPI.extraMsg, parent: self)
            }
        })
    }

    //MARK: Open Calendar
    func openCalender() {
        let selector = CalendarPickerCommon.picker()
        selector.delegate = self
        present(selector, animated: true, completion: nil)
    }
    
    //MARK: Add Competition
    @objc func saveBtnClicked() {
        if checkValidation() {
            viewModel.doAddCompetition(activityName: nameTF.text!, activityType: typeTF.text!, notify: self.checkedBtn.on, title: titleTF.text!, frequency: frequencyTF.text!, startDate: startDate, endDate: endDate)
        }
    }
    
}

//MARK: TextField Delegate
extension AddCompetitionVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == startDateTF {
            self.startDateBool = true
            self.openCalender()
            return false
        } else if textField == endDateTF {
            self.startDateBool = false
            self.openCalender()
            return false
        } else if textField == frequencyTF {
            if frequencyTF.text == "" {
                self.frequencyTF.text = reminderOption.first
            } else {
                let index = reminderOption.firstIndex(of: self.frequencyTF.text!)!
                reminderPicker.selectRow(index, inComponent: 0, animated: true)
            }
        } else if textField == typeTF {
            if typeTF.text == "" {
                self.typeTF.text = typeOption.first
            } else {
                let index = typeOption.firstIndex(of: self.typeTF.text!)!
                typePicker.selectRow(index, inComponent: 0, animated: true)
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
   
}


//MARK: Calendar Delegate
extension AddCompetitionVC: WWCalendarTimeSelectorProtocol {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
//        self.viewModel.curDate.value = date
        if startDateBool == true {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM-dd-yyyy"
            self.startDateTF.text = formatter2.string(from: date)
            formatter2.dateFormat = "yyyy-MM-dd"
            self.startDate = formatter2.string(from: date)
        } else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM-dd-yyyy"
            self.endDateTF.text = formatter2.string(from: date)
            formatter2.dateFormat = "yyyy-MM-dd"
            self.endDate = formatter2.string(from: date)
        }
        print(date)
    }
}

//MARK: View Extension
extension UIView {
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        let layer = self.layer
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.cornerRadius = 5
        
        let backgroundCGColor = self.backgroundColor?.cgColor
        self.backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}



extension AddCompetitionVC: IChooseRosterForChat {
    func onChooseRosters(val: GroupSelectionResult) {
        self.viewModel.selResult.value = val
    }
}


//MARK: Picker View Delegate and Datasource
extension AddCompetitionVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == typePicker {
            return typeOption.count
        } else {
            return reminderOption.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == typePicker {
            let row = typeOption[row]
            return row
        } else {
            let row = reminderOption[row]
            return row
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == typePicker {
            typeTF.text = typeOption[row]
            
            if typeOption[row] == "Custom" {
                nameL.isHidden = false
                nameTF.isHidden = false
                nameTF.text = ""
                nameTF.isUserInteractionEnabled = true
                nameTF.becomeFirstResponder()
            } else {
                nameL.isHidden = true
                nameTF.isHidden = true
                nameTF.isUserInteractionEnabled = false
                nameTF.text = typeOption[row]
            }
        } else {
            frequencyTF.text = reminderOption[row]
        }
        
    }
}

