//
//  CometitionListVC.swift
//  beclutch
//
//  Created by TechQuadra on 13/05/21.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

class CometitionListVC: UIViewController {
  
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appBarView: UIView!
    @IBOutlet weak var selectTeamBtn: UIButton!
    @IBOutlet weak var addTeamBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = CompetitionViewModel()
    var disposal = Disposal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObser()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshUI()
        self.viewModel.doLoadLeaderList()
        self.navigationController?.isNavigationBarHidden = true

        self.selectTeamBtn.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.selectTeamBtn.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
        self.addTeamBtn.addTarget(self, action: #selector(goToAddView), for: .touchUpInside)
    }
    
    
    // MARK: - Observer
    func addObser() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)
        
        SwiftEventBus.onMainThread(self, name: "RefreshCompetitionList", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            self.viewModel.doLoadLeaderList()
        })

        SwiftEventBus.onMainThread(self, name: "LeaderListRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! LeaderListRes
            if resAPI.result! {
                if let array: [LeaderBoardInfo] = resAPI.fees {
                    self.viewModel.fees = array


                    self.tableView.reloadData()
                }
            }
        })
        
        SwiftEventBus.onMainThread(self, name: "RefreshCompetitionList", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            self.viewModel.doLoadLeaderList()
        })


    }

    
    // MARK: - UI

    func refreshUI() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appBarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        selectTeamBtn.makeStrokeOnlyButton()

        addTeamBtn.layer.cornerRadius = addTeamBtn.width/2.0
        addTeamBtn.backgroundColor = UIColor.COLOR_ACCENT

        addTeamBtn.isHidden = !AccountCommon.isCoachManagerOrTreasure(roster: AppDataInstance.instance.currentSelectedTeam)
    }

    
    //MARK: ADD NEW
    @objc func goToAddView() {
        
        if let addFeeVC = AddCompetitionVC.initiateFromStoryboard() as? AddCompetitionVC {

            self.present(addFeeVC, animated: true, completion: nil)
        }
    }
}


//MARK: TableView Delegate and Datasource
extension CometitionListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.fees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompetitionListCell") as! CompetitionListCell
        cell.startDateL.text = "Start Time:  \(self.viewModel.fees[indexPath.row].startDate ?? "")"
        cell.endDateL.text = "End Time:  \(self.viewModel.fees[indexPath.row].endDate ?? "")"
        cell.titleL.text = "\(self.viewModel.fees[indexPath.row].name ?? "")"
        cell.typeL.text = "[\(self.viewModel.fees[indexPath.row].colnName ?? "")]"
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteCompetition(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if let addFeeVC = LeaderBoardVC.initiateFromStoryboard() as? LeaderBoardVC {
            addFeeVC.data = self.viewModel.fees[indexPath.row]
            self.present(addFeeVC, animated: true, completion: nil)
        }
    }
    
    
    //MARK: Delete Competition
    @objc private func deleteCompetition(_ sender: UIButton) {
        
        self.viewModel.doRemoveCompetition(leaderboardId: self.viewModel.fees[sender.tag].id ?? "") { (error) in
            if let error = error {
                AlertUtils.showGeneralError(message: error, parent: self)
            } else {
                self.viewModel.doLoadLeaderList()
            }

        }
    }
}


