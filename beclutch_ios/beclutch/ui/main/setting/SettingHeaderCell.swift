//
//  SettingHeaderCell.swift
//  beclutch
//
//  Created by FAT_MAC on 6/25/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SettingHeaderCell: UIView {

    @IBOutlet weak var txtHeader: UILabel!

    override init(frame: CGRect) {
      super.init(frame: frame)

      setupView()
    }

    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setupView()
    }

    //common func to init our view
    private func setupView() {
        //Bundle.main.loadNibNamed("SettingHeaderCell", owner: self, options: nil)

        backgroundColor = UIColor.COLOR_BLUE
    }

    public func setHeaderText( header: String) {
        txtHeader.text = header
    }

}
