//
//  VolunteersTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class VolunteersTableViewCell: UITableViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    var disposeBag = DisposeBag()
    
    var onMessageClosure: (() -> ())?
    var onDeleteClosure: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()

        btnMessage.isHidden = !AccountCommon.isCoachOrManager()
        btnDelete.isHidden = btnMessage.isHidden

        btnMessage.tintColor = .COLOR_PRIMARY
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onMessage(_ sender: Any) {
        onMessageClosure?()
    }
    
    @IBAction func onDelete(_ sender: Any) {
        onDeleteClosure?()
    }
}
