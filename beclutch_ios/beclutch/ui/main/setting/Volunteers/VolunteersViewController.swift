//
//  VolunteersViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VolunteersViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let service: VolunteerServices = VolunteerServices()
    
    var modelObservable = BehaviorRelay<VolunteerListResponse?>(value: nil)
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var tbv: UITableView!
    
    private func dataObservalbe() -> Observable<Result<VolunteerListResponse, Error>> {
        return service.getVolunteerListService("", "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStyle()
        setUpUI()
        
        self.tbv.register(UINib(nibName: "VolunteersTableViewCell", bundle: nil), forCellReuseIdentifier: "VolunteersTableViewCell")
        
        tbv.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        tbv.rx.modelSelected(Volunteer.self)
            .subscribe(onNext: { [unowned self] model in
                let vc = ListAssignmentsViewController()
                vc.volunteerModel = model
                self.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: disposeBag)
        
        modelObservable
            .map { $0?.extra ?? [Volunteer]() }
            .bind(to: tbv.rx.items) { (tv, row, item) -> UITableViewCell in
                let cell = tv.dequeueReusableCell(withIdentifier: "VolunteersTableViewCell", for: IndexPath(row: row, section: 0)) as! VolunteersTableViewCell
                cell.selectionStyle = .none
                cell.lbTitle.text = item.name
                cell.lbDesc.text = self.convertDateToUserStyle(item.date)

                cell.btnMessage.tintColor = .COLOR_PRIMARY_DARK
                cell.btnMessage.setImage(UIImage(named: "ic_message")?.withRenderingMode(.alwaysTemplate), for: [])
                
                cell.btnDelete.tintColor = .COLOR_PRIMARY_RED
                cell.btnDelete.setImage(UIImage(named: "ic_remove")?.withRenderingMode(.alwaysTemplate), for: [])
                
                cell.onMessageClosure = {
                    let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in
                        self.showIndicator()
                        self.service.sendEmailVolunteer(item.volunteerId)
                            .successValue()
                            .filter { $0.result == true }
                            .subscribe(onNext: { [unowned self] value in
                                self.hideIndicator()
                                self.alert(message: "Email sent successfully")
                            })
                            .disposed(by: cell.disposeBag)
                    }
                    
                    let noAction = UIAlertAction(title: "No", style: .default) { _ in
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    self.alert(title: "Warnings", message: "Are you sure you want to Email this Volunteer List?", actions: [yesAction, noAction])
                }
                
                cell.onDeleteClosure = { [weak self] in
                    self?.showVolunteerDeleteAlert(item: item, cell: cell)
                }
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showIndicator()
        
        dataObservalbe()
            .successValue()
            .subscribe(onNext: { [unowned self] value in
                self.hideIndicator()
                self.modelObservable.accept(value)
            })
            //              .bind(to: modelObservable)
            .disposed(by: disposeBag)
    }

    private func setupStyle() {
        self.view.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    private func setUpUI() {
        addBtn.isHidden = !AccountCommon.isCoachOrManager()
    }

    private func showVolunteerDeleteAlert(item: Volunteer, cell: VolunteersTableViewCell) {
        let message = "Do you want to delete \(item.name)?"

        AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: message, confirmTitle: "Yes", parent: self, handler: { [weak self] in
            guard let strongSelf = self else {
                return
            }

            strongSelf.showIndicator()
            strongSelf.service.removeVolunteer(item.volunteerId)
                .successValue()
                .filter { $0.result == true }
                .subscribe(onNext: { [unowned strongSelf] value in
                    strongSelf.hideIndicator()

                    strongSelf.dataObservalbe()
                        .successValue()
                        .bind(to: strongSelf.modelObservable)
                        .disposed(by: strongSelf.disposeBag)
                })
                .disposed(by: cell.disposeBag)
        })
    }
    
    //MARK: -IBAction
    @IBAction func onAddNewList(_ sender: Any) {
        let vc = AssigmentsViewController()
        vc.dataModel = AssignmentModel()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func convertDateToUserStyle(_ date: String) -> String? {
        if date == "" { return nil }
        let components = date.components(separatedBy: "-")
        return "\(components[1])-\(components[2])-\(components[0])"
    }
}

extension VolunteersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
