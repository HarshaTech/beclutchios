//
//  AssignmentItemTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 24/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AssignmentItemTableViewCell: UITableViewCell {
    private let service: VolunteerServices            = VolunteerServices()
    
    var dataModel   = [AssignmentMember]()
    var disposeBag  = DisposeBag()
    
    @IBOutlet weak var lbAssignmentName: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbMemo: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data: Assignment?
    var isShowCollectionView = false
    var onChangeDropDownClosure: (()->())?
    var onEditClosure: (()->())?
    var onDeleteClosure: (()->())?
    var onAssignMemberClosure: ((_ oldRoster: [String], _ index: Int)->())?
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        collectionView.register(UINib(nibName: "RosterAssignedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RosterAssignedCollectionViewCell")
        
        collectionView.isScrollEnabled = false
        collectionViewHeight.constant = 0
        
        btnEdit.setImage(UIImage(named: "ic_edit")?.withRenderingMode(.alwaysTemplate), for: [])
        btnEdit.tintColor = .COLOR_PRIMARY_DARK
        
        btnDelete.setImage(UIImage(named: "ic_remove")?.withRenderingMode(.alwaysTemplate), for: [])
        btnDelete.tintColor = .COLOR_PRIMARY_RED

        btnEdit.isHidden = !AccountCommon.isCoachOrManager()
        btnDelete.isHidden = btnEdit.isHidden
        
        btnDropDown.setImage(UIImage(named: "circle_down")?.withRenderingMode(.alwaysTemplate), for: [])
        btnDropDown.tintColor = .COLOR_PRIMARY_DARK
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    func setData(_ data: Assignment) {
        self.data = data
        
        lbAssignmentName.text = "\(data.name)(\(data.slots))"
        if data.isAllDay.boolValueFromServer() {
            lbDate.text = "ALL DAY"
        } else {
            let startTime = "\(data.startTimeHour):\(data.startTimeMin) \(data.startTimeAMPM)"
            let endTime = "\(data.endTimeHour):\(data.endTimeMin) \(data.endTimeAMPM)"
            lbDate.text = "\(startTime) - \(endTime)"
        }
        lbMemo.text = data.memo
        
        collectionView.reloadData()
    }
    
    func hideCollectionView() {
        btnDropDown.setImage(UIImage(named: "circle_down")?.withRenderingMode(.alwaysTemplate), for: [])
        isShowCollectionView = false
        collectionViewHeight.constant = 0
        collectionView.isHidden = true
    }
    
    @IBAction func onChangeDropDown(_ sender: Any) {
        if isShowCollectionView {
            btnDropDown.setImage(UIImage(named: "circle_down")?.withRenderingMode(.alwaysTemplate), for: [])
            
            collectionViewHeight.constant = 0
            collectionView.isHidden = true
            onChangeDropDownClosure?()
        } else {
            btnDropDown.setImage(UIImage(named: "circle_up")?.withRenderingMode(.alwaysTemplate), for: [])
            
            self.service.getAssignmentMember(data?.assignmentId ?? "")
                .successValue()
                .filter { $0.result == true }
                .map { $0.extra }
                .subscribe(onNext: { [unowned self] value in
                    dataModel = value
                    
                    collectionViewHeight.constant = CGFloat(((data?.slots.intValue() ?? 0) / 4 + 1) * 40)
                    collectionView.isHidden = false
                    collectionView.reloadData()
                    onChangeDropDownClosure?()
                })
                .disposed(by: disposeBag)
        }
        isShowCollectionView = !isShowCollectionView
    }
    @IBAction func onEdit(_ sender: Any) {
        onEditClosure?()
    }
    
    @IBAction func onDelete(_ sender: Any) {
        onDeleteClosure?()
    }
}

extension AssignmentItemTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.slots.intValue() ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.width - 50) / 3)
        return CGSize(width: width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RosterAssignedCollectionViewCell", for: indexPath) as! RosterAssignedCollectionViewCell
        if dataModel.count > indexPath.row {
            cell.lbRosterName.text = dataModel[indexPath.row].lastName
        } else {
            cell.lbRosterName.text = "Sign Up"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let oldRoster = dataModel.compactMap { $0.memberId }
            onAssignMemberClosure?(oldRoster, indexPath.row)
    }
}
