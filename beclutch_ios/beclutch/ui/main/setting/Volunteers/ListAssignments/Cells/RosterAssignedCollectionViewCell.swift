//
//  RosterAssignedCollectionViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 24/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit

class RosterAssignedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lbRosterName: UILabel!
    @IBOutlet weak var downImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .COLOR_PRIMARY_DARK
        lbRosterName.textColor = .white
        downImageView.isHidden = !AccountCommon.isCoachOrManager()
    }
}
