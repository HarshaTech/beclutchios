//
//  ListAssignmentsViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 23/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ListAssignmentsViewController: UIViewController, IChooseRosterForChat {
    
    private let disposeBag = DisposeBag()
    private let service: VolunteerServices = VolunteerServices()
    
    let modelObservable = BehaviorRelay<AssignmentListResponse?>(value: nil)
    var volunteerModel: Volunteer?
    
    //change roster
    var oldRoster       = [String]()
    var index           = 0
    var assigmentID     = ""
    var isAssignSuccess = false
    
    private func dataObservalbe() -> Observable<Result<AssignmentListResponse, Error>> {
        return service.getVolunteerAssignments(volunteerModel?.volunteerId ?? "")
    }
    
    @IBOutlet weak var tbv: UITableView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var addButton: CircleButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupStyle()
        setupUI()
        
        lbName.text = volunteerModel?.name
        lbDate.text = convertDateToUserStyle(volunteerModel?.date ?? "")
        
        self.tbv.register(UINib(nibName: "AssignmentItemTableViewCell", bundle: nil), forCellReuseIdentifier: "AssignmentItemTableViewCell")
        
        tbv.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        modelObservable
            .map { $0?.extra ?? [Assignment]() }
            .bind(to: tbv.rx.items) { (tv, row, model) -> UITableViewCell in
                let cell = tv.dequeueReusableCell(withIdentifier: "AssignmentItemTableViewCell", for: IndexPath(row: row, section: 0)) as! AssignmentItemTableViewCell
                if self.isAssignSuccess {
                    cell.hideCollectionView()
                }
                
                cell.selectionStyle = .none
                
                cell.onChangeDropDownClosure = {
                    self.isAssignSuccess = false
                    self.tbv.reloadData()
                }
                
                cell.onAssignMemberClosure = { (oldRoster, index) in
                    if !AccountCommon.isCoachOrManager() {
                        return
                    }

                    self.oldRoster = oldRoster
                    self.index = index
                    self.assigmentID = model.assignmentId
                    
                    if let rosterController = ChooseRostersForChatVC.initiateFromStoryboard() as? ChooseRostersForChatVC {
                        
                        rosterController.parentDelegate = self
                        rosterController.viewModel.editPossible = true
                        
                        self.present(rosterController, animated: true, completion: nil)
                    }
                }
                
                cell.onEditClosure = {
                    let vc = AssigmentsViewController()
                    vc.flow = .editAssignment
                    vc.dataModel = AssignmentModel()
                    vc.dataModel.assignmentId = model.assignmentId
                    vc.dataModel.assignmentName = model.name
                    
                    vc.dataModel.volunteerId = model.volunteerId
                    vc.dataModel.volunteerName = model.volunteerName
                    vc.dataModel.volunteerDate = Date.dateServerFromString(dateStr: model.volunteerDate)?.volunteerDateString()
                    
                    vc.dataModel.reminderFrequency = model.reminderFrequency.intValue()
                    vc.dataModel.slots = model.slots.intValue()
                    
                    vc.dataModel.startTimeHour = model.startTimeHour
                    vc.dataModel.startTimeMin = model.startTimeMin
                    vc.dataModel.startTimeAMPM = model.startTimeAMPM
                    
                    vc.dataModel.endTimeHour = model.endTimeHour
                    vc.dataModel.endTimeMin = model.endTimeMin
                    vc.dataModel.endTimeAMPM = model.endTimeAMPM
                    
                    vc.dataModel.isAllDay = model.isAllDay.boolValueFromServer()
                    vc.dataModel.memo = model.memo
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                cell.onDeleteClosure = { [weak self] in
                    self?.showAssignmentDeleteAlert(model: model)
                }
                
                cell.setData(model)
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showIndicator()
        dataObservalbe()
            .successValue()
            .subscribe(onNext: { [unowned self] value in
                self.hideIndicator()
                self.modelObservable.accept(value)
            })
            .disposed(by: disposeBag)
    }

    deinit {
        print("ListAssignmentsViewController deinit")
    }

    private func setupStyle() {
        self.view.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    private func setupUI() {
        self.addButton.isHidden = !AccountCommon.isCoachOrManager()
    }
    
    @IBAction func onPopNavi(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onAddNewAssignment(_ sender: Any) {
        let vc = AssigmentsViewController()
        vc.flow = .newAssignment
        vc.dataModel = AssignmentModel()
        vc.dataModel.volunteerName = volunteerModel?.name

        if let dateStr = volunteerModel?.date {
            vc.dataModel.volunteerDate = Date.dateServerFromString(dateStr: dateStr)?.volunteerDateString()
        }

        vc.dataModel.volunteerId = volunteerModel?.volunteerId
        self.navigationController?.pushViewController(vc, animated: true)
    }

    private func showAssignmentDeleteAlert(model: Assignment) {
        let message = "Do you want to delete \(model.name)?"

        AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: message, confirmTitle: "Yes", parent: self, handler: { [weak self] in
            guard let strongSelf = self else {
                return
            }

            strongSelf.showIndicator()
            strongSelf.service.removeAssignment(model.assignmentId)
                .successValue()
                .filter { $0.result == true }
                .subscribe(onNext: { [unowned strongSelf] value in
                    strongSelf.hideIndicator()
                    strongSelf.dataObservalbe()
                        .successValue()
                        .bind(to: strongSelf.modelObservable)
                        .disposed(by: strongSelf.disposeBag)
                })
                .disposed(by: strongSelf.disposeBag)
        })
    }
    
    func convertDateToUserStyle(_ date: String) -> String? {
        if date == "" { return nil }
        let components = date.components(separatedBy: "-")
        return "\(components[1])-\(components[2])-\(components[0])"
    }
    
    func onChooseRosters(val: GroupSelectionResult) {
        if val.dto?.count == 0 { return }
        self.isAssignSuccess = false
        
        var newRoster = oldRoster
        if oldRoster.count > index {
            newRoster[index] = val.dto!.first!.rosterId!
        } else {
            newRoster.append(val.dto!.first!.rosterId!)
        }
        
        let rosterJson = newRoster.map { RosterJson(roster_id: $0.intValue()) }
        let oldRosterJson = oldRoster.map { RosterJson(roster_id: $0.intValue())  }
        
        service.addAssignmentMember(self.assigmentID,
                                    parseJson(rosterJson),
                                    parseJson(oldRosterJson))

            .successValue()
            .filter { $0.result == true}
            .subscribe(onNext: { [unowned self] value in
                self.isAssignSuccess = true
                self.tbv.reloadData()
//                self.isAssignSuccess = false
            })
            .disposed(by: disposeBag)
    }
    
    func parseJson(_ data: [RosterJson]) -> String {
        let jsonEncoder = JSONEncoder()
        let jsonData = try! jsonEncoder.encode(data)
        return String(data: jsonData, encoding: String.Encoding.utf8)!
        
    }
}

struct RosterJson: Codable {
    var roster_id: Int
    
    init(roster_id: Int) {
        self.roster_id = roster_id
    }
}

extension ListAssignmentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

