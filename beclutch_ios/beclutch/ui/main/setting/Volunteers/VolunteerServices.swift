//
//  VolunteerServices.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import RxSwift
class AssignmentModel {
    var assignmentId: String?
    var volunteerId: String?
    var volunteerName: String?
    var volunteerDate: String?
    var eventId: String?
    var eventType: String?
    var assignmentName: String?
    var reminderFrequency: Int?
    var slots: Int?
    var startTimeHour: String?
    var startTimeMin: String?
    var startTimeAMPM: String?
    var endTimeHour: String?
    var endTimeMin: String?
    var endTimeAMPM: String?
    var isAllDay: Bool?
    var memo: String?
    
    init() {
        assignmentId = nil
        volunteerId = nil
        volunteerName = nil
        volunteerDate = nil
        eventId = nil
        eventType = nil
        assignmentName = nil
        reminderFrequency = nil
        slots = nil
        startTimeHour = nil
        startTimeMin = nil
        startTimeAMPM = nil
        endTimeHour = nil
        endTimeMin = nil
        endTimeAMPM = nil
        isAllDay = nil
        memo = nil
    }
}

class VolunteerServices {
    func getVolunteerListService(_ eventId: String, _ volunteerId: String) -> Observable<Result<VolunteerListResponse, Error>> {
        APIManager.shared.getVolunteerList(eventId: eventId, volunteerId: volunteerId)
    }
    
    func addAssignment(_ assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>> {
        APIManager.shared.addAssignment(assignment: assignment)
    }
    
    func getVolunteerAssignments(_ id: String) -> Observable<Result<AssignmentListResponse, Error>> {
        APIManager.shared.getVolunteerAssignments(volunteerId: id)
    }
    
    func removeVolunteer(_ id: String) -> Observable<Result<CommonResponseNumber, Error>> {
        APIManager.shared.removeVolunteer(volunteerId: id)
    }
    
    func removeAssignment(_  id: String) -> Observable<Result<CommonResponseNumber, Error>> {
        APIManager.shared.removeAssignment(assignmentId: id)
    }
    
    func updateAssignment(assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>> {
        APIManager.shared.updateAssignment(assignment: assignment)
    }
    
    func getAssignmentMember(_ id: String) -> Observable<Result<AssignmentMemberListResponse, Error>> {
        APIManager.shared.getAssignmentMember(assignmentId: id)
    }
    
    func sendEmailVolunteer(_ id: String) -> Observable<Result<CommonResponseNumber, Error>> {
        APIManager.shared.sendEmailVolunteer(volunteerId: id)
    }
    
    func addAssignmentMember(_ assignmentId: String,
                             _ rosterJson: String,
                             _ oldRosterJson: String) -> Observable<Result<CommonResponseWithoutExtra, Error>> {
        APIManager.shared.addAssignmentMember(assignmentId: assignmentId, memberId: "", rosterJson: rosterJson, oldMemberId: "", oldRosterJson: oldRosterJson)
    }
}
