//
//  AssigmentsViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum AssignmentFlow {
    case newVolunteersAssignment
    case editAssignment
    case newAssignment
}

class AssigmentsViewController: UIViewController {
    @IBOutlet weak var titleString: UILabel!
    
    private let disposeBag                      = DisposeBag()
    private let service: VolunteerServices      = VolunteerServices()
    
    var flow: AssignmentFlow                    = .newVolunteersAssignment
    var dataModel                               : AssignmentModel!
    
    var fakeBindingList: BehaviorRelay<[[String : String]]> =
        BehaviorRelay(value:
                        [["number":"1.","name":""],
                         ["number":"2.","name":""],
                         ["number":"3.","name":""],
                         ["number":"4.","name":""],
                         ["number":"5.","name":""]])
    
    
    @IBOutlet weak var tbv: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStyle()
        
        if flow == .editAssignment {
            titleString.text = "Edit Assignment"
        }
        
        self.tbv.register(UINib(nibName: "BasicInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicInfoTableViewCell")
        self.tbv.register(UINib(nibName: "AssignmentTableViewCell", bundle: nil), forCellReuseIdentifier: "AssignmentTableViewCell")
        self.tbv.register(UINib(nibName: "TimeSelectionTableViewCell", bundle: nil), forCellReuseIdentifier: "TimeSelectionTableViewCell")
        self.tbv.register(UINib(nibName: "MemoTableViewCell", bundle: nil), forCellReuseIdentifier: "MemoTableViewCell")
        self.tbv.register(UINib(nibName: "BottomViewTableViewCell", bundle: nil), forCellReuseIdentifier: "BottomViewTableViewCell")
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tbv.tableHeaderView = UIView(frame: frame)
        tbv.separatorStyle = .none
        tbv.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        fakeBindingList
            .bind(to: tbv.rx.items) { (tv, row, item) -> UITableViewCell in
                switch row {
                case 0:
                    let cell = tv.dequeueReusableCell(withIdentifier: "BasicInfoTableViewCell", for: IndexPath(row: 0, section: 0)) as! BasicInfoTableViewCell
                    cell.selectionStyle = .none
                    
                    if self.flow != .newVolunteersAssignment {
                        cell.tfTitle.isUserInteractionEnabled = false
                        cell.tfDate.isUserInteractionEnabled = false
                    }
                    
                    cell.tfTitle.text = self.dataModel.volunteerName
                    cell.tfDate.text = self.dataModel.volunteerDate
                    
                    if self.dataModel.reminderFrequency == 0 {
                        cell.tfReminder.text = "Never"
                    } else {
                        cell.tfReminder.text = "24 Hours Prior"
                    }
                    
                    cell.tfTitle
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            self.dataModel.volunteerName = text
                        })
                        .disposed(by: cell.disposeBag)

                    cell.tfDate.rx.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            self.dataModel.volunteerDate = text
                        })
                        .disposed(by: cell.disposeBag)
                    
                    cell.tfReminder
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            if text == "Never" {
                                self.dataModel.reminderFrequency = 0
                            } else {
                                self.dataModel.reminderFrequency = 1
                            }
                        })
                        .disposed(by: cell.disposeBag)

                    cell.dateButton.rx.tap.subscribe(onNext: { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        let selector = CalendarPickerCommon.picker()
                        selector.delegate = strongSelf

                        strongSelf.present(selector, animated: true, completion: nil)
                    })
                    .disposed(by: cell.disposeBag)
                    
                    return cell
                case 1:
                    let cell = tv.dequeueReusableCell(withIdentifier: "AssignmentTableViewCell", for: IndexPath(row: 1, section: 0)) as! AssignmentTableViewCell
                    cell.selectionStyle = .none
                    cell.tfAssignment.text = self.dataModel.assignmentName
                    cell.btnSelectSlots.setTitle(self.dataModel.slots != nil ? "\(self.dataModel.slots!)" : "# of Slots", for: [])

                    cell.pickerIndex = self.dataModel.slots

                    cell.tfAssignment
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            self.dataModel.assignmentName = text
                        })
                        .disposed(by: cell.disposeBag)
                    
                    cell.onChangeSlots = {
                        self.dataModel.slots = $0
                    }
                    return cell
                case 2:
                    let cell = tv.dequeueReusableCell(withIdentifier: "TimeSelectionTableViewCell", for: IndexPath(row: 2, section: 0)) as! TimeSelectionTableViewCell
                    cell.selectionStyle = .none
                    
                    if let startTimeHour = self.dataModel.startTimeHour,
                       let startTimeMin = self.dataModel.startTimeMin,
                       let startTimeAMPM = self.dataModel.startTimeAMPM {
                        cell.tfStartTime.text = "\(startTimeHour):\(startTimeMin) \(startTimeAMPM)"
                    }
                    
                    if let endTimeHour = self.dataModel.endTimeHour,
                       let endTimeMin = self.dataModel.endTimeMin,
                       let endTimeAMPM = self.dataModel.endTimeAMPM {
                        cell.tfEndTime.text = "\(endTimeHour):\(endTimeMin) \(endTimeAMPM)"
                    }
                 
                    cell.isAllDay.on = self.dataModel.isAllDay ?? false
                    
                    if self.dataModel.isAllDay ?? false {
                        cell.tfStartTime.text = ""
                        cell.tfEndTime.text = ""
                        
                        cell.tfStartTime.isUserInteractionEnabled = false
                        cell.tfEndTime.isUserInteractionEnabled = false
                    } else {
                        cell.tfStartTime.isUserInteractionEnabled = true
                        cell.tfEndTime.isUserInteractionEnabled = true
                    }
                    
                    cell.tfStartTime
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            let (hour, min, am_pm) = splitDateString(text)
                            self.dataModel.startTimeHour = hour
                            self.dataModel.startTimeMin = min
                            self.dataModel.startTimeAMPM = am_pm
                        })
                        .disposed(by: cell.disposeBag)
                    
                    cell.tfEndTime
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            let (hour, min, am_pm) = splitDateString(text)
                            self.dataModel.endTimeHour = hour
                            self.dataModel.endTimeMin = min
                            self.dataModel.endTimeAMPM = am_pm
                        })
                        .disposed(by: cell.disposeBag)
                    
                    cell.changeOnDateClosure = {
                        self.dataModel.isAllDay = $0
                    }
                    
                    return cell
                case 3:
                    let cell = tv.dequeueReusableCell(withIdentifier: "MemoTableViewCell", for: IndexPath(row: 3, section: 0)) as! MemoTableViewCell
                    cell.selectionStyle = .none
                    cell.tfMemo.text = self.dataModel.memo
                    
                    cell.tfMemo
                        .rx.textInput.text.orEmpty
                        .asDriver()
                        .drive(onNext: { [unowned self] text in
                            self.dataModel.memo = text
                        })
                        .disposed(by: cell.disposeBag)
                    
                    return cell
                case 4:
                    let cell = tv.dequeueReusableCell(withIdentifier: "BottomViewTableViewCell", for: IndexPath(row: 4, section: 0)) as! BottomViewTableViewCell
                    
                    if self.flow != .newVolunteersAssignment {
                        cell.btnOptionalHeightContrainst.constant = 0
                    } else {
                        cell.btnOptionalHeightContrainst.constant = 50
                    }
                    
                    cell.selectionStyle = .none
                    
                    cell.onSaveAndAddClosure = {
                        self.onAddNewAssignment(cell.disposeBag, true)
                    }
                    
                    cell.onSaveClosure = {
                        self.onAddNewAssignment(cell.disposeBag)
                    }
                    
                    cell.onCancelClosure = {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    return cell
                    
                default:
                    break
                }
                
                return UITableViewCell()
            }
            .disposed(by: disposeBag)
    }

    private func setupStyle() {
        self.view.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }
    
    //ViewModel Func
    func onAddNewAssignment(_ bag: DisposeBag, _ isAddMoreAssignment: Bool = false) {
        guard let title = self.dataModel.volunteerName,
              title != "" else {
            self.alert(message: "Enter title to continue.")
            return
        }
        
        guard let date = self.dataModel.volunteerDate,
              date != "" else {
            self.alert(message: "Enter date to continue.")
            return
        }
        
        guard let _ = self.dataModel.reminderFrequency else {
            self.alert(message: "Enter Assignment Reminder to continue.")
            return
        }
        
        guard let assignName = self.dataModel.assignmentName,
              assignName != "" else {
            self.alert(message: "Enter Assignment to continue.")
            return
        }
        
        guard let _ = self.dataModel.slots else {
            self.alert(message: "Please choose # of slots to continue.")
            return
        }
        
        if !(self.dataModel.isAllDay ?? false || (self.dataModel.startTimeHour != nil && self.dataModel.endTimeHour != nil)) {
            self.alert(message: "Enter Time to continue.")
            return
        }
        
        if flow == .editAssignment {
            self.showIndicator()
            self.service.updateAssignment(assignment: self.dataModel)
                .successValue()
                .subscribe(onNext: { [unowned self] value in
                    self.hideIndicator()
                    if value.result != true {
                        self.alert(title: "Notification", message: value.msg ?? "", okHandler: { (_) in
                            
                        })
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                .disposed(by: bag)
        } else {
            self.showIndicator()
            self.service.addAssignment(self.dataModel)
                .successValue()
                .subscribe(onNext: { [unowned self] value in
                    self.hideIndicator()
                    if value.result != true {
                        self.alert(title: "Notification", message: value.msg ?? "", okHandler: { (_) in
                            
                        })
                    } else {
                        self.alert(title: "Notification", message: "Successfully Added Assignment", okHandler: { (_) in
                            if isAddMoreAssignment {
                                self.dataModel.volunteerId = String(value.extra ?? 0)

                                if let cell = tbv.cellForRow(at: IndexPath(row: 1, section: 0)) as? AssignmentTableViewCell {
                                    cell.tfAssignment.text = ""
                                    cell.tfAssignment.sendActions(for: .valueChanged)
                                }


                                self.dataModel.slots = nil
                                self.dataModel.reminderFrequency = nil
                                self.dataModel.isAllDay = nil

                                self.dataModel.startTimeHour = nil
                                self.dataModel.startTimeMin = nil
                                self.dataModel.startTimeAMPM = nil

                                self.dataModel.endTimeHour = nil
                                self.dataModel.endTimeMin = nil
                                self.dataModel.endTimeAMPM = nil

                                self.dataModel.memo = nil

                                tbv.reloadData()
                            } else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        })
                    }
                })
                .disposed(by: bag)
        }
    }
    
    func splitDateString(_ date: String) -> (String?, String?, String?) {
        if date == "" { return (nil, nil, nil) }
        let dateComponents = date.components(separatedBy: ":")
        if dateComponents.count < 2  { return (nil, nil, nil) }
        let hour = date.components(separatedBy: ":")[0]
        let min = date.components(separatedBy: ":")[1].components(separatedBy: " ")[0]
        let AM_PM = date.components(separatedBy: ":")[1].components(separatedBy: " ")[1]
        return (hour, min, AM_PM)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AssigmentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 236
        case 1:
            return 92
        case 2:
            return 132
        case 3:
            return 92
        default:
            if flow != .newVolunteersAssignment {
                return 80
            }
            return 131
        }
    }
}

extension AssigmentsViewController: WWCalendarTimeSelectorProtocol {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }

    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        if let cell = tbv.cellForRow(at: IndexPath(row: 0, section: 0)) as? BasicInfoTableViewCell {
            cell.tfDate.text = date.volunteerDateString()
            cell.tfDate.sendActions(for: .valueChanged)
        }
    }
}
