//
//  AssignmentTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class AssignmentTableViewCell: UITableViewCell {
    @IBOutlet weak var tfAssignment: UITextField!
    @IBOutlet weak var btnSelectSlots: UIButton!
    
    var fakeTf = UITextField()
    var onChangeSlots: ((_ slots: Int)->())?
    
    let slotsPicker = UIPickerView()
    
    var disposeBag = DisposeBag()

    var pickerIndex: Int?
    
    func setupPickerView() {
        slotsPicker.dataSource = self
        slotsPicker.delegate = self

        let index = pickerIndex ?? 1
        slotsPicker.delegate?.pickerView?(slotsPicker, didSelectRow: index - 1, inComponent: 0)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelPicker))
        
        toolbar.setItems([cancelButton], animated: false)
        
        fakeTf.inputAccessoryView = toolbar
        fakeTf.inputView = slotsPicker
        self.addSubview(fakeTf)
        fakeTf.becomeFirstResponder()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        btnSelectSlots.backgroundColor = .COLOR_PRIMARY_DARK
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }

    @IBAction func onSelectSlots(_ sender: Any) {
        setupPickerView()
    }
    
    @objc func cancelPicker() {
        self.endEditing(true)
    }
}

extension AssignmentTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 20
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row+1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerIndex = row + 1
        btnSelectSlots.setTitle("\(row+1)", for: [])
        onChangeSlots?(row+1)
    }
}
