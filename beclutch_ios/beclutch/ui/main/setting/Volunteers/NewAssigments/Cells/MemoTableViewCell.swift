//
//  MemoTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class MemoTableViewCell: UITableViewCell {
    @IBOutlet weak var tfMemo: UITextField!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
