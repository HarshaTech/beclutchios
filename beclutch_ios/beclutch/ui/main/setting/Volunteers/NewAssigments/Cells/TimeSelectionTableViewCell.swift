//
//  TimeSelectionTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift
import BEMCheckBox

class TimeSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var isAllDay: BEMCheckBox!
    
    let startTimeDatePicker = UIDatePicker()
    let endTimeDatePicker = UIDatePicker()
    
    var changeOnDateClosure: ((_ allDay: Bool)->())?
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDatePicker(picker: startTimeDatePicker, tf: tfStartTime)
        setDatePicker(picker: endTimeDatePicker, tf: tfEndTime)
        
        tfStartTime.delegate = self
        tfEndTime.delegate = self
        
        if isAllDay.on {
            tfStartTime.text = ""
            tfEndTime.text = ""
            
            tfStartTime.isUserInteractionEnabled = false
            tfEndTime.isUserInteractionEnabled = false
        } else {
            tfStartTime.isUserInteractionEnabled = true
            tfEndTime.isUserInteractionEnabled = true
        }

        isAllDay.tintColor = .COLOR_PRIMARY_DARK
        isAllDay.onCheckColor = .COLOR_PRIMARY_DARK
        isAllDay.onTintColor = .COLOR_PRIMARY_DARK
    }
    
    func setDatePicker(picker: UIDatePicker, tf: UITextField) {
        //Format Date
        picker.datePickerMode = .time
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
            picker.sizeToFit()
        } else {
            // Fallback on earlier versions
        }

        picker.date = Date.dateWith12PM()
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let doneButton: UIBarButtonItem?
        if tf == tfStartTime {
            doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneStartTimeDatePicker))
        } else {
            doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneEndTimeDatePicker))
        }
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton! ,spaceButton,cancelButton], animated: false)
        
        tf.inputAccessoryView = toolbar
        tf.inputView = picker
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    @objc func doneStartTimeDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        tfStartTime.text = formatter.string(from: startTimeDatePicker.date)
        self.endEditing(true)
    }
    
    @objc func doneEndTimeDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        tfEndTime.text = formatter.string(from: endTimeDatePicker.date)
        self.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.endEditing(true)
    }
    @IBAction func onSelect(_ sender: Any) {
        if isAllDay.on {
            tfStartTime.text = ""
            tfEndTime.text = ""
            
            tfStartTime.isUserInteractionEnabled = false
            tfEndTime.isUserInteractionEnabled = false
        } else {
            tfStartTime.isUserInteractionEnabled = true
            tfEndTime.isUserInteractionEnabled = true
        }
        changeOnDateClosure?(isAllDay.on)
    }
}

extension TimeSelectionTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}
