//
//  BottomViewTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 23/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class BottomViewTableViewCell: UITableViewCell {
    @IBOutlet weak var saveAndAddButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    var onSaveAndAddClosure: (()->())?
    var onSaveClosure: (()->())?
    var onCancelClosure: (()->())?
    
    @IBOutlet weak var btnOptionalHeightContrainst: NSLayoutConstraint!
  
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        saveAndAddButton.backgroundColor = .COLOR_PRIMARY_DARK
        saveButton.backgroundColor = .COLOR_PRIMARY
        cancelButton.backgroundColor = .COLOR_ACCENT
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }

    @IBAction func onSaveAndAdd(_ sender: Any) {
        onSaveAndAddClosure?()
    }
    
    @IBAction func onSave(_ sender: Any) {
        onSaveClosure?()
    }
    
    @IBAction func onCancel(_ sender: Any) {
        onCancelClosure?()
    }
}
