//
//  BasicInfoTableViewCell.swift
//  beclutch
//
//  Created by Triet Nguyen on 22/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class BasicInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfReminder: UITextField!
    
    @IBOutlet weak var dateButton: UIButton!
    
    let reminderOption = ["Never", "24 Hours Prior"]
    
    let datePicker = UIDatePicker()
    let reminderPicker = UIPickerView()
    
    var disposeBag = DisposeBag()

    var dateClosure: (() -> Void)?
    
//    func setDatePicker() {
        //Format Date
//        datePicker.datePickerMode = .date
//        if #available(iOS 13.4, *) {
//            datePicker.preferredDatePickerStyle = .wheels
//            datePicker.sizeToFit()
//        } else {
//            // Fallback on earlier versions
//        }
//
//
//        //ToolBar
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
//
//        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
//
//        tfDate.inputAccessoryView = toolbar
//        tfDate.inputView = datePicker
//        tfDate.delegate = self
//    }
    
    func setupPickerView() {
        reminderPicker.dataSource = self
        reminderPicker.delegate = self
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelPicker))
        
        toolbar.setItems([cancelButton], animated: false)
        
        tfReminder.text = reminderOption.first
        tfReminder.inputAccessoryView = toolbar
        tfReminder.delegate = self
        tfReminder.inputView = reminderPicker
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        setDatePicker()
        setupPickerView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    @objc func doneDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        tfDate.text = formatter.string(from: datePicker.date)
        self.endEditing(true)
    }
    
    @objc func cancelPicker(){
        self.endEditing(true)
    }

    @IBAction func dateAction(_ sender: Any) {
        dateClosure?()
    }
}

extension BasicInfoTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reminderOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = reminderOption[row]
        return row
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfReminder.text = reminderOption[row]
    }
}

extension BasicInfoTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if tfReminder.text == "" {
            self.tfReminder.text = reminderOption.first
        } else {
            let index = reminderOption.firstIndex(of: self.tfReminder.text!)!
            reminderPicker.selectRow(index, inComponent: 0, animated: true)
        }
    }
}
