//
//  AddSurveyViewController.swift
//  beclutch
//
//  Created by kakabala on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import BEMCheckBox
import ActionSheetPicker_3_0
import SwiftEventBus
import DropDown

let memberDropDown = DropDown()

protocol AddSurveyViewControllerDelegate: NSObject {
    func didAddSurveySuccess()
}

class AddSurveyViewController: BaseViewController, WWCalendarTimeSelectorProtocol {
    @IBOutlet weak var answerTableView: UITableView!

    @IBOutlet weak var btTo: UIButton!
    @IBOutlet weak var btToImage: UIImageView!

    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!

    @IBOutlet weak var anomynousCheckBox: BEMCheckBox!
    @IBOutlet weak var multipleCheckBox: BEMCheckBox!

    @IBOutlet weak var frequencyButton: UIButton!
    @IBOutlet weak var expireDayButton: UIButton!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    var viewModel = AddSurveyViewModel()
    var expirationPicked = false
    let memberDropDown = DropDown()

    weak var delegate: AddSurveyViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configUI()
        addObserver()

        self.expireDayButton.setTitle("Enter Date", for: .normal)
        self.expireDayButton.setTitleColor(.lightGray, for: .normal)
    }

    // MARK: - UI
    func configUI() {
        anomynousCheckBox.boxType = .square
        multipleCheckBox.boxType = .square

        frequencyButton.makeGrayRadius()
        expireDayButton.makeGrayRadius()

//        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        btTo.makeGreenButton()

        memberDropDown.anchorView = btTo
               memberDropDown.dataSource = ["To all Players", "To all Members", "Select from Roster"]
               memberDropDown.textFont = UIFont(name: "Cambria", size: 15)!
               memberDropDown.cellConfiguration = {(index, item) in return "\(item)"}
    }

    func showMemberDropDown() {
        btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        memberDropDown.selectionAction = {
            (index: Int, item: String) in print("Selected item: \(item) at index: \(index)")
            self.btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.btTo.setTitle(self.memberDropDown.dataSource[index], for: .normal)
            if index == 0 {
                self.viewModel.selResult.value.isSelelctAll = false
                self.viewModel.selResult.value.isSelelctPlayers = true
            } else if index == 1 {
                self.viewModel.selResult.value.isSelelctAll = true
                self.viewModel.selResult.value.isSelelctPlayers = false
            } else if index == 2 {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRostersForChatVC") as! ChooseRostersForChatVC
                nextNav.viewModel.initial = self.viewModel.selResult.value
                nextNav.parentDelegate = self
                self.present(nextNav, animated: true, completion: nil)
            }
        }
        memberDropDown.cancelAction = {
            self.btToImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
        }
        memberDropDown.width = memberDropDown.anchorView?.plainView.width
        memberDropDown.bottomOffset = CGPoint(x: 0, y: ((memberDropDown.anchorView?.plainView.bounds.height)!))
        memberDropDown.show()
    }

    func addObserver() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.expiredDate.observe(DispatchQueue.main) {
        newDate, _ in
            self.expireDayButton.setTitle(newDate.dateCommonString(), for: .normal)
            self.expireDayButton.setTitleColor(.black, for: .normal)
        }.add(to: &disposal)

        self.viewModel.selectedFrequencyIndex.observe(DispatchQueue.main) {
        newFrequency, _ in
            self.frequencyButton.setTitle(ReminderFrequencyEnum.titles()[newFrequency], for: .normal)
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "SurveyAddRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! GeneralRes
            if resAPI.result != nil {
                self.dismiss(animated: true, completion: nil)
                self.delegate?.didAddSurveySuccess()
            } else {
                AlertUtils.showGeneralError(message: resAPI.msg, parent: self)
            }
        })

        self.viewModel.selResult.observe(DispatchQueue.main) {
                   newSel, _ in
                   if newSel.isSelelctAll! {
                       self.btTo.setTitle("To all Players", for: .normal)
                   } else {
                       if newSel.dto?.count == 0 {
                           self.btTo.setTitle("To all Players", for: .normal)
                       } else {
                           var strVal: String = ""
                           var disp: String = ""
                           for item in newSel.dto! {
                               strVal += (item.rosterName! + ", ")
                           }
                           if strVal.count > 0 {
                               disp = String(strVal.dropLast(2))
                           }
                           self.btTo.setTitle(disp, for: .normal)
                       }
                   }
               }.add(to: &disposal)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func checkValidation() -> Bool {
        var error: String?
        if titleTextField.text!.isEmpty {
            error = "Please enter Title"
        } else if questionTextField.text!.isEmpty {
            error = "Please enter Question"
        } else if viewModel.answers.filter({ $0.count > 0 }).count < 2 {
            error = "Please select at least 2 Answers"
        } else if !expirationPicked {
            error = "Please enter Expiration Date"
        }

        if let err = error {
            AlertUtils.showWarningAlert(title: "Warning", message: err, parent: self)
            return false
        }

        return true
    }

    // MARK: - Action
    @IBAction func frequencyPressed(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Choose Frequency", rows: ReminderFrequencyEnum.titles(), initialSelection: 0, doneBlock: { [weak self] (_, index, _) in
            self?.viewModel.selectedFrequencyIndex.value = index
        }, cancel: nil, origin: self.view)
    }

    @IBAction func expireDatePressed(_ sender: Any) {
        let selector = CalendarPickerCommon.picker()
        selector.delegate = self
        
        present(selector, animated: true, completion: nil)
    }

    @IBAction func cancelPressed(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func savePressed(_ sender: Any) {
        if checkValidation() {
            viewModel.doAddNewSurvey(name: titleTextField.text, question: questionTextField.text, isMultiple: multipleCheckBox.on, isAnonymous: anomynousCheckBox.on, frequency: viewModel.selectedFrequencyIndex.value)
        }
    }
    @IBAction func handleToPressed(_ sender: Any) {
        showMemberDropDown()
    }

    @IBAction func backPressed(_ sender: Any) {
        cancelPressed(sender)
    }
}

extension AddSurveyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.answers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let answerCell = tableView.dequeueReusableCell(withIdentifier: NewSurveyAnswerCell.reuseIdentifier()) as? NewSurveyAnswerCell {
            answerCell.config(text: viewModel.answers[indexPath.row])
            if answerCell.questionTextField?.text?.count ?? 0 > 0 {
                answerCell.deleteButton.isHidden = false
            } else {
                answerCell.deleteButton.isHidden = true
            }

            answerCell.textChangeClosure = { [weak self] text in
                guard let self = self else {
                    return
                }

                self.viewModel.answers[indexPath.row] = text

                if text.count > 0, indexPath.row == self.viewModel.answers.count - 1 {
                    self.viewModel.answers.append("")
                    self.answerTableView.insertRows(at: [IndexPath(row: indexPath.row + 1, section: indexPath.section)], with: .automatic)
                }
            }

            answerCell.deleteClosure = { [weak self] in
                guard let self = self else {
                    return
                }

                if self.viewModel.answers.count == 1 {
                    return
                }

                self.viewModel.answers.remove(at: indexPath.row)
//                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.reloadData()
            }

            return answerCell
        }

        return UITableViewCell()
    }
}

extension AddSurveyViewController: IChooseRosterForChat {
    func onChooseRosters(val: GroupSelectionResult) {
        self.viewModel.selResult.value = val
    }
}

extension AddSurveyViewController {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        self.viewModel.expiredDate.value = date
        self.expirationPicked = true
    }
}
