//
//  SurveyResultViewController.swift
//  beclutch
//
//  Created by kakabala on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import SwiftEventBus

class SurveyResultViewController: BaseViewController {
    @IBOutlet weak var btSelectTeam: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    var viewModel: SurveyResultViewModel = SurveyResultViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.title = "Survey Results"
        viewModel.updateExpandTrack()
        addObserver()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = true

//        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
      //  self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func addObserver() {
        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "SurveyGetResponseDetailRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false
            let resAPI = res?.object as! SurveyGetResponseDetailRes
            if resAPI.result! {
                if let array: [SurveyRosterResponse] = resAPI.surveyRosterResponses {
                    let tableData = Dictionary(grouping: array, by: { $0.fullName }).map { $0 }.sorted(by: { (element1, element2) -> Bool in
                        return element1.0 < element2.0
                    })

                    if let detailResultController = SurveyDetailResultViewController.initiateFromStoryboard() as? SurveyDetailResultViewController {
                        detailResultController.viewModel.tableData = tableData
                        self.navigationController?.pushViewController(detailResultController, animated: true)
                    }
                }
            }
        })
    }

    // MARK: - Action
    @IBAction func onClickSelectTeam(_ sender: Any) {
//        let showVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectTeamVC") as! SelectTeamVC
//        showVC.modalPresentationStyle = .fullScreen
//        showVC.delegate = self
//
//        self.present(showVC, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

}

extension SurveyResultViewController: SelectTeamVCDelegate {
    func didSelectTeam() {
//        self.viewModel.doLoadTeamSurveys()
    }
}

extension SurveyResultViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let teamSurvey = viewModel.tableData[indexPath.row]
        return CGFloat(SurveyResultCell.height(survey: teamSurvey, isExpand: self.viewModel.expandsTrack[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SurveyResultCell = tableView.dequeueReusableCell(withIdentifier: SurveyResultCell.reuseIdentifier()) as? SurveyResultCell {
            let survey = self.viewModel.tableData[indexPath.row]
            cell.config(survey: survey, isExpand: self.viewModel.expandsTrack[indexPath.row])

            cell.adjustHeightClosure = { [weak self] in
                guard let self = self else {
                    return
                }

                let newExpand = !self.viewModel.expandsTrack[indexPath.row]
                self.viewModel.expandsTrack[indexPath.row] = newExpand
                cell.updateExpandImage(isExpand: newExpand)

                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    tableView.beginUpdates()
                    tableView.endUpdates()

                    if cell.frame.maxY > tableView.frame.maxY {
                        tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: { _ in
                    cell.isAnimatining = false
                })
            }

            cell.showDetailResultClosure = { [weak self] in
                if let surveyId = survey.id {
                    self?.viewModel.doLoadSurveyDetailResult(surveyId: surveyId)
                }
            }

            return cell
        }

        return UITableViewCell()
    }
}
