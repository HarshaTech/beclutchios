//
//  SurveyCell.swift
//  beclutch
//
//  Created by NamViet on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SurveyCell: UITableViewCell {
    @IBOutlet weak var surveyLabel: UILabel!
    @IBOutlet weak var createDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!

    @IBOutlet weak var anonymousLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!

    @IBOutlet weak var answerTableView: UITableView!

    @IBOutlet weak var adjustHeightButton: UIButton!
    @IBOutlet weak var detailResultButton: UIButton!
    @IBOutlet weak var submitBtn: BLButton!
    @IBOutlet weak var deleteSurveyButton: UIButton!

    var showDetailResultClosure: (() -> Void)?
    var deleteSurveyItem: (() -> Void)?
    var adjustHeightClosure: (() -> Void)?
    var submitAnswerClosure: ((_ answer: [SurveyAnswerDetail]) -> Void)?

    var isAnimatining = false

    private var survey: TeamSurvey = TeamSurvey(val: nil)

    static var answerRowHeight: CGFloat = 70

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static func height(survey: TeamSurvey, isExpand: Bool) -> Int {
        var detailHeight = 0
        let answers = survey.answerDetails
        if answers.count > 0 {
            detailHeight = answers.count * Int(answerRowHeight) + 120
        }

        let baseHeight = 120
        return isExpand ? baseHeight + detailHeight : baseHeight
    }

    func config(survey: TeamSurvey, isExpand: Bool) {
        self.survey = survey

        submitBtn.makeGreenButton()
        submitBtn.contentEdgeInsets = UIEdgeInsets(top: 10, left: 50, bottom: 10, right: 50)
        self.anonymousLabel.backgroundColor = UIColor.COLOR_BLUE
        self.anonymousLabel.layer.cornerRadius = 5
        anonymousLabel?.layer.masksToBounds = true

        self.anonymousLabel.text = survey.isAnonymous ? "Anonmynous" : "Not Anonymous"

        if let startDate = survey.startDate?.dateCommonString() {
            createDateLabel.text = "Created: " + startDate
        }

        if let endDate = survey.endDate?.dateCommonString() {
            endDateLabel.text = "Expires: " + endDate
        }

        surveyLabel.text = survey.name
        //let fontName = survey.isResponse ? "Cambria" : "Cambria-Bold"
        let fontSize = surveyLabel.font.pointSize
        let font = survey.isResponse ? UIFont(name: "Cambria", size: fontSize) :
            UIFont(name: "Cambria-Bold", size: fontSize)//UIFont.boldSystemFont(ofSize: fontSize)//UIFont(name: fontName, size: surveyLabel.font.pointSize)
        surveyLabel.font = font

        questionLabel.text = survey.questionText
        detailResultButton.isHidden = AccountCommon.checkRightError() != nil

        answerTableView.reloadData()

        updateExpandImage(isExpand: isExpand)
    }

    func updateExpandImage(isExpand: Bool) {
        let imageName = isExpand ? "chevron_up_circle_fill" : "chevron_down_circle_fill"
        adjustHeightButton.setImage(UIImage(named: imageName), for: .normal)
        adjustHeightButton.tintColor = UIColor.COLOR_BLUE
    }

    // MARK: - Action
    @IBAction func adjustHeightPressed(_ sender: Any) {
        if isAnimatining {
            return
        }

        adjustHeightClosure?()
    }

    @IBAction func detailResultPressed(_ sender: Any) {
        showDetailResultClosure?()
    }

    @IBAction func submitPressed(_ sender: Any) {
        let summitedAnswers = survey.answerDetails.filter { $0.isSelected == true }
        self.submitAnswerClosure?(summitedAnswers)
    }
    @IBAction func deleteSurveyPressed(_ sender: Any) {
        deleteSurveyItem?()
    }

}

extension SurveyCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SurveyCell.answerRowHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return survey.answerDetails.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SurveyAnswerCell = tableView.dequeueReusableCell(withIdentifier: SurveyAnswerCell.reuseIdentifier()) as? SurveyAnswerCell {
            let answer = survey.answerDetails[indexPath.row]
            cell.config(answer: answer)

            cell.updateOptionText = { [weak self] text in
                self?.survey.answerDetails[indexPath.row].optionalText = text
            }

            cell.updateCheckBoxClosure = { [weak self] check, optionText in
                guard let self = self else {
                    return
                }

                if self.survey.isMultiple || check == false {
                    self.survey.answerDetails[indexPath.row].isSelected = check

                    self.answerTableView.reloadRows(at: [indexPath], with: .automatic)
                } else {
                    for i in 0 ..< self.survey.answerDetails.count {
                        self.survey.answerDetails[i].isSelected = i == indexPath.row
                    }

                    self.answerTableView.reloadData()
                }
            }

            return cell
        }

        return UITableViewCell()
    }
}
