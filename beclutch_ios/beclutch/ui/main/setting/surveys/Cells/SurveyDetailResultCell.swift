//
//  SurveyDetailResultCell.swift
//  beclutch
//
//  Created by Vu Trinh on 5/9/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SurveyDetailResultCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statisticLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(name: String, statistic: String) {
        nameLabel.text = name
        statisticLabel.text = statistic
    }
}
