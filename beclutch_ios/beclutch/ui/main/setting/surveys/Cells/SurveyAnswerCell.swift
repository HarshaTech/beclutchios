//
//  SurveyAnswerCell.swift
//  beclutch
//
//  Created by NamViet on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox

class SurveyAnswerCell: UITableViewCell {
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionTextField: UITextField!

    var updateCheckBoxClosure: ((_ isCheck: Bool, _ optionText: String?) -> Void)?
    var updateOptionText: ((_ text: String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        checkBox.delegate = self
        checkBox.boxType = .square

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(answer: SurveyAnswerDetail) {
        titleLabel.text = answer.text
        checkBox.on = answer.isSelected
        optionTextField.text = answer.optionalText
    }

    // MARK: - Action
    @IBAction func optionTextChanged(_ sender: Any) {
        updateOptionText?(optionTextField.text ?? "")
    }
}

extension SurveyAnswerCell: BEMCheckBoxDelegate {
    func animationDidStop(for checkBox: BEMCheckBox) {
        updateCheckBoxClosure?(checkBox.on, optionTextField.text)
    }
}
