//
//  NewSurveyAnswerCell.swift
//  beclutch
//
//  Created by NamViet on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class NewSurveyAnswerCell: UITableViewCell {
    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!

    var deleteClosure: (() -> Void)?
    var textChangeClosure: ((_ text: String) -> Void)?

    override func awakeFromNib() {
         super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(text: String?) {
        questionTextField.text = text
    }

    // MARK: - Action
    @IBAction func answerValueChanged(_ sender: UITextField) {
        guard let text = sender.text else {
            return
        }

        textChangeClosure?(text)
        deleteButton.isHidden = text.count <= 0
    }

    @IBAction func deletePressed(_ sender: Any) {
        deleteClosure?()
    }

}
