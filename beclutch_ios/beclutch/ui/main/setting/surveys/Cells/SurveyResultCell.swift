//
//  SurveyResultCell.swift
//  beclutch
//
//  Created by NamViet on 5/3/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SurveyResultCell: UITableViewCell {
    @IBOutlet weak var adjustHeightButton: UIButton!
    @IBOutlet weak var detailResultButton: UIButton!

    @IBOutlet weak var answerTableView: UITableView!

    @IBOutlet weak var surveyTitleLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var statisticLabel: UILabel!

    @IBOutlet weak var showDetailButtonHeightConstraint: NSLayoutConstraint!

    var showDetailResultClosure: (() -> Void)?
    var adjustHeightClosure: (() -> Void)?

    var isAnimatining = false

    private var survey: TeamSurvey = TeamSurvey(val: nil)

    static var answerRowHeight: CGFloat = 40

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static func height(survey: TeamSurvey, isExpand: Bool) -> Int {
        var detailHeight = 0
        let showDetailAreaHeight = survey.isAnonymous ? 0 : 40

        let answers = survey.answerDetails
        if answers.count > 0 {
            detailHeight = answers.count * Int(answerRowHeight) + showDetailAreaHeight + 25
        }

        let baseHeight = 85
        return isExpand ? baseHeight + detailHeight : baseHeight
    }

    func config(survey: TeamSurvey, isExpand: Bool) {
        self.survey = survey

        surveyTitleLabel.text = survey.name
        questionLabel.text = survey.questionText
        statisticLabel.text = "\(survey.response)/\(survey.teamMemberCount) response(s)"

        detailResultButton.isHidden = survey.isAnonymous
        showDetailButtonHeightConstraint.constant = survey.isAnonymous ? 0 : 32

        answerTableView.reloadData()

        updateExpandImage(isExpand: isExpand)
    }

    func updateExpandImage(isExpand: Bool) {
        let imageName = isExpand ? "chevron_up_circle_fill" : "chevron_down_circle_fill"
        adjustHeightButton.setImage(UIImage(named: imageName), for: .normal)
        adjustHeightButton.tintColor = UIColor.COLOR_BLUE
    }

    // MARK: - Action
    @IBAction func adjustHeightPressed(_ sender: Any) {
        if isAnimatining {
            return
        }

        adjustHeightClosure?()
    }

    @IBAction func detailResultPressed(_ sender: Any) {
        showDetailResultClosure?()
    }
}

extension SurveyResultCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SurveyResultCell.answerRowHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return survey.answerDetails.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let answer = survey.answerDetails[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") {
            if let text = answer.text {
                cell.textLabel?.text = text + " - " + String(answer.count) + " response(s)"
                cell.textLabel?.font = UIFont(name: "Cambria", size: 13.0)
            }

            return cell
        }

        return UITableViewCell()
    }
}
