//
//  SurveysViewController.swift
//  beclutch
//
//  Created by kakabala on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import SwiftEventBus

class SurveysViewController: BaseViewController {
    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet weak var btAddToolbar: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    var isManager = true

    let viewModel: SurveysViewModel = SurveysViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addObser()
        viewModel.doLoadTeamSurveys()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTheme()

        self.navigationController?.isNavigationBarHidden = true

        self.btSelectTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btSelectTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
    }
    
    deinit {
        print("SurveysViewController deinit")
    }
    
    func refreshTheme() {
        if let role = AppDataInstance.instance.currentSelectedTeam?.ROLE_INFO.ID, let roleInt = Int(role) {
            self.isManager = roleInt <= 3
        }
        
        btAddToolbar.backgroundColor = UIColor.COLOR_ACCENT
        if self.isManager {
            btAddToolbar.isHidden = false
        } else {
            btAddToolbar.isHidden = true
        }
        btSelectTeam.makeStrokeOnlyButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Observer
        func addObser() {
            self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
                newBusy, oldBusy in
                guard let self = self else {
                    return
                }
                
                if newBusy == oldBusy {return}

                if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

            SwiftEventBus.onMainThread(self, name: "SurveyTeamGetRes", handler: { [weak self]
                res in
                guard let self = self else {
                    return
                }

                self.viewModel.isBusy.value = false
                let resAPI = res?.object as! SurveyTeamGetRes
                if resAPI.result! {
                    if let array: [TeamSurvey] = resAPI.teamSurveys {
                        self.viewModel.tableData = array

                        self.viewModel.updateExpandTrack()

                        self.tableView.reloadData()
                    }
                }
            })

            SwiftEventBus.onMainThread(self, name: "SurveyDeleteRes", handler: { [weak self]
                res in
                guard let self = self else {
                    return
                }

                self.viewModel.isBusy.value = false
                let resAPI = res?.object as! GeneralRes
                if resAPI.result != nil {
                    self.viewModel.doLoadTeamSurveys()
                } else {
                    AlertUtils.showGeneralError(message: resAPI.msg, parent: self)
                }
            })

            SwiftEventBus.onMainThread(self, name: "SurveySubmitRes", handler: { [weak self]
                res in
                let resAPI = res?.object as! GeneralRes
                self?.handleSubmitResponse(resAPI: resAPI)
            })
            
            SwiftEventBus.onMainThread(self, name: "SurveyMultipleSubmitRes", handler: { [weak self]
                res in
                let resAPI = res?.object as! GeneralRes
                self?.handleSubmitResponse(resAPI: resAPI)
            })

            SwiftEventBus.onMainThread(self, name: "GetResponsibleRes", handler: { [weak self]
                res in
                guard let self = self else {
                    return
                }

                let resAPI = res?.object as! GetResponsibleRes

                if resAPI.result! {
                    if let roster = resAPI.rosters {
                        if roster.count > 1 {
                            let vc = GetResponsibleViewController()
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.playerList = resAPI.rosters
                            vc.delegate = self
                            self.present(vc, animated: true, completion: nil)
                            self.viewModel.isBusy.value = false
                        } else {
//                            self.viewModel.doSumbitAnswerForMultipleRoster(rosters: [AppDataInstance.instance.currentSelectedTeam?.ID ?? ""])
                            self.viewModel.doSumbitAnswer()
                        }
                    }
                }
            })
        }

    // MARK: - Action
    @IBAction func onClickSelectTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            showVC.delegate = self

            self.present(showVC, animated: true, completion: nil)
        }
    }

    @IBAction func onClickAddNew(_ sender: Any) {
        if let error = AccountCommon.checkRightError() {
            AlertUtils.showWarningAlert(title: "Warning", message: error, parent: self)
            return
        }

        if let addSurveyVC = AddSurveyViewController.initiateFromStoryboard() as? AddSurveyViewController {
            addSurveyVC.delegate = self

            self.present(addSurveyVC, animated: true, completion: nil)
        }
    }

    

    // MARK: - UI

    func configUI() {
        self.viewModel.doLoadTeamSurveys()
    }
    
    private func handleSubmitResponse(resAPI: GeneralRes) {
        self.viewModel.isBusy.value = false
        
        if resAPI.result! {
            AlertUtils.showWarningAlert(title: "Success", message: "Survey Response Successful", parent: self)
            self.viewModel.doLoadTeamSurveys()
        }
    }
}

extension SurveysViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let teamSurvey = viewModel.tableData[indexPath.row]
        return CGFloat(SurveyCell.height(survey: teamSurvey, isExpand: self.viewModel.expandsTrack[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SurveyCell = tableView.dequeueReusableCell(withIdentifier: SurveyCell.reuseIdentifier()) as? SurveyCell {
            let survey = self.viewModel.tableData[indexPath.row]
            cell.config(survey: survey, isExpand: self.viewModel.expandsTrack[indexPath.row])

            if self.isManager {
                cell.deleteSurveyButton.isHidden = false
            } else {
                cell.deleteSurveyButton.isHidden = true
            }

            cell.deleteSurveyItem = {
                [weak self] in
                guard self != nil else {
                    return
                }

                AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: "Are you sure you would like to delete this Survey?", confirmTitle: "Yes", parent: self!, handler: {
                    self!.viewModel.doDeleteSurveyItem(surveyId: survey.id)
                })

            }

            cell.showDetailResultClosure = { [weak self] in
                guard let self = self else {
                    return
                }

                if let surveyResultVC = SurveyResultViewController.initiateFromStoryboard() as? SurveyResultViewController {
                    surveyResultVC.viewModel.tableData = self.viewModel.tableData

                    self.navigationController?.pushViewController(surveyResultVC, animated: true)
                }
            }

            cell.adjustHeightClosure = { [weak self] in
                guard  let self = self else {
                    return
                }

                let newExpand = !self.viewModel.expandsTrack[indexPath.row]
                self.viewModel.expandsTrack[indexPath.row] = newExpand
                cell.updateExpandImage(isExpand: newExpand)

                UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    tableView.beginUpdates()
                    tableView.endUpdates()

                    if cell.frame.maxY > tableView.frame.maxY {
                        tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }, completion: { _ in
                    cell.isAnimatining = false
                })
            }

            cell.submitAnswerClosure = { [weak self] answers in
                guard  let self = self else {
                    return
                }
                
                self.viewModel.currentSurvey = survey
                self.viewModel.currentAnswers = answers
                self.viewModel.doGetResponsible()
            }

            return cell
        }

        return UITableViewCell()
    }
}

extension SurveysViewController: SelectTeamVCDelegate {
    func didSelectTeam() {
        self.viewModel.doLoadTeamSurveys()
    }
}

extension SurveysViewController: AddSurveyViewControllerDelegate {
    func didAddSurveySuccess() {
        self.viewModel.doLoadTeamSurveys()
    }
}

extension SurveysViewController: GetResponsibleViewControllerDelegate {
    func submitPressed(rosters: [String]) {
        self.viewModel.doSumbitAnswerForMultipleRoster(rosters: rosters)
    }
}
