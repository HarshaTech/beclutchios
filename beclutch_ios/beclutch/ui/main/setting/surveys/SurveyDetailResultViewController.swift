//
//  SurveyDetailResultViewController.swift
//  beclutch
//
//  Created by kakabala on 4/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class SurveyDetailResultViewController: BaseViewController {
    var viewModel: SurveyDetailResultViewModel = SurveyDetailResultViewModel()

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.title = "Detailed Results"
        addObserver()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func addObserver() {
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SurveyDetailResultViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SurveyDetailResultCell = tableView.dequeueReusableCell(withIdentifier: SurveyDetailResultCell.reuseIdentifier()) as? SurveyDetailResultCell {
            let rosterStatistic = viewModel.tableData[indexPath.row]
            let answers = rosterStatistic.1
            var statisticText = ""
            for i in 0 ..< answers.count {
                let answer = answers[i]
                if let text = answer.answerText {
                    statisticText += text
                }

                if let text = answer.answerOptionText, text.count > 0 {
                    statisticText += " - " + text
                }

                if i != answers.count - 1 {
                    statisticText += " \n"
                }
            }

            cell.config(name: rosterStatistic.0 + ":", statistic: statisticText)

            return cell
        }

        return UITableViewCell()
    }
}
