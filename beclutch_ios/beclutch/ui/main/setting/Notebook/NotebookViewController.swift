//
//  NotebookViewController.swift
//  beclutch
//
//  Created by Vu Trinh on 27/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class NotebookViewController: BaseViewController {
    @IBOutlet weak var btSelectTeam: UIButton!
    @IBOutlet weak var btAddToolbar: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    let tableData: [NoteBookType] = [.checklist, .note, .album]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        refreshTheme()
    }

    func refreshTheme() {
        btAddToolbar.backgroundColor = UIColor.COLOR_ACCENT
        btAddToolbar.isHidden = !AccountCommon.isCoachOrManager()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Action
    @IBAction func onClickSelectTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            showVC.delegate = self

            self.present(showVC, animated: true, completion: nil)
        }
    }

    @IBAction func onClickAddNew(_ sender: Any) {
        if let error = AccountCommon.checkRightError() {
            AlertUtils.showWarningAlert(title: "Warning", message: error, parent: self)
            return
        }

        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let checklistAction = UIAlertAction(title: "New Checklist", style: .default) { (_) in

        }

        let noteAction = UIAlertAction(title: "New Note", style: .default) { [weak self] (_) in
            if let noteViewController = ViewControllerFactory.newNoteViewController() {
                self?.present(noteViewController, animated: true, completion: nil)
            }
        }

        let albumAction = UIAlertAction(title: "New Album", style: .default) { (_) in

        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { [weak self] (_) in
            self?.dismiss(animated: true, completion: nil)
        }

        actionSheet.addAction(checklistAction)
        actionSheet.addAction(noteAction)
        actionSheet.addAction(albumAction)
        actionSheet.addAction(cancelAction)

        self.present(actionSheet, animated: true, completion: nil)
    }

}

extension NotebookViewController: SelectTeamVCDelegate {
    func didSelectTeam() {
//        self.viewModel.doLoadTeamSurveys()
    }
}

extension NotebookViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotebookCell") as? NotebookCell {
            let data = tableData[indexPath.row]
            cell.config(icon: data.icon, title: data.title)

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tableData[indexPath.row]
        switch data {
        case .note:
            if let noteListVC = ViewControllerFactory.noteListViewController() {
                self.navigationController?.pushViewController(noteListVC, animated: true)
            }
        default:
            return
        }
    }
}
