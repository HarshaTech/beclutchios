//
//  NewNoteViewController.swift
//  beclutch
//
//  Created by Vu Trinh on 27/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class NewNoteViewController: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextField!

    var currentNote: Note?

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let note = currentNote {
            APIManager.shared.getNoteData(noteId: note.noteId)
                .successValue()
                .subscribe(onNext: { [weak self] response in
                    guard let strongSelf = self else {
                        return
                    }

                    if response.result,
                       let note = response.extra.first {
                        strongSelf.titleTextField.text = note.name
                        strongSelf.contentTextField.text = note.text
                    }
                })
                .disposed(by: disposeBag)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        refreshTheme()
    }

    private func refreshTheme() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func checkValidation() -> Bool {
        var errorMsg: String? = nil

        if titleTextField.text?.isEmpty == true {
            errorMsg = "Please enter title"
        } else if contentTextField.text?.isEmpty == true {
            errorMsg = "Please enter title"
        }

        if let msg = errorMsg {
            AlertUtils.showWarningAlert(title: "Warning", message: msg, parent: self)
        }

        return errorMsg == nil
    }

    // MARK: - Action
    @IBAction func onClickCancel(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation(),
           let title = titleTextField.text,
           let content = contentTextField.text {
            showIndicator()
            APIManager.shared.createNoteText(noteId: currentNote?.noteId, name: title, text: content)
                .do(onNext: { [weak self] _ in
                    self?.hideIndicator()
                })
                .successValue()
                .subscribe(onNext: { [weak self] result in
//                    if result.result {
//                        self?.dismiss(animated: true, completion: nil)
//                    } else {
//                        self?.alert(message: result.extra)
//                    }
                })
                .disposed(by: disposeBag)
        }
    }
}
