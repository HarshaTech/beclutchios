//
//  NotebookCell.swift
//  beclutch
//
//  Created by Vu Trinh on 27/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit

class NotebookCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLable: UILabel!

    func config(icon: String, title: String) {
        iconImageView.image = UIImage(named: icon)
        titleLabel.text = title
    }

    func config(note: Note) {
        titleLabel.text = note.name
        dateLable.text = note.date
    }
}
