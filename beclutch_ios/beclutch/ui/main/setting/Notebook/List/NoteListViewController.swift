//
//  NoteListViewController.swift
//  beclutch
//
//  Created by Vu Trinh on 28/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import UIKit
import RxSwift

class NoteListViewController: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var tableView: UITableView!

    let disposeBag = DisposeBag()

    var tableData: [Note] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.showIndicator()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        refreshTheme()

        APIManager.shared.getNotebookList(type: .note)
            .do(onNext: { [weak self] _ in
                self?.hideIndicator()
            })
            .successValue()
            .subscribe(onNext: { [weak self] result in
                if result.result {
                    self?.tableData = result.extra
                    self?.tableView.reloadData()
                }
            })
            .disposed(by: disposeBag)
    }

    func refreshTheme() {
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func dissmissPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NoteListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: NotebookCell.reuseIdentifier()) as? NotebookCell {
            cell.config(note: tableData[indexPath.row])

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let newNoteVC = ViewControllerFactory.newNoteViewController(note: tableData[indexPath.row]) {
            self.present(newNoteVC, animated: true, completion: nil)
        }
    }
}
