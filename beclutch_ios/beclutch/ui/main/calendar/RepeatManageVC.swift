//
//  RepeatManageVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import Observable
import ActionSheetPicker_3_0
protocol IRepeatManage: class {
    func onChooseRepeat(val: RepeatModel)
}

class RepeatManageVC: UIViewController {

    @IBOutlet weak var chkNotRepeat: BEMCheckBox!

    @IBOutlet weak var btDaily: UIButton!
    @IBOutlet weak var btWeekly: UIButton!
    @IBOutlet weak var btMonthly: UIButton!
    @IBOutlet weak var btYearly: UIButton!

    @IBOutlet weak var chkDaily: BEMCheckBox!
    @IBOutlet weak var chkWeekly: BEMCheckBox!
    @IBOutlet weak var chkMonthly: BEMCheckBox!
    @IBOutlet weak var chkYearly: BEMCheckBox!

    @IBOutlet weak var weekDayPicker: RZGDayOfWeekPicker!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!

    var allButton: [UIButton] {
        return [btDaily, btWeekly, btMonthly, btYearly]
    }
    var allCheckBox: [BEMCheckBox] {
        return [chkDaily, chkWeekly, chkMonthly, chkYearly]
    }

    var viewModel: RepeatManageViewModel = RepeatManageViewModel()
    private var disposal = Disposal()
    weak var parentDelegate: IRepeatManage?
    var origin: RepeatModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.chkNotRepeat.boxType = .square
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()

        weekDayPicker.selectedColor = UIColor.COLOR_BLUE
        weekDayPicker.delegate = self

        allCheckBox.forEach {
            $0.boxType = .square
        }

        allButton.forEach {
            $0.makeGrayRadius()
        }

        self.viewModel.repeatType = self.origin!.type!

        weekDayPicker.setDaysOfWeek(daily: self.origin?.dailyInt)
        if self.origin?.type == RepeatModel.REPEAT_TYPE_DAILY_REPEAT {
            self.viewModel.tillDaily.value = self.origin!.formattedTillDate
            self.chkDaily.on = true
        } else if self.origin?.type == RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT {
            self.viewModel.tillWeekly.value = self.origin!.formattedTillDate
            self.chkWeekly.on = true
        } else if self.origin?.type == RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT {
            self.viewModel.tillMonthly.value = self.origin!.formattedTillDate
            self.chkMonthly.on = true
        } else if self.origin?.type == RepeatModel.REPEAT_TYPE_YEARLY_REPEAT {
            self.viewModel.tillYearly.value = self.origin!.formattedTillDate
            self.chkYearly.on = true
        } else {
            self.chkNotRepeat.on = true
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.tillDaily.observe(DispatchQueue.main) {
        newBusy, _ in
        if newBusy.isEmpty {
            self.btDaily.setTitle("Until Date", for: .normal)
        } else {
            self.btDaily.setTitle(newBusy, for: .normal)
        }

        }.add(to: &disposal)
        self.viewModel.tillWeekly.observe(DispatchQueue.main) {
            newBusy, _ in
            if newBusy.isEmpty {
                self.btWeekly.setTitle("Until Date", for: .normal)
            } else {
                self.btWeekly.setTitle(newBusy, for: .normal)
            }

            }.add(to: &disposal)
        self.viewModel.tillMonthly.observe(DispatchQueue.main) {
            newBusy, _ in
            if newBusy.isEmpty {
                self.btMonthly.setTitle("Until Date", for: .normal)
            } else {
                self.btMonthly.setTitle(newBusy, for: .normal)
            }
            }.add(to: &disposal)
        self.viewModel.tillYearly.observe(DispatchQueue.main) {
            newBusy, _ in
            if newBusy.isEmpty {
                self.btYearly.setTitle("Until Date", for: .normal)
            } else {
                self.btYearly.setTitle(newBusy, for: .normal)
            }
            }.add(to: &disposal)
    }

    func initText() {
        self.viewModel.tillDaily.value = ""
        self.viewModel.tillWeekly.value = ""
        self.viewModel.tillMonthly.value = ""
        self.viewModel.tillYearly.value = ""
    }

    func selectCheckBox(checkBox: BEMCheckBox) {
        self.chkNotRepeat.on = false

        for box in allCheckBox {
            if box == checkBox {
                box.on = true
                continue
            }

            box.on = false
        }
    }

    @IBAction func onClickNotRepeatCheck(_ sender: Any) {
        self.viewModel.repeatType = RepeatModel.REPEAT_TYPE_NO_REPEAT
        initText()
        self.chkNotRepeat.on = true

        allCheckBox.forEach {
            $0.on = false
        }
    }

    @IBAction func onClickDailyCheck(_ sender: Any) {
        if self.viewModel.repeatType != RepeatModel.REPEAT_TYPE_DAILY_REPEAT {
            self.viewModel.repeatType = RepeatModel.REPEAT_TYPE_DAILY_REPEAT
            initText()
        }

        selectCheckBox(checkBox: chkDaily)
    }

    @IBAction func onClickWeeklyCheck(_ sender: Any) {
        if self.viewModel.repeatType != RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT {
            self.viewModel.repeatType = RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT
            initText()
        }

        selectCheckBox(checkBox: chkWeekly)
    }

    @IBAction func onClickMonthlyCheck(_ sender: Any) {
        if self.viewModel.repeatType != RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT {
            self.viewModel.repeatType = RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT
            initText()
        }

        selectCheckBox(checkBox: chkMonthly)
    }

    @IBAction func onClickYearlyCheck(_ sender: Any) {
        if self.viewModel.repeatType != RepeatModel.REPEAT_TYPE_YEARLY_REPEAT {
            self.viewModel.repeatType = RepeatModel.REPEAT_TYPE_YEARLY_REPEAT
            initText()
        }

        selectCheckBox(checkBox: chkYearly)
    }

    @IBAction func onClickDailyUntil(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Daily Until Date",
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: viewModel.tillDailyDate,
                                               doneBlock: { _, date, origin in
                                                if let datePick = date as? Date {
                                                    self.viewModel.chooseDate = datePick
                                                    self.viewModel.tillDailyDate = datePick
                                                    self.viewModel.tillDaily.value = datePick.praceDateString()
                                                }
                                                return
        },
                                               cancel: { _ in
                                                return
        },
                                               origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }

    @IBAction func onClickWeeklyUntil(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Weekly Until Date",
                                               datePickerMode: UIDatePicker.Mode.date,
                                                selectedDate: viewModel.tillWeeklyDate,
                                               doneBlock: { _, date, origin in
                                                if let datePick = date as? Date {
                                                    self.viewModel.chooseDate = datePick
                                                    self.viewModel.tillWeeklyDate = datePick
                                                    self.viewModel.tillWeekly.value = datePick.praceDateString()
                                                }
                                                return
        },
                                               cancel: { _ in
                                                return
        },
                                               origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }

    @IBAction func onClickMonthlyUntil(_ sender: Any) {
        var curDate = Date()
        let formatter = DateFormatter()
        if !self.viewModel.tillMonthly.value.isEmpty {
            curDate = formatter.date(from: self.viewModel.tillMonthly.value)!
        }

        let datePicker = ActionSheetDatePicker(title: "Monthly Until Date",
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: curDate,
                                               doneBlock: { _, date, origin in
                                                if let datePick = date as? Date {
                                                    self.viewModel.chooseDate = datePick
                                                    self.viewModel.tillMonthly.value = datePick.praceDateString()
                                                }
            return
        },
                                               cancel: { _ in
                                                return
        },
                                               origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }

    @IBAction func onClickYearlyUntil(_ sender: Any) {
        var curDate = Date()
        let formatter = DateFormatter()
        if !self.viewModel.tillYearly.value.isEmpty {
            curDate = formatter.date(from: self.viewModel.tillYearly.value)!
        }

        let datePicker = ActionSheetDatePicker(title: "Yearly Until Date",
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: curDate,
                                               doneBlock: { _, date, origin in
                                                if let datePick = date as? Date {
                                                    self.viewModel.chooseDate = datePick
                                                    self.viewModel.tillYearly.value = datePick.praceDateString()
                                                }
                                                return
        },
                                               cancel: { _ in
                                                return
        },
                                               origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            self.parentDelegate?.onChooseRepeat(val: self.viewModel.makeRetVal(daily: weekDayPicker.indexOfSelectedDays()))
            self.dismiss(animated: true, completion: nil)
        }
    }

    func checkValidation() -> Bool {
        if self.viewModel.repeatType.isEmpty {
            alert(title: "Warning", message: "Choose Repeat mode.")
            return false
        }

        if self.viewModel.repeatType == RepeatModel.REPEAT_TYPE_DAILY_REPEAT && self.viewModel.tillDaily.value.isEmpty {
            alert(title: "Warning", message: "Choose Until Date.")
            return false
        }

        if self.viewModel.repeatType == RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT && self.viewModel.tillWeekly.value.isEmpty {
            alert(title: "Warning", message: "Choose Until Date.")
            return false
        }

        if self.viewModel.repeatType == RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT && self.viewModel.tillMonthly.value.isEmpty {
            alert(title: "Warning", message: "Choose Until Date.")
            return false
        }

        if self.viewModel.repeatType == RepeatModel.REPEAT_TYPE_YEARLY_REPEAT && self.viewModel.tillYearly.value.isEmpty {
            alert(title: "Warning", message: "Choose Until Date.")
            return false
        }
        return true
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RepeatManageVC: RZGDayOfWeekPickerDelegate {

}
