//
//  HomeAwayButton.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class HomeAwayButton: UIButton {
    var isChoose: Bool = false {
        didSet {
            if isChoose {
                self.backgroundColor = UIColor.COLOR_PRIMARY
                self.setTitleColor(UIColor.white, for: .normal)
            } else {
                self.backgroundColor = UIColor.white
                self.setTitleColor(UIColor.black, for: .normal)
            }
        }
    }

    override func awakeFromNib() {
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 19
    }

}
