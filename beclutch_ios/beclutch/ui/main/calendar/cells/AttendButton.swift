//
//  AttendButton.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class AttendButton: UIButton {
    var isAttending = "-1"
    var isChoose: Bool = false {
        didSet {
            if isChoose {
                if isAttending == "1" {
                    self.backgroundColor = UIColor.COLOR_ATTENDING_YES
                    self.layer.borderColor = UIColor.COLOR_ATTENDING_YES.cgColor
                } else if isAttending == "0" {
                    self.backgroundColor = UIColor.COLOR_ATTENDING_NO
                    self.layer.borderColor = UIColor.COLOR_ATTENDING_NO.cgColor
                } else if isAttending == "2" {
                    self.backgroundColor = UIColor.COLOR_ATTENDING_MAYBE
                    self.layer.borderColor = UIColor.COLOR_ATTENDING_MAYBE.cgColor
                }

                self.setTitleColor(UIColor.white, for: .normal)
            } else {
                self.backgroundColor = UIColor.white
                self.setTitleColor(UIColor.black, for: .normal)
                self.layer.borderColor = UIColor.gray.cgColor
            }
        }
    }

    override func awakeFromNib() {
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func setAttending(attending: String) {
        self.isAttending = attending
    }

    func commonInit() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 19
    }
}
