//
//  ContentHeaderCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/9.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ContentHeaderCell: UITableViewCell {

    @IBOutlet weak var txtMonthName: UILabel!
    @IBOutlet weak var topLineView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func config(title: String) {
        txtMonthName.text = title
    }
}
