//
//  CalendarProtocol.swift
//  beclutch
//
//  Created by Vu Trinh on 13/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import UIKit

protocol CalendarCommon: UIViewController {
    
}

extension CalendarCommon {
    func shareMessage(model: EventModel) -> String {
        let name = AppDataInstance.instance.getUserName
        
        var result = "\(name) has shared \(model.title) information via BeClutch!\n\n"
        if model.type == .game, let game = model as? GameModel {
            result += AppDataInstance.instance.getTeamName + " vs " + game.getOponentName + "\n"
        } else if model.type == .event {
            result += AppDataInstance.instance.getTeamName + " - " + model.getName + "\n"
        } else {
            result += AppDataInstance.instance.getTeamName + "\n"
        }
        
        result += model.startTimeDisplay + ", "
        result += model.slashesDateDisplay + "\n"
        result += model.getDisplayedAddress + "\n"
        result += model.getDisplayedDetailAddress + "\n"
        
        if let mapUrl = model.getAddressUrl {
            result += mapUrl.absoluteString + "\n\n"
        }
        
        result += "Always BeClutch!"
        
        return result
    }
    
    func shareModel(model: EventModel) {
        //Set the default sharing message.
        let message = shareMessage(model: model)
        //Set the link to share.
        
//        if let mapUrl = model.getAddressUrl
//        {
        let objectsToShare: [Any] = [message]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        self.present(activityVC, animated: true, completion: nil)
//        }
    }
}
