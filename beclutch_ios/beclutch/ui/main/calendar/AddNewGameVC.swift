//
//  AddNewGameVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/9.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import ActionSheetPicker_3_0
import PopupDialog
import SwiftEventBus

class AddNewGameVC: UIViewController, IOpponentChooser, ILocationChooser, IColorPickerDelegate, WWCalendarTimeSelectorProtocol {
    @IBOutlet weak var txtMemo: UITextField!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var segHomeAway: UISegmentedControl!
    @IBOutlet weak var txtAddr: UITextField!
    @IBOutlet weak var btLocation: UIButton!
    @IBOutlet weak var btDuration: UIButton!
    @IBOutlet weak var btArrival: UIButton!
    @IBOutlet weak var btStart: UIButton!
    @IBOutlet weak var btOppo: UIButton!
    @IBOutlet weak var btDate: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var imgUniform: UIImageView!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var saveBtn: UIButton!

    var origin: GameModel?
    var isUpdate: Bool = false
    var viewModel: AddGameViewModel = AddGameViewModel()
    private var disposal = Disposal()

    override func viewDidLoad() {
        super.viewDidLoad()
        txtAddr.isUserInteractionEnabled = false
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        saveBtn.makeGreenButton()

        self.btLocation.makeGrayRadius()
        self.btDuration.makeGrayRadius()
        self.btArrival.makeGrayRadius()
        self.btStart.makeGrayRadius()
        self.btOppo.makeGrayRadius()
        self.btDate.makeGrayRadius()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        self.imgUniform.isUserInteractionEnabled = true
        self.imgUniform.addGestureRecognizer(tapGestureRecognizer)
        if self.isUpdate {
            self.txtName.isEnabled = true
            self.txtTitle.text = "Update Game"
            self.txtName.text = self.origin!.NAME!

            self.viewModel.oppo.value = self.origin!.opponent_detail!
            self.viewModel.uniformColor.value = "\(self.origin!.UNIFORM_COLOR!)"
            self.viewModel.isHomeTeam = Int(self.origin!.IS_HOME!)!
            switch self.viewModel.isHomeTeam {
            case 0:
                self.segHomeAway.selectedSegmentIndex = 2
                break
            case 1:
                self.segHomeAway.selectedSegmentIndex = 1
                break
            case -1:
                self.segHomeAway.selectedSegmentIndex = 0
                break
            default:
                self.segHomeAway.selectedSegmentIndex = 0
                break
            }

            self.viewModel.year = Int(self.origin!.DATE_YEAR!)!
            self.viewModel.month = Int(self.origin!.DATE_MONTH!)!
            self.viewModel.dayOfMonth = Int(self.origin!.DATE_DAY_OF_MONTH!)!
            self.viewModel.dayOfWeek = self.origin!.DAY_OF_WEEK!
            self.viewModel.eventTime = Date.makeDateFromInt(year: viewModel.year, month: viewModel.month, day: viewModel.dayOfMonth)
            self.btDate.setTitle(origin?.dateDisplay, for: .normal)

            if let eventTime = viewModel.eventTime {
                self.viewModel.startTimeAMPM = self.origin!.START_TIME_AMPM!
                self.viewModel.startTimeHour = Int(origin!.START_TIME_HOUR!)! > 12 ? Int(origin!.START_TIME_HOUR!)! - 12 : Int(origin!.realStartTimeHour!)!
                self.viewModel.startTimeMin = Int(self.origin!.START_TIME_MIN!)!
//                self.viewModel.startTime = eventTime.adjustDate(hour: viewModel.startTimeHour, minute: viewModel.startTimeMin, ampm: viewModel.startTimeAMPM)

                if !(self.viewModel.startTimeAMPM.isEmpty) {
                    let hourTime: String = String(self.viewModel.startTimeHour)
                    self.btStart.setTitle(hourTime + ":" + String(format: "%02d", Int(self.viewModel.startTimeMin)) + " " + self.viewModel.startTimeAMPM, for: .normal)
                }

                self.viewModel.arrivalTimeAMPM = self.origin!.ARRIVAL_TIME_AMPM!
                self.viewModel.arrivalTimeHour = Int(origin!.ARRIVAL_TIME_HOUR!)! > 12 ? Int(origin!.ARRIVAL_TIME_HOUR!)! - 12 : Int(origin!.realArriveTimeHour!)!
                self.viewModel.arrivalTimeMin = Int(self.origin!.ARRIVAL_TIME_MIN!)!
                self.viewModel.arrivalTime = eventTime.adjustDate(hour: viewModel.arrivalTimeHour, minute: viewModel.arrivalTimeMin, ampm: viewModel.arrivalTimeAMPM)

                if !(self.viewModel.arrivalTimeAMPM.isEmpty) {
                    let hourTime: String = String(self.viewModel.arrivalTimeHour)
                    self.btArrival.setTitle(hourTime + ":" + String(format: "%02d", Int(self.viewModel.arrivalTimeMin)) + " " + self.viewModel.arrivalTimeAMPM, for: .normal)
                }
            }

            self.viewModel.durationHour = origin!.getDurationHour
            self.viewModel.durationMin = origin!.getDurationMinute
            if self.viewModel.durationHour != 0 || self.viewModel.durationMin != 0 {
                self.btDuration.setTitle(origin!.DURATION_HOUR! + " hour(s) " + String(format: "%02d min(s)", Int(self.viewModel.durationMin)), for: .normal)
            }

            self.viewModel.teamLocation.value = self.origin!.location_detail!
            let deadlineTime = DispatchTime.now() + .milliseconds(90)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.txtAddr.text = self.origin!.ADDR!
            }
            self.txtMemo.text = self.origin!.MEMO!
        } else {
            self.txtTitle.text = "Add New Game"
            self.txtName.isEnabled = false
            self.txtName.text = "New Game"
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)

        self.viewModel.teamLocation.observe(DispatchQueue.main) {
            newLoc, oldLoc in
            if newLoc.id == oldLoc?.id {
                return
            }

            if !newLoc.id!.isEmpty {
                self.btLocation.setTitle(newLoc.name, for: .normal)
                self.txtAddr.text = StringCheckUtils.getAddressFromLocation(model: newLoc)
            } else {
                self.btLocation.setTitle("Choose Location", for: .normal)
                self.txtAddr.text = ""
            }
        }.add(to: &disposal)

        self.viewModel.oppo.observe(DispatchQueue.main) {
            newLoc, _ in

            if !newLoc.ID!.isEmpty {
                self.btOppo.setTitle(newLoc.OPPONENT_NAME, for: .normal)
            } else {
                self.btOppo.setTitle("Choose Opponent", for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel.uniformColor.observe(DispatchQueue.main) {
            newCol, _ in

            if !newCol.isEmpty {
                if newCol == "-1" {
                    self.viewColor.backgroundColor = UIColor(white: 1, alpha: 0)
                } else {
                    self.viewColor.backgroundColor = ColorUtils.getUIColorFromRGB(val: Int32(Int(newCol)!))
                }
            } else {
                self.viewColor.backgroundColor = UIColor.white
            }

        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateGameRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            let resAPI = res?.object as! CreateGameRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.alert(title: "Warning", message: "Failed to create game")
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateGameRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            let resAPI = res?.object as! UpdateGameRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.alert(title: "Warning", message: "Failed to update game")
            }
        })
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let colorPicker = self.storyboard?.instantiateViewController(withIdentifier: "ChooseColorViewController") as! ChooseColorViewController
        colorPicker.parentDelegate = self
        colorPicker.colorPickType = ColorPickerForType.ALTER_2_COLOR
        self.present(colorPicker, animated: true, completion: nil)
    }

    func onSetColor(selCol: UIColor, type: ColorPickerForType) {
        self.viewModel.uniformColor.value = "\(selCol.rgb())"
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            if self.isUpdate {
                alert(title: "Confirm", message: "Notify Team?", okTitle: "Yes", okHandler: { (_) in
                    if let origin = self.origin {
                        self.viewModel.doUpdateGame(name: self.txtName.text!, memo: self.txtMemo.text!, addr: self.txtAddr.text!, originId: origin.ID!, notify: true)
                    }
                }) { (_) in
                    if let origin = self.origin {
                        self.viewModel.doUpdateGame(name: self.txtName.text!, memo: self.txtMemo.text!, addr: self.txtAddr.text!, originId: origin.ID!, notify: false)
                    }
                }
            } else {
                alert(title: "Confirm", message: "Notify Team?", okTitle: "Yes", okHandler: { (_) in
                    self.viewModel.doCreateNewGame(addr: self.txtAddr.text!, memo: self.txtMemo.text!, notify: true)
                }) { (_) in
                    self.viewModel.doCreateNewGame(addr: self.txtAddr.text!, memo: self.txtMemo.text!, notify: false)
                }
            }
        }
    }

    func checkValidation() -> Bool {
        if self.viewModel.dayOfWeek.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Date is required", parent: self)
            return false
        }

        if self.viewModel.oppo.value.ID!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Opponent is required", parent: self)
            return false
        }

        if self.viewModel.startTimeAMPM.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Start time is required", parent: self)
            return false
        }

        if self.viewModel.teamLocation.value.id!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Location is required", parent: self)
            return false
        }

        let currentDate = Date()
        var startDateComponents = DateComponents()
        startDateComponents.year = self.viewModel.year
        startDateComponents.month = self.viewModel.month
        startDateComponents.day = self.viewModel.dayOfMonth
        let startDate = Calendar.current.date(from: startDateComponents)
        if startDate?.compare(Calendar.current.startOfDay(for: Date())) == .orderedSame {
            if let startTime = self.viewModel.startTime, startTime.compare(currentDate) == .orderedAscending {
                AlertUtils.showWarningAlert(title: "Warning", message: "Start time cannot be earlier than the current time.", parent: self)
                return false
            }
        } else if startDate?.compare(Calendar.current.startOfDay(for: Date())) == .orderedAscending {
            AlertUtils.showWarningAlert(title: "Warning", message: "Start time cannot be earlier than the current time.", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickDate(_ sender: Any) {
        let datePicker = CalendarPickerCommon.picker()
        datePicker.delegate = self
        
        present(datePicker, animated: true, completion: nil)
    }

    @IBAction func onClickOpponent(_ sender: Any) {
        let oppoNav = self.storyboard?.instantiateViewController(withIdentifier: "OpponentListViewController") as! OpponentListViewController
        oppoNav.viewModel.isForChoose = true
        oppoNav.parentDelegate = self
        self.present(oppoNav, animated: true, completion: nil)
    }

    func chooseOpponent(val: OpponentModel) {
        self.viewModel.oppo.value = val
    }

    @IBAction func onClickStartTime(_ sender: Any) {
        if self.viewModel.startTime == nil {
            let date = viewModel.eventTime ?? Date()
            let updatedDate = Calendar.current.date(bySettingHour: 12, minute: 00, second: 0, of: date)
            self.viewModel.startTime = updatedDate
        }

        let datePicker = ActionSheetDatePicker(title: "Start Time",
                                               datePickerMode: UIDatePicker.Mode.time,
                                               selectedDate: self.viewModel.startTime,
                                               target: self,
                                               action: #selector(startTimePicked(_:)),
                                               origin: self.view)

        datePicker?.minuteInterval = 5
        datePicker?.show()

    }

    @objc func startTimePicked(_ chooseDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "h:mm a"
        self.viewModel.startTime = chooseDate
        self.viewModel.startTimeHour = chooseDate.hourIn12Format
        self.viewModel.startTimeMin = Calendar.current.component(.minute, from: chooseDate)
        self.viewModel.startTimeAMPM = formatter.string(from: chooseDate).uppercased()

        self.btStart.setTitle(formatter2.string(from: chooseDate), for: .normal)

        if let arriveEarlyHour = Int(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ARRIVE_HOURS ?? "0"),
            let arriveEarlyMin = Int(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.ARRIVE_MINS ?? "0") {
            //            if arriveEarlyMin <= self.viewModel.startTimeMin {
            //                self.viewModel.arrivalTimeHour = self.viewModel.startTimeHour - arriveEarlyHour
            //                self.viewModel.arrivalTimeMin = self.viewModel.startTimeMin - arriveEarlyMin
            //            } else {
            //                self.viewModel.arrivalTimeHour = self.viewModel.startTimeHour - arriveEarlyHour - 1
            //                self.viewModel.arrivalTimeMin = self.viewModel.startTimeMin - arriveEarlyMin + 60
            //            }
            let timeInteval = Double((arriveEarlyHour * 60 + arriveEarlyMin) * 60)

            let arriveTime = chooseDate.addingTimeInterval(-timeInteval)
            self.viewModel.arrivalTime = arriveTime
            self.viewModel.arrivalTimeHour = arriveTime.hourIn12Format
            self.viewModel.arrivalTimeMin = Calendar.current.component(.minute, from: arriveTime)
            self.viewModel.arrivalTimeAMPM = formatter.string(from: arriveTime).uppercased()

            //            self.viewModel.arrivalTime = Date.makeDate(hr: self.viewModel.arrivalTimeHour, min: self.viewModel.arrivalTimeMin)

            if let date = self.viewModel.arrivalTime {
                self.btArrival.setTitle(formatter2.string(from: date), for: .normal)

                //                self.viewModel.arrivalTimeAMPM = date.ampmStr
            }
        }
    }

    @IBAction func onClickArrivalTime(_ sender: Any) {
        if self.viewModel.arrivalTime == nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd HH:mm"
            self.viewModel.arrivalTime = formatter.date(from: "2016/10/08 12:00")
        }

        let datePicker = ActionSheetDatePicker(title: "Arrival Time",
                                               datePickerMode: UIDatePicker.Mode.time,
                                               selectedDate: self.viewModel.arrivalTime,
                                               target: self,
                                               action: #selector(arrivalTimePicked(_:)),
                                               origin: self.view)

        datePicker?.minuteInterval = 5
        datePicker?.show()
    }

    @objc func arrivalTimePicked(_ chooseDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "h:mm a"
        self.viewModel.arrivalTime = chooseDate
        self.viewModel.arrivalTimeHour = chooseDate.hourIn12Format
        self.viewModel.arrivalTimeMin = Calendar.current.component(.minute, from: chooseDate)
        self.viewModel.arrivalTimeAMPM = formatter.string(from: chooseDate).uppercased()

        self.btArrival.setTitle(formatter2.string(from: chooseDate), for: .normal)
    }

    @IBAction func onClickDuration(_ sender: Any) {
        var arrHr = [String]()
        var arrMin = [String]()

        for hourMark in 0...24 {
            arrHr.append("\(hourMark)")
        }

        for tickMark in stride(from: 0, to: 60, by: 5) {
            arrMin.append("\(tickMark)")
        }

        ActionSheetMultipleStringPicker.show(withTitle: "Choose Duration(Hr Min)", rows: [
            arrHr,
            arrMin
            ], initialSelection: [self.viewModel.durationHour, self.viewModel.durationMin / 5], doneBlock: {
                _, indexes, _ in
                self.viewModel.durationHour = Int(arrHr[indexes![0] as! Int])!
                self.viewModel.durationMin = Int(arrMin[indexes![1] as! Int])!
                self.btDuration.setTitle(arrHr[indexes![0] as! Int] + " hour(s) " + String(format: "%02d min(s)", Int(arrMin[indexes![1] as! Int])!), for: .normal)
                return
        }, cancel: { _ in return }, origin: sender)
    }

    @IBAction func onClickLocation(_ sender: Any) {
        let nextViewControlelr = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as!  LocationListViewController
        nextViewControlelr.viewModel.isForChoose = true
        nextViewControlelr.parentDelegate = self
        self.present(nextViewControlelr, animated: true, completion: nil)
    }

    func onChooseLocation(loc: TeamLocation) {
        self.viewModel.teamLocation.value = loc
    }

    @IBAction func onChangeColorSegment(_ sender: Any) {
        switch self.segHomeAway.selectedSegmentIndex {
        case 0:
            self.viewModel.isHomeTeam = -1
            self.viewModel.uniformColor.value = "-1"
            break
        case 1:
            self.viewModel.isHomeTeam = 1
            self.viewModel.uniformColor.value = AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.HOME_COLOR
            break
        case 2:
            self.viewModel.isHomeTeam = 0
            self.viewModel.uniformColor.value = AppDataInstance.instance.currentSelectedTeam!.CLUB_TEAM_INFO.AWAY_COLOR
            break
        default:
            //do nothing
            break
        }
    }
}

extension AddNewGameVC {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        self.viewModel.year = calendar.component(.year, from: date)
        self.viewModel.month = calendar.component(.month, from: date)
        self.viewModel.dayOfMonth = calendar.component(.day, from: date)
        self.viewModel.dayOfWeek = formatter.string(from: date)

        self.btDate.setTitle(date.praceDateString(), for: .normal)
    }
}
