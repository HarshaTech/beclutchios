//
//  ShowRosterListForEventTVCell.swift
//  beclutch
//
//  Created by Jack Ily on 08/04/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ShowRosterListForEventTVCell: UITableViewCell {

    @IBOutlet weak var btChat: UIButton!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtPo: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var attendentCheck: UIButton!

    @IBOutlet weak var adjustHeightButton: UIButton!

    @IBOutlet weak var btAttendYes: AttendButton!
    @IBOutlet weak var btAttendNo: AttendButton!
    @IBOutlet weak var btAttendMaybe: AttendButton!

    var isAnimatining = false

    var actionChat: (() -> Void)?
    var resendInvitationClosure: (() -> Void)?
    var adjustHeightClosure: (() -> Void)?
    var updateAttendanceClosure: ((_ attendance: Int) -> Void)?

    static func height(isExpand: Bool) -> Int {
        let baseHeight = 90
        return isExpand ? baseHeight + 100 : baseHeight
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        adjustHeightButton.isHidden = !AccountCommon.isCoachOrManager()

        adjustHeightButton.setImage(UIImage(named: "chevron_down_circle_fill"), for: .normal)
        attendentCheck.setImage(UIImage(named: "yellowCross"), for: .normal)

        btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
        btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
        btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func config(roster: RosterForEvent, isExpand: Bool) {
        configAnttendance(attendance: roster.attendance)

        updateExpandImage(isExpand: isExpand)
    }

    func configAnttendance(attendance: String?) {
        if let attendance = attendance {
            btAttendYes.isChoose = false
            btAttendNo.isChoose = false
            btAttendMaybe.isChoose = false

            if attendance == Attendance.ATTENDING_YES {
                btAttendYes.isChoose = true
            } else if attendance == Attendance.ATTENDING_NO {
                btAttendNo.isChoose = true
            } else if attendance == Attendance.ATTENDING_MAYBE {
                btAttendMaybe.isChoose = true
            }
        }
    }

    func updateExpandImage(isExpand: Bool) {
        let imageName = isExpand ? "chevron_up_circle_fill" : "chevron_down_circle_fill"
        adjustHeightButton.setImage(UIImage(named: imageName), for: .normal)
        adjustHeightButton.tintColor = UIColor.COLOR_BLUE
    }

    func setHealthStatus(status: Int) {
        if status == 0 {
            self.attendentCheck.tintColor = .COLOR_PRIMARY_RED
        } else if status == 1 {
            self.attendentCheck.tintColor = .COLOR_ATTENDING_YES
        } else {
            self.attendentCheck.tintColor = .COLOR_PRIMARY_YELLOW
        }
    }

    // MARK: - Action
    @IBAction func adjustHeightPressed(_ sender: Any) {
        if isAnimatining {
            return
        }

        adjustHeightClosure?()
    }

    @IBAction func onClickChat(_ sender: Any) {
        self.actionChat!()
    }

    @IBAction func updateAttendancePressed(_ sender: AttendButton) {
        if sender.isChoose {
            return
        }

        updateAttendanceClosure?(sender.tag)
    }

}
