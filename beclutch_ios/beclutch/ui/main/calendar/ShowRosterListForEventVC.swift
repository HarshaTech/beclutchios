//
//  ShowRosterListVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/11.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
class ShowRosterListForEventVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    static let KIND_EVENT_FOR_PRACTICE: String = "0"
    static let KIND_EVENT_FOR_GAME: String = "1"
     private var disposal = Disposal()

    @IBOutlet weak var tb: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    var viewModel: ShowRosterCalendarViewModel = ShowRosterCalendarViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadRosterInCalendarToUI", handler: {
            res in
            let resAPI = res?.object as! LoadRosterInCalendarToUI
            self.viewModel.isBusy.value = false
            if let _ = resAPI.result {
                self.viewModel.dp = resAPI.rosters!
                self.viewModel.updateExpandTrack()
                self.tb.reloadData()
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: "Failed to load data.", parent: self)
            }
        })

        self.viewModel.doLoadRoster()
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel.dp[indexPath.row].type == CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentHeaderCell", for: indexPath) as! ContentHeaderCell
            cell.txtMonthName.text = self.viewModel.dp[indexPath.row].title
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowRosterListForEventTVCell") as! ShowRosterListForEventTVCell

            if let roster = self.viewModel.dp[indexPath.row].roster {
                cell.txtName.text = self.viewModel.dp[indexPath.row].roster!.firstName! + " " + self.viewModel.dp[indexPath.row].roster!.secondName!
                cell.txtPo.text = self.viewModel.dp[indexPath.row].roster?.userLevelStr

                cell.setHealthStatus(status: self.viewModel.dp[indexPath.row].roster?.healthStatus ?? 2)

                cell.txtStatus.isHidden = true
                cell.btChat.isHidden = true
                cell.txtStatus.backgroundColor = UIColor.COLOR_BLUE
                cell.txtStatus.layer.cornerRadius = 5
                cell.btChat.tintColor = UIColor.COLOR_PRIMARY_DARK

                cell.config(roster: roster, isExpand: self.viewModel.expandsTrack[indexPath.row])

                cell.adjustHeightClosure = { [weak self] in
                    guard let self = self else {
                        return
                    }

                    let newExpand = !self.viewModel.expandsTrack[indexPath.row]
                    self.viewModel.expandsTrack[indexPath.row] = newExpand
                    cell.updateExpandImage(isExpand: newExpand)

                    UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                        tableView.beginUpdates()
                        tableView.endUpdates()

                        if cell.frame.maxY > tableView.frame.maxY {
                            tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                        }
                    }, completion: { _ in
                        cell.isAnimatining = false
                    })
                }

                cell.updateAttendanceClosure = { [weak self] attendanceValue in
                    guard let self = self else {
                        return
                    }

                    let attendance = String(attendanceValue)
                    if let eventId = self.viewModel.eventId, let eventType = self.viewModel.kind {
                        self.viewModel.doUpdateAttendForMultiple(rosters: [roster.rosterId ?? ""], status: attendance, calendarItemId: eventId, calendarItemType: eventType) { (error) in
                            if let error = error {
                                AlertUtils.showGeneralError(message: error, parent: self)
                            } else {
                                roster.attendance = attendance
                                cell.configAnttendance(attendance: attendance)
                            }
                        }
                    }
                }
            }

            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.viewModel.dp[indexPath.row].type == CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL {
            return 45
        } else {
            return CGFloat(ShowRosterListForEventTVCell.height(isExpand: self.viewModel.expandsTrack[indexPath.row]))
        }
    }

}
