//
//  AddNewPracticeVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import ActionSheetPicker_3_0
import PopupDialog
import SwiftEventBus
import EventKit

enum PracticeScreenType {
    case practice
    case other
}

class AddNewPracticeVC: UIViewController, ILocationChooser, IRepeatManage, WWCalendarTimeSelectorProtocol {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtToolbarTitle: UILabel!
    @IBOutlet weak var btDate: UIButton!
    @IBOutlet weak var btRepeat: UIButton!
    @IBOutlet weak var btStartTime: UIButton!
    @IBOutlet weak var btArrivalTime: UIButton!
    @IBOutlet weak var btDuration: UIButton!
    @IBOutlet weak var btLocation: UIButton!
    @IBOutlet weak var txtMemo: UITextField!
    @IBOutlet weak var txtAddr: UITextField!
    @IBOutlet weak var btSave: UIButton!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet var backgroundView: UIView!

    @IBOutlet weak var eventTypeLabel: UILabel!

    var origin: PracticeModel?
    var isUpdate: Bool = false
    var isApplyAll: Bool = false
    var viewModel: AddPracticeViewModel = AddPracticeViewModel()
    private var disposal = Disposal()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()

        if self.isUpdate {
            self.txtName.isEnabled = true
            self.txtToolbarTitle.text = "Update Practice"

            self.txtName.text = origin!.NAME!
            self.txtName.isUserInteractionEnabled = false
            self.viewModel.repeatModel.value = origin!.REPEAT_DETAIL!

            self.viewModel.year = Int(origin!.DATE_YEAR!)!
            self.viewModel.month = Int(origin!.DATE_MONTH!)!
            self.viewModel.dayOfMonth = Int(origin!.DATE_DAY_OF_MONTH!)!
            self.viewModel.dayOfWeek = origin!.DAY_OF_WEEK!
            self.viewModel.eventTime = Date.makeDateFromInt(year: viewModel.year, month: viewModel.month, day: viewModel.dayOfMonth)
            self.btDate.setTitle(origin?.dateDisplay, for: .normal)

            if let eventTime = viewModel.eventTime {
                self.viewModel.startTimeAMPM = origin!.START_TIME_AMPM!
                //                self.viewModel.startTimeHour = Int(origin!.START_TIME_HOUR!)! > 12 ? Int(origin!.START_TIME_HOUR!)! - 12 : Int(origin!.START_TIME_HOUR!)!
                self.viewModel.startTimeHour = Int((origin?.realStartTimeHour)!)!
                self.viewModel.startTimeMin = Int(origin!.START_TIME_MIN!)!
//                self.viewModel.startTime = eventTime.adjustDate(hour: viewModel.startTimeHour, minute: viewModel.startTimeMin, ampm: viewModel.startTimeAMPM)

                if !(self.viewModel.startTimeAMPM.isEmpty) {
                    let hourTime: String = String(self.viewModel.startTimeHour)
                    let minTime: String = String(format: "%02d", Int(self.viewModel.startTimeMin))
                    let title = hourTime + ":" + minTime + " " + self.viewModel.startTimeAMPM
                    self.btStartTime.setTitle(title, for: .normal)
                }

                self.viewModel.arrivalTimeAMPM = origin!.ARRIVAL_TIME_AMPM!
                self.viewModel.arrivalTimeHour = Int(origin!.ARRIVAL_TIME_HOUR!)! > 12 ? Int(origin!.ARRIVAL_TIME_HOUR!)! - 12 : Int(origin!.realArriveTimeHour!)!
                self.viewModel.arrivalTimeMin = Int(origin!.ARRIVAL_TIME_MIN!)!
                self.viewModel.arrivalTime = eventTime.adjustDate(hour: viewModel.arrivalTimeHour, minute: viewModel.arrivalTimeMin, ampm: viewModel.arrivalTimeAMPM)

                if !(self.viewModel.arrivalTimeAMPM.isEmpty) {
                    let hourTime: String = String(self.viewModel.arrivalTimeHour)
                    self.btArrivalTime.setTitle(hourTime + ":" + String(format: "%02d", Int(self.viewModel.arrivalTimeMin)) + " " + self.viewModel.arrivalTimeAMPM, for: .normal)
                }
            }

            self.viewModel.durationHour = Int(origin!.DURATION_HOUR!)!
            self.viewModel.durationMin = Int(origin!.DURATION_MIN!)!

            if self.viewModel.durationHour != 0 || self.viewModel.durationMin != 0 {
                self.btDuration.setTitle(origin!.DURATION_HOUR! + " hour(s) " + String(format: "%02d min(s)", Int(self.viewModel.durationMin)), for: .normal)
            }

            self.viewModel.teamLocation.value = origin!.location_detail!
            let deadlineTime = DispatchTime.now() + .milliseconds(90)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.txtAddr.text = self.origin!.ADDR!
            }

            self.txtMemo.text = origin!.MEMO!
        } else {
            self.txtToolbarTitle.text = "Add New Practice"
            self.txtName.isEnabled = false
            self.txtName.text = "New Practice"

            if let durationHour = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PRACTICE_HOURS,
                let durationMin = AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.PRACTICE_MINS {
                if !durationHour.isEmpty && !durationMin.isEmpty {
                    self.viewModel.durationHour = Int(durationHour) ?? 0
                    self.viewModel.durationMin = Int(durationMin) ?? 0

                    self.btDuration.setTitle(durationHour + " hour(s) " + durationMin + " min(s)", for: .normal)
                }
            }

            if viewModel.screenType == .other {
                self.txtToolbarTitle.text = "Add New Event"
                self.txtName.isEnabled = true
                self.txtName.text = ""

                self.btDuration.setTitle("Choose Duration", for: .normal)
                self.eventTypeLabel.text = "Event Type: *"
            }
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
        }.add(to: &disposal)
        self.viewModel.repeatModel.observe(DispatchQueue.main) {
            newRepeat, _ in

            if newRepeat.type == RepeatModel.REPEAT_TYPE_NO_REPEAT {
                self.btRepeat.setTitle("Does not repeat", for: .normal)
            } else if newRepeat.type == RepeatModel.REPEAT_TYPE_CUSTOM_REPEAT {
                self.btRepeat.setTitle("Custom Repeat", for: .normal)
            } else if newRepeat.type == RepeatModel.REPEAT_TYPE_DAILY_REPEAT {
                let title = newRepeat.formattedTillDate + " " + RZGWeekDay.toTitle(array: newRepeat.dailyInt)
                self.btRepeat.setTitle("Daily Repeat till \(title)", for: .normal)
            } else if newRepeat.type == RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT {
                self.btRepeat.setTitle("Weekly Repeat till \(newRepeat.formattedTillDate)", for: .normal)
            } else if newRepeat.type == RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT {
                self.btRepeat.setTitle("Monthly Repeat till \(newRepeat.formattedTillDate)", for: .normal)
            } else if newRepeat.type == RepeatModel.REPEAT_TYPE_YEARLY_REPEAT {
                self.btRepeat.setTitle("Yearly Repeat till \(newRepeat.formattedTillDate)", for: .normal)
            }
        }.add(to: &disposal)

        self.viewModel.teamLocation.observe(DispatchQueue.main) {
            newLoc, oldLoc in
            if newLoc.id == oldLoc?.id {
                return
            }

            if !newLoc.id!.isEmpty {
                self.btLocation.setTitle(newLoc.name, for: .normal)
                self.txtAddr.text = StringCheckUtils.getAddressFromLocation(model: newLoc)
            } else {
                self.btLocation.setTitle("Choose Location", for: .normal)
                self.txtAddr.text = ""
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreatePracticeRes", handler: {
            res in
            let resAPI = res?.object as! CreatePracticeRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.alert(title: "Warning", message: "Failed to create practice")
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdatePracticeRes", handler: {
            res in
            let resAPI = res?.object as! UpdatePracticeRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.alert(title: "Warning", message: "Failed to update practice")
            }
        })

    }

    // MARK: - UI
    func configUI() {
        self.appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        self.backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        self.btRepeat.makeGreenButton()
        self.btSave.makeGreenButton()
        self.btDate.makeGrayRadius()
        self.btArrivalTime.makeGrayRadius()
        self.btDuration.makeGrayRadius()
        self.btLocation.makeGrayRadius()
        self.btStartTime.makeGrayRadius()

        btRepeat.titleLabel?.minimumScaleFactor = 0.5
        btRepeat.titleLabel?.adjustsFontSizeToFitWidth = true
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            if self.isUpdate {
                alert(title: "Confirm", message: "Notify Team?", okTitle: "Yes", okHandler: { [weak self] (_) in
                    guard let self = self else {
                        return
                    }

                    self.viewModel.doUpdatePractice(originId: self.origin!.ID!, name: self.txtName.text!, addr: self.txtAddr.text!, memo: self.txtMemo.text!, needNotify: true, isApplyAll: self.isApplyAll)
                }) { [weak self] (_) in
                    guard let self = self else {
                        return
                    }

                    self.viewModel.doUpdatePractice(originId: self.origin!.ID!, name: self.txtName.text!, addr: self.txtAddr.text!, memo: self.txtMemo.text!, needNotify: false, isApplyAll: self.isApplyAll)
                }
            } else {
                alert(title: "Confirm", message: "Notify Team?", okTitle: "Yes", okHandler: { [weak self] (_) in
                    guard let self = self else {
                        return
                    }

                    self.viewModel.doCreatePractice(name: self.txtName.text!, addr: self.txtAddr.text!, memo: self.txtMemo.text!, needNotify: true)
                }) { [weak self] (_) in
                    guard let self = self else {
                        return
                    }

                    self.viewModel.doCreatePractice(name: self.txtName.text!, addr: self.txtAddr.text!, memo: self.txtMemo.text!, needNotify: false)
                }
            }
        }
    }

    func checkValidation() -> Bool {
        if let text = self.txtName.text, !text.isEmpty {
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "Fill Event Type.", parent: self)
            return false
        }

        if self.viewModel.dayOfMonth == 0 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Date.", parent: self)
            return false
        }

        if self.viewModel.startTimeAMPM.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Start Time.", parent: self)
        }

        if self.viewModel.teamLocation.value.id!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Team Location.", parent: self)
            return false
        }

        if self.viewModel.repeatModel.value.type!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Repeat Mode.", parent: self)
            return false
        }

        let currentDate = Date()
        var startDateComponents = DateComponents()
        startDateComponents.year = self.viewModel.year
        startDateComponents.month = self.viewModel.month
        startDateComponents.day = self.viewModel.dayOfMonth
        let startDate = Calendar.current.date(from: startDateComponents)
        if startDate?.compare(Calendar.current.startOfDay(for: Date())) == .orderedSame {
            if let startTime = self.viewModel.startTime, startTime.compare(currentDate) == .orderedAscending {
                AlertUtils.showWarningAlert(title: "Warning", message: "Start time cannot be earlier than the current time.", parent: self)
                return false
            }
        } else if startDate?.compare(Calendar.current.startOfDay(for: Date())) == .orderedAscending {
            AlertUtils.showWarningAlert(title: "Warning", message: "Start time cannot be earlier than the current time.", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickDate(_ sender: Any) {
        let selector = CalendarPickerCommon.picker()
        selector.delegate = self
        
        present(selector, animated: true, completion: nil)
    }

    @IBAction func onClickRepeat(_ sender: Any) {
        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "RepeatManageVC") as! RepeatManageVC
        nextNav.origin = self.viewModel.repeatModel.value
        nextNav.parentDelegate = self
        self.present(nextNav, animated: true, completion: nil)
    }

    @IBAction func onClickStartTime(_ sender: Any) {
        if self.viewModel.startTime == nil {
            let date = viewModel.eventTime ?? Date()
            let updatedDate = Calendar.current.date(bySettingHour: 12, minute: 00, second: 0, of: date)
            self.viewModel.startTime = updatedDate
        }

        let datePicker = ActionSheetDatePicker(title: "Start Time",
                                               datePickerMode: UIDatePicker.Mode.time,
                                               selectedDate: self.viewModel.startTime,
                                               target: self,
                                               action: #selector(startTimePicked(_:)),
                                               origin: self.view)

        datePicker?.minuteInterval = 5
        datePicker?.show()
    }

    @objc func startTimePicked(_ chooseDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "h:mm a"

        self.viewModel.startTime = chooseDate
        self.viewModel.startTimeHour = chooseDate.hourIn12Format
        self.viewModel.startTimeMin = Calendar.current.component(.minute, from: chooseDate)
        self.viewModel.startTimeAMPM = formatter.string(from: chooseDate).uppercased()

        self.btStartTime.setTitle(formatter2.string(from: chooseDate), for: .normal)
    }

    @IBAction func onClickArrivalTime(_ sender: Any) {
        if self.viewModel.arrivalTime == nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd HH:mm"
            self.viewModel.arrivalTime = formatter.date(from: "2016/10/08 12:00")
        }

        let datePicker = ActionSheetDatePicker(title: "Arrival Time",
                                               datePickerMode: UIDatePicker.Mode.time,
                                               selectedDate: self.viewModel.arrivalTime,
                                               target: self,
                                               action: #selector(arrivalTimePicked(_:)),
                                               origin: self.view)

        datePicker?.minuteInterval = 5
        datePicker?.show()
    }

    @objc func arrivalTimePicked(_ chooseDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"

        let formatter2 = DateFormatter()
        formatter2.dateFormat = "h:mm a"
        self.viewModel.arrivalTime = chooseDate
        self.viewModel.arrivalTimeHour = chooseDate.hourIn12Format
        self.viewModel.arrivalTimeMin = Calendar.current.component(.minute, from: chooseDate)
        self.viewModel.arrivalTimeAMPM = formatter.string(from: chooseDate).uppercased()

        self.btArrivalTime.setTitle(formatter2.string(from: chooseDate), for: .normal)
    }

    @IBAction func onClickDuration(_ sender: Any) {
        var arrHr = [String]()
        var arrMin = [String]()

        for hourMark in 0...24 {
            arrHr.append("\(hourMark)")
        }

        for tickMark in stride(from: 0, to: 60, by: 5) {
            arrMin.append("\(tickMark)")
        }

        ActionSheetMultipleStringPicker.show(withTitle: "Choose Duration(Hr Min)", rows: [
            arrHr,
            arrMin
            ], initialSelection: [self.viewModel.durationHour, self.viewModel.durationMin / 5], doneBlock: {
                _, indexes, _ in
                self.viewModel.durationHour = Int(arrHr[indexes![0] as! Int])!
                self.viewModel.durationMin = Int(arrMin[indexes![1] as! Int])!
                self.btDuration.setTitle(arrHr[indexes![0] as! Int] + " hour(s) " + String(format: "%02d min(s)", Int(arrMin[indexes![1] as! Int])!), for: .normal)
                return
        }, cancel: { _ in return }, origin: sender)
    }

    @IBAction func onClickLocation(_ sender: Any) {
        let nextViewControlelr = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as!  LocationListViewController
        nextViewControlelr.viewModel.isForChoose = true
        nextViewControlelr.parentDelegate = self
        self.present(nextViewControlelr, animated: true, completion: nil)
    }

    func onChooseRepeat(val: RepeatModel) {
        self.viewModel.repeatModel.value = val
    }

    func onChooseLocation(loc: TeamLocation) {
        self.viewModel.teamLocation.value = loc
    }
}

extension AddNewPracticeVC {
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "y-M-d"

        self.viewModel.eventTime = date
        self.viewModel.year = calendar.component(.year, from: date)
        self.viewModel.month = calendar.component(.month, from: date)
        self.viewModel.dayOfMonth = calendar.component(.day, from: date)
        self.viewModel.dayOfWeek = formatter.string(from: date)

        self.btDate.setTitle(date.praceDateString(), for: .normal)
    }
}
