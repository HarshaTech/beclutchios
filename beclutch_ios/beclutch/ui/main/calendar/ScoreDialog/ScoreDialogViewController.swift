//
//  ScoreDialogViewController.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/21/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

import Firebase
import MessageKit
import FirebaseFirestore
import FirebaseStorage

class ScoreDialogViewController: UIViewController {
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnFinal: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var tfhome: UITextField!
    @IBOutlet weak var tfAway: UITextField!
    @IBOutlet weak var tvMemo: MultilineTextField!

    var viewModel: ScoreDialogViewModel = ScoreDialogViewModel()
    private var disposal = Disposal()
    var scoreID: String?
    var scoreModel: ScoreModel?
    var isFinal: Bool?

    //Message
    private let db = Firestore.firestore()
    private var reference: CollectionReference?
    private let storage = Storage.storage().reference()

    private var messageListener: ListenerRegistration?

    var teamId: String?
    let roster = AppDataInstance.instance.currentSelectedTeam ?? RosterInfo(val: [:])

    private var teamChatReference: CollectionReference {
        return db.collection("team_chats")
    }

    deinit {
        print("ScoreDialogViewController deinit")
        messageListener?.remove()
    }

    fileprivate func configUI() {
        tvMemo.placeholder = "Memo"
        tvMemo.placeholderColor = .lightGray
        tvMemo.isPlaceholderScrollEnabled = true
        tvMemo.isScrollEnabled = true
        tvMemo.textContainerInset.left = 10
        tvMemo.textContainerInset.right = 10
        tvMemo.leftViewOrigin = CGPoint(x: 8, y: 8)

        btnCancel.makeRedButton()
        btnFinal.makeBlueButton()
        btnUpdate.makeGreenButton()

        btnCancel.tintColor = .white
        btnFinal.tintColor = .white
        btnUpdate.tintColor = .white

        let myColor = UIColor.COLOR_PRIMARY_DARK
        tvMemo.layer.borderColor = myColor.cgColor
        tfhome.layer.borderColor = myColor.cgColor
        tfAway.layer.borderColor = myColor.cgColor

        tfhome.layer.cornerRadius = 5
        tvMemo.layer.cornerRadius = 5
        tfAway.layer.cornerRadius = 5
        tfhome.layer.borderWidth = 1.0
        tvMemo.layer.borderWidth = 1.0
        tfAway.layer.borderWidth = 1.0

        tfhome.clipsToBounds = true
        tvMemo.clipsToBounds = true
        tfAway.clipsToBounds = true
        
//        tfhome.delegate = self
//        tfAway.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
        self.configMessage()

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "GetGameScoreRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let regRes = res?.object as! ScoreRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                self.scoreModel = regRes.score

                self.tfhome.text = "\(self.scoreModel!.TEAM_SCORE)"
                self.tfAway.text = "\(self.scoreModel!.OPPONENT_SCORE)"
                //                self.tvMemo.text = self.scoreModel?.MEMO
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed to get score game. Try again.", parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateGameScoreRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let regRes = res?.object as! GeneralRes
            self.viewModel.isBusy.value = false
            if (regRes.result ) == true {
                if let usScore = self.tfhome.text,
                    let themScore = self.tfAway.text {
                    let memo = self.tvMemo.text.isEmpty ? "" : " - [\(self.tvMemo.text!)]"
                    self.sendMessage(message: "\(self.isFinal ?? false ? "Final" : "Update") Score - Us[\(usScore)] - Them[\(themScore)]\(memo)")
                }

                self.view.endEditing(true)
                self.dismiss(animated: false, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Error", message: "Failed update score game. Try again.", parent: self)
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.doGetScoreGame(id: self.scoreID!)
    }

    func doUpdateScore(flag: Bool) {
        self.viewModel.doUpdateScoreGame(id: self.scoreModel?.GAME_ID ?? "",
                                         usScore: self.tfhome.text ?? "0",
                                         themScore: self.tfAway.text ?? "0",
                                         memo: self.tvMemo.text ?? "",
                                         flag: flag)
    }

    @IBAction func handleCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func handleFinal(_ sender: Any) {
        isFinal = true
        doUpdateScore(flag: true)
    }

    @IBAction func handleUpdate(_ sender: Any) {
        isFinal = false
        doUpdateScore(flag: false)
    }
    @IBAction func usPressed(_ sender: Any) {
        tfhome.selectAll(nil)
    }
    @IBAction func themPressed(_ sender: Any) {
        tfAway.selectAll(nil)
    }
}

//MessageKit
extension ScoreDialogViewController {
    func configMessage() {
        self.teamId = "team_" + self.roster.TEAM_ID
        reference = db.collection(["team_chats", teamId!, "messages"].joined(separator: "/"))
        //
        //        messageListener = reference?.addSnapshotListener { querySnapshot, error in
        //            guard let snapshot = querySnapshot else {
        //                print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
        //                return
        //            }
        //
        //        }
    }

    private func save(_ message: Message) {
        reference?.addDocument(data: message.representation) { error in
            if let e = error {
                print("Error sending message: \(e.localizedDescription)")
                return
            }

            let req = SendTeamTextChatReq(message: message)
            ChatService.instance.doSendTeamText(req: req)

            self.teamChatReference.document(self.teamId!).setData(ChatEntryReadDTO(message: message).firebaseDict()) { (error) in
                print(error ?? "")
            }

        }
    }

    func sendMessage(message: String) {
        let message = Message(roster: roster, content: message)
        save(message)
    }
}

//extension ScoreDialogViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.selectAll(nil)
//    }
//}
