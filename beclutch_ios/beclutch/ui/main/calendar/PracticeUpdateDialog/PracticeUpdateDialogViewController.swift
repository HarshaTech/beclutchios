//
//  PracticeUpdateDialogViewController.swift
//  beclutch
//
//  Created by Vu Trinh on 20/10/2020.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

import BEMCheckBox

import SwiftEventBus

protocol PracticeUpdateDialogViewControllerDelegate {
    func refreshCalendarUI()
    func doUpdatePractice(practice: PracticeModel, isApplyAll: Bool)
}

class PracticeUpdateDialogViewController: UIViewController {
    enum DialogType {
        case cancel
        case delete
        case update
        
        var title: String {
            switch self {
            case .cancel:
                return "Cancel"
            case .delete:
                return "Delete"
            case .update:
                return "Edit"
            }
        }
    }
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var actionPrefixLabel: UILabel!
    
    @IBOutlet weak var thisCheckbox: BEMCheckBox!
    @IBOutlet weak var allCheckBox: BEMCheckBox!
    
    var viewModel: PracticeUpdateDialogViewModel = PracticeUpdateDialogViewModel()
    var delegate: PracticeUpdateDialogViewControllerDelegate?
    
    var dialogType: DialogType = .update
    var practice: PracticeModel = PracticeModel(val: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configUI()
        listenObsever()
    }
    
    deinit {
        print("PracticeUpdateDialogViewController deinit")
    }

    private func configUI() {
        thisCheckbox.on = true
        
        backButton.makeRedButton()
        confirmButton.makeBlueButton()
        
        thisCheckbox.on = true
        allCheckBox.on = false
        
        actionPrefixLabel.text = dialogType.title
        
        backgroundView.alpha = 0.3
    }
    
    private func listenObsever() {
        SwiftEventBus.onMainThread(self, name: "CancelPracticeRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            self.viewModel.isBusy.value = false
            
            let resAPI = res?.object as! CancelPracticeRes

            if resAPI.result! {
                self.delegate?.refreshCalendarUI()
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        })
        
        SwiftEventBus.onMainThread(self, name: "DeleteCalendarItemRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            self.viewModel.isBusy.value = false
            
            let resAPI = res?.object as! DeleteCalendarItemRes

            if resAPI.result! {
                self.delegate?.refreshCalendarUI()
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Action
    @IBAction func checkBoxPress(_ checkbox: BEMCheckBox) {
        let otherCheckBox = checkbox == thisCheckbox ? allCheckBox : thisCheckbox
        otherCheckBox?.on = !checkbox.on
    }
    
    @IBAction func backPress(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmPress(_ sender: Any) {
        switch dialogType {
        case .cancel:
            self.viewModel.doCancelPractice(practiceId: practice.ID, isApplyAll: allCheckBox.on)
        case .delete:
            self.viewModel.doDeletePractice(practiceId: practice.ID, isApplyAll: allCheckBox.on)
        case .update:
            self.delegate?.doUpdatePractice(practice: practice, isApplyAll: allCheckBox.on)
        }
    }
}
