//
//  CalendarVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import MapKit
import PopupDialog
import GoogleMobileAds

enum HEIGHT_CONST {
    static let HEADER_HEIGHT: CGFloat = 45
    static let CLOSE_CELL_HEIGHT: CGFloat = 128
    static var OPEN_PRACTICE_HEIGHT: CGFloat {
        return AccountCommon.isCoachManagerOrTreasure() ? 450 : 410
    }
    static var OPEN_GAME_HEIGHT: CGFloat {
        return AccountCommon.isCoachManagerOrTreasure() ? 562 : 522
    }
}

class CalendarVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CalendarCommon, AdsProtocol {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btTeamToolbar: UIButton!
    @IBOutlet weak var btAddToolbar: UIButton!
    @IBOutlet weak var btSyncToolbar: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var adView: GADBannerView!

    var selectPracticeModel: PracticeModel?
    var selectGameModel: GameModel?
    var selectStatus: String?
    var isGameCell: Bool?
    var selectedIndex: Int?

    var isHealCheck = false

    var viewModel: CalendarListViewModel = CalendarListViewModel()
    private var disposal = Disposal()
    private var refreshControl = UIRefreshControl()

    var cellHeights: [CGFloat] = []
    var historyPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTheme()
        
        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
        }.add(to: &disposal)
        
        SwiftEventBus.onMainThread(self, name: "CalendarContentRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! CalendarContentRes
            if resAPI.result {
                self.viewModel.dp = resAPI.dtos

                self.cellHeights = []
                for item in self.viewModel.dp {
                    if item.type == CALENDAR_ITEM_TYPE.HEADER_MODEL {
                        self.cellHeights.append(HEIGHT_CONST.HEADER_HEIGHT)
                    } else { //GAME OR RPACTICE MODEL
                        self.cellHeights.append(HEIGHT_CONST.CLOSE_CELL_HEIGHT)
                    }
                }
                self.tableView.reloadData()
            }
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "CancelGameRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! CancelGameRes
            if resAPI.result! {
                self.viewModel.dp[resAPI.pos!].game?.IS_ACTIVE = "\(GameModel.GAME_CANCEL)"
                self.tableView.reloadData()
            }
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "GetHistoryEventsRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! GetHistoryEventsRes
            if resAPI.result! {
                self.configUIForHistory(games: resAPI.games, practices: resAPI.practices)
                self.historyPage += 1
                self.tableView.reloadData()
            }
            self.refreshControl.endRefreshing()
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "DeleteCalendarItemRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! DeleteCalendarItemRes

            if resAPI.result! {
                if resAPI.pos! > -1 {
                    if self.viewModel.dp.count <= 2 {
                        self.viewModel.dp = []
                    } else if self.viewModel.dp.count > 2 {
                        self.viewModel.dp.remove(at: resAPI.pos!)
                    }
                    self.cellHeights = []
                    for item in self.viewModel.dp {
                        if item.type == CALENDAR_ITEM_TYPE.HEADER_MODEL {
                            self.cellHeights.append(HEIGHT_CONST.HEADER_HEIGHT)
                        } else { //GAME OR RPACTICE MODEL
                            self.cellHeights.append(HEIGHT_CONST.CLOSE_CELL_HEIGHT)
                        }
                    }

                    self.tableView.reloadData()
                }
            }
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "UpdateAttendanceRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! UpdateAttendanceRes

            if resAPI.result! {
                for i in 0...(self.viewModel.dp.count - 1) {
                    if self.viewModel.dp[i].id == resAPI.itemID && self.viewModel.dp[i].type == resAPI.type {
                        if self.viewModel.dp[i].type == CALENDAR_ITEM_TYPE.GAME_MODEL {
                            self.viewModel.dp[i].game?.attendance_detail = resAPI.attendance
                            break
                        } else if self.viewModel.dp[i].type == CALENDAR_ITEM_TYPE.PRACTICE_MODEL {
                            self.viewModel.dp[i].practice?.attendance_detail = resAPI.attendance
                            break
                        }
                    }
                }
                self.tableView.reloadData()
            }
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "GetResponsibleRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! GetResponsibleRes

            if resAPI.result! {
                if let roster = resAPI.rosters {
                    if roster.count > 1 {
                        let vc = GetResponsibleViewController()
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.playerList = resAPI.rosters
                        vc.delegate = self
                        self.present(vc, animated: true, completion: nil)
                        self.viewModel.isBusy.value = false
                    } else {
                        if self.isHealCheck {
                            let vc = HealthCheckViewController()
                            vc.modalPresentationStyle = .overFullScreen

                            vc.submitSuccessClousure = { status in
                                self.selectStatus = "\(status)"
                                self.updateAttendance(rosters: [AppDataInstance.instance.currentSelectedTeam?.ID ?? ""])
                            }

                            self.present(vc, animated: true, completion: nil)
                        } else {
                            self.updateAttendance(rosters: [AppDataInstance.instance.currentSelectedTeam?.ID ?? ""])
                        }
                    }
                }
            }
        })

        SwiftEventBus.onMainThread(self, name: "CancelPracticeRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! CancelPracticeRes

            if resAPI.result! {
                if let pos = resAPI.pos {
                    self.viewModel.dp[pos].practice?.IS_ACTIVE = "\(PracticeModel.PRACTICE_CANCEL)"
                    self.tableView.reloadData()
                }
            }
            self.viewModel.isBusy.value = false
        })

        SwiftEventBus.onMainThread(self, name: "CreateGameRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! CreateGameRes

            if resAPI.result! {
                self.viewModel.doLoadCalendarItem()
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateGameRes", handler: { [weak self]
            res in
            let resAPI = res?.object as! UpdateGameRes

            if resAPI.result! {
                self?.viewModel.doLoadCalendarItem()
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdatePracticeRes", handler: { [weak self]
            res in
            let resAPI = res?.object as! UpdatePracticeRes

            if resAPI.result! {
                self?.viewModel.doLoadCalendarItem()
            }
        })

        SwiftEventBus.onMainThread(self, name: "CreatePracticeRes", handler: { [weak self]
            res in
            let resAPI = res?.object as! CreatePracticeRes

            if resAPI.result! {
                self?.viewModel.doLoadCalendarItem()
            }
        })
        
        self.btTeamToolbar.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btTeamToolbar.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
        
        refreshCalendarUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        SwiftEventBus.unregister(self)
    }

    func configUIForHistory(games: [GameModel]?, practices: [PracticeModel]?) {
        if !self.viewModel.dp.isEmpty, self.viewModel.dp[0].type == CALENDAR_ITEM_TYPE.HEADER_MODEL, self.viewModel.dp[0].month == "History" {

        } else {
            let model = CalendarListDTO()
            model.type = CALENDAR_ITEM_TYPE.HEADER_MODEL
            model.month = "History"
            self.viewModel.dp.insert(model, at: 0)
        }

        var temp: [EventModel] = []
        //        temp.append(games![0])
        temp += games ?? [EventModel]()
        temp += practices ?? [EventModel]()

        temp = temp.sorted { (firstObj, secondObject) -> Bool in
            if let firstObj = firstObj as? GameModel, let secondObject = secondObject as? GameModel {
                if let fullDate = secondObject.fullDate {
                    return firstObj.fullDate?.compare(fullDate) == .orderedDescending
                }

            }

            if let firstObj = firstObj as? GameModel, let secondObject = secondObject as? PracticeModel {
                if let fullDate = secondObject.fullDate {
                    return firstObj.fullDate?.compare(fullDate) == .orderedDescending
                }
            }

            if let firstObj = firstObj as? PracticeModel, let secondObject = secondObject as? GameModel {
                if let fullDate = secondObject.fullDate {
                    return firstObj.fullDate?.compare(fullDate) == .orderedDescending
                }
            }

            if let firstObj = firstObj as? PracticeModel, let secondObject = secondObject as? PracticeModel {
                if let fullDate = secondObject.fullDate {
                    return firstObj.fullDate?.compare(fullDate) == .orderedDescending
                }
            }
            return false
        }

        temp.forEach { (obj) in
            let model = CalendarListDTO()

            if let game = obj as? GameModel {
                model.game = game
                model.isHistory = true
                model.type = CALENDAR_ITEM_TYPE.GAME_MODEL
                self.viewModel.dp.insert(model, at: 1)
            }
            if let practice = obj as? PracticeModel {
                model.practice = practice
                model.isHistory = true
                model.type = CALENDAR_ITEM_TYPE.PRACTICE_MODEL
                self.viewModel.dp.insert(model, at: 1)
            }
        }

        self.cellHeights = []
        for item in self.viewModel.dp {
            if item.type == CALENDAR_ITEM_TYPE.HEADER_MODEL {
                self.cellHeights.append(HEIGHT_CONST.HEADER_HEIGHT)
            } else { //GAME OR RPACTICE MODEL
                self.cellHeights.append(HEIGHT_CONST.CLOSE_CELL_HEIGHT)
            }
        }

    }

    func configUI() {
        //setup TableView
        configAdView(adView: adView, from: self)
        self.tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(loadHistoricalData(_:)), for: .valueChanged)
    }
    
    func refreshTheme() {
        btAddToolbar.layer.cornerRadius = btAddToolbar.width/2.0
        btAddToolbar.backgroundColor = UIColor.COLOR_ACCENT
        btSyncToolbar.layer.cornerRadius = btAddToolbar.width/2.0
        btSyncToolbar.backgroundColor = UIColor.COLOR_ACCENT
        btTeamToolbar.makeStrokeOnlyButton()
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        btSyncToolbar.isHidden = true// AppDataInstance.instance.currentSelectedTeam == nil
        btAddToolbar.isHidden = !AccountCommon.isCoachOrManager()
    }

    @objc private func loadHistoricalData(_ sender: Any) {
        refreshControl.beginRefreshing()
        self.viewModel.doGetHistoryEvents(memberId: AppDataInstance.instance.currentSelectedTeam!.ID, page: historyPage)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp.count
    }

    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.viewModel.dp[indexPath.row].type != CALENDAR_ITEM_TYPE.HEADER_MODEL {
            cell.backgroundColor = .clear
            
            if self.viewModel.dp[indexPath.row].type == CALENDAR_ITEM_TYPE.GAME_MODEL {
                let cellVal = cell as! GameCell
                
                let model = self.viewModel.dp[indexPath.row].game!
                if Int(model.IS_ACTIVE!)! == GameModel.GAME_CANCEL {
                    cellVal.btAttendNo.isEnabled = false
                    cellVal.btAttendYes.isEnabled = false
                    cellVal.btAttendMaybe.isEnabled = false
                }

                if cellHeights[indexPath.row] == HEIGHT_CONST.CLOSE_CELL_HEIGHT {
                    cellVal.unfold(false, animated: false, completion: nil)
                } else {
                    cellVal.unfold(true, animated: false, completion: nil)
                }
            } else {
                let cellVal = cell as! PracticeCell
                
                let model = self.viewModel.dp[indexPath.row].practice!
                if Int(model.IS_ACTIVE!)! == PracticeModel.PRACTICE_CANCEL {
                    cellVal.btAttendNo.isEnabled = false
                    cellVal.btAttendYes.isEnabled = false
                    cellVal.btAttendMaybe.isEnabled = false
                }

                if cellHeights[indexPath.row] == HEIGHT_CONST.CLOSE_CELL_HEIGHT {
                    cellVal.unfold(false, animated: false, completion: nil)
                } else {
                    cellVal.unfold(true, animated: false, completion: nil)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel.dp[indexPath.row].type == CALENDAR_ITEM_TYPE.HEADER_MODEL {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentHeaderCell", for: indexPath) as! ContentHeaderCell
            cell.txtMonthName.text = self.viewModel.dp[indexPath.row].month
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.COLOR_BLUE

            return cell
        } else if self.viewModel.dp[indexPath.row].type == CALENDAR_ITEM_TYPE.GAME_MODEL {
            let model = self.viewModel.dp[indexPath.row].game!
            return gameCell(indexPath: indexPath, model: model)
        } else {
            let model = self.viewModel.dp[indexPath.row].practice!
            return practiceCell(indexPath: indexPath, model: model)
        }
    }
    
    private func gameCell(indexPath: IndexPath, model: GameModel) -> GameCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath) as! GameCell
        
        cell.config(model: model)
        
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        
        /////////////////////////////////
        cell.txtCloseName.textColor = UIColor.COLOR_BLUE
        cell.btDown.tintColor = UIColor.COLOR_BLUE
        cell.btUp.tintColor = UIColor.COLOR_BLUE
        cell.txtOpenName.textColor = UIColor.COLOR_ACCENT
        cell.btCancel.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btDelete.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btEdit.backgroundColor = UIColor.COLOR_PRIMARY
        
        let isShown = AccountCommon.isCoachManagerOrTreasure()
        cell.btDelete.isHidden = !isShown
        cell.btEdit.isHidden = !isShown
        cell.btCancel.isHidden = !isShown
        
        if Int(model.IS_ACTIVE!)! == GameModel.GAME_CANCEL {
            cell.btEdit.isHidden = true
            cell.btCancel.isHidden = true
        }

        cell.btAttendMaybe.isChoose = false
        cell.btAttendYes.isChoose = false
        cell.btAttendNo.isChoose = false

        let isHistoryCell = self.viewModel.dp[indexPath.row].isHistory ?? false
        if isHistoryCell {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.subContentView.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.btDown.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.leftViewContainer.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.txtMonth.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
            cell.txtMonth.isHidden = false
            cell.txtMonthExpand.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
            cell.txtMonthExpand.isHidden = false
        } else {
            cell.contentView.backgroundColor = .white
            cell.leftViewContainer.backgroundColor = .white
            cell.subContentView.backgroundColor = .white
            cell.btDown.backgroundColor = .white
            cell.txtMonth.isHidden = true
            cell.txtMonthExpand.isHidden = true
        }

        cell.btUp.backgroundColor = .clear

        if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
            cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
            cell.btAttendYes.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
            cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
            cell.btAttendNo.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
            cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
            cell.btAttendMaybe.isChoose = true
        }

        cell.txtCloseAddr.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtOpenAddr.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        
        cell.txtCloseName.text = model.NAME
        cell.txtOpenName.text = model.NAME
        cell.txtCloseOppoName.text = "vs " + model.opponent_detail!.OPPONENT_NAME!
        cell.txtOpenOppoName.text = "vs " + model.opponent_detail!.OPPONENT_NAME!

        let strTime = String(format: "%d:%02d", Int(model.START_TIME_HOUR!)! > 12 ? Int(model.START_TIME_HOUR!)! - 12 : Int(model.realStartTimeHour!)!, Int(model.START_TIME_MIN!)!) + " " + model.START_TIME_AMPM!
        if model.DURATION_HOUR == nil {
            cell.txtCloseTime.text = strTime
            cell.txtOpenTime.text = strTime
        } else if model.DURATION_HOUR == "-1" || (model.DURATION_HOUR == "0" && model.DURATION_MIN == "0") {
            cell.txtCloseTime.text = strTime
            cell.txtOpenTime.text = strTime
        } else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "h:mm a"

            var startDate: Date = formatter2.date(from: strTime)!
            startDate = Calendar.current.date(byAdding: .hour, value: Int(model.DURATION_HOUR!)!, to: startDate)!
            let date  = Calendar.current.date(byAdding: .minute, value: Int(model.DURATION_MIN!)!, to: startDate)!

            cell.txtCloseTime.text = strTime + " - " + formatter2.string(from: date)
            cell.txtOpenTime.text = strTime + " - " + formatter2.string(from: date)
        }
        cell.txtCloseDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtCloseDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenLocation.text = model.location_detail!.id!.isEmpty ? "No Location yet" : StringCheckUtils.getAddressFromLocation(model: model.location_detail!)

        cell.txtMemo.text = model.MEMO == nil ? "No Memo" : model.MEMO

        cell.btHome.isChoose = model.IS_HOME == "1"
        cell.btAway.isChoose = model.IS_HOME == "0"

        cell.viewWearColor.backgroundColor = ColorUtils.getUIColorFromRGB(val: model.UNIFORM_COLOR!)
        
        cell.txtOpenCancel.isHidden = cell.txtCloseCancel.isHidden
        cell.txtMonthExpand.isHidden = !cell.txtCloseCancel.isHidden
        
        if !isHistoryCell {
            cell.txtMonth.isHidden = true
            cell.txtMonthExpand.isHidden = true
        }

        cell.actionOpen = {
            self.cellHeights[indexPath.row] = HEIGHT_CONST.OPEN_GAME_HEIGHT
            cell.unfold(true, animated: true, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.tableView.beginUpdates()
                self.tableView.endUpdates()

                if cell.frame.maxY > self.tableView.frame.maxY {
                    self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            }, completion: nil)
        }

        cell.actionClose = {
            if cell.isAnimating() {
                return
            }
            self.cellHeights[indexPath.row] = HEIGHT_CONST.CLOSE_CELL_HEIGHT
            cell.unfold(false, animated: true, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.tableView.beginUpdates()
                self.tableView.endUpdates()

                if cell.frame.maxY > self.tableView.frame.maxY {
                    self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            }, completion: nil)
        }

        cell.actionDrive = {
            if model.location_detail!.id!.isEmpty {
                AlertUtils.showWarningAlert(title: "Warning", message: "No address yet", parent: self)
            } else {
                if let targetURL = model.getAddressUrl {
                    UIApplication.shared.open(targetURL, options: [:], completionHandler: nil)
                }
            }
        }

        cell.actionCarPool = {
            self.alert(message: "Are you sure you want to request a carpool?", okTitle: "Yes", okHandler: { _ in
                let data = self.viewModel.dp[indexPath.row]
                self.viewModel.doCarpool(calendar: data) { (error) in
                    if error == nil {
                        self.gotoCarPoolChatViewController()
                    }
                }
            }, cancelHandler: { _ in})
        }

        cell.actionHealthCheck = { [weak self] in
            self?.volunterAction()
        }

        cell.actionAttendYes = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_YES
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            //game
            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendNo = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_NO
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendMaybe = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectGameModel = model
            self.isGameCell = true
            self.selectStatus = Attendance.ATTENDING_MAYBE
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionRosters = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.validatePendingState() {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ShowRosterListForEventVC") as! ShowRosterListForEventVC
                nextNav.viewModel.kind = ShowRosterListForEventVC.KIND_EVENT_FOR_GAME
                nextNav.viewModel.eventId = model.ID
                self.present(nextNav, animated: true, completion: nil)
            }
        }

        cell.actionScore = { [weak self] in
           guard let self = self else {
               return
           }
            
            let vc = ScoreDialogViewController()
            vc.scoreID = model.ID
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }

        cell.actionEdit = {
            if self.validatePendingState() {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGameVC") as! AddNewGameVC
                nextNav.isUpdate = true
                nextNav.origin = model
                self.present(nextNav, animated: true, completion: nil)
            }
        }

        cell.actionDelete = {
            if self.validatePendingState() {
                self.viewModel.doDeleteGamePractice(eventId: model.ID!, kind: "1", pos: indexPath.row)
            }
        }

        cell.actionCancel = { [weak self] in
            guard let self = self else {
                return
            }

            if self.validatePendingState() {
                self.alert(title: "Confirm", message: "Notify Team?", okHandler: { [weak self] (_) in
                    self?.viewModel.doCancelGame(gameId: model.ID!, pos: indexPath.row, notify: true)
                }) { [weak self] (_) in
                    self?.viewModel.doCancelGame(gameId: model.ID!, pos: indexPath.row, notify: false)
                }
            }
        }

        cell.actionOppoInfo = {
            DispatchQueue.main.async {
                if self.validatePendingState() {
                    if model.opponent_detail!.ID!.isEmpty {
                        AlertUtils.showWarningAlert(title: "Warning", message: "You don't have opponent yet.", parent: self)
                    } else {
                        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "OpponentAddViewController") as! OpponentAddViewController
                        nextNav.isEidt = true
                        nextNav.isShowing = true
                        nextNav.viewModel.originOppo = model.opponent_detail
                        self.present(nextNav, animated: true, completion: nil)
                    }
                }
            }
        }
        
        cell.actionShare = { [weak self] in
            self?.shareModel(model: model)
        }
        
        return cell
    }
    
    private func practiceCell(indexPath: IndexPath, model: PracticeModel) -> PracticeCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeCell", for: indexPath) as! PracticeCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations

        cell.txtCloseName.textColor = UIColor.COLOR_BLUE
        cell.txtOpenName.textColor = UIColor.COLOR_ACCENT
        cell.btExpand?.tintColor = UIColor.COLOR_BLUE
        cell.btCollapse?.tintColor = UIColor.COLOR_BLUE
        cell.btCancel.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btDelete.backgroundColor = UIColor.COLOR_PRIMARY
        cell.btEdit.backgroundColor = UIColor.COLOR_PRIMARY
        
        let isShown = AccountCommon.isCoachManagerOrTreasure()
        cell.btDelete.isHidden = !isShown
        cell.btEdit.isHidden = !isShown
        cell.btCancel.isHidden = !isShown
        
        if Int(model.IS_ACTIVE!)! == PracticeModel.PRACTICE_CANCEL {
            cell.btEdit.isHidden = true
            cell.btCancel.isHidden = true
        }

        let isHistoryCell = self.viewModel.dp[indexPath.row].isHistory ?? false
        if  isHistoryCell {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.btExpand?.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.leftViewContainer.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            cell.txtMonth.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
            cell.txtMonthExpand.text = CommonUtils.monthStr[Int(model.DATE_MONTH!)! - 1]
        } else {
            cell.contentView.backgroundColor = .white
            cell.leftViewContainer.backgroundColor = .white
            cell.btExpand?.backgroundColor = .white
        }

        cell.txtCloseCancel.isHidden = Int(model.IS_ACTIVE!)! != PracticeModel.PRACTICE_CANCEL
        cell.txtMonth.isHidden = !cell.txtCloseCancel.isHidden
        
        cell.txtOpenCancel.isHidden = cell.txtCloseCancel.isHidden
        cell.txtMonthExpand.isHidden = !cell.txtCloseCancel.isHidden
        
        if !isHistoryCell {
            cell.txtMonth.isHidden = true
            cell.txtMonthExpand.isHidden = true
        }
        
        cell.txtCloseLocation.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtOpenLocation.text = model.location_detail!.id!.isEmpty ? "No Location" : model.location_detail?.name
        cell.txtCloseName.text = model.NAME
        cell.txtOpenName.text = model.NAME
        let startHour = Int(model.START_TIME_HOUR!)! > 12 ? Int(model.START_TIME_HOUR!)! - 12 : Int(model.realStartTimeHour!)!
        let startMin = Int(model.START_TIME_MIN!)!
        let strTime = String(format: "%d:%02d", startHour, startMin) + " " + model.START_TIME_AMPM!

        if model.DURATION_HOUR == nil {
            cell.txtCloseHour.text = strTime
            cell.txtOpenTime.text = strTime
        } else if model.DURATION_HOUR == "-1" || (model.DURATION_HOUR == "0" && model.DURATION_MIN == "0") {
            cell.txtCloseHour.text = strTime
            cell.txtOpenTime.text = strTime
        } else {
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "h:mm a"

//            var startDate: Date = formatter2.date(from: strTime)!
            if let ampm = model.START_TIME_AMPM,
                var startDate = Date().adjustDate(hour: startHour, minute: startMin, ampm: ampm) {

                startDate = Calendar.current.date(byAdding: .hour, value: Int(model.DURATION_HOUR!)!, to: startDate)!
                let date  = Calendar.current.date(byAdding: .minute, value: Int(model.DURATION_MIN!)!, to: startDate)!

                cell.txtCloseHour.text = strTime + " - " + formatter2.string(from: date)
                cell.txtOpenTime.text = strTime + " - " + formatter2.string(from: date)
            }
        }

        cell.btAttendMaybe.isChoose = false
        cell.btAttendYes.isChoose = false
        cell.btAttendNo.isChoose = false

        if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
            cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
            cell.btAttendYes.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
            cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
            cell.btAttendNo.isChoose = true
        } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
            cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
            cell.btAttendMaybe.isChoose = true
        }

        cell.txtCloseDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayWeek.text = model.DAY_OF_WEEK?.uppercased()
        cell.txtOpenDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtCloseDayMonth.text = model.DATE_DAY_OF_MONTH
        cell.txtOpenAddr.text = model.location_detail!.id!.isEmpty ? "No Location yet" : StringCheckUtils.getAddressFromLocation(model: model.location_detail!)
        cell.txtMemo.text = model.MEMO == nil ? "No Memo" : model.MEMO
        cell.actionOpen = {
            self.cellHeights[indexPath.row] = HEIGHT_CONST.OPEN_PRACTICE_HEIGHT
            cell.unfold(true, animated: true, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.tableView.beginUpdates()
                self.tableView.endUpdates()

                if cell.frame.maxY > self.tableView.frame.maxY {
                    self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }

            }, completion: nil)
        }

        cell.actionClose = {
            if cell.isAnimating() {
                return
            }
            self.cellHeights[indexPath.row] = HEIGHT_CONST.CLOSE_CELL_HEIGHT
            cell.unfold(false, animated: true, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.tableView.beginUpdates()
                self.tableView.endUpdates()

                if cell.frame.maxY > self.tableView.frame.maxY {
                    self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            }, completion: nil)
        }

        cell.actionDirection = {
            if model.location_detail!.id!.isEmpty {
                AlertUtils.showWarningAlert(title: "Warning", message: "No address yet", parent: self)
            } else {
                if let targetURL = model.getAddressUrl {
                    UIApplication.shared.open(targetURL, options: [:], completionHandler: nil)
                }
            }

        }

        cell.actionHealthCheck = { [weak self] in
            self?.volunterAction()
        }

        cell.actionAttendYes = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_YES
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendNo = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_NO
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionAttendMaybe = {
            guard self.viewModel.dp[indexPath.row].id != nil else {
                return
            }

            self.selectPracticeModel = model
            self.isGameCell = false
            self.selectStatus = Attendance.ATTENDING_MAYBE
            self.selectedIndex = indexPath.row
            self.isHealCheck = false

            if self.validatePendingState() {
                self.viewModel.doGetResponsible(memberId: AppDataInstance.instance.currentSelectedTeam?.ID ?? "")
            }
        }

        cell.actionRosters = {
            if self.validatePendingState() {
                let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "ShowRosterListForEventVC") as! ShowRosterListForEventVC
                nextNav.viewModel.kind = ShowRosterListForEventVC.KIND_EVENT_FOR_PRACTICE
                nextNav.viewModel.eventId = model.ID
                self.present(nextNav, animated: true, completion: nil)
            }
        }

        cell.actionEdit = {
            if self.validatePendingState() {
                if let detail = model.REPEAT_DETAIL, detail.isRepeatEvent() {
                    self.showPraciteUpdateDialog(type: .update, model: model)
                } else {
                    self.doUpdatePractice(practice: model, isApplyAll: false)
                }
            }
        }

        cell.actionDelete = {
            if self.validatePendingState() {
                if let detail = model.REPEAT_DETAIL, detail.isRepeatEvent() {
                    self.showPraciteUpdateDialog(type: .delete, model: model)
                } else {
                    self.viewModel.doDeleteGamePractice(eventId: model.ID!, kind: "0", pos: indexPath.row)
                }
            }
        }

        cell.actionCancel = { [weak self] in
            guard let self = self else {
                return
            }

            if self.validatePendingState() {
                if let detail = model.REPEAT_DETAIL, detail.isRepeatEvent() {
                    self.showPraciteUpdateDialog(type: .cancel, model: model)
                } else {
                    self.alert(title: "Confirm", message: "Notify Team?", okHandler: { (_) in
                        self.viewModel.doCancelPractice(practiceId: model.ID!, pos: indexPath.row, notifyTeam: true)
                    }) { (_) in
                        self.viewModel.doCancelPractice(practiceId: model.ID!, pos: indexPath.row, notifyTeam: false)
                    }
                }
            }
        }

        cell.actionAddCalendar = { [weak self] in
            SystemCalendarService.shared.addReminder(title: model.NAME, note: model.MEMO, eventTime: model.eventDate, duration: TimeInterval(model.duration)) {error in
                if error == nil {
                    self?.alert(title: "", message: "Add practice to calendar Successfully.")
                }
            }
        }

        cell.actionCarPool = {
            self.alert(message: "Are you sure you want to request a carpool?", okTitle: "Yes", okHandler: { _ in
                let data = self.viewModel.dp[indexPath.row]
                self.viewModel.doCarpool(calendar: data) { (error) in
                    if error == nil {
                        self.gotoCarPoolChatViewController()
                    }
                }
            }, cancelHandler: { _ in})
        }
        
        cell.actionShare = { [weak self] in
            self?.shareModel(model: model)
        }

        return cell
    }
    
    private func showPraciteUpdateDialog(type: PracticeUpdateDialogViewController.DialogType, model: PracticeModel) {
        let practiceUpdateDialog = PracticeUpdateDialogViewController()
        practiceUpdateDialog.delegate = self
        practiceUpdateDialog.practice = model
        practiceUpdateDialog.dialogType = type
        practiceUpdateDialog.modalPresentationStyle = .overFullScreen
        
        self.present(practiceUpdateDialog, animated: true, completion: nil)
    }

    private func checkResponsible(memberId: String) {
        self.viewModel.doGetResponsible(memberId: memberId)
    }

    private func volunterAction() {
        if let settingNavigationVC = self.tabBarController?.viewControllers?.last as? UINavigationController {
            settingNavigationVC.popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 4

            let volunteersVC = VolunteersViewController()
            settingNavigationVC.pushViewController(volunteersVC, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }

    @IBAction func onClickAddNew(_ sender: Any) {
        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Accept invitation first", parent: self)
            return
        }

        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //sheet.view.tintColor = .primary

        let newPractice = UIAlertAction(title: "New Practice", style: .default) { _ in
            self.addNewPractice(type: .practice)
        }
        newPractice.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(newPractice)

        let newGame = UIAlertAction(title: "New Game", style: .default) { _ in
            self.addNewGame()
        }
        newGame.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(newGame)

        let other = UIAlertAction(title: "New Event", style: .default) { _ in
            self.addNewPractice(type: .other)
        }
        other.setValue(UIColor.COLOR_PRIMARY_DARK, forKey: "titleTextColor")
        sheet.addAction(other)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .destructive) { _ in

        }
        cancelActionButton.setValue(UIColor.COLOR_ACCENT, forKey: "titleTextColor")
        sheet.addAction(cancelActionButton)

        self.present(sheet, animated: true, completion: nil)
    }

    func addNewPractice(type: PracticeScreenType) {
        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPracticeVC") as! AddNewPracticeVC
        nextNav.isUpdate = false
        nextNav.viewModel.screenType = type

        self.present(nextNav, animated: true, completion: nil)
    }

    func addNewGame() {
        let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGameVC") as! AddNewGameVC

        self.present(nextNav, animated: true, completion: nil)
    }

    @IBAction func onClickTeamToolbar(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            self.present(showVC, animated: true, completion: nil)
        }
        
        self.historyPage = 1
    }

    @IBAction func upDidTap(_ sender: UIButton) {
        var title = ""
        var message = ""
        showIndicator()

        DispatchQueue.main.async {
            for calendar in self.viewModel.dp {
                if let pr = calendar.practice {
                    SystemCalendarService.shared.addReminder(title: pr.NAME, note: pr.MEMO, eventTime: pr.eventDate, duration: TimeInterval(pr.duration)) { (error) in
                        if let error = error {
                            title = "Oops!"
                            message = error.localizedDescription

                        } else {
                            title = "Success"
                            message = "Finished sync your practices with calendar."
                        }
                    }
                }
            }
        }

        delay(1.0) {
            self.hideIndicator()

            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let act = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(act)
            self.present(alert, animated: true, completion: nil)
        }
    }

    func delay(_ duration: TimeInterval, completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: completion)
    }

    // MARK: - Util
    func gotoCarPoolChatViewController() {
        DispatchQueue.main.async {
            if let roster = AppDataInstance.instance.currentSelectedTeam {
                let teamId = "team_" + roster.TEAM_ID
                let carpoolChatViewController = CarpoolChatViewController(teamId: teamId)
                self.navigationController?.pushViewController(carpoolChatViewController, animated: true)
            }
        }
    }
    
    func validatePendingState() -> Bool {
        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Please accept the Invitation sent to your Email.", parent: self)
            return false
        }
        
        return true
    }
}

extension CalendarVC: GetResponsibleViewControllerDelegate {
    fileprivate func handleErrForGameCell(_ err: String?) {
        if let err = err {
            self.alert(message: err)
        } else {
            guard let model = self.viewModel.dp[self.selectedIndex!].game else {return}
            model.attendance_detail!.ATTENDTING_DECISION! = self.selectStatus!

            let indexPath = IndexPath(row: self.selectedIndex!, section: 0)

            if let cell = self.tableView.cellForRow(at: indexPath) as? GameCell {
                cell.btAttendMaybe.isChoose = false
                cell.btAttendYes.isChoose = false
                cell.btAttendNo.isChoose = false

                //                                                                        if !model.attendance_detail!.ID!.isEmpty {
                if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
                    cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
                    cell.btAttendYes.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
                    cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
                    cell.btAttendNo.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
                    cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
                    cell.btAttendMaybe.isChoose = true
                }
            }
            self.tableView.reloadRows(at: [indexPath], with: .none)
            //                                                                    }

        }
    }

    fileprivate func handleErrorForPracticeCell(_ err: String?) {
        if let err = err {
            self.alert(message: err)
        } else {
            guard let model = self.viewModel.dp[self.selectedIndex!].practice else {return}
            model.attendance_detail!.ATTENDTING_DECISION! = self.selectStatus!

            let indexPath = IndexPath(row: self.selectedIndex!, section: 0)

            if let cell = self.tableView.cellForRow(at: indexPath) as? PracticeCell {
                cell.btAttendMaybe.isChoose = false
                cell.btAttendYes.isChoose = false
                cell.btAttendNo.isChoose = false

                //                                                                        if !model.attendance_detail!.ID!.isEmpty {
                if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_YES {
                    cell.btAttendYes.setAttending(attending: Attendance.ATTENDING_YES)
                    cell.btAttendYes.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_NO {
                    cell.btAttendNo.setAttending(attending: Attendance.ATTENDING_NO)
                    cell.btAttendNo.isChoose = true
                } else if model.attendance_detail!.ATTENDTING_DECISION! == Attendance.ATTENDING_MAYBE {
                    cell.btAttendMaybe.setAttending(attending: Attendance.ATTENDING_MAYBE)
                    cell.btAttendMaybe.isChoose = true
                }
                //                                                                        }
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }

        }
    }

    func updateAttendance(rosters: [String]) {
        guard let isGameCell = self.isGameCell else {return}

        if isGameCell {
            if isHealCheck {
                self.viewModel.doUpdateHealCheckForMultipleRoster(rosters: rosters,
                                                                  status: self.selectStatus ?? "",
                                                                  calendarItemId: self.selectGameModel!.ID ?? "",
                                                                  calendarItemType: CALENDAR_ITEM_TYPE.GAME_MODEL.itemKindString()) { [weak self] (err) in
                                                                    if let err = err {
                                                                        self?.alert(message: err)
                                                                    }
                }
            } else {
                self.viewModel.doUpdateAttendanceForMultipleRoster(rosters: rosters,
                                                                   status: self.selectStatus ?? "",
                                                                   calendarItemId: self.selectGameModel!.ID ?? "",
                                                                   calendarItemType: CALENDAR_ITEM_TYPE.GAME_MODEL.itemKindString()) { (err) in
                                                                    self.handleErrForGameCell(err)
                }
            }

        } else {
            if isHealCheck {
                self.viewModel.doUpdateHealCheckForMultipleRoster(rosters: rosters,
                                                                  status: self.selectStatus ?? "",
                                                                  calendarItemId: self.selectPracticeModel!.ID ?? "",
                                                                  calendarItemType: CALENDAR_ITEM_TYPE.PRACTICE_MODEL.itemKindString()) { [weak self] (err) in
                                                                    if let err = err {
                                                                        self?.alert(message: err)
                                                                    }
                }
            } else {
                self.viewModel.doUpdateAttendanceForMultipleRoster(rosters: rosters,
                                                                   status: self.selectStatus ?? "",
                                                                   calendarItemId: self.selectPracticeModel!.ID ?? "",
                                                                   calendarItemType: CALENDAR_ITEM_TYPE.PRACTICE_MODEL.itemKindString()) { (err) in
                                                                    self.handleErrorForPracticeCell(err)

                }
            }

        }

    }

    func submitPressed(rosters: [String]) {
        guard rosters.count > 0 else {
            return
        }
        
        if self.isHealCheck {
            let vc = HealthCheckViewController()
            vc.modalPresentationStyle = .overFullScreen

            vc.submitSuccessClousure = { [weak self] status in
                self?.selectStatus = "\(status)"
                self?.updateAttendance(rosters: rosters)
            }

            self.present(vc, animated: true, completion: nil)
        } else {
            self.updateAttendance(rosters: rosters)
        }
    }
}

extension CalendarVC: PracticeUpdateDialogViewControllerDelegate {
    func refreshCalendarUI() {
        //do load practices, events here
        self.viewModel.doLoadCalendarItem()
    }
    
    func doUpdatePractice(practice: PracticeModel, isApplyAll: Bool) {
        dismiss(animated: true) {
            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPracticeVC") as! AddNewPracticeVC
            nextNav.isUpdate = true
            nextNav.origin = practice
            nextNav.isApplyAll = isApplyAll
            
            self.present(nextNav, animated: true, completion: nil)
        }
    }
}
