//
//  RosterVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import ActionSheetPicker_3_0
import Firebase
import FirebaseMessaging

import RxSwift
import RxCocoa
import GoogleMobileAds

class RosterVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AdsProtocol {
    @IBOutlet weak var btCurTeam: UIButton!
    @IBOutlet weak var btToolbarAdd: UIButton!
    @IBOutlet weak var btRoster: UIButton!
    @IBOutlet weak var tb: UITableView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    @IBOutlet weak var adView: GADBannerView!
    
    var viewModel: RosterListViewModel = RosterListViewModel()
    private var disposal = Disposal()
    var isFirstLoad = true
    
    var createService = CreateService()
    
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        configAdView(adView: adView, from: self)

        let fcmToken = Messaging.messaging().fcmToken
        if fcmToken != nil {
            AppDataInstance.instance.setFirebaseToken(val: fcmToken!)
            print("Fcm Token:" + fcmToken!)
        }
        
        if AppDataInstance.instance.me?.FIRST_LOG_IN == "0" && (AppDataInstance.instance.currentSelectedTeam?.ROLE_INFO.ID == UserRoles.TEAM_COACH || AppDataInstance.instance.currentSelectedTeam?.ROLE_INFO.ID == UserRoles.TEAM_MANAGER) {

            AlertUtils.showWarningAlert(title: "Welcome to BeClutch!", message: "Build your Roster.", parent: self)

            if AppDataInstance.instance.me?.FIRST_LOG_IN == "0" {
                AppDataInstance.instance.me?.FIRST_LOG_IN = "1"
            }
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.curSelectRolePos.observe(DispatchQueue.main) {
            newPos, _ in

            if self.viewModel.roles.count == 0 {
                self.btRoster.setTitle("Choose Roster", for: .normal)
                return
            }

            if self.viewModel.roles.count >= newPos {
                self.btRoster.setTitle(self.viewModel.roles[newPos].ROLE_NAME, for: .normal)
                
                if !self.isFirstLoad {
                    self.viewModel.doLoadMyRoster()
                }
                
                self.isFirstLoad = false
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadMyRosterRes", handler: {
            res in
            let resAPI = res?.object as! LoadMyRosterRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.viewModel.dp = self.filterRoster(rosters: resAPI.rosters)
                self.tb.reloadData()
            }
        })

        SwiftEventBus.onMainThread(self, name: "IndividualConversationCheckRes", handler: {
            res in
            let resAPI = res?.object as! NewConversationAddRes

            if resAPI.result! {
                self.gotoConversationViewController(converse: resAPI.conversation)
            }
        })

        SwiftEventBus.onMainThread(self, name: "ResendInviteRosterEmailRes", handler: { [weak self]
            res in
            self?.viewModel.isBusy.value = false
            let resAPI = res?.object as! GeneralRes

            if resAPI.result! {
                self?.alert(title: "", message: "Resent invitation email successfully.", okTitle: "OK")
            }
        })

        SwiftEventBus.onMainThread(self, name: "LoginRes", handler: {
                  res in
                  // go to main home screen here
          let loginRes = res?.object as! LoginRes
          if loginRes.result == true {
              if loginRes.default_member == nil || (loginRes.default_member?.ID.isEmpty)! {
                  loginRes.default_member = loginRes.clubs?.first
              }

              AppDataInstance.instance.setToken(token: loginRes.msg!)
              AppDataInstance.instance.setEmail(val: loginRes.user!.EMAIL)
              AppDataInstance.instance.me = loginRes.user
              AppDataInstance.instance.setMyClubTeamList(myClubTeamList: loginRes.clubs)
              AppDataInstance.instance.currentSelectedTeam = loginRes.default_member
              AppDataInstance.instance.roles = loginRes.roles

            CustomLabelManager.shared.updateLabelDict(dict: loginRes.labels)

                  self.viewModel.buildRole()
                for i in 0...(self.viewModel.roles.count - 1) {
                       if self.viewModel.roles[i].ID == UserRoles.EVERYONE {
                           self.viewModel.curSelectRolePos.value = i
                           break
                       }
                   }
          }
      })

        self.viewModel.doSplashAction()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshTheme()
        
        self.viewModel.dp = []
        self.viewModel.roles = []

       self.viewModel.buildRole()
       for i in 0...(self.viewModel.roles.count - 1) {
           if self.viewModel.roles[i].ID == UserRoles.EVERYONE {
               self.viewModel.curSelectRolePos.value = i
               break
           }
       }

        self.btCurTeam.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.btCurTeam.setTitle(AppDataInstance.instance.currentSelectedTeam?.CLUB_TEAM_INFO.NAME, for: .normal)
    }
    
    // MARK: - UI
    func refreshTheme() {
        //changing work
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        btCurTeam.makeStrokeOnlyButton()

        btToolbarAdd.layer.cornerRadius = btToolbarAdd.width/2.0
        btToolbarAdd.backgroundColor = UIColor.COLOR_ACCENT
        btToolbarAdd.isHidden = !AccountCommon.isCoachOrManager()

        btRoster.makeGrayRadius()
    }

    @IBAction func onClickRosterKind(_ sender: Any) {
        var arr = [String]()
        for role in self.viewModel.roles {
            arr.append(role.ROLE_NAME)
        }

        ActionSheetStringPicker.show(withTitle: "", rows: arr, initialSelection: self.viewModel.curSelectRolePos.value, doneBlock: { (_, index, _) in
            self.viewModel.curSelectRolePos.value = index
        }, cancel: nil, origin: self.view)
    }

    @IBAction func onClickToolbarAdd(_ sender: Any) {

        if (Int(AppDataInstance.instance.currentSelectedTeam!.ROLE_INFO.ID)!) > Int(UserRoles.TEAM_MANAGER)! && (Int(AppDataInstance.instance.currentSelectedTeam!.ROLE_INFO.ID)!) < Int(UserRoles.TEAM_TREASURE)! {
            AlertUtils.showWarningAlert(title: "Info", message: "You don't have right to add roster", parent: self)
            return
        }

        if AppDataInstance.instance.currentSelectedTeam?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "You need to accept invitation", parent: self)
        } else {
            //create invite roster screen
            if let rosterInviteVC = ViewControllerFactory.makeRosterInviteVC() {
                self.present(rosterInviteVC, animated: true, completion: nil)
            }
        }
    }

    @IBAction func onClickCurrentTeam(_ sender: Any) {
        if let showVC = ViewControllerFactory.makeSelectTeamVC() {
            self.present(showVC, animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dp.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RosterCell") as! RosterCell

        if viewModel.dp.count == 0 {return UITableViewCell()}

        if viewModel.dp.count > 0, self.viewModel.dp[indexPath.row].ROSTER_PROFILE_PIC.isEmpty {
            cell.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            cell.img.kf.setImage(with: URL(string: self.viewModel.dp[indexPath.row].ROSTER_PROFILE_PIC))
        }

        cell.txtName.text = self.viewModel.dp[indexPath.row].ROSTER_FIRST_NAME + " " + self.viewModel.dp[indexPath.row].ROSTER_LAST_NAME
        cell.txtPo.text = self.viewModel.dp[indexPath.row].ROLE_INFO.ROLE_NAME
        cell.txtTeamName.text = self.viewModel.dp[indexPath.row].CLUB_TEAM_INFO.NAME
        cell.txtStatus.backgroundColor = UIColor.COLOR_BLUE
        cell.txtStatus.layer.cornerRadius = 5
        cell.txtStatus.clipsToBounds = true
        
        cell.resendButton.backgroundColor = UIColor.COLOR_PRIMARY
        cell.resendButton.layer.cornerRadius = 5
        cell.resendButton.isHidden = true
        
        cell.btChat.tintColor = UIColor.COLOR_PRIMARY

        if self.viewModel.dp[indexPath.row].IS_ACTIVE == "0" {
            cell.txtStatus.isHidden = false
            cell.btChat.isHidden = true

            if AccountCommon.isCoachManagerOrTreasure() {
                cell.resendButton.isHidden = false
            }
        } else {
            cell.txtStatus.isHidden = true
            cell.btChat.isHidden = self.viewModel.dp[indexPath.row].ID == AppDataInstance.instance.currentSelectedTeam!.ID
        }

        cell.actionChat = {
            //show chat with indexpath.row
            let secondRosterId = self.viewModel.dp[indexPath.row].ID
            self.viewModel.doStartIndividualConversation(rosterId: secondRosterId)
        }
        
        cell.resendButton.rx.tap.subscribe(onNext: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            let roster = strongSelf.viewModel.dp[indexPath.row]
            APIManager.shared.getInvitationCode(rosterId: roster.ID)
                .successValue()
                .filter { $0.result == true }
                .map { $0.extra }
                .subscribe(onNext: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    let message = "Invite code: \($0)"
                    strongSelf.alert(title: "Resend Invite", message: message, okTitle: "Send", cancelTitle: "Back") { _ in
                        strongSelf.viewModel.doResendInviteEmail(index: indexPath.row)
                    } cancelHandler: { _ in }
                })
                .disposed(by: strongSelf.disposeBag)
        })
        .disposed(by: disposeBag)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < self.viewModel.dp.count else {
            return
        }

        let roster = self.viewModel.dp[indexPath.row]
        let isCoachManagerTreasure = AccountCommon.isCoachManagerOrTreasure(roster: roster)
        let isPlayer = AccountCommon.isPlayer(roster: roster)
        
        if isPlayer || isCoachManagerTreasure {
            let playerStoryboard = UIStoryboard(name: "PlayerInfo", bundle: nil)
            let nextNav = playerStoryboard.instantiateViewController(withIdentifier: "PlayerInfoVC") as! PlayerInfoVC
            nextNav.viewModel.origin = self.viewModel.dp[indexPath.row]
            nextNav.delegate = self
            nextNav.modalPresentationStyle = .overFullScreen
            self.present(nextNav, animated: true, completion: nil)
        } else {
            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "RosterInfoVC") as! RosterInfoVC
            nextNav.viewModel.origin = self.viewModel.dp[indexPath.row]
            nextNav.delegate = self
            nextNav.modalPresentationStyle = .overFullScreen

            self.present(nextNav, animated: true, completion: nil)
        }
    }

    func filterRoster(rosters: [RosterInfo]?) -> [RosterInfo] {
        guard let rosters = rosters else {
            return []
        }

        var result: [RosterInfo] = []

        for roster in rosters {
            if let rosterId = Int(roster.ROLE_INFO.ID), let treasureId = Int(UserRoles.TEAM_TREASURE) {
                if rosterId <= treasureId {
                    result.append(roster)
                }
            }
        }

        return result
    }
}

// MARK: - Chat
extension RosterVC {
    func gotoConversationViewController(converse: ConversationEntryDTO?) {
        guard let converse = converse else {
            return
        }

        let converseViewController = ConversationChatViewController(converse: converse)

        self.navigationController?.pushViewController(converseViewController, animated: true)
    }
}

extension RosterVC: PlayerInfoVCDelegate, RosterInfoVCDelegate {
    func doReloadRosterInfo() {
        self.navigationController?.dismiss(animated: true, completion: nil)
        viewModel.doLoadMyRoster()
    }
}
