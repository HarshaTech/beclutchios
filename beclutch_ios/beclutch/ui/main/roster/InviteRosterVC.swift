//
//  InviteRosterVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox
import Observable
import SwiftEventBus
import ActionSheetPicker_3_0
class InviteRosterVC: UIViewController {

    @IBOutlet weak var chkInvite: BEMCheckBox!
    @IBOutlet weak var btRoster: UIButton!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var appBarView: UIView!
    @IBOutlet weak var roleView: UIView!
    @IBOutlet weak var rosterView: UIView!
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var addToRosterBtn: UIButton!

    @IBOutlet weak var isPlayerCheckView: UIView!
    @IBOutlet weak var playerCheckBox: BEMCheckBox!
    @IBOutlet weak var parentCheckBox: BEMCheckBox!
    
    private var disposal = Disposal()
    var viewModel: InviteRosterViewModel = InviteRosterViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appBarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        roleView.applyShadow()
        rosterView.applyShadow()
        personalView.applyShadow()
        buttonView.applyShadow()
        addToRosterBtn.makeGreenButton()

        self.chkInvite.boxType = .square
        self.chkInvite.setOn(true, animated: true)
        
        self.playerCheckBox.boxType = .circle
        self.playerCheckBox.setOn(false, animated: true)
        
        self.parentCheckBox.boxType = .circle
        self.parentCheckBox.setOn(false, animated: true)

        self.btRoster.makeGrayRadius()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        self.viewModel.curSelRole.observe(DispatchQueue.main) {
            newPos, oldPos in

            if self.viewModel.roles.count == 0 {
                self.btRoster.setTitle("Choose Role", for: .normal)
                return
            }

            if newPos == oldPos {return}
            if self.viewModel.roles.count >= newPos {
                self.btRoster.setTitle(self.viewModel.roles[newPos].ROLE_NAME, for: .normal)
            }
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "CreateInviteRosterToThisTeamRes", handler: {
            res in
            let resAPI = res?.object as! CreateInviteRosterToThisTeamRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                if resAPI.wantedEmail! {
                    AlertUtils.showWarningAlertWithHandler(title: "Info", message: resAPI.msg!, parent: self, handler: {
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    AlertUtils.showWarningAlertWithHandler(title: "Info", message: resAPI.msg!, parent: self, handler: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.msg!, parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "CreateInviteTeamToClubRes", handler: {
            res in
            let resAPI = res?.object as! CreateInviteTeamToClubRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                if resAPI.sendmail! {
                    AlertUtils.showWarningAlertWithHandler(title: "Info", message: resAPI.msg!, parent: self, handler: {
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    AlertUtils.showWarningAlertWithHandler(title: "Info", message: resAPI.msg!, parent: self, handler: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.msg!, parent: self)
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.doBuildRole()
        if self.viewModel.roles.count == 0 {return}

        for i in 0...(self.viewModel.roles.count - 1) {
            if self.viewModel.roles[i].ID == UserRoles.PLAYER {
                self.viewModel.curSelRole.value = i
                break
            }
        }
    }

    func checkValidation() -> Bool {
        if self.txtFName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required", parent: self)
            return false
        }

        if self.txtLName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required", parent: self)
            return false
        }

        if self.txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email is required", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: self.txtEmail.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invalid email entered", parent: self)
            return false
        }
        
        if isPlayerCheckView.isHidden == false && parentCheckBox.on == false && playerCheckBox.on == false {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Parent or Player.", parent: self)
            return false
        }

        if self.viewModel.roles.count == 0 {
            AlertUtils.showWarningAlert(title: "Warning", message: "You have no authority to add roster.", parent: self)
            return false
        }

        if self.viewModel.curSelRole.value == -1 {
            AlertUtils.showWarningAlert(title: "Warning", message: "Choose Role", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickSave(_ sender: Any) {
        if checkValidation() {
            if self.viewModel.roles[self.viewModel.curSelRole.value].LEVEL == UserRoles.TEAM_COACH && AppDataInstance.instance.currentSelectedTeam?.ROLE_INFO.LEVEL != UserRoles.TEAM_COACH {
                self.viewModel.doInviteTeamToThisClub(email: self.txtEmail.text!, fname: self.txtFName.text!, lname: self.txtLName.text!, sendMail: self.chkInvite.on)
            } else {
                self.viewModel.doInviteRosterToThisTeam(email: self.txtEmail.text!, fname: self.txtFName.text!, lname: self.txtLName.text!, sendMail: self.chkInvite.on, isPlayer: self.parentCheckBox.on)
            }
        }
    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickBtRoster(_ sender: Any) {
        var arr = [String]()
        for role in self.viewModel.roles {
            arr.append(role.ROLE_NAME)
        }
        if self.viewModel.curSelRole.value == -1 {
            self.viewModel.curSelRole.value = 0 //select first item by default
        }

        ActionSheetStringPicker.show(withTitle: "Choose Role", rows: arr, initialSelection: self.viewModel.curSelRole.value, doneBlock: { (_, index, _) in
            self.viewModel.curSelRole.value = index
            
            self.isPlayerCheckView.isHidden = self.viewModel.roles[index].ID != UserRoles.PLAYER
        }, cancel: nil, origin: self.view)
    }
    
    @IBAction func parentPlayerChanged(_ checkBox: BEMCheckBox) {
        let otherCheckBox = checkBox === parentCheckBox ? playerCheckBox : parentCheckBox
        
        otherCheckBox?.on = !checkBox.on
    }
}
