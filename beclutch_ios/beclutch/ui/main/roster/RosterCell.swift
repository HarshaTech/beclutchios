//
//  RosterCell.swift
//  beclutch
//
//  Created by zeus on 2019/12/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class RosterCell: UITableViewCell {

    @IBOutlet weak var btChat: UIButton!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtPo: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var resendButton: BLButton!
    @IBOutlet weak var txtTeamName: UILabel!

    var actionChat: (() -> Void)?
    var resendInvitationClosure: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        resendButton.setUpCorner(radius: 0.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onClickChat(_ sender: Any) {
        self.actionChat!()
    }

    @IBAction func resendInvitationPressed(_ sender: Any) {
        resendInvitationClosure?()
    }
}
