//
//  RosterInfoContentVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit

class RosterInfoContentVC: UITableViewController {

    @IBOutlet weak var txtHome: UILabel!
    @IBOutlet weak var txtCell: UILabel!
    @IBOutlet weak var txtWork: UILabel!

    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtRole: UILabel!
    @IBOutlet weak var txtTeam: UILabel!

    var roster: RosterInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateView()
    }

    func updateView() {
        self.txtTeam.text = (roster?.CLUB_TEAM_INFO.NAME)!
        self.txtRole.text = (roster?.ROLE_INFO.ROLE_NAME)!

        if self.roster!.ROSTER_PROFILE_PIC.isEmpty {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            self.img.kf.setImage(with: URL(string: self.roster!.ROSTER_PROFILE_PIC))
        }

        self.txtName.text = roster!.ROSTER_FIRST_NAME + " " + roster!.ROSTER_LAST_NAME
        self.txtHome.text  = roster!.ROSTER_HOME_PHONE
        self.txtEmail.text = roster!.ROSTER_EMAIL
        self.txtCell.text = roster!.ROSTER_CELL_PHONE
        self.txtWork.text = roster!.ROSTER_WORK_PHONE

    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

}
