//
//  RosterInfoEditVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import PopupDialog
import SwiftEventBus
import InputMask
class RosterInfoEditVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var txt_work: UITextField!
    @IBOutlet weak var txt_home: UITextField!
    @IBOutlet weak var txt_cell: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_lname: UITextField!
    @IBOutlet weak var txt_fname: UITextField!
    @IBOutlet weak var img: UIImageView!

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var removePhotoBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var appbarView: UIView!

    private var disposal = Disposal()
    var origin: RosterInfo?
    var viewModel: RosterInfoEditViewModel = RosterInfoEditViewModel()
    var imagePicker = UIImagePickerController()

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = textField.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        textField.text = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        txt_cell.addTarget(self, action: #selector(RosterInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)
        txt_home.addTarget(self, action: #selector(RosterInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)
        txt_work.addTarget(self, action: #selector(RosterInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        editPhotoBtn.makeGreenButton()
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()
        removePhotoBtn.backgroundColor = UIColor.COLOR_BLUE

        self.imagePicker.delegate = self
        self.txt_work.text = self.origin?.ROSTER_WORK_PHONE
        self.txt_cell.text = self.origin?.ROSTER_CELL_PHONE
        self.txt_home.text = self.origin?.ROSTER_HOME_PHONE
        self.txt_email.text = self.origin?.ROSTER_EMAIL
        self.txt_fname.text = self.origin?.ROSTER_FIRST_NAME
        self.txt_lname.text = self.origin?.ROSTER_LAST_NAME
        self.viewModel.originPhotoURL = self.origin?.ROSTER_PROFILE_PIC

        if (self.origin?.ROSTER_PROFILE_PIC.isEmpty)! {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            self.img.kf.setImage(with: URL(string: self.origin!.ROSTER_PROFILE_PIC))
        }

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "UpdateRosterProfileRes", handler: {
            res in
            let resAPI = res?.object as! UpdateRosterProfileRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.message!, parent: self)
            }
        })
    }

    @IBAction func onClickRemoveUploadPhoto(_ sender: Any) {
        self.viewModel.newUploadPhoto = nil
        if (self.origin?.ROSTER_PROFILE_PIC.isEmpty)! {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            self.img.kf.setImage(with: URL(string: self.origin!.ROSTER_PROFILE_PIC))
        }
    }

    @IBAction func onEditPhoto(_ sender: Any) {
        let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
        let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
            self.openCamera()
        }

        let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
            self.openGallery()
        }

        popup.addButtons([camera, gallery])
        self.present(popup, animated: true, completion: nil)
    }

    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {
            self.viewModel.newUploadPhoto = pickedImage.jpegData(compressionQuality: 0.6)
            self.img.image = pickedImage
        }

        picker.dismiss(animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if self.txt_fname.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required", parent: self)
            return false
        }

        if self.txt_lname.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required", parent: self)
            return false
        }

        if self.txt_email.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email is required", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: self.txt_email.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invalid Email is entered", parent: self)
            return false
        }

        return true
    }

    @IBAction func onClickToolbarSave(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.doUpdateRosterInfo(rosterId: self.origin!.ID, email: self.txt_email.text!, fname: self.txt_fname.text!, lname: self.txt_lname.text!, cell: self.txt_cell.text!, home: self.txt_home.text!, work: self.txt_work.text!)
        }
    }

    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.doUpdateRosterInfo(rosterId: self.origin!.ID, email: self.txt_email.text!, fname: self.txt_fname.text!, lname: self.txt_lname.text!, cell: self.txt_cell.text!, home: self.txt_home.text!, work: self.txt_work.text!)
        }
    }

    @IBAction func oonClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
