//
//  RosterInfoVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus

protocol RosterInfoVCDelegate: NSObject {
    func doReloadRosterInfo()
}

class RosterInfoVC: UIViewController {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    weak var delegate: RosterInfoVCDelegate?
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!

    var viewModel: RosterInfoViewModel = RosterInfoViewModel()
    var contentVC: RosterInfoContentVC?
    private var disposal = Disposal()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentVC?.roster = self.viewModel.origin
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "LoadRosterInfoRes", handler: {
            res in
            let resAPI = res?.object as! LoadRosterInfoRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.viewModel.origin = resAPI.roster
                self.contentVC?.roster = resAPI.roster
                self.contentVC?.updateView()

                self.refreshDeleteButton()
                self.refreshEditButton()
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.message!, parent: self)
            }
        })

        SwiftEventBus.onMainThread(self, name: "RemoveRosterFromTeamRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false

            let resAPI = res?.object as! GeneralRes
            if resAPI.result! {
                self.delegate?.doReloadRosterInfo()
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.doLoadRosterInfo()
    }

    private func isEditRight() -> Bool {
        let isSuperUser = AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam)
        let isMyProfile = AccountCommon.isMe(rosterId: viewModel.origin?.USER_INFO.ID) 

        return isSuperUser || isMyProfile
    }
    
    private func refreshDeleteButton() {
        deleteButton.isHidden = AccountCommon.isCoachOrManager(roster: self.contentVC?.roster) || !AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam)
    }
    
    private func refreshEditButton() {
        editButton.isHidden = !isEditRight()
    }

    @IBAction func deletePressed(_ sender: Any) {
        if let error = AccountCommon.checkRightError() {
            AlertUtils.showWarningAlert(title: "Warning", message: error, parent: self)
            return
        }

        alert(title: "", message: "Are you sure you want to delete this member?", okHandler: { [weak self] _ in
            self?.viewModel.doRemoveRosterFromTeam()
        })
    }

    @IBAction func onClickToolbarBack(_ sender: Any) {
        delegate?.doReloadRosterInfo()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickEdit(_ sender: Any) {
        let editor = self.storyboard?.instantiateViewController(withIdentifier: "RosterInfoEditVC") as! RosterInfoEditVC
        editor.modalPresentationStyle = .fullScreen
        editor.origin = self.viewModel.origin
        self.present(editor, animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc: AnyObject = segue.destination
        if vc.isKind(of: RosterInfoContentVC.self) {
            self.contentVC = (vc as! RosterInfoContentVC)
            self.contentVC?.roster = self.viewModel.origin
        }
    }
}
