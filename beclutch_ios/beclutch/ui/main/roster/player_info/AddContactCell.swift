//
//  AddContactCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/5.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class AddContactCell: UITableViewCell {
    var actionAdd: (() -> Void)?
    @IBOutlet weak var addContactBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickAddContact(_ sender: Any) {
        self.actionAdd!()
    }
}
