//
//  AddContactVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import PopupDialog
import ActionSheetPicker_3_0
import InputMask

protocol AddRosterDelegate: NSObject {
    func didAddRoster(email: String?)
}

class AddContactVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var txt_work: UITextField!
    @IBOutlet weak var txt_home: UITextField!
    @IBOutlet weak var txt_cell: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_lname: UITextField!
    @IBOutlet weak var txt_fname: UITextField!
    @IBOutlet weak var btContactType: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var removePhotoBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var txt_title: UILabel!

    weak var delegate: AddRosterDelegate?

    var isTheSameEmail = false
    var origin: RosterInfo?
    var viewModel: AddContactViewModel = AddContactViewModel()
    private var disposal = Disposal()
    var imagePicker = UIImagePickerController()

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = textField.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        textField.text = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        txt_cell.addTarget(self, action: #selector(AddContactVC.textFieldDidChange(_:)), for: .editingChanged)
        txt_home.addTarget(self, action: #selector(AddContactVC.textFieldDidChange(_:)), for: .editingChanged)
        txt_work.addTarget(self, action: #selector(AddContactVC.textFieldDidChange(_:)), for: .editingChanged)
        configUI()
        self.imagePicker.delegate = self

        if self.viewModel.isUpdate {
            self.viewModel.curKind.value = self.origin!.LEVEL
            self.txt_fname.text = self.origin?.ROSTER_FIRST_NAME
            self.txt_lname.text = self.origin?.ROSTER_LAST_NAME
            self.txt_email.text = self.origin?.ROSTER_EMAIL
            self.txt_cell.text = self.origin?.ROSTER_CELL_PHONE
            self.txt_home.text = self.origin?.ROSTER_HOME_PHONE
            self.txt_work.text = self.origin?.ROSTER_WORK_PHONE
            self.viewModel.originPhotoURL = self.origin!.ROSTER_PROFILE_PIC
            if (self.origin?.ROSTER_PROFILE_PIC.isEmpty)! {
                self.img.image = UIImage(named: "img_photo_placeholder")
            } else {
                self.img.kf.setImage(with: URL(string: self.origin!.ROSTER_PROFILE_PIC))
            }
            self.txt_title.text = "Edit Contact"
        } else {
            self.img.image = UIImage(named: "img_photo_placeholder")
            btContactType.setTitle("Choose Contact Type", for: .normal)
            self.txt_title.text = "Add New Contact"
        }
        btContactType.makeGrayRadius()
        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
            }.add(to: &disposal)
        self.viewModel.curKind.observe(DispatchQueue.main) { [weak self]
            newKind, _ in
            guard let self = self else {
                return
            }
            
            if newKind.isEmpty {
                self.btContactType.setTitle("Choose Contact Type", for: .normal)
            } else {
                if newKind == UserRoles.PARENT {
                    self.btContactType.setTitle("Parent", for: .normal)
                } else {
                    self.btContactType.setTitle("Other", for: .normal)
                }
            }
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "AddPlayerContactRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! AddPlayerContactRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.delegate?.didAddRoster(email: self.txt_email.text)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.message!, parent: self)
            }
        })
        SwiftEventBus.onMainThread(self, name: "UpdatePlayerContactRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! UpdatePlayerContactRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.message!, parent: self)
            }
        })
    }
    
    deinit {
        print("AddContactVC deinit")
    }

    // MARK: - UI
    func configUI() {
        txt_lname.autocapitalizationType = .words
        txt_fname.autocapitalizationType = .words
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        editPhotoBtn.makeGreenButton()
        removePhotoBtn.backgroundColor = UIColor.COLOR_BLUE
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        txt_email.isUserInteractionEnabled = !self.isTheSameEmail
    }

    @IBAction func onClickRemovePhoto(_ sender: Any) {
        self.viewModel.newUploadPhoto = nil

        if let url = self.origin?.ROSTER_PROFILE_PIC {
            self.img.kf.setImage(with: URL(string: url))
        } else {
            self.img.image = UIImage(named: "img_photo_placeholder")
        }
    }

    @IBAction func onClickEditPhoto(_ sender: Any) {
        let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
        let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
            self.openCamera()
        }

        let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
            self.openGallery()
        }

        popup.addButtons([camera, gallery])
        self.present(popup, animated: true, completion: nil)
    }

    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {
            self.viewModel.newUploadPhoto = pickedImage.jpegData(compressionQuality: 0.6)
            self.img.image = pickedImage
        }

        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickContactType(_ sender: Any) {
        let arr = ["Parent", "Other"]

        ActionSheetStringPicker.show(withTitle: "Choose Contact Type", rows: arr, initialSelection: self.viewModel.curKind.value == UserRoles.OTHER ? 1 : 0, doneBlock: { (_, index, _) in
            self.viewModel.curKind.value = "\(index + 10)"
        }, cancel: nil, origin: self.view)
    }

    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {
            if self.viewModel.isUpdate {
                self.viewModel.doUpdateExistContact(email: self.txt_email.text!, fName: self.txt_fname.text!, lName: self.txt_lname.text!, phoneCell: self.txt_cell.text!, phoneHome: self.txt_home.text!, phoneWork: self.txt_work.text!, originRosterId: self.origin!.ID)
            } else {
                self.viewModel.doCreateNewContact(email: self.txt_email.text!, f_name: self.txt_fname.text!, l_name: self.txt_lname.text!, phoneCell: self.txt_cell.text!, phoneHome: self.txt_home.text!, phoneWork: self.txt_work.text!)
            }
        }
    }

    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if self.viewModel.curKind.value.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Contact Type is required.", parent: self)
            return false
        }

        if self.txt_fname.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required.", parent: self)
            return false
        }

        if self.txt_lname.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required.", parent: self)
            return false
        }

        if self.txt_email.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email is required.", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: self.txt_email.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invalid Email is entered.", parent: self)
            return false
        }
        return true
    }

}
