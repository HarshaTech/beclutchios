//
//  PlayerInfoEditVC.swift
//  beclutch
//
//  Created by zeus on 2020/1/6.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import PopupDialog
import InputMask
import BEMCheckBox

class PlayerInfoEditVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var txtWork: UITextField!
    @IBOutlet weak var txtHome: UITextField!
    @IBOutlet weak var txtCell: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPlayerNumber: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var img: UIImageView!

    @IBOutlet weak var txtContactFName: UITextField!
    @IBOutlet weak var txtContactLName: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var removePhotoBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var appbarView: UIView!
    
    @IBOutlet weak var representingView: UIView!
    @IBOutlet weak var chkIsPlayer: BEMCheckBox!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var representingViewConst: NSLayoutConstraint!

    private var disposal = Disposal()
    var origin: RosterInfo?
    var viewModel: PlayerEditViewModel = PlayerEditViewModel()
    var imagePicker = UIImagePickerController()
    
    var isRepresentingPlayer = false

    @objc func textFieldDidChange(_ textField: UITextField) {
        let mask: Mask = try! Mask(format: "[000]-[000]-[0000]")
        let input: String = textField.text!
        let result: Mask.Result = mask.apply(
            toText: CaretString(
                string: input,
                caretPosition: input.endIndex, caretGravity: CaretString.CaretGravity.forward(autocomplete: true)
            )
        )
        let output: String = result.formattedText.string
        print(output)
        textField.text = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupStyle()

        self.viewModel.isBusy.observe(DispatchQueue.main) {
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self.showIndicator()} else {self.hideIndicator()}
            }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "UpdatePlayerProfileRes", handler: {
            res in
            let resAPI = res?.object as! UpdatePlayerProfileRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertUtils.showWarningAlert(title: "Warning", message: resAPI.message!, parent: self)
            }
        })
    }
    
    private func setupStyle() {
        setupCustomLabel()

        txtCell.addTarget(self, action: #selector(PlayerInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)
        txtHome.addTarget(self, action: #selector(PlayerInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)
        txtWork.addTarget(self, action: #selector(PlayerInfoEditVC.textFieldDidChange(_:)), for: .editingChanged)

        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        removePhotoBtn.backgroundColor = UIColor.COLOR_BLUE
        editPhotoBtn.makeGreenButton()
        saveBtn.makeGreenButton()
        cancelBtn.makeRedButton()

        self.imagePicker.delegate = self
        
        self.txtFName.text = self.origin?.ROSTER_FIRST_NAME
        self.txtLName.text = self.origin?.ROSTER_LAST_NAME

        self.txtEmail.isUserInteractionEnabled = false

        if let count = self.origin?.PRIMARY_CONTACT.PRIMARY_EMAIL.count, count > 0 {
            self.txtContactFName.text = self.origin?.PRIMARY_CONTACT.PRIMARY_FIRST_NAME
            self.txtContactLName.text = self.origin?.PRIMARY_CONTACT.PRIMARY_LAST_NAME
            
            self.txtWork.text = self.origin?.PRIMARY_CONTACT.PRIMARY_WORK_PHONE
            self.txtCell.text = self.origin?.PRIMARY_CONTACT.PRIMARY_CELL_PHONE
            self.txtHome.text = self.origin?.PRIMARY_CONTACT.PRIMARY_HOME_PHONE
            self.txtEmail.text = self.origin?.PRIMARY_CONTACT.PRIMARY_EMAIL
        } else {
            self.txtContactFName.text = self.origin?.ROSTER_FIRST_NAME
            self.txtContactLName.text = self.origin?.ROSTER_LAST_NAME
            
            self.txtWork.text = self.origin?.ROSTER_WORK_PHONE
            self.txtCell.text = self.origin?.ROSTER_CELL_PHONE
            self.txtHome.text = self.origin?.ROSTER_HOME_PHONE
            self.txtEmail.text = self.origin?.ROSTER_EMAIL
        }
        
        
        self.viewModel.originPhotoURL = self.origin?.ROSTER_PROFILE_PIC

        if (self.origin?.ROSTER_PROFILE_PIC.isEmpty)! {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            self.img.kf.setImage(with: URL(string: self.origin!.ROSTER_PROFILE_PIC))
        }
        
//        self.txtLName.isEnabled = !isRepresentingPlayer
//        self.txtFName.isEnabled = !isRepresentingPlayer
        
        chkIsPlayer.boxType = .square
        helpBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
        
        let isCheck = origin?.IS_PLAYER == "1"
        chkIsPlayer.setOn(isCheck, animated: false)
        
        if !AccountCommon.isCoachOrManager(roster: self.origin) {
            representingView.isHidden = false
            representingViewConst.constant = 38
        } else {
            representingView.isHidden = true
            representingViewConst.constant = 0
        }
    }

    private func setupCustomLabel() {
        txtPlayerNumber.placeholder = CustomLabelManager.shared.getCutomLabel(label: CustomLableEnum.txt_player_number.rawValue)
    }
    
    @IBAction func onClickRemovePhoto(_ sender: Any) {
        self.viewModel.newUploadPhoto = nil
        if (self.origin?.ROSTER_PROFILE_PIC.isEmpty)! {
            self.img.image = UIImage(named: "img_photo_placeholder")
        } else {
            self.img.kf.setImage(with: URL(string: self.origin!.ROSTER_PROFILE_PIC))
        }

    }
    @IBAction func onClickSave(_ sender: Any) {
        if self.checkValidation() {
            self.viewModel.doUpdatePlayerInfo(rosterId: self.origin!.ID, playerNo: self.txtPlayerNumber.text!, email: self.txtEmail.text!, fname: self.txtFName.text!, lname: self.txtLName.text!, cell: self.txtCell.text!, home: self.txtHome.text!, work: self.txtWork.text!)
        }
    }
    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onEditPhoto(_ sender: Any) {
        let popup = PopupDialog(title: "Confirm", message: "Choose option", image: nil)
        let camera = DefaultButton(title: "Camera", dismissOnTap: true) {
            self.openCamera()
        }

        let gallery = DefaultButton(title: "Gallery", dismissOnTap: true) {
            self.openGallery()
        }

        popup.addButtons([camera, gallery])
        self.present(popup, animated: true, completion: nil)
    }

    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertUtils.showWarningAlert(title: "Warning", message: "You don't have camera", parent: self)
        }
    }

    func openGallery() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {
            self.viewModel.newUploadPhoto = pickedImage.jpegData(compressionQuality: 0.6)
            self.img.image = pickedImage
        }

        picker.dismiss(animated: true, completion: nil)
    }

    func checkValidation() -> Bool {
        if self.txtFName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "First name is required", parent: self)
            return false
        }

        if self.txtLName.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Last name is required", parent: self)
            return false
        }

        if self.txtEmail.text!.isEmpty {
            AlertUtils.showWarningAlert(title: "Warning", message: "Email is required", parent: self)
            return false
        }

        if !StringCheckUtils.checkValidEmailAddress(email: self.txtEmail.text!) {
            AlertUtils.showWarningAlert(title: "Warning", message: "Invalid Email is entered", parent: self)
            return false
        }
        return true
    }
}
