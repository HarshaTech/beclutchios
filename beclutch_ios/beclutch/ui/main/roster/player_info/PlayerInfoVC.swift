//
//  PlayerInfoVC.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import UIKit
import Observable
import SwiftEventBus
import Kingfisher

protocol PlayerInfoVCDelegate: NSObject {
    func doReloadRosterInfo()
}

class PlayerInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var tb: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var editButton: UIButton!
    
    enum CellType {
        case profile
        case contact
        case addContact
        case guest
        case addGuest
    }
    
    var isPlayer = false
    var playerEmailInfo: String?
    var tableData: [CellType] = [.profile]

    weak var delegate: PlayerInfoVCDelegate?

    var viewModel: PlayerInfoViewModel = PlayerInfoViewModel()
    private var disposal = Disposal()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.COLOR_PRIMARY_DARK
        appbarView.backgroundColor = UIColor.COLOR_PRIMARY_DARK

        self.viewModel.isBusy.observe(DispatchQueue.main) { [weak self]
            newBusy, oldBusy in
            if newBusy == oldBusy {return}

            if newBusy {self?.showIndicator()} else {self?.hideIndicator()}
        }.add(to: &disposal)

        SwiftEventBus.onMainThread(self, name: "PlayerProfileLoadRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }
            
            let resAPI = res?.object as! PlayerProfileLoadRes
            self.viewModel.isBusy.value = false
            if resAPI.result! {
                self.viewModel.dp = resAPI.player_profile
                
                self.buildDataAndReloadTable()
            }
        })

        SwiftEventBus.onMainThread(self, name: "RemoveRosterFromTeamRes", handler: { [weak self]
            res in
            guard let self = self else {
                return
            }

            self.viewModel.isBusy.value = false

            let resAPI = res?.object as! GeneralRes
            if resAPI.result! {
                self.delegate?.doReloadRosterInfo()
            }
        })

        SwiftEventBus.onMainThread(self, name: "RemoveContactGuestRes", handler: { [weak self] res in
            guard let self = self else {
                return
            }
            self.viewModel.isBusy.value = false
            print("Reload table view")

            let resAPI = res?.object as! RemoveContactGuestRes
            if resAPI.result! {
                if let rosterInfo = resAPI.player_profile {
                    self.viewModel.dp = rosterInfo
                    
                    self.buildDataAndReloadTable()
                }
            }
        })

        SwiftEventBus.onMainThread(self, name: "ResendInviteRosterEmailRes", handler: { [weak self]
            res in
            self?.viewModel.isBusy.value = false
            let resAPI = res?.object as! GeneralRes

            if resAPI.result! {
                self?.alert(title: "", message: "Resent invitation email successfully.", okTitle: "OK")
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.doLoadPlayerInfo()
    }
    
    deinit {
        print("PlayerInfoVC deinit")
    }
    
    private func buildDataAndReloadTable() {
        self.tableData = [.profile]
        if self.viewModel.dp != nil {
            if let contacts = self.viewModel.dp?.contacts {
                for _ in 0 ..< contacts.count {
                    self.tableData.append(.contact)
                }
            }
            
            if self.isEditRight() {
                self.tableData.append(.addContact)
            }
            
            if let guests = self.viewModel.dp?.guests {
                for _ in 0 ..< guests.count {
                    self.tableData.append(.guest)
                }
            }
            
            if self.isEditRight() {
                self.tableData.append(.addGuest)
            }
        }
        
        self.tb.reloadData()
        self.refreshDeleteButton()
        self.refreshEditButton()
    }

    private func refreshDeleteButton() {
        isPlayer = !AccountCommon.isCoachOrManager(roster: self.viewModel.dp?.player)
        deleteButton.isHidden = !AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam)
    }

    private func isEditRight() -> Bool {
        let isSuperUser = AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam)
        let isMyProfile = AppDataInstance.instance.me?.ID == viewModel.dp?.player?.USER_INFO.ID

        return isSuperUser || isMyProfile
    }
    private func refreshEditButton() {
        editButton.isHidden = !isEditRight()
    }

    @IBAction func onClickBackToolbar(_ sender: Any) {
        delegate?.doReloadRosterInfo()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func deletePressed(_ sender: Any) {
        if let error = AccountCommon.checkRightError() {
            AlertUtils.showWarningAlert(title: "Warning", message: error, parent: self)
            return
        }

        alert(title: "", message: "Are you sure you want to delete this member?", okTitle: "Yes", okHandler: { [weak self] _ in
            self?.viewModel.doRemoveRosterFromTeam()
        })
    }

    @IBAction func onClickEditToolbar(_ sender: Any) {
        if !AccountCommon.isCoachOrManager() && self.viewModel.origin?.IS_ACTIVE == "0" {
            AlertUtils.showWarningAlert(title: "Warning", message: "Accept invitation first", parent: self)
            return
        }

        let playerInfoStoryboard = UIStoryboard(name: "PlayerInfoEdit", bundle: nil)
        let editor = playerInfoStoryboard.instantiateViewController(withIdentifier: "PlayerInfoEditVC") as! PlayerInfoEditVC
        editor.modalPresentationStyle = .fullScreen
        if self.viewModel.dp == nil {
            editor.origin = self.viewModel.origin
        } else {
            editor.origin = self.viewModel.dp?.player
        }
        
        if let player = self.viewModel.dp?.player, let isPlayer = player.IS_PLAYER, isPlayer == "1" {
            editor.isRepresentingPlayer = true
        } else {
            editor.isRepresentingPlayer = false
        }
        
        self.present(editor, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = tableData[indexPath.row]
        
        switch item {
        case .addContact, .addGuest:
            return 52
        case .contact, .guest:
            return 240
        case .profile:
            if isPlayer {
                return 480
            } else {
                return 430
            }
        }
    }

    private func profileCell(indexPath: IndexPath) -> PlayerInfoCell {
        let playerInfoCell = tb.dequeueReusableCell(withIdentifier: "PlayerInfoCell") as! PlayerInfoCell
        
        if let player = self.viewModel.dp?.player {
            if !player.ROSTER_PROFILE_PIC.isEmpty {
                playerInfoCell.img.kf.setImage(with: URL(string: player.ROSTER_PROFILE_PIC))
            } else {
                playerInfoCell.img.image = UIImage(named: "img_photo_placeholder")
            }
            playerInfoCell.roleView.applyShadow()
            playerInfoCell.userView.applyShadow()
            playerInfoCell.primaryContactView.applyShadow()

            playerInfoCell.chkIsPlayer.boxType = .square
            
            if let isPlayer = player.IS_PLAYER, isPlayer == "1" {
                playerInfoCell.chkIsPlayer.setOn(true, animated: false)
            } else {
                playerInfoCell.chkIsPlayer.setOn(false, animated: false)
            }
            
            playerInfoCell.helpBtn.tintColor = UIColor.COLOR_PRIMARY_DARK
            if isPlayer {
                playerInfoCell.representingView.isHidden = false
                playerInfoCell.representingViewConst.constant = 38
            } else {
                playerInfoCell.representingView.isHidden = true
                playerInfoCell.representingViewConst.constant = 0
            }

            playerInfoCell.txtTeamName.text = player.CLUB_TEAM_INFO.NAME
            playerInfoCell.txtRoleName.text = player.ROLE_INFO.ROLE_NAME
            playerInfoCell.txtUserName.text = player.ROSTER_FIRST_NAME + " " + player.ROSTER_LAST_NAME

            if player.PRIMARY_CONTACT.PRIMARY_FIRST_NAME == "" && player.PRIMARY_CONTACT.PRIMARY_LAST_NAME == "" {
                playerInfoCell.txtName.text = "Name: " + self.viewModel.origin!.ROSTER_FIRST_NAME + " " + self.viewModel.origin!.ROSTER_LAST_NAME
                playerInfoCell.txtName.text = "Name: " + self.viewModel.origin!.ROSTER_FIRST_NAME + " " + self.viewModel.origin!.ROSTER_LAST_NAME
            } else {
                   playerInfoCell.txtName.text = "Name: " +  player.PRIMARY_CONTACT.PRIMARY_FIRST_NAME + " " + player.PRIMARY_CONTACT.PRIMARY_LAST_NAME
            }

            if player.PRIMARY_CONTACT.PRIMARY_EMAIL != "" {
                playerInfoCell.txtEmail.text = "Email: " +  player.PRIMARY_CONTACT.PRIMARY_EMAIL
            } else {
                playerInfoCell.txtEmail.text = "Email: " + self.viewModel.origin!.ROSTER_EMAIL
            }

            if player.PRIMARY_CONTACT.PRIMARY_CELL_PHONE != "" {
                playerInfoCell.txtCell.text = "Cell: " + player.PRIMARY_CONTACT.PRIMARY_CELL_PHONE
            } else {
                playerInfoCell.txtCell.text = "Cell: " + self.viewModel.origin!.ROSTER_CELL_PHONE
            }

            if player.PRIMARY_CONTACT.PRIMARY_WORK_PHONE != "" {
                playerInfoCell.txtWork.text = "Work: " + player.PRIMARY_CONTACT.PRIMARY_WORK_PHONE
            } else {
                playerInfoCell.txtWork.text = "Work: " + self.viewModel.origin!.ROSTER_WORK_PHONE
            }

            if player.PRIMARY_CONTACT.PRIMARY_HOME_PHONE != "" {
                playerInfoCell.txtHome.text = "Home: " + player.PRIMARY_CONTACT.PRIMARY_HOME_PHONE
            } else {
                playerInfoCell.txtHome.text = "Home: " + self.viewModel.origin!.ROSTER_HOME_PHONE
            }

            playerInfoCell.txtNo.text = player.ADDITION_INFO_ROSTER.isEmpty ? "#" : player.ADDITION_INFO_ROSTER

            self.playerEmailInfo = playerInfoCell.txtEmail.text
        }
        
        return playerInfoCell
    }
    
    private func contactCell(indexPath: IndexPath) -> ContactCell {
        let contactCell = tb.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        let conDp = self.viewModel.dp?.contacts![realContactIndex(index: indexPath.row
        )]
        
        contactCell.config(roster: conDp, primaryRosterId: viewModel.origin?.USER_ID)
        
        contactCell.txtItemType.text = "Contact Information:"
        contactCell.txtKind.text = conDp!.ROLE_INFO.ROLE_NAME
        contactCell.txtName.text = conDp!.ROSTER_FIRST_NAME + " " + conDp!.ROSTER_LAST_NAME
        contactCell.txtEmail.text = conDp!.ROSTER_EMAIL
        contactCell.txtCell.text = conDp!.ROSTER_CELL_PHONE
        contactCell.txtHome.text = conDp!.ROSTER_HOME_PHONE
        contactCell.txtWork.text = conDp!.ROSTER_WORK_PHONE
        contactCell.btEdit.tintColor = UIColor.COLOR_PRIMARY_DARK
        contactCell.btRemove.tintColor = UIColor.COLOR_PRIMARY_DARK

        if AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam) {
            contactCell.btEdit.isHidden = false
            contactCell.btRemove.isHidden = false
        } else {
            contactCell.btEdit.isHidden = true
            contactCell.btRemove.isHidden = true

            if AppDataInstance.instance.me?.ID == viewModel.dp?.player?.USER_INFO.ID {
                contactCell.btEdit.isHidden = false
                contactCell.btRemove.isHidden = false
            }
        }
        
        contactCell.actionEdit = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Accept invitation first", parent: self)
                return
            }
            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            nextNav.viewModel.originPlayerId = self.viewModel.dp?.player?.ID
            nextNav.viewModel.isUpdate = true
            if self.isPlayer, (self.playerEmailInfo ?? "") == ("Email: \(contactCell.txtEmail.text ?? "")") {
                nextNav.isTheSameEmail = true
            }

            nextNav.origin = conDp
            nextNav.modalPresentationStyle = .fullScreen
            self.present(nextNav, animated: true, completion: nil)
        }

        contactCell.actionRemove = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Player didn't accept invitation yet", parent: self)
                return
            }
            AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: "Do you want to remove this Contact?", confirmTitle: "Yes", parent: self, handler: {
                self.viewModel.doRemoveContactGuest(rosterId: conDp!.ID)
            })
        }
        
        contactCell.resendInviteAction = { [weak self] roster in
            self?.viewModel.doResendInviteEmail(roster: roster)
        }

        return contactCell
    }
    
    private func addContactCell(indexPath: IndexPath) -> AddContactCell {
        let addContactCell = tb.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
        addContactCell.addContactBtn.makeGreenButton()
        addContactCell.actionAdd = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Accept invitation first", parent: self)
                return
            }

            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            nextNav.delegate = self
            nextNav.viewModel.originPlayerId = self.viewModel.dp?.player?.ID
            nextNav.viewModel.isUpdate = false
            nextNav.origin = nil
            nextNav.modalPresentationStyle = .fullScreen
            self.present(nextNav, animated: true, completion: nil)
        }

        return addContactCell
    }
    
    private func realGuestIndex(index: Int) -> Int {
        var startIndex = 0
        
        for i in 0 ..< tableData.count {
            if tableData[i] == .guest {
                startIndex = i
                break
            }
        }
        
        return index - startIndex
    }
    
    private func realContactIndex(index: Int) -> Int {
        var startIndex = 0
        
        for i in 0 ..< tableData.count {
            if tableData[i] == .contact {
                startIndex = i
                break
            }
        }
        
        return index - startIndex
    }
    
    private func guestCell(indexPath: IndexPath) -> ContactCell {
        let guestCell = tb.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        let idx = realGuestIndex(index: indexPath.row)
        let conDp = self.viewModel.dp?.guests![idx]
        
        guestCell.config(roster: conDp, primaryRosterId: viewModel.origin?.USER_ID)
        
        guestCell.txtItemType.text = "Guest Information:"
        guestCell.txtKind.text = "" //conDp!.ROLE_INFO.ROLE_NAME
        guestCell.txtName.text = conDp!.ROSTER_FIRST_NAME + " " + conDp!.ROSTER_LAST_NAME
        guestCell.txtEmail.text = conDp!.ROSTER_EMAIL
        guestCell.txtCell.text = conDp!.ROSTER_CELL_PHONE
        guestCell.txtHome.text = conDp!.ROSTER_HOME_PHONE
        guestCell.txtWork.text = conDp!.ROSTER_WORK_PHONE
        guestCell.btEdit.tintColor = UIColor.COLOR_PRIMARY_DARK
        guestCell.btRemove.tintColor = UIColor.COLOR_PRIMARY_DARK

        if AccountCommon.isCoachOrManager(roster: AppDataInstance.instance.currentSelectedTeam) {
            guestCell.btEdit.isHidden = false
            guestCell.btRemove.isHidden = false
        } else {
            guestCell.btEdit.isHidden = true
            guestCell.btRemove.isHidden = true
            if AppDataInstance.instance.me?.ID == viewModel.dp?.player?.USER_INFO.ID {
                guestCell.btEdit.isHidden = false
                guestCell.btRemove.isHidden = false
            }
        }

        guestCell.actionEdit = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Player didn't accept the invitation yet", parent: self)
                return
            }
            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestVC") as! AddGuestVC
            nextNav.viewModel.isUpdate = true
            nextNav.origin = conDp
            nextNav.modalPresentationStyle = .fullScreen
            self.present(nextNav, animated: true, completion: nil)
        }

        guestCell.actionRemove = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Player didn't accept invitation yet", parent: self)
                return
            }

            AlertUtils.showConfirmAlertWithHandler(title: "Confirm", message: "Do you want to remove this Guest?", confirmTitle: "Yes", parent: self, handler: {
                self.viewModel.doRemoveContactGuest(rosterId: conDp!.ID)
            })
        }

        return guestCell
    }
    
    private func addGuestCell(indexPath: IndexPath) -> AddGuestCell {
        let addGuestCell = tb.dequeueReusableCell(withIdentifier: "AddGuestCell") as! AddGuestCell
        addGuestCell.addGuestBtn.makeGreenButton()
        addGuestCell.actionAdd = { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.viewModel.dp?.player?.IS_ACTIVE == "0" {
                AlertUtils.showWarningAlert(title: "Warning", message: "Player didn't accept invitation yet", parent: self)
                return
            }
            let nextNav = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestVC") as! AddGuestVC
            nextNav.delegate = self
            nextNav.viewModel.isUpdate = false
            nextNav.viewModel.originPlayerId = self.viewModel.dp?.player?.ID
            nextNav.modalPresentationStyle = .fullScreen
            self.present(nextNav, animated: true, completion: nil)
        }

        return addGuestCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = tableData[indexPath.row]
        
        switch item {
        case .profile:
            return profileCell(indexPath: indexPath)
        case .contact:
            return contactCell(indexPath: indexPath)
        case .addContact:
            return addContactCell(indexPath: indexPath)
        case .guest:
            return guestCell(indexPath: indexPath)
        case .addGuest:
            return addGuestCell(indexPath: indexPath)
        }
    }
}

extension PlayerInfoVC: AddRosterDelegate {
    func didAddRoster(email: String?) {
        if let emailTxt = email {
            self.dismiss(animated: true) {
                self.alert(message: "Invitation has been successfully sent to \(emailTxt)")
            }
        }
    }
}
