//
//  PlayerInfoCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/5.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit
import BEMCheckBox

class PlayerInfoCell: UITableViewCell {

    @IBOutlet weak var txtWork: UILabel!
    @IBOutlet weak var txtHome: UILabel!
    @IBOutlet weak var txtCell: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtNo: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtUserName: UILabel!
    @IBOutlet weak var txtTeamName: CopyableLabel!
    @IBOutlet weak var txtRoleName: UILabel!

    @IBOutlet weak var roleView: UIView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var primaryContactView: UIView!

    @IBOutlet weak var representingView: UIView!
    @IBOutlet weak var chkIsPlayer: BEMCheckBox!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var representingViewConst: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension PlayerInfoCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
