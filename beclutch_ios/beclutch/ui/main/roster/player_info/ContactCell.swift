//
//  ContactCell.swift
//  beclutch
//
//  Created by zeus on 2020/1/5.
//  Copyright © 2020 zeus. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    @IBOutlet weak var txtItemType: UILabel!
    @IBOutlet weak var txtKind: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var txtCell: UILabel!
    @IBOutlet weak var txtHome: UILabel!
    @IBOutlet weak var txtWork: UILabel!
    @IBOutlet weak var btEdit: UIButton!
    @IBOutlet weak var btRemove: UIButton!
    
    @IBOutlet weak var pendingButton: BLButton!
    @IBOutlet weak var resendInviteButton: BLButton!
    
    @IBOutlet weak var pendingStackView: UIStackView!
    
    var roster: RosterInfo?

    var actionEdit: (() -> Void)?
    var actionRemove: (() -> Void)?
    
    var resendInviteAction: ((_ roster: RosterInfo) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        pendingButton.makeBlueButton()
        resendInviteButton.makeGreenButton()
    }
    
    func config(roster: RosterInfo?, primaryRosterId: String?) {
        if let roster = roster, let rosterId = primaryRosterId {
            if AccountCommon.isCoachOrManager() || AccountCommon.isMe(rosterId: rosterId) {
                pendingStackView.isHidden = roster.IS_ACTIVE.boolValueFromServer()
            }
            
            self.roster = roster
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onClickEdit(_ sender: Any) {
        self.actionEdit!()
    }

    @IBAction func onClickRemove(_ sender: Any) {
        self.actionRemove!()
    }
    
    @IBAction func resendInviteClick(_ sender: Any) {
        if let roster = self.roster {
            self.resendInviteAction?(roster)
        }
    }
    
}
