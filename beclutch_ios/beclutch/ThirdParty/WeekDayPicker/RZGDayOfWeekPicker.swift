//
//  RZGDayOfWeekPicker.swift
//  DayOfTheWeekPicker
//
//  Created by Ryan Zegray on 2016-08-27.
//  Copyright © 2016 Ryan Zegray. All rights reserved.
//

import UIKit

enum RZGWeekDay: Int, CaseIterable {
    case monday = 0
    case tuesday
    case wednesday
    case thursday
    case friday
    case sarturday
    case sunday

    func title() -> String {
        switch self {
        case .monday:
            return "M"
        case .tuesday:
            return "T"
        case .wednesday:
            return "W"
        case .thursday:
            return "T"
        case .friday:
            return "F"
        case .sarturday:
            return "S"
        case .sunday:
            return "S"
        }
    }

    static func toTitle(array: [Int]) -> String {
        if array.count == RZGWeekDay.allCases.count {
            return "(All week days.)"
        }

        if array.count == 0 {
            return ""
        }

        let string = array.reduce("") { (result, element) -> String in
            let weekDay = RZGWeekDay(rawValue: element)
            if let title = weekDay?.title() {
                let start = result != "" ? result + ", " : result
                return start + title
            }

            return result
        }

        return "(" + string + ")"
    }
}

protocol RZGDayOfWeekPickerDelegate: class {
    func dayWasSelected(picker: RZGDayOfWeekPicker, selectedIndex: Int)
    func dayWasDeselected(picker: RZGDayOfWeekPicker, deselectedIndex: Int)
}

extension RZGDayOfWeekPickerDelegate {
    func dayWasSelected(picker: RZGDayOfWeekPicker, selectedIndex: Int) {

    }

    func dayWasDeselected(picker: RZGDayOfWeekPicker, deselectedIndex: Int) {

    }
}

class RZGDayOfWeekPicker: UIView {

    weak var delegate: RZGDayOfWeekPickerDelegate?

    // MARK: Attribute Inspector variables

    @IBInspectable
    var unselectedColor: UIColor = UIColor.black { didSet { customizeButtonAppearance() } }

    @IBInspectable
    var selectedColor: UIColor = UIColor.red { didSet { customizeButtonAppearance() } }

    @IBInspectable
    var textColor: UIColor = UIColor.white { didSet { customizeButtonAppearance() } }

    @IBInspectable
    var fontSize: CGFloat = 16 { didSet { customizeButtonAppearance() } }

    @IBInspectable
    var spacing: CGFloat = 5 { didSet { positionButtons() } }

    // MARK: Instance variables and structs

    private struct RZGDayOfWeekPickerDay {
        var name: String
        var selected: Bool
    }

    private var daysOfWeek = [
        RZGDayOfWeekPickerDay(name: "M", selected: true),
        RZGDayOfWeekPickerDay(name: "T", selected: true),
        RZGDayOfWeekPickerDay(name: "W", selected: true),
        RZGDayOfWeekPickerDay(name: "T", selected: true),
        RZGDayOfWeekPickerDay(name: "F", selected: true),
        RZGDayOfWeekPickerDay(name: "S", selected: true),
        RZGDayOfWeekPickerDay(name: "S", selected: true)
    ]

    private var daysOfWeekButtons = [UIButton]()

    // MARK: Inits

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createButtons()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        createButtons()
    }

    // MARK: - Public
    func setDaysOfWeek(daily: [Int]?) {
        guard let daily = daily else {
            return
        }

        for index in 0..<daysOfWeek.count {
            daysOfWeek[index].selected = daily.contains(index)
        }

        positionButtons()
        customizeButtonAppearance()
    }

    // MARK: Placing and Customizing the button's

    /// Creates and places the buttons for each day of the week
    private func createButtons() {
        for _ in daysOfWeek {
            let button = UIButton()
            button.addTarget(self, action: #selector(dayOfWeekButtonTapped), for: [.touchDown, .touchDragEnter, .touchDragExit])
            daysOfWeekButtons.append(button)
            addSubview(button)
        }
    }

    /// Positions each button within the frame
    private func positionButtons() {
        let areaUsedBySpacing = spacing * CGFloat(daysOfWeek.count + 1)
        let buttonSideLength = min((frame.width - areaUsedBySpacing) / CGFloat(daysOfWeek.count), frame.height)
        let buttonSize = CGSize(width: buttonSideLength, height: buttonSideLength)

        for index in 0..<daysOfWeekButtons.count {
            let button = daysOfWeekButtons[index]

            let buttonOrigin = CGPoint(x: spacing + (CGFloat(index) * (buttonSideLength + spacing)), y: 0)
            button.frame = CGRect(origin: buttonOrigin, size: buttonSize)
            button.layer.cornerRadius = 0.5 * button.bounds.size.width
            button.setTitle(daysOfWeek[index].name, for: .normal)
        }
    }

    /// Customizes the appearance of each button
    private func customizeButtonAppearance() {
        for index in 0..<daysOfWeekButtons.count {
            let button = daysOfWeekButtons[index]

            button.backgroundColor = daysOfWeek[index].selected ?  selectedColor :  unselectedColor
            button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
            button.setTitleColor(textColor, for: .normal)
        }
    }

    override func layoutSubviews() {
        positionButtons()
        customizeButtonAppearance()
    }

    // MARK: DayOfWeekPicker User Interaction

    @objc private func dayOfWeekButtonTapped(sender: UIButton) {
        if let index = daysOfWeekButtons.firstIndex(of: sender) {
            if daysOfWeek[index].selected == true {
                deselectDayWithIndex(index: index)
            } else {
                selectDayWithIndex(index: index)
            }
        }
    }

    // MARK: Methods for getting and setting the data in the DayOfTheWeekPicker

    /// Given a index of a day it returns if that day is selected.
    func dayIsSelectedAtIndex(index: Int) -> Bool {
        if index >= 0 && index < 7 {
            return daysOfWeek[index].selected
        }
        return false
    }

    /// Returns an array of ints for all selected days
    func indexOfSelectedDays() -> [Int] {
        return daysOfWeek.indices.filter {daysOfWeek[$0].selected == true}
    }

    /// Sets the day with the index given to selected
    func selectDayWithIndex(index: Int) {
        if index >= 0 && index < 7 {
            daysOfWeek[index].selected = true
            daysOfWeekButtons[index].backgroundColor = selectedColor
            delegate?.dayWasSelected(picker: self, selectedIndex: index)
        }
    }

    /// Sets the day with the index given to not-selected
    func deselectDayWithIndex(index: Int) {
        if index >= 0 && index < 7 {
            daysOfWeek[index].selected = false
            daysOfWeekButtons[index].backgroundColor = unselectedColor
            delegate?.dayWasDeselected(picker: self, deselectedIndex: index)
        }
    }

}
