//
//  FinanceService.swift
//  beclutch
//
//  Created by Trinh Vu on 2/28/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Alamofire
import SwiftEventBus

class FinanceService {
    static let instance = FinanceService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doAddNewFee(req: FeeAddReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_add_fee, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: FeeAddRes = FeeAddRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = FeeAddRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("FeeAddRes", sender: res)
                }
            }
        }
    }

    func doGetFinanceTeam(req: FinanceTeamReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_team_get, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = FinanceTeamRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = FinanceTeamRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("FinanceTeamRes", sender: res)
                }
            }
        }
    }

    func doGetFinanceRoster(req: FinanceRosterReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_roster_get, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = FinanceRosterRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = FinanceRosterRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("FinanceRosterRes", sender: res)
                }
            }
        }
    }

    func doRemoveFee(req: FinanceFeeRemovesReq, completed: @escaping (_ response: GeneralRes?, _ error: String?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_remove_finance, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    if let result = res.result, result {
                        completed(res, nil)
                    } else {
                        completed(nil, res.msg)
                    }

                    break
                case .failure(let error):
                    completed(nil, error.localizedDescription)
                    break
                }
            }
        }
    }

    func doRemoveFeeDetail(req: FinanceFeeDetailRemoveReq, completed: @escaping (_ response: GeneralRes?, _ error: String?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_remove_finance_detail, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    if let result = res.result, result {
                        completed(res, nil)
                    } else {
                        completed(nil, res.msg)
                    }

                    break
                case .failure(let error):
                    completed(nil, error.localizedDescription)
                    break
                }
            }
        }
    }

    func doApplyPaid(req: FinancePayAddReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.finance_add_pay, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = FinancePayAddRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = FinancePayAddRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("FinancePayAddRes", sender: res)
                }
            }
        }
    }
}

class AddCompetitionService {
    static let instance = AddCompetitionService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doAddCompetition(req: LeaderAddReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.addCompetition, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: AddCompetitionRes = AddCompetitionRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = AddCompetitionRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("AddCompetitionRes", sender: res)
                }
            }
        }
    }

    
    func doGetLeaderList(req: LeaderListReq) {
            req.getDict().printAsJson()
            DispatchQueue.global().async {
                self.sharedManager.request(ApiConstants.getLeaderboardList, method: .post, parameters: req.getDict()).responseJSON {
                    response in
                    var res = LeaderListRes(val: nil)
                    switch response.result {
                    case .success(let JSON_IN_RESULT):
                        let dict = JSON_IN_RESULT as! NSDictionary
                        res = LeaderListRes(val: dict)
                        break
                    case .failure:
                        break
                    }

                    DispatchQueue.main.async {
                        SwiftEventBus.post("LeaderListRes", sender: res)
                    }
                }
            }
        }
    
    func doRemoveCompetitionList(req: CompetitionRemoveReq, completed: @escaping (_ response: GeneralRes?, _ error: String?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.deleteLeaderBoard, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    if let result = res.result, result {
                        completed(res, nil)
                    } else {
                        completed(nil, res.msg)
                    }

                    break
                case .failure(let error):
                    completed(nil, error.localizedDescription)
                    break
                }
            }
        }
    }
    
    func doGetTeamList(req: CompetitionTeamReq) {
            req.getDict().printAsJson()
            DispatchQueue.global().async {
                self.sharedManager.request(ApiConstants.getLeaderBoardTeamList, method: .post, parameters: req.getDict()).responseJSON {
                    response in
                    var res = TeamListRes(val: nil)
                    switch response.result {
                    case .success(let JSON_IN_RESULT):
                        let dict = JSON_IN_RESULT as! NSDictionary
                        res = TeamListRes(val: dict)
                        break
                    case .failure:
                        break
                    }

                    DispatchQueue.main.async {
                        SwiftEventBus.post("TeamListRes", sender: res)
                    }
                }
            }
        }
    
    func doUpdateScore(req: UpdateScoreReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.updateScoreAPI, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateScoreRes = UpdateScoreRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateScoreRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateScoreRes", sender: res)
                }
            }
        }
    }

}
