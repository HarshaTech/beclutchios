//
//  SurveyService.swift
//  beclutch
//
//  Created by NamViet on 5/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Alamofire
import SwiftEventBus

class SurveyService {
    static let instance = SurveyService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doCreateNewSurvey(req: SurveyAddReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_add_new, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveyAddRes", sender: res)
                }
            }
        }
    }

    func doSubmitRosterResponse(req: SurveySubmitReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_submit_roster_response, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveySubmitRes", sender: res)
                }
            }
        }
    }
    
    func doSubmitMultipleRosterResponse(req: SurveyMultipleSubmitReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_submit_multiple_roster_response, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveyMultipleSubmitRes", sender: res)
                }
            }
        }
    }

    func doGetTeamSurvey(req: SurveyTeamGetReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_get_team_survey, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: SurveyTeamGetRes = SurveyTeamGetRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = SurveyTeamGetRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveyTeamGetRes", sender: res)
                }
            }
        }
    }

    func doGetSurveyDetailResponse(req: SurveyGetResponseDetailReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_get_detail_response, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: SurveyGetResponseDetailRes = SurveyGetResponseDetailRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = SurveyGetResponseDetailRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveyGetResponseDetailRes", sender: res)
                }
            }
        }
    }

    func doDeleteSurvey(req: SurveyDeleteReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.survey_delete, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SurveyDeleteRes", sender: res)
                }
            }
        }
    }
}
