//
//  SettingService.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire

final class SettingService {
    static let instance = SettingService()
    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()
    func doLoadOpponentList(req: OpponentLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_oppo_list, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: OpponentLoadRes = OpponentLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = OpponentLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("OpponentLoadRes", sender: res)
                }
            }
        }
    }

    func doAddOpponent(req: AddOpponentReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_oppo_add, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: AddOpponentRes = AddOpponentRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = AddOpponentRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("AddOpponentRes", sender: res)
                }
            }
        }
    }

    func doUpdateOpponent(req: UpdateOpponentReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_oppo_update, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateOpponentRes = UpdateOpponentRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateOpponentRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateOpponentRes", sender: res)
                }
            }
        }
    }

    func doLoadLocations(req: LocationLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_location_list, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LocationLoadRes = LocationLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LocationLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LocationLoadRes", sender: res)
                }
            }
        }
    }

    func doAddLocation(req: LocationAddReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_location_add, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LocationAddRes = LocationAddRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LocationAddRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LocationAddRes", sender: res)
                }
            }
        }
    }

    func doUpdateLocation(req: LocationUpdateReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_location_update, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LocationUpdateRes = LocationUpdateRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LocationUpdateRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LocationUpdateRes", sender: res)
                }
            }
        }
    }

    func doLoadNotificationSetting(req: NotificationLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_notification_read, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: NotificationLoadRes = NotificationLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = NotificationLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("NotificationLoadRes", sender: res)
                }
            }
        }
    }

    func doUpdateNotificationSetting(req: NotificationUpdateReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_notification_update, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: NotificationUpdateRes = NotificationUpdateRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = NotificationUpdateRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("NotificationUpdateRes", sender: res)
                }
            }
        }
    }

    func doLoadTimezone(req: LoadTimeZoneReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_load_timezone, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadTimeZoneRes = LoadTimeZoneRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadTimeZoneRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadTimeZoneRes", sender: res)
                }
            }
        }
    }

    func doUpdateTeamInfo(req: TeamInfoUpdateReq) {

        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.fileData != nil {
                    if let data = req.fileData {
                        multipartFormData.append(data, withName: "img", fileName: "img.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.setting_update_team, method: .post, headers: headers) {
                response in
                var loginRes: TeamInfoUpdateRes = TeamInfoUpdateRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = TeamInfoUpdateRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("TeamInfoUpdateRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doUpdateTeamPlan(req: TeamPlanUpdateReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_update_plan, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: TeamPlanUpdateRes = TeamPlanUpdateRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = TeamPlanUpdateRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("TeamPlanUpdateRes", sender: res)
                }
            }
        }
    }

    func doCreateTeamSupport(req: TeamSupportReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_add_support, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: TeamSupportRes = TeamSupportRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = TeamSupportRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("TeamSupportRes", sender: res)
                }
            }
        }
    }

    func doUpdateClubInfo(req: ClubInfoUpdateReq) {

        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.fileData != nil {
                    if let data = req.fileData {
                        multipartFormData.append(data, withName: "img", fileName: "img.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.setting_update_club, method: .post, headers: headers) {
                response in
                var loginRes: TeamInfoUpdateRes = TeamInfoUpdateRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = TeamInfoUpdateRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("TeamInfoUpdateRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doLoadTeamsInClub(req: LoadTeamsInClubReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_load_teams_in_club, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadTeamsInClubRes = LoadTeamsInClubRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadTeamsInClubRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadTeamsInClubRes", sender: res)
                }
            }
        }
    }
}
