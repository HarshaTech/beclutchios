//
//  ChatStatusManager.swift
//  beclutch
//
//  Created by kakabala on 4/24/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Firebase
import FirebaseFirestore

class ChatStatusManager {
    private let db = Firestore.firestore()
    private var teamChatListener: ListenerRegistration?
    private var conversationListener: ListenerRegistration?
    
    let userDefaultKey = "ChatStatusManagerUserDefaultKey"

    static var shared: ChatStatusManager = ChatStatusManager()

    lazy var chatStatusDict: [String: Bool] = UserDefaults.standard.object(forKey: userDefaultKey) as? [String: Bool] ?? [:]

    func readStatus(team: String) -> Bool {
        return ChatStatusManager.shared.chatStatusDict[team] ?? true
    }

    func otherTeamsReadStatus() -> Bool {
        guard let currentTeamId = AppDataInstance.instance.currentSelectedTeam?.TEAM_ID else {
            return true
        }

        for key in chatStatusDict.keys {
            if key != currentTeamId {
                if let isRead = chatStatusDict[key], !isRead {
                    return false
                }
            }
        }

        return true
    }

    func updateReadStatus(status: Bool, team: String? = nil) {
        let teamId = team ?? AppDataInstance.instance.currentSelectedTeam?.TEAM_ID
        guard let teamStr = teamId else {
            return
        }

        ChatStatusManager.shared.chatStatusDict[teamStr] = status
        NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
    }

    func updateAllTeamReadStatus(status: Bool) {
        for key in chatStatusDict.keys {
            chatStatusDict[key] = status
        }

        NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
    }

    func saveDataToUserDefault() {
        UserDefaults.standard.set(ChatStatusManager.shared.chatStatusDict, forKey: userDefaultKey)
        UserDefaults.standard.synchronize()
    }
    
    func doListenForRedBlueDot() {
        teamChatListener?.remove()
        conversationListener?.remove()
        
        doListenNewAllTeamChat()
        doListenNewConversationChat()
    }
    
    private func doListenNewAllTeamChat() {
        CommonUtils.BLUE_DOT_LIST.removeAll()
        CommonUtils.IS_RED_DOT = false
        
        let fbPath = "team_chats";
        teamChatListener = db.collection(fbPath).addSnapshotListener { (task, error) in
            guard error == nil, let documents = task?.documents else {
                return
            }
            
            for document in documents {
                let tmpObj = ChatEntryReadDTO(dict: document.data())
                
                let allTeamIdList = AppDataInstance.instance.allTeamIdList
                let allRosterIdList = AppDataInstance.instance.allRosterIdList
                
                if tmpObj.teamId != nil,
                    let teamId = tmpObj.teamId?.intValue(),
                    let curTeamId = AppDataInstance.instance.currentSelectedTeam?.TEAM_ID.intValue(),
                    let rosterId = AppDataInstance.instance.currentSelectedTeam?.ID.intValue()
                {
                    if allTeamIdList.contains(teamId) && !CommonUtils.BLUE_DOT_LIST.contains(teamId) && teamId != curTeamId {
                        CommonUtils.BLUE_DOT_LIST.append(teamId)
                        
                        for i in 0 ..< tmpObj.readEntries.count {
                            if allRosterIdList.contains(tmpObj.readEntries[i]) && CommonUtils.BLUE_DOT_LIST.contains(teamId) {
                                CommonUtils.BLUE_DOT_LIST.removeAll { (team) -> Bool in
                                    teamId == team
                                }
                            }
                        }
                    }
                    
                    if teamId == curTeamId && !tmpObj.readEntries.contains(rosterId) {
                        CommonUtils.IS_RED_DOT = true
                    }
                }
            }
            
            NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
        }
    }
    
    private func doListenNewConversationChat() {
        let fbPath = "converse_chats";
        conversationListener = db.collection(fbPath).addSnapshotListener { (task, error) in
            let documents = task?.documents
            if error != nil || documents == nil {
                return
            }
            
            var tmpData: [ChatEntryReadDTO] = []
            for document in documents! {
                let tmpObj = ChatEntryReadDTO(dict: document.data())
                tmpData.append(tmpObj)
            }
            
//            tmpData.sort { (o1, o2) -> Bool in
//                if let date1 = o1.lastSendDate,
//                    let date2 = o2.lastSendDate {
//                    return date1.compare(date2) == .orderedAscending
//                }
//
//                return false
//            }
            
            for conver in tmpData {
                if let teamId = conver.teamId {
                    if let rosterId = AppDataInstance.instance.currentSelectedTeam?.rosterIdInt {
                        if teamId == AppDataInstance.instance.currentSelectedTeam?.TEAM_ID, conver.memberIds.contains(rosterId) && !conver.readEntries.contains(rosterId) {
                            CommonUtils.IS_RED_DOT = true
                            
                            NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
                        } else {
                            if let memberId = AppDataInstance.instance.getMemberIdInTeam(teamId: teamId) {
                                if conver.memberIds.contains(memberId) && !conver.readEntries.contains(memberId) {
                                    CommonUtils.BLUE_DOT_LIST.append(teamId.intValue())
                                    
                                    NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
                                }
                            }
                        }
                    }
                }
            }
            
//            if tmpData.count > 1,
//                let teamId = tmpData.last?.teamId {
//                if teamId == AppDataInstance.instance.currentSelectedTeam?.TEAM_ID {
//                    CommonUtils.IS_RED_DOT = true
//
//                    NotificationCenter.default.post(name: .ChatStatusUpdateNotification, object: nil)
//                }
//            }
        }
    }
}
