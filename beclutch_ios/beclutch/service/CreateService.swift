//
//  CreateService.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire

class CreateService {
    static let instance = CreateService()
    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doCreateMyClubInSetting(req: CreateClubInSettingReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_create_myclub, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreateClubInSettingRes = CreateClubInSettingRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateClubInSettingRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateClubInSettingRes", sender: res)
                }
            }
        }
    }

    func doCreateMyTeamInSetting(req: CreateTeamInSettingReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_create_myteam, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreateMyTeamInSettingRes = CreateMyTeamInSettingRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateMyTeamInSettingRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateMyTeamInSettingRes", sender: res)
                }
            }
        }
    }

    func doCreateInviteTeamManager(req: CreateInviteTeamToClubReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_create_invite_manager, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreateInviteTeamToClubRes = CreateInviteTeamToClubRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateInviteTeamToClubRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateInviteTeamToClubRes", sender: res)
                }
            }
        }
    }

    func doLoadMyTeamToAddClub(req: LoadMyClubReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.setting_load_teams_add_club, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadMyClubRes = LoadMyClubRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadMyClubRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadMyClubRes", sender: res)
                }
            }
        }
    }

    func doInviteRosterToTeam(req: CreateInviteRosterToThisTeamReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_invite_roster_to_team, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreateInviteRosterToThisTeamRes = CreateInviteRosterToThisTeamRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateInviteRosterToThisTeamRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateInviteRosterToThisTeamRes", sender: res)
                }
            }
        }
    }

    func doResendEmailInviteRosterToTeam(req: ResendInviteRosterEmailReq) {
        print(req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_resend_email_invitation, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ResendInviteRosterEmailRes", sender: res)
                }
            }
        }
    }
}
