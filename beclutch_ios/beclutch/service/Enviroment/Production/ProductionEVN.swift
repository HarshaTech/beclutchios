//
//  Production.swift
//  beclutch
//
//  Created by Vu Trinh on 8/15/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension ApiConstants {
    static let BASE_URL = "https://mobile.beclutch.club/"
}

struct AdsConfig {
    static let bannerAppId = "ca-app-pub-1717491321787176/8279138016"
}
