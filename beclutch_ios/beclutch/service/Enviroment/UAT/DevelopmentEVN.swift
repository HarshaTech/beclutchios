//
//  Development.swift
//  beclutch DEV
//
//  Created by Vu Trinh on 8/15/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

extension ApiConstants {
//    static let BASE_URL = "https://beclutchdev.sierralingo.com/"
    static let BASE_URL = "https://dev.beclutch.club/"
}

struct AdsConfig {
    static let bannerAppId = "ca-app-pub-3940256099942544/2934735716"
}
