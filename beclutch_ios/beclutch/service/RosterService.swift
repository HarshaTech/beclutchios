//
//  RosterService.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus
class RosterService {
    static let instance = RosterService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doLoadRosterInformation(req: LoadRosterInfoReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_load_common_information, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadRosterInfoRes = LoadRosterInfoRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadRosterInfoRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadRosterInfoRes", sender: res)
                }
            }
        }
    }

    func doUpdateRosterProfile(req: UpdateRosterProfileReq) {

        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.uploadFile != nil {
                    if let data = req.uploadFile {
                        multipartFormData.append(data, withName: "profile_photo", fileName: "profile_photo.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.roster_update_common_profile, method: .post, headers: headers) {
                response in
                var loginRes: UpdateRosterProfileRes = UpdateRosterProfileRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = UpdateRosterProfileRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("UpdateRosterProfileRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doLoadPlayerInformation(req: PlayerProfileLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_load_player_information, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: PlayerProfileLoadRes = PlayerProfileLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = PlayerProfileLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("PlayerProfileLoadRes", sender: res)
                }
            }
        }
    }

    func doUpdatePlayerProfile(req: UpdatePlayerProfileReq) {

        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.uploadFile != nil {
                    if let data = req.uploadFile {
                        multipartFormData.append(data, withName: "profile_photo", fileName: "profile_photo.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.roster_update_player_profile, method: .post, headers: headers) {
                response in
                var loginRes: UpdatePlayerProfileRes = UpdatePlayerProfileRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = UpdatePlayerProfileRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("UpdatePlayerProfileRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doAddPlayerContact(req: AddPlayerContactReq) {

        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.uploadFile != nil {
                    if let data = req.uploadFile {
                        multipartFormData.append(data, withName: "profile_photo", fileName: "profile_photo.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.roster_add_player_contact, method: .post, headers: headers) {
                response in
                var loginRes: AddPlayerContactRes = AddPlayerContactRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = AddPlayerContactRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("AddPlayerContactRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doUpdatePlayerContact(req: UpdatePlayerContactReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {

            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.uploadFile != nil {
                    if let data = req.uploadFile {
                        multipartFormData.append(data, withName: "profile_photo", fileName: "profile_photo.jpg", mimeType: "image/jpg")
                    }
                }

            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.roster_update_player_contact, method: .post, headers: headers) {
                response in
                var loginRes: UpdatePlayerContactRes = UpdatePlayerContactRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                        case .success(let json_result_in_dic):
                            loginRes = UpdatePlayerContactRes(val: (json_result_in_dic as! NSDictionary))
                            break
                        case .failure:
                            break
                        }

                        DispatchQueue.main.async {
                            SwiftEventBus.post("UpdatePlayerContactRes", sender: loginRes)
                        }
                    })

                    break
                case .failure:
                    break
                }
            }
        }

    }

    func doRemoveContactGuest(req: RemoveContactGuestReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_remove_player_contact, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: RemoveContactGuestRes = RemoveContactGuestRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = RemoveContactGuestRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RemoveContactGuestRes", sender: res)
                }
            }
        }
    }

    func doRemoveRosterFromTeam(req: RemoveRosterFromTeamReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_remove_from_team, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RemoveRosterFromTeamRes", sender: res)
                }
            }
        }
    }
}
