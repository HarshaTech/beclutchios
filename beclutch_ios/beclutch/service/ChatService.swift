//
//  ChatService.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus
class ChatService {
    static let instance = ChatService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doSendNewReminder(req: SendReminderReq) {

        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_reminder_send, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ChatServiceBaseRes = ChatServiceBaseRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ChatServiceBaseRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("SendReminderRes", sender: res)
                }
            }
        }
    }

    func doLoadAllReminder(req: ReminderLoadReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_reminder_load, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ReminderLoadRes = ReminderLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ReminderLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ReminderLoadRes", sender: res)
                }
            }
        }
    }

    func doDeleteReminder(req: ReminderDeleteReq, completed: @escaping (_ response: GeneralRes?, _ error: Error?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_reminder_remove, method: .post, parameters: req.getDict()).responseJSON { response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    completed(res, nil)
                    break
                case .failure(let error):
                    completed(nil, error)
                    break
                }
            }
        }
    }

    func doUpdateReminderRead(req: UpdateReadStatusReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.update_reminder_read, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateReadStatusRes", sender: res)
                }
            }
        }
    }

    func doSendNotificationToTeam(req: SendNotificationToTeamReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.send_notification_to_team, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("DoSendNotificationToTeamRes", sender: res)
                }
            }
        }
    }

    func doSendEmailToTeam(req: SendNotificationToTeamReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.send_email_to_team, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("DoSendEmailToTeamRes", sender: res)
                }
            }
        }
    }

    func doSendTeamImageMedia(req: UploadImageMediaChatReq, completed: @escaping (_ response: UploadImageMediaChatRes?, _ error: Error?) -> Void) {
        DispatchQueue.global().async {
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-Type": "multipart/form-data"
            ]

            self.sharedManager.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in req.getDict() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                if req.uploadFile != nil {
                    if let data = req.uploadFile {
                        multipartFormData.append(data, withName: "img", fileName: "file.jpg", mimeType: "image/jpg")
                    }
                }
            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: ApiConstants.chat_team_image_media_send, method: .post, headers: headers) {
                response in
                var loginRes: UploadImageMediaChatRes = UploadImageMediaChatRes(val: nil)
                switch response {
                case .success(let UPLOAD_IN_RESULT, _, _):
                    UPLOAD_IN_RESULT.responseJSON(completionHandler: { (json_result) in
                        switch json_result.result {
                            case .success(let json_result_in_dic):
                                loginRes = UploadImageMediaChatRes(val: (json_result_in_dic as! NSDictionary))
                                completed(loginRes, nil)
                                break
                            case .failure(let error):
                                completed(nil, error)
                                break
                        }
                    })

                    break
                case .failure(let error):
                    completed(nil, error)
                    break
                }
            }
        }
    }

    func doLoadAllConversationEntries(req: ConversationEntryLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_conversation_entry_load, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ConversationEntryLoadRes = ConversationEntryLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ConversationEntryLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ConversationEntryLoadRes", sender: res)
                }
            }
        }
    }

    func doAddConversation(req: NewConversationAddReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_conversation_add, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: NewConversationAddRes = NewConversationAddRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = NewConversationAddRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("NewConversationAddReq", sender: res)
                }
            }
        }
    }

    func doDeleteConversation(req: ConversationThreadRemoveRes, completed: @escaping (_ response: ChatServiceBaseRes?, _ error: Error?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_conversation_remove, method: .post, parameters: req.getDict()).responseJSON { response in

                print("Delete conversation response: ", response)
                var res: ChatServiceBaseRes = ChatServiceBaseRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ChatServiceBaseRes(val: dict)
                    completed(res, nil)
                    break
                case .failure(let error):
                    completed(nil, error)
                    break
                }
            }
        }
    }

    func doCheckIndividualChat(req: IndividualConversationCheckReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_conversation_check_individual, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: NewConversationAddRes = NewConversationAddRes(val: nil)

                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = NewConversationAddRes(val: dict)
                    print("Individual checking result: " + (res.conversation?.id ?? ""))

                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("IndividualConversationCheckRes", sender: res)
                }
            }
        }
    }

    func doSendTeamText(req: SendTeamTextChatReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_team_text_send, method: .post, parameters: req.getDict()).responseJSON {
                _ in
            }
        }
    }

    func doSendCarpoolText(req: SendTeamTextChatReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_carpool_text_send, method: .post, parameters: req.getDict()).responseJSON {
                _ in
            }
        }
    }

    func doSendConversationText(req: SendTeamTextChatReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.chat_conversation_text_send, method: .post, parameters: req.getDict()).responseJSON {
                _ in
            }
        }
    }
}
