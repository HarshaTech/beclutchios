//
//  AuthService.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire

final class AuthService {
    static let instance = AuthService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doSplashAction(req: SplashRequest) {
        if req.token.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                SwiftEventBus.post("SplashCompleteEvent")
            }
        } else {
            DispatchQueue.global().async {
                self.sharedManager.request(ApiConstants.login_by_token, method: .post, parameters: req.getDict()).responseJSON {
                    response in
                    var loginRes: LoginRes = LoginRes(val: nil)
                    switch response.result {
                    case .success(let JSON_IN_RESULT):
                        let dict = JSON_IN_RESULT as! NSDictionary
                        loginRes = LoginRes(val: dict)
                        break
                    case .failure:
                        break
                    }
                    SwiftEventBus.post("LoginRes", sender: loginRes)
                }
            }
        }
    }

    func doLogin(req: LoginReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.login, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var loginRes: LoginRes = LoginRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    loginRes = LoginRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoginRes", sender: loginRes)
                }
            }
        }

    }

    func doRequestPasswordEmailVerify(req: PasswordResetEmailVerifyReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.forgot_reset_email, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ForgotPasswordResetEmailVerifyResult = ForgotPasswordResetEmailVerifyResult(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ForgotPasswordResetEmailVerifyResult(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ForgotPasswordResetEmailVerifyResult", sender: res)
                }
            }
        }
    }

    func doUpdateAccountInfomation(req: AccountInfoReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.update_user_info, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: AccountInfoRes = AccountInfoRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = AccountInfoRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateAccountInfoResult", sender: res)
                }
            }
        }
    }

    func doRequestPasswordResetVerifyCodeSubmit(req: PasswordResetVerificationCodeReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.forgot_reset_confirm, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ForgotPasswordVerifyCodeRes = ForgotPasswordVerifyCodeRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ForgotPasswordVerifyCodeRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ForgotPasswordVerifyCodeRes", sender: res)
                }
            }
        }
    }

    func doRequestNewPasswordSubmit(req: ForgotConfirmReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.forgot_change_password, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ForgotConfirmRes = ForgotConfirmRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ForgotConfirmRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ForgotConfirmRes", sender: res)
                }
            }
        }
    }

    func doRegisterEmailInfo(req: RegisterEmailRequest) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_email_url, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: RegisterEmailRes = RegisterEmailRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = RegisterEmailRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RegisterEmailRes", sender: res)
                }
            }
        }
    }

    func doRegisterResendEmail(req: RegisterEmailResendReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_send_email, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: RegisterEmailResendRes = RegisterEmailResendRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = RegisterEmailResendRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RegisterEmailResendRes", sender: res)
                }
            }
        }
    }

    func doRegisterSubmitCode(req: RegisterSubmitCodeReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_email_verify_submit, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: RegisterSubmitRes = RegisterSubmitRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = RegisterSubmitRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RegisterSubmitRes", sender: res)
                }
            }
        }
    }

    func doRegisterSubmitUserInfo(req: RegisterUserInfoReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_user_info, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: RegisterUserInfoRes = RegisterUserInfoRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = RegisterUserInfoRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("RegisterUserInfoRes", sender: res)
                }
            }
        }
    }

    func doLoadSportItem() {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.load_productions, method: .post, parameters: [:]).responseJSON {
                response in
                var res: LoadSportItemRes = LoadSportItemRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadSportItemRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadSportItemRes", sender: res)
                }
            }
        }
    }

    func doCreateClubTeam(val: CreateClubTeamReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_create_club, method: .post, parameters: val.getDict()).responseJSON {
                response in
                var res: CreateClubTeamRes = CreateClubTeamRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateClubTeamRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateClubTeamRes", sender: res)
                }
            }
        }
    }

    func doSubmitInvitationCode(val: InviteAcceptReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.register_invite_accept, method: .post, parameters: val.getDict()).responseJSON {
                response in
                var res: InviteAccpetRes = InviteAccpetRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = InviteAccpetRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("InviteAccpetRes", sender: res)
                }
            }
        }
    }
}
