//
//  HomeService.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus
class HomeService {
    static let instance = HomeService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doLoadClubTree(req: ClubTreeLoadReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_load_clubtree, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ClubTreeLoadRes = ClubTreeLoadRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ClubTreeLoadRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("ClubTreeLoadRes", sender: res)
                }
            }
        }
    }

    func doLoadHomeDashBoard(req: LoadHomeDashboardReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_load_dashboard, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadHomeDashboardRes = LoadHomeDashboardRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadHomeDashboardRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadHomeDashboardRes", sender: res)
                }
            }
        }
    }

    func doUpdateDefaultTeam(req: UpdateDefaultTeamReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_update_default_team, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateDefaultTeamRes = UpdateDefaultTeamRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateDefaultTeamRes(val: dict)
                    print("update default member")
                    break
                case .failure:
                    break
                }

                SwiftEventBus.post("UpdateDefaultTeamRes", sender: res)
            }
        }
    }

    func doSubmitInvitationCode(req: InviteAcceptReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_invite_accept, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: InviteAccpetRes = InviteAccpetRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = InviteAccpetRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("InviteAccpetRes", sender: res)
                }
            }
        }
    }

    func doLoadMyRosters(req: LoadMyRosterReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.home_load_my_roster, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadMyRosterRes = LoadMyRosterRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadMyRosterRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadMyRosterRes", sender: res)
                }
            }
        }
    }
}
