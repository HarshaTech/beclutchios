//
//  CalendarService.swift
//  beclutch
//
//  Created by zeus on 2020/1/8.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus

class CalendarService {
    static let instance = CalendarService()

    public lazy var sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = 350
        configuration.timeoutIntervalForResource = 350

        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()

    func doCreatePractice(req: CreatePracticeReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_create_practice, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreatePracticeRes = CreatePracticeRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreatePracticeRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreatePracticeRes", sender: res)
                }
            }
        }
    }

    func doCreateGame(req: CreateNewGameReq) {
        print("Create game request: ", req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_create_game, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CreateGameRes = CreateGameRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CreateGameRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateGameRes", sender: res)
                }
            }
        }
    }

    func doCreateHealthCheck(req: CreateHealthCheckReq) {
        print("Create health check request: ", req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_create_health_check, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CreateHealthCheckRes", sender: res)
                }
            }
        }
    }

    func doGetGameScore(req: GetGameScoreReq) {
        print("Get score request: ", req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_get_game_score, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: ScoreRes = ScoreRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = ScoreRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("GetGameScoreRes", sender: res)
                }
            }
        }
    }

    func doUpdateGameScore(req: UpdateGameScoreReq) {
        print("Update score request: ", req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_game_score, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateGameScoreRes", sender: res)
                }
            }
        }
    }

    func doGetCalendarContentsByTeamId(req: GetCalendarByTeamIdReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_get_team_id, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GetCalendarByTeamIdRes = GetCalendarByTeamIdRes(val: nil)
                let resToView: CalendarContentRes = CalendarContentRes()

                switch response.result {
                case .success(let JSON_IN_RESULT):
                    if let dict = JSON_IN_RESULT as? NSDictionary {
                        res = GetCalendarByTeamIdRes(val: dict)
                        
                        resToView.result = res.result ?? false
                        for fromBackDTO in res.items {
                            let headerDTO: CalendarListDTO = CalendarListDTO()
                            headerDTO.type = .HEADER_MODEL
                            headerDTO.dayOfMonth = "0"
                            headerDTO.month = fromBackDTO.monthName
                            headerDTO.hoursOfStart = "0"
                            headerDTO.id = UUID().uuidString

                            var tmpList: [CalendarListDTO] = []
                            for practice in fromBackDTO.practices {
                                guard let eventDate = practice.eventDate else {
                                    continue
                                }
                                
                                let dto: CalendarListDTO = CalendarListDTO()

                                if Date() > eventDate {
                                    continue
                                }

                                dto.type = .PRACTICE_MODEL
                                dto.practice = practice
                                dto.dayOfMonth = practice.DATE_DAY_OF_MONTH
                                dto.hoursOfStart = practice.START_TIME_HOUR
                                dto.ampmStart = practice.START_TIME_AMPM
                                dto.id = UUID().uuidString
                                dto.month = practice.DATE_MONTH
                                tmpList.append(dto)
                            }

                            for game in fromBackDTO.games {
                                guard let pp = game.eventDate else {
                                    continue
                                }
                                
                                let dto: CalendarListDTO = CalendarListDTO()
                                
                                if Date() > pp {
                                    continue
                                }

                                dto.type = .GAME_MODEL
                                dto.game = game
                                dto.hoursOfStart = game.START_TIME_HOUR
                                dto.ampmStart = game.START_TIME_AMPM
                                dto.id = UUID().uuidString
                                dto.dayOfMonth = game.DATE_DAY_OF_MONTH
                                dto.month = game.DATE_MONTH
                                tmpList.append(dto)
                            }

                            if tmpList.count == 0 {
                                continue
                            }

                            tmpList.sort(by: self.sortCalendarItem)
                            resToView.dtos.append(headerDTO)
                            resToView.dtos += tmpList
                        }

                        break
                    }
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CalendarContentRes", sender: resToView)
                }
            }
        }
    }

    func sortCalendarItem(first: CalendarListDTO, second: CalendarListDTO) -> Bool {
        if Int(first.dayOfMonth!)! > Int(second.dayOfMonth!)! {
            return false
        } else if Int(first.dayOfMonth!)! == Int(second.dayOfMonth!)! {
            if second.hoursOfStart == "12" && second.ampmStart == "PM" {
                return false
            }

            if first.ampmStart == "PM" && second.ampmStart == "AM" {
                return false
            }
            
            return Int(first.hoursOfStart!)! < Int(second.hoursOfStart!)!
        } else {
            return true
        }
    }

    func doLoadRosterForEvent(req: LoadRosterInCalendarReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_rosters_for_event, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: LoadRosterInCalendarRes = LoadRosterInCalendarRes(val: nil)
                let resToUI = LoadRosterInCalendarToUI()
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = LoadRosterInCalendarRes(val: dict)
                    resToUI.result = res.result
                    resToUI.rosters = []
                    var attendYes: [RosterForEvent] = []
                    var attendNo: [RosterForEvent] = []
                    var attendMaybe: [RosterForEvent] = []
                    var attendNothing: [RosterForEvent] = []
                    for rosterFromBack in res.rosters! {
                        if rosterFromBack.userLevelStr != "Player" {
                            continue
                        }

                        if rosterFromBack.attendance!.isEmpty {
                            attendNothing.append(rosterFromBack)
                        } else if rosterFromBack.attendance == Attendance.ATTENDING_YES {
                            attendYes.append(rosterFromBack)
                        } else if rosterFromBack.attendance == Attendance.ATTENDING_NO {
                            attendNo.append(rosterFromBack)
                        } else if rosterFromBack.attendance == Attendance.ATTENDING_MAYBE {
                            attendMaybe.append(rosterFromBack)
                        }
                    }

                    if attendYes.count > 0 {
                        let yesHeader  = CalendarRosterDTO()
                        yesHeader.title = "YES"
                        yesHeader.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL
                        yesHeader.roster = nil

                        resToUI.rosters?.append(yesHeader)

                        for item in attendYes {
                            let item_ui = CalendarRosterDTO()
                            item_ui.title = ""
                            item_ui.roster = item
                            item_ui.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.ROSTER_MODEL
                            resToUI.rosters?.append(item_ui)
                        }
                    }

                    if attendNo.count > 0 {
                        let yesHeader  = CalendarRosterDTO()
                        yesHeader.title = "NO"
                        yesHeader.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL
                        yesHeader.roster = nil

                        resToUI.rosters?.append(yesHeader)

                        for item in attendNo {
                            let item_ui = CalendarRosterDTO()
                            item_ui.title = ""
                            item_ui.roster = item
                            item_ui.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.ROSTER_MODEL
                            resToUI.rosters?.append(item_ui)
                        }
                    }

                    if attendMaybe.count > 0 {
                        let yesHeader  = CalendarRosterDTO()
                        yesHeader.title = "MAYBE"
                        yesHeader.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL
                        yesHeader.roster = nil

                        resToUI.rosters?.append(yesHeader)

                        for item in attendMaybe {
                            let item_ui = CalendarRosterDTO()
                            item_ui.title = ""
                            item_ui.roster = item
                            item_ui.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.ROSTER_MODEL
                            resToUI.rosters?.append(item_ui)
                        }
                    }

                    if attendNothing.count > 0 {
                        let yesHeader  = CalendarRosterDTO()
                        yesHeader.title = "UNDECIDED"
                        yesHeader.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.HEADER_MODEL
                        yesHeader.roster = nil

                        resToUI.rosters?.append(yesHeader)

                        for item in attendNothing {
                            let item_ui = CalendarRosterDTO()
                            item_ui.title = ""
                            item_ui.roster = item
                            item_ui.type = CalendarRosterDTO.CALENDAR_ROSTER_TYPE.ROSTER_MODEL
                            resToUI.rosters?.append(item_ui)
                        }
                    }

                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("LoadRosterInCalendarToUI", sender: resToUI)
                }
            }
        }
    }

    func doCancelGame(req: CancelGameReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_cancel_game, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CancelGameRes = CancelGameRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CancelGameRes(val: dict)
                    res.pos = req.pos!
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CancelGameRes", sender: res)
                }
            }
        }
    }

    func doDeletePracticeGame(req: DeleteCalendarItemReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_delete_game_practice, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: DeleteCalendarItemRes = DeleteCalendarItemRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = DeleteCalendarItemRes(val: dict)
                    if let pos = req.pos {
                        res.pos = pos
                    }
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("DeleteCalendarItemRes", sender: res)
                }
            }
        }
    }

    func doGetHistoryEvents(req: GetHistoryEventsReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_get_history_events, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GetHistoryEventsRes = GetHistoryEventsRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GetHistoryEventsRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("GetHistoryEventsRes", sender: res)
                }
            }
        }
    }

    func doCreateAttendanceForEvent(req: CreateAttendaceReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_create_attendance, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateAttendanceRes = UpdateAttendanceRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateAttendanceRes(val: dict)
                    res.itemID = req.calendarItemId
                    res.type =  req.calendarItemType
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateAttendanceRes", sender: res)
                }
            }
        }
    }

    func doUpdateAttendanceForEvent(req: UpdateAttendanceReq) {
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_attendance, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateAttendanceRes = UpdateAttendanceRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateAttendanceRes(val: dict)
                    res.itemID = req.calendarItemId
                    res.type =  req.calendarItemType
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateAttendanceRes", sender: res)
                }
            }
        }
    }

    func doUpdateAttendanceForMultipleRoster(req: UpdateAttendanceMultipleReq, completed: @escaping (_ response: GeneralRes?, _ error: String?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_attendance_multiple, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GeneralRes = GeneralRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GeneralRes(val: dict)
                    if let result = res.result, result {
                        completed(res, nil)
                    } else {
                        completed(nil, res.msg)
                    }

                    break
                case .failure(let error):
                    completed(nil, error.localizedDescription)
                    break
                }
            }
        }
    }

    func doUpdateHealthCheckMultipleRoster(req: UpdateAttendanceMultipleReq, completed: @escaping (_ response: UpdateHealthCheckMultipleRes?, _ error: String?) -> Void) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_healthcheck_multiple, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateHealthCheckMultipleRes = UpdateHealthCheckMultipleRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateHealthCheckMultipleRes(val: dict)
                    if let result = res.result, result {
                        completed(res, nil)
                    } else {
                        completed(nil, res.msg)
                    }

                    break
                case .failure(let error):
                    completed(nil, error.localizedDescription)
                    break
                }
            }
        }
    }

    func doCancelPractice(req: CancelPracticeReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_cancel_practice, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: CancelPracticeRes = CancelPracticeRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = CancelPracticeRes(val: dict)
                    res.pos = req.pos
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("CancelPracticeRes", sender: res)
                }
            }
        }
    }

    func doGetResponsible(req: GetResponsibleReq) {
        print(req.getDict())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.roster_get_responsible, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: GetResponsibleRes = GetResponsibleRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = GetResponsibleRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("GetResponsibleRes", sender: res)
                }
            }
        }
    }

    func doUpdatePractice(req: UpdatePracticeReq) {
        req.getDict().printAsJson()
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_practice, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdatePracticeRes = UpdatePracticeRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdatePracticeRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdatePracticeRes", sender: res)
                }
            }
        }
    }

    func doUpdateGame(req: UpdateGameReq) {
        print("Update game request: ", req.getDict().printAsJson())
        DispatchQueue.global().async {
            self.sharedManager.request(ApiConstants.calendar_update_game, method: .post, parameters: req.getDict()).responseJSON {
                response in
                var res: UpdateGameRes = UpdateGameRes(val: nil)
                switch response.result {
                case .success(let JSON_IN_RESULT):
                    let dict = JSON_IN_RESULT as! NSDictionary
                    res = UpdateGameRes(val: dict)
                    break
                case .failure:
                    break
                }

                DispatchQueue.main.async {
                    SwiftEventBus.post("UpdateGameRes", sender: res)
                }
            }
        }
    }
}
