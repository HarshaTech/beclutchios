//
//  CalendarRxService.swift
//  beclutch
//
//  Created by Vu Trinh on 11/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

import RxSwift

protocol CalendarRxService {
    func fetchCalendarIcalLink() -> Observable<Swift.Result<IcalResponse, Error>>
    func sendCalendarInstruction(calendarType: Int, rosterId: Int) -> Observable<Swift.Result<CommonResponse, Error>>
}

extension APIManager: CalendarRxService {
    func fetchCalendarIcalLink() -> Observable<Swift.Result<IcalResponse, Error>> {
        let service: CalendarEndpoint = CalendarEndpoint.getTeamIcal

        return sendRequest(endpoint: service)
    }
    
    func sendCalendarInstruction(calendarType: Int, rosterId: Int) -> Observable<Swift.Result<CommonResponse, Error>> {
        let service: CalendarEndpoint = CalendarEndpoint.sendEmail(calendarType: calendarType, rosterId: rosterId)

        return sendRequest(endpoint: service)
    }
}
