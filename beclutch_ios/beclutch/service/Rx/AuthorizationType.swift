//
//  AuthorizationType.swift
//  SportBooks
//
//  Created by Ron Magno on 12/9/19.
//  Copyright © 2019 Spiral. All rights reserved.
//

import Foundation

enum AuthorizationType {
    case bearer
    case none
}
