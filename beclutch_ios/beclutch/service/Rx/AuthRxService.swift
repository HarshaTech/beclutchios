//
//  AuthRxService.swift
//  beclutch
//
//  Created by Vu Trinh on 13/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

import RxSwift

protocol AuthRxService {
    func getProductionTypes() -> Observable<Swift.Result<ProductionTypeResponse, Error>>
    func getProductionGroupItems(groupId: Int) -> Observable<Swift.Result<ProductionTypeResponse, Error>>
    func getSportRole(sportId: Int) -> Observable<Swift.Result<ProductionTypeResponse, Error>>
}

extension APIManager: AuthRxService {
    func getProductionTypes() -> Observable<Swift.Result<ProductionTypeResponse, Error>> {
        let service: AuthEndpoint = AuthEndpoint.getProductionGroups

        return sendRequest(endpoint: service)
    }
    
    func getProductionGroupItems(groupId: Int) -> Observable<Swift.Result<ProductionTypeResponse, Error>> {
        let service: AuthEndpoint = AuthEndpoint.getProductionGroupItems(groupId: groupId)

        return sendRequest(endpoint: service)
    }
    
    func getSportRole(sportId: Int) -> Observable<Swift.Result<ProductionTypeResponse, Error>> {
        let service: AuthEndpoint = AuthEndpoint.getSportRoles(sportId: sportId)
        
        return sendRequest(endpoint: service)
    }
}
