//
//  CreateRxService.swift
//  beclutch
//
//  Created by Vu Trinh on 28/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import RxSwift

protocol CreateRxService {
    func resendInvitationEmail(userId: String, memberId: String) -> Observable<Swift.Result<CommonResponse, Error>>
    func getInvitationCode(rosterId: String) -> Observable<Swift.Result<CommonResponse, Error>>
}

extension APIManager: CreateRxService {
    func resendInvitationEmail(userId: String, memberId: String) -> Observable<Swift.Result<CommonResponse, Error>> {
        let service: CreateEndpoint = CreateEndpoint.resendInviationEmail(userId: userId, memberId: memberId)
        
        return sendRequest(endpoint: service)
    }
    
    func getInvitationCode(rosterId: String) -> Observable<Swift.Result<CommonResponse, Error>> {
        let service: CreateEndpoint = CreateEndpoint.getInviteCode(rosterId: rosterId)
        return sendRequest(endpoint: service)
    }
}
