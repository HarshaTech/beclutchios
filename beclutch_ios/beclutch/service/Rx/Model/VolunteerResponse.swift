//
//  VolunteerResponse.swift
//  beclutch
//
//  Created by Vu Trinh on 21/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct Volunteer: Decodable {
    var volunteerId: String
    var name: String
    var date: String

    private enum CodingKeys: String, CodingKey {
        case volunteerId = "ID"
        case name = "NAME"
        case date = "DATE"
    }
}

struct Assignment: Decodable {
    var assignmentId: String
    var name: String
    var volunteerId: String
    var volunteerName: String
    var reminderFrequency: String
    var slots: String
    var isAllDay: String
    var memo: String

    var startTimeHour: String
    var startTimeMin: String
    var startTimeAMPM: String

    var endTimeHour: String
    var endTimeMin: String
    var endTimeAMPM: String

    var createdAt: String
    var updatedAt: String
    var volunteerDate: String

    private enum CodingKeys: String, CodingKey {
        case assignmentId = "ID"
        case name = "NAME"
        case volunteerId = "VOLUNTEER_ID"
        case volunteerName = "VOLUNTEER_NAME"
        case reminderFrequency = "REMINDER_FREQ"
        case slots = "SLOTS"
        case isAllDay = "IS_ALL_DAY"
        case memo = "MEMO"

        case startTimeHour = "START_TIME_HOUR"
        case startTimeMin = "START_TIME_MIN"
        case startTimeAMPM = "START_TIME_AMPM"

        case endTimeHour = "END_TIME_HOUR"
        case endTimeMin = "END_TIME_MIN"
        case endTimeAMPM = "END_TIME_AMPM"

        case createdAt = "CREATED_AT"
        case updatedAt = "UPDATED_AT"
        case volunteerDate = "VOLUNTEER_DATE"
    }
}

class AssignmentMember: Codable {
    var memberId: String?
    var clubOrTeam: String?
    var clubId: String?
    var teamId: String?
    var userId: String?
    var level: String?
    var inviteCode: String?

    var firstName: String?
    var lastName: String?
    var email: String?
    var cellPhone: String?
    var homePhone: String?
    var workPhone: String?
    var pic: String?
    var additionInfo: String?
    var isActive: String?
    var isPlayer: String?

    var deletedAt: String?
    var createAt: String?
    var boatName: String?
    
    init() {
        memberId = nil
        clubOrTeam = nil
        clubId = nil
        teamId = nil
        userId = nil
        level = nil
        inviteCode = nil

        firstName = nil
        lastName = nil
        email = nil
        cellPhone = nil
        homePhone = nil
        workPhone = nil
        pic = nil
        additionInfo = nil
        isActive = nil
        isPlayer = nil

        deletedAt = nil
        createAt = nil
        boatName = nil
    }

    private enum CodingKeys: String, CodingKey {
        case memberId = "ID"
        case clubOrTeam = "CLUB_OR_TEAM"
        case clubId = "CLUB_ID"
        case teamId = "TEAM_ID"
        case userId = "USER_ID"
        case level = "LEVEL"
        case inviteCode = "FROM_INVITE_CODE"

        case firstName = "ROSTER_FIRST_NAME"
        case lastName = "ROSTER_LAST_NAME"
        case email = "ROSTER_EMAIL"
        case cellPhone = "ROSTER_CELL_PHONE"
        case homePhone = "ROSTER_HOME_PHONE"
        case workPhone = "ROSTER_WORK_PHONE"
        case pic = "ROSTER_PROFILE_PIC"
        case additionInfo = "ADDITION_INFO_ROSTER"
        case isActive = "IS_ACTIVE"
        case isPlayer = "IS_PLAYER"

        case deletedAt = "DELETED_AT"
        case createAt = "CREATED_AT"
        case boatName = "BOAT_NAME"
    }
}

struct AddAssignmentResponse: Decodable {
    let msg: String
    let result: Bool
    let volunteerId: Int

    private enum CodingKeys: String, CodingKey {
        case msg = "msg"
        case result = "result"
        case volunteerId = "extra"
    }
}

class VolunteerListResponse: Decodable {
    var msg: String
    var result: Bool
    var extra: [Volunteer]
}

class AssignmentListResponse: Decodable {
    var msg: String
    var result: Bool
    var extra: [Assignment]
}

struct AssignmentMemberListResponse: Decodable {
    let msg: String
    let result: Bool
    let extra: [AssignmentMember]
}
