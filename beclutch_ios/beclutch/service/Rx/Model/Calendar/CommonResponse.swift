//
//  CommonResponse.swift
//  beclutch
//
//  Created by Vu Trinh on 12/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct CommonResponse: Decodable {
    let msg: String
    let result: Bool
    let extra: String
}

struct CommonResponseNumber: Codable {
    let msg: String?
    let result: Bool?
    let extra: Int?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        msg = try values.decodeIfPresent(String.self, forKey: .msg)
        result = try values.decodeIfPresent(Bool.self, forKey: .result)
        extra = try values.decodeIfPresent(Int.self, forKey: .extra)
    }
}

struct CommonResponseWithoutExtra: Codable {
    let msg: String?
    let result: Bool?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        msg = try values.decodeIfPresent(String.self, forKey: .msg)
        result = try values.decodeIfPresent(Bool.self, forKey: .result)
    }
}

