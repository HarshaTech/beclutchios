//
//  IcalResponse.swift
//  beclutch
//
//  Created by Vu Trinh on 12/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct IcalResponse: Decodable {
    let extra: String
    let msg: String
    let result: Bool
}
