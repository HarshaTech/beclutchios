//
//  ProductionTypesResponse.swift
//  beclutch
//
//  Created by Vu Trinh on 13/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct ProductionTypeResponse: Decodable {
    let msg: [Production]
    let result: Bool
}
