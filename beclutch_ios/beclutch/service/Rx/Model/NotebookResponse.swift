//
//  NotebookResponse.swift
//  beclutch
//
//  Created by Vu Trinh on 28/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

struct Note: Decodable {
    var noteId: String
    var name: String
    var notebookType: String
    var rosterId: String
    var viewAll: String
    var date: String?

    var text: String?

    private enum CodingKeys: String, CodingKey {
        case noteId = "ID"
        case name = "NAME"
        case notebookType = "NOTEBOOK_TYPE"
        case rosterId = "ROSTER_ID"
        case viewAll = "VIEW_ALL"
        case date = "DATE"
        case text = "TEXT"
    }
}

struct NoteListResponse: Decodable {
    let msg: String
    let result: Bool
    let extra: [Note]
}
