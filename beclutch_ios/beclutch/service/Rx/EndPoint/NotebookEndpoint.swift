//
//  NotebookEndpoint.swift
//  beclutch
//
//  Created by Vu Trinh on 28/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import Alamofire


enum NotebookEndpoint {
    case getNotebookList(type: NoteBookType)
    case createNoteText(noteId: String?, name: String, text: String)
    case getNoteData(noteId: String)
    case deleteNote(noteId: String)
}

extension NotebookEndpoint: Endpoint {
    var headers: [String: String]? {
        return nil
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .post
        }
    }

    var parameters: [String: Any] {
        switch self {
        case let .getNotebookList(type):
            let parameters: [String: Any] = ["team_id": AppDataInstance.instance.currentTeamId, "notebook_type": type.rawValue]

            return parameters
        case let .createNoteText(noteId, name, text):
            var parameters: [String: Any] = ["team_id": AppDataInstance.instance.currentTeamId,
                                             "roster_id": AppDataInstance.instance.currentRosterId,
                                             "name": name,
                                             "text": text]
            if let noteId = noteId {
                parameters["id"] = noteId
            }

            return parameters
        case let .getNoteData(noteId):
            let parameters: [String: Any] = ["id": noteId]

            return parameters
        case let .deleteNote(noteId):
            let parameters: [String: Any] = ["roster_id": AppDataInstance.instance.currentRosterId, "id": noteId]

            return parameters
        }
    }

    var path: String {
        switch self {
        case .getNotebookList:
            return "api/notebook/get_notebook_list"
        case .createNoteText:
            return "api/notebook/create_note_text"
        case .getNoteData:
            return "api/notebook/get_text_note_data"
        case .deleteNote:
            return "api/notebook/delete_note_text"
        }
    }
}
