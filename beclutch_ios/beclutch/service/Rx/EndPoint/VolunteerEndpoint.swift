//
//  VolunteerEndpoint.swift
//  beclutch
//
//  Created by Vu Trinh on 21/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import Alamofire

enum VolunteerEndpoint {
    // eventId: pass "", eventType: pass "", volunteerDate: YYYY-MM-DD

    // volunteerDate: get from now
    case createVolunteer(eventId: String, eventType: String, volunteerName: String, volunteerDate: String)
    case updateVolunteer(volunteerId: String, volunteerName: String, volunteerDate: String)
    case getVolunteerList(eventId: String, volunteerId: String)
    case removeVolunteer(volunteerId: String)

    // volunteerDate: get from current volunteer, reminderFrequency:
    case addAssignment(assignment: AssignmentModel)

    case updateAssignment(assignment: AssignmentModel)
    case getVolunteerAssignments(volunteerId: String)
    case removeAssignment(assignmentId: String)

    // 
    case addAssignmentMember(assignmentId: String, memberId: String, rosterJson: String,
                             oldMemberId: String, oldRosterJson: String)
    case getAssignmentMember(assignmentId: String)
    case removeAssignmentMember(assignmentId: String, memberId: String)

    case sendEmailVolunteer(volunteerId: String)
}

extension VolunteerEndpoint: Endpoint {
    var headers: [String: String]? {
        return nil
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .post
        }
    }

    var parameters: [String: Any] {
        switch self {
        case let .createVolunteer(eventId, eventType, volunteerName, volunteerDate):
            let parameters: [String: Any] = ["team_id": AppDataInstance.instance.currentTeamId, "event_id": eventId, "event_type": eventType, "volunteer_name": volunteerName, "volunteer_date": volunteerDate]

            return parameters
        case let .updateVolunteer(volunteerId, volunteerName, volunteerDate):
            let parameters: [String: Any] = ["volunteer_id": volunteerId,"volunteer_name": volunteerName, "volunteer_date": volunteerDate]

            return parameters
        case let .getVolunteerList(eventId, volunteerId):
            let parameters: [String: Any] = ["team_id": AppDataInstance.instance.currentTeamId, "volunteer_id": volunteerId, "event_id": eventId]

            return parameters
        case let .removeVolunteer(volunteerId):
            let parameters: [String: Any] = ["volunteer_id": volunteerId]

            return parameters
        case let .addAssignment(assignmentModel):
            let parameters: [String: Any] = ["team_id": AppDataInstance.instance.currentTeamId,
                                             "volunteer_id": assignmentModel.volunteerId?.intValue() ?? 0,
                                             "volunteer_name": assignmentModel.volunteerName ?? "",
                                             "volunteer_date": convertDateToServerStyle(assignmentModel.volunteerDate),
                                             "event_id": assignmentModel.eventId ?? "",
                                             "event_type": assignmentModel.eventType ?? "",
                                             "assignment_title": assignmentModel.assignmentName ?? "",
                                             "reminder_freq": assignmentModel.reminderFrequency ?? "",
                                             "slots": assignmentModel.slots ?? "",
                                             "start_time_hour": assignmentModel.startTimeHour ?? "",
                                             "start_time_min": assignmentModel.startTimeMin ?? "",
                                             "start_time_ampm": assignmentModel.startTimeAMPM ?? "",
                                             "end_time_hour": assignmentModel.endTimeHour ?? "",
                                             "end_time_min": assignmentModel.endTimeMin ?? "",
                                             "end_time_ampm": assignmentModel.endTimeAMPM ?? "",
                                             "is_all_day": assignmentModel.isAllDay ?? "",
                                             "memo": assignmentModel.memo ?? ""]

            return parameters

        case let .updateAssignment(assignmentModel):

            let parameters: [String: Any] = ["assignment_id": assignmentModel.assignmentId ?? "",
                                             "assignment_title": assignmentModel.assignmentName ?? "",
                                             "reminder_freq": assignmentModel.reminderFrequency ?? "",
                                             "slots": assignmentModel.slots ?? "",
                                             "start_time_hour": assignmentModel.startTimeHour ?? "",
                                             "start_time_min": assignmentModel.startTimeMin ?? "",
                                             "start_time_ampm": assignmentModel.startTimeAMPM ?? "",
                                             "end_time_hour": assignmentModel.endTimeHour ?? "",
                                             "end_time_min": assignmentModel.endTimeMin ?? "",
                                             "end_time_ampm": assignmentModel.endTimeAMPM ?? "",
                                             "is_all_day": assignmentModel.isAllDay ?? "",
                                             "memo": assignmentModel.memo ?? ""]

            return parameters
        case let .getVolunteerAssignments(volunteerId):
            let parameters: [String: Any] = ["volunteer_id": volunteerId]

            return parameters
        case let .removeAssignment(assignmentId):
            let parameters: [String: Any] = ["assignment_id": assignmentId]

            return parameters
        case let .addAssignmentMember(assignmentId, memberId, rosterJson,
                                      oldMemberId, oldRosterJson):
            let parameters: [String: Any] = ["assignment_id": assignmentId,
                                             "member_id": memberId,
                                             "rosters": rosterJson,
                                             "old_member_id": oldMemberId,
                                             "old_rosters": oldRosterJson]

            return parameters
        case let .getAssignmentMember(assignmentId):
            let parameters: [String: Any] = ["assignment_id": assignmentId]

            return parameters
        case let .removeAssignmentMember(assignmentId, memberId):
            let parameters: [String: Any] = ["assignment_id": assignmentId, "member_id": memberId]

            return parameters
        case let .sendEmailVolunteer(volunteerId):
            let parameters: [String: Any] = ["volunteer_id": volunteerId]

            return parameters
        }
    }

    var path: String {
        switch self {
        case .createVolunteer:
            return "api/volunteer/create_volunteer"
        case .updateVolunteer:
            return "api/volunteer/update_volunteer"
        case .getVolunteerList:
            return "api/volunteer/get_volunteer_list"
        case .removeVolunteer:
            return "api/volunteer/remove_volunteer"
        case .addAssignment:
            return "api/volunteer/add_assignment"
        case .updateAssignment:
            return "api/volunteer/update_assignment"
        case .getVolunteerAssignments:
            return "api/volunteer/get_volunteer_assignment"
        case .removeAssignment:
            return "api/volunteer/remove_assignment"
        case .addAssignmentMember:
            return "api/volunteer/add_assignment_member"
        case .getAssignmentMember:
            return "api/volunteer/get_assignment_member"
        case .removeAssignmentMember:
            return "api/volunteer/remove_assignment_member"
        case .sendEmailVolunteer:
            return "api/volunteer/send_email_volunteer"
        }
    }
    
    func convertDateToServerStyle(_ date: String?) -> String {
        guard let date = date else { return "" }
        let components = date.components(separatedBy: "-")
        return "\(components[2])-\(components[0])-\(components[1])"
    }
}
