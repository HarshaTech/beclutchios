//
//  Service.swift
//  SportBooks
//
//  Created by Ron Magno on 12/9/19.
//  Copyright © 2019 Spiral. All rights reserved.
//

import Alamofire
import Foundation

protocol Endpoint {
    var baseURL: URL { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any] { get }
    var path: String { get }
    var headers: [String: String]? { get }
    var retryable: Bool { get }
    var url: String { get }
}

extension Endpoint {

    var baseURL: URL {
        return URL(string: ApiConstants.BASE_URL)!
    }

    var retryable: Bool {
        return true
    }

    var authorizationType: AuthorizationType {
        return .none
    }

    var url: String {
        switch method {
        case .get:
            let encodedParameters = parameters
                .map { (arg0) -> String? in
                    let (key, value) = arg0
                    let encodedValue = URLEncoding.default.escape("\(value)")
                    return "\(key)=\(encodedValue)"
                }
                .compactMap { $0 }
                .joined(separator: "&")

            let encodedURL =  "\(baseURL)\(path)?\(encodedParameters)"
            return encodedURL
        default:
            return path
        }
    }
}
