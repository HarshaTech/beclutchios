//
//  CreateEndpoint.swift
//  beclutch
//
//  Created by Vu Trinh on 28/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import Alamofire

enum CreateEndpoint {
    case resendInviationEmail(userId: String, memberId: String)
    case getInviteCode(rosterId: String)
}

extension CreateEndpoint: Endpoint {
    var headers: [String: String]? {
        return nil
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .post
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .resendInviationEmail(userId, memberId):
            let parameters: [String: Any] = ["user_id": userId, "member_id": memberId]
            return parameters
        case let .getInviteCode(rosterId):
            let parameters: [String: Any] = ["roster_id": rosterId]
            return parameters
        }
    }
    
    var path: String {
        switch self {
        case .resendInviationEmail:
            return "api/create/resend_invite"
        case .getInviteCode:
            return "api/rosters/get_invite_code"
        }
    }
}
