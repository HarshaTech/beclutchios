//
//  CalendarEndpoint.swift
//  beclutch
//
//  Created by Vu Trinh on 11/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import Alamofire

enum CalendarEndpoint {
    case getTeamIcal
    case sendEmail(calendarType: Int, rosterId: Int)
}

extension CalendarEndpoint: Endpoint {
    var headers: [String: String]? {
        return nil
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .post
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .getTeamIcal:
            let parameters: [String: Any] = [
                "team_id": AppDataInstance.instance.currentTeamId
            ]
            return parameters
        case let .sendEmail(calendarType: type, rosterId: rosterId):
            let parameters: [String: Any] = [
                "calendar_type": type,
                "roster_id": rosterId,
                "team_id": AppDataInstance.instance.currentTeamId
            ]
            return parameters
        }
    }

    var path: String {
        switch self {
        case .getTeamIcal:
            return "api/calendar/get_team_icals"
        case .sendEmail:
            return "api/calendar/send_calendar_instructions"
        }
    }

    var authorizationType: AuthorizationType {
        return .none
    }
}
