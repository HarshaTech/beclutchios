//
//  AuthEndpoint.swift
//  beclutch
//
//  Created by Vu Trinh on 13/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import Alamofire

enum AuthEndpoint {
    case getProductionGroups
    case getProductionGroupItems(groupId: Int)
    case getSportRoles(sportId: Int)
}

extension AuthEndpoint: Endpoint {
    var headers: [String: String]? {
        return nil
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .post
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .getProductionGroups:
            let parameters: [String: Any] = [:]
            return parameters
        case let .getProductionGroupItems(groupId: groupId):
            let parameters: [String: Any] = ["group_id" : groupId]
            return parameters
        case let .getSportRoles(sportId: sportId):
            let parameters: [String: Any] = [
                "sport": sportId,
                "limit_supperuser_roles": 1
            ]
            return parameters
        }
    }

    var path: String {
        switch self {
        case .getProductionGroups:
            return "api/auth/production_groups"
        case .getProductionGroupItems:
            return "api/auth/production_group_items"
        case .getSportRoles:
            return "api/auth/get_sport_roles"
        }
    }

    var authorizationType: AuthorizationType {
        return .none
    }
}
