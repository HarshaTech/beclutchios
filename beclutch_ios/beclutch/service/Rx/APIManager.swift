//
//  APIManager.swift
//  beclutch
//
//  Created by Vu Trinh on 11/01/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct StatusCodes {
    static let unauthorized = 401
}

class APIManager {
    static let shared = APIManager()
    
    lazy var headers: HTTPHeaders = [:]
    
    private func setHeaderFor(endpoint: Endpoint) {
        switch endpoint.authorizationType {
//        case .bearer:
//            headers["Authorization"] = "Bearer \(authManager.token)"
        default:
            break
        }
    }
    
    private func encodingFor(endpoint: Endpoint) -> ParameterEncoding {
        switch endpoint.method {
        case .post, .put:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    func sendRequest(endpoint: Endpoint, completion: @escaping (DataResponse<Any>) -> Void) {
        var parametersWithToken = endpoint.parameters
        parametersWithToken["token"] = AppDataInstance.instance.getToken()
        
        print("Sending request: \(parametersWithToken)")

        Alamofire
            .request("\(endpoint.baseURL)\(endpoint.path)",
                     method: endpoint.method,
                     parameters: parametersWithToken)
            .responseJSON { response in
                print("Server response: \(response)")
                completion(response)
            }
    }
    
    func sendRequest<T: Decodable, S: Endpoint>(endpoint: S) -> Observable<Swift.Result<T, Error>> {
        return Observable.create { observer -> Disposable in
            self.sendRequest(endpoint: endpoint) { response in
                switch response.result {
                case .success:
                    if let statusCode = response.response?.statusCode,
                        statusCode == StatusCodes.unauthorized {
                        let error = AFError
                            .responseValidationFailed(reason: .unacceptableStatusCode(code: statusCode))
                        observer.onError(error)
                    } else {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: response.data!)
                            observer.onNext(Swift.Result.success(model))
                        } catch {
                            let reason = AFError
                                .ResponseSerializationFailureReason
                                .jsonSerializationFailed(error: error)
                            let error = AFError.responseSerializationFailed(reason: reason)
                            print(error)
                            observer.onNext(Swift.Result.failure(error))
                        }
                    }

                case let .failure(error):
                    print(error)
                    observer.onNext(Swift.Result.failure(error))
                }

                observer.onCompleted()
            }
            return Disposables.create()
        }
        .catchError { error in
            Observable.just(Swift.Result.failure(error))
        }
    }
}
