//
//  NotebookRxService.swift
//  beclutch
//
//  Created by Vu Trinh on 27/03/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation
import RxSwift

enum NoteBookType: Int {
    case checklist = 1
    case note = 2
    case album = 3

    var icon: String {
        switch self {
        case .checklist:
            return "ic_check_list"
        case .note:
            return "ic_note"
        case .album:
            return "ic_album"
        }
    }

    var title: String {
        switch self {
        case .checklist:
            return "CHECKLISTS"
        case .note:
            return "NOTES"
        case .album:
            return "ALBUMS"
        }
    }
}

extension APIManager {
    func getNotebookList(type: NoteBookType) -> Observable<Result<NoteListResponse, Error>> {
        let service = NotebookEndpoint.getNotebookList(type: type)

        return sendRequest(endpoint: service)
    }

    func createNoteText(noteId: String? = nil, name: String, text: String) -> Observable<Result<CommonResponseNumber, Error>> {
        let service = NotebookEndpoint.createNoteText(noteId: noteId, name: name, text: text)

        return sendRequest(endpoint: service)
    }

    func getNoteData(noteId: String) -> Observable<Result<NoteListResponse, Error>> {
        let service = NotebookEndpoint.getNoteData(noteId: noteId)

        return sendRequest(endpoint: service)
    }

    func deleteNote(noteId: String) -> Observable<Result<CommonResponseNumber, Error>> {
        let service = NotebookEndpoint.deleteNote(noteId: noteId)

        return sendRequest(endpoint: service)
    }
}
