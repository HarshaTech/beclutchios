//
//  VolunteerRxService.swift
//  beclutch
//
//  Created by Vu Trinh on 21/02/2021.
//  Copyright © 2021 zeus. All rights reserved.
//

import Foundation

import RxSwift

protocol VolunteerRxService {
 
    func getVolunteerList(eventId: String, volunteerId: String) -> Observable<Result<VolunteerListResponse, Error>>
    
    func removeVolunteer(volunteerId: String) -> Observable<Result<CommonResponseNumber, Error>>
    
    func addAssignment(assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>>
    
    func updateAssignment(assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>>
    
    func getVolunteerAssignments(volunteerId: String) -> Observable<Result<AssignmentListResponse, Error>>
    
    func removeAssignment(assignmentId: String) -> Observable<Result<CommonResponseNumber, Error>>
    
    func getAssignmentMember(assignmentId: String) -> Observable<Result<AssignmentMemberListResponse, Error>>

    func sendEmailVolunteer(volunteerId: String) -> Observable<Result<CommonResponseNumber, Error>>
    
    func addAssignmentMember(assignmentId: String, memberId: String, rosterJson: String,
                             oldMemberId: String, oldRosterJson: String) -> Observable<Result<CommonResponseWithoutExtra, Error>>
}

extension APIManager: VolunteerRxService {
    func createVolunteer(eventId: String, eventType: String, volunteerName: String, volunteerDate: String) -> Observable<Result<CommonResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.createVolunteer(eventId: eventId, eventType: eventType, volunteerName: volunteerName, volunteerDate: volunteerDate)
        
        return sendRequest(endpoint: service)
    }
    
    func updateVolunteer(volunteerId: String, volunteerName: String, volunteerDate: String) -> Observable<Result<CommonResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.updateVolunteer(volunteerId: volunteerId, volunteerName: volunteerName, volunteerDate: volunteerDate)
        
        return sendRequest(endpoint: service)
    }
    
    func getVolunteerList(eventId: String, volunteerId: String) -> Observable<Result<VolunteerListResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.getVolunteerList(eventId: eventId, volunteerId: volunteerId)
        
        return sendRequest(endpoint: service)
    }
    
    func removeVolunteer(volunteerId: String) -> Observable<Result<CommonResponseNumber, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.removeVolunteer(volunteerId: volunteerId)
        
        return sendRequest(endpoint: service)
    }
    
    // Return volunteerId
    func addAssignment(assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.addAssignment(assignment: assignment)
        
        return sendRequest(endpoint: service)
    }
    
    func updateAssignment(assignment: AssignmentModel) -> Observable<Result<CommonResponseNumber, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.updateAssignment(assignment: assignment)
        
        return sendRequest(endpoint: service)
    }
    
    func getVolunteerAssignments(volunteerId: String) -> Observable<Result<AssignmentListResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.getVolunteerAssignments(volunteerId: volunteerId)
        
        return sendRequest(endpoint: service)
    }
    
    func removeAssignment(assignmentId: String) -> Observable<Result<CommonResponseNumber, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.removeAssignment(assignmentId: assignmentId)
        
        return sendRequest(endpoint: service)
    }
    
    func addAssignmentMember(assignmentId: String, memberId: String, rosterJson: String,
                             oldMemberId: String, oldRosterJson: String) -> Observable<Result<CommonResponseWithoutExtra, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.addAssignmentMember(assignmentId: assignmentId, memberId: memberId, rosterJson: rosterJson, oldMemberId: oldMemberId, oldRosterJson: oldRosterJson)
        
        return sendRequest(endpoint: service)
    }
    
    func getAssignmentMember(assignmentId: String) -> Observable<Result<AssignmentMemberListResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.getAssignmentMember(assignmentId: assignmentId)
        
        return sendRequest(endpoint: service)
    }
    
    func removeAssignmentMember(assignmentId: String, memberId: String) -> Observable<Result<CommonResponse, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.removeAssignmentMember(assignmentId: assignmentId, memberId: memberId)
        
        return sendRequest(endpoint: service)
    }

    func sendEmailVolunteer(volunteerId: String) -> Observable<Result<CommonResponseNumber, Error>> {
        let service: VolunteerEndpoint = VolunteerEndpoint.sendEmailVolunteer(volunteerId: volunteerId)

        return sendRequest(endpoint: service)
    }
}
