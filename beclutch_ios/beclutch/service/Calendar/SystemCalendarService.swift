//
//  SystemCalendarService.swift
//  beclutch
//
//  Created by Chung Le on 3/15/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import EventKit

class SystemCalendarService {
    static let shared = SystemCalendarService()

    // MARK: - Reminder
    func deleteReminder(title: String?, note: String?) {
        guard let title = title, let note = note else {
            return
        }

        let eventStore = EKEventStore()

        if let reminderCalendar = eventStore.defaultCalendarForNewReminders() {
            let predicate = eventStore.predicateForReminders(in: [reminderCalendar])
            eventStore.fetchReminders(matching: predicate) { (reminders) in
                if let reminders = reminders {
                    for reminder in reminders {
                        if reminder.title == title, reminder.notes == note {
                            do {
                                try eventStore.remove(reminder, commit: true)
                            } catch {
                                print("Errore when delete reminder: \(error)")
                            }
                        }
                    }
                }
            }
        }

    }
    func addReminder(title: String?, note: String?, eventTime: Date?, duration: TimeInterval, completed: @escaping ((Error?) -> Void)) {
        guard let title = title, let note = note, let eventTime = eventTime else {
            return
        }

        let eventStore = EKEventStore()

        eventStore.requestAccess(to: EKEntityType.event) { granted, error in
            if (granted) && (error == nil) {
                print("granted \(granted)")

                let event: EKEvent = EKEvent(eventStore: eventStore)

                //  what should i need to change here for specific date and time?

                event.title = title
                event.startDate = eventTime
                event.endDate = eventTime.addingTimeInterval(duration)
                event.isAllDay = false
                event.notes = note
                event.calendar = eventStore.defaultCalendarForNewEvents

                do {
                    // Adding an Alert 1 hour before the startDate
                    let reminder = EKAlarm(relativeOffset: -3600)
                    event.addAlarm(reminder)

                    try eventStore.save(event, span: .thisEvent)
                    print("Saved Event")

                    completed(nil)
                } catch let error {
                    print(error.localizedDescription)
                    completed(error)
                }
            } else {
//                completed(error)
                print("Not granted \(granted)")
            }
        }
    }

//    func addReminder(title: String?, note: String?, repeatStr: String, eventTime: Date) {
//        guard let title = title, let note = note else {
//            return
//        }
//
//        let eventStore = EKEventStore()
//        eventStore.requestAccess(to: EKEntityType.reminder, completion: {
//          granted, error in
//
//          if (granted) && (error == nil) {
//            print("granted \(granted)")
//
//            let reminder:EKReminder = EKReminder(eventStore: eventStore)
//
//            reminder.title = title
//            reminder.priority = 1
//            //  How to show completed
//
//            reminder.notes = note
//
//            let reminderDate = eventTime
//
//            var weekly: [EKRecurrenceDayOfWeek] = []
//            if repeatStr == RepeatModel.REPEAT_TYPE_WEEKLY_REPEAT {
//                weekly = [EKRecurrenceDayOfWeek(reminderDate.dayOfWeek)]
//            }
//
//            var monthly: [NSNumber] = []
//            if repeatStr == RepeatModel.REPEAT_TYPE_MONTHLY_REPEAT {
//                monthly = [reminderDate.dayOfMonth]
//            }
//
//            var yearly: [NSNumber] = []
//            if repeatStr == RepeatModel.REPEAT_TYPE_YEARLY_REPEAT {
//                yearly = [reminderDate.dayOfYear]
//            }
//
//            let rule = EKRecurrenceRule(recurrenceWith: .weekly,
//                interval: 1,
//                daysOfTheWeek: weekly,
//                daysOfTheMonth: monthly,
//                monthsOfTheYear: nil,
//                weeksOfTheYear: nil,
//                daysOfTheYear: yearly,
//                setPositions: nil,
//                end: nil)
//            reminder.addRecurrenceRule(rule)
//
//              let alarm = EKAlarm(relativeOffset: -3600)
//
//              reminder.addAlarm(alarm)
//              reminder.calendar = eventStore.defaultCalendarForNewReminders()
//
//              do {
//                try eventStore.save(reminder, commit: true)
//              } catch {
//                print("Cannot save")
//                return
//              }
//              print("Reminder saved")
//            }
//        })
//    }
}
