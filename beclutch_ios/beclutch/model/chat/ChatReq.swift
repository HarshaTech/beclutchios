//
//  ChatReq.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class SendReminderReq {
    var token: String?
    var expirationDate: Date?
    var groupSelectionResult: GroupSelectionResult?
    var subject: String?
    var content: String?
    var senderFirstName: String?
    var senderLastName: String?

    var senderRosterId: String?
    var senderTeamId: String?
    var readStatus: [Int]?

    func getDict() -> [String: String] {
        return
            [
                "token": self.token!,
                "to_members": self.groupSelectionResult!.toJsonString(),
                "expiration": "\(Int(self.expirationDate!.timeIntervalSince1970))",
                "subject": self.subject!,
                "content": self.content!,
                "team_id": self.senderTeamId!,
                "sender_roster_id": self.senderRosterId!,
                "sender_first_name": self.senderFirstName!,
                "sender_last_name": self.senderLastName!,
                "read_status": self.getJsonFromReadStatus()
        ]
    }

    func getJsonFromReadStatus() -> String {
        if self.readStatus == nil {
            self.readStatus = []
        }

        guard let data = try? JSONSerialization.data(withJSONObject: self.readStatus!, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8)!
    }
}

class ReminderLoadReq {
    var token: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "roster_id": self.rosterId!]
    }
}

class ReminderDeleteReq {
    var token: String?
    var reminderId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "reminder_id": self.reminderId!, "member_id": self.rosterId!]
    }
}

class UpdateReadStatusReq {
    var token: String?
    var reminderId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "reminder_id": self.reminderId!, "reader_roster_id": self.rosterId!]
    }
}

class SendNotificationToTeamReq {
    var token: String?
    var teamId: String?
    var subject: String?
    var content: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token ?? "",
                "team_id": self.teamId ?? "",
                "subject": self.subject ?? "",
                "content": self.content ?? "",
                "roster_id": self.rosterId ?? ""]
    }
}

class ConversationEntryLoadReq {
    var token: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "my_id": self.rosterId!]
    }
}

class NewConversationAddReq {
    var token: String?
    var rosterIds: [String]?
    var users: GroupChatResult?
    var type: Int?
    var teamId: String?

    func getDict() -> [String: String] {
        if let rosterStr = [Any].toJsonString(array: rosterIds) {
            return ["token": self.token!, "type": String(self.type!), "users_in_converse": users!.toJsonString(), "roster_ids": rosterStr, "team_id": teamId!]
        }

        return ["token": self.token!, "type": String(self.type!), "users_in_converse": users!.toJsonString(), "roster_ids": "", "team_id": teamId!]
    }
}

class ConversationThreadRemoveRes {
    var token: String?
    var threadId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "threadId": String(threadId!)]
    }
}

class IndividualConversationCheckReq {
    var token: String?
    var firstRosterId: String?
    var secondRosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "f_id": firstRosterId!, "s_id": secondRosterId!]
    }
}

class CarpoolChatEntryLoadReq {
    var token: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "my_id": self.rosterId!]
    }
}

class SendTeamTextChatReq {
    var token: String?
    var rosterId: String?
    var content: String
    var converseId: String?

    init(message: Message, converseId: String? = nil) {
        self.token = AppDataInstance.instance.getToken()
        self.rosterId = message.sender.id
        self.content = message.content
        if let id = converseId {
            self.converseId = id
        }
    }

    func getDict() -> [String: String] {
        var result = ["token": self.token!, "sender_roster_id": self.rosterId!, "content": self.content]
        if let converseId = self.converseId {
            result["converse_id"] = converseId
        }

        return result
    }
}

class UploadImageMediaChatReq {
    var token: String?
    var rosterId: String?
    var uploadFile: Data?

    //    init(message: Message) {
    //        self.token = AppDataInstance.instance.getToken()
    //        self.rosterId = message.sender.id
    //        self.content = message.content
    //    }

    func getDict() -> [String: String] {
        return ["token": self.token!, "sender_roster_id": self.rosterId!]
    }
}
