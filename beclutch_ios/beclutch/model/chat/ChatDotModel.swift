//
//  ChatDotModel.swift
//  beclutch
//
//  Created by Vu Trinh on 9/9/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class ChatDotModel {
    private let teamId: Int
    private let lastMsg: String
    private let lastTime: String
    private let lastSenderName: String
    
    init(teamId: Int, msg: String, time: String, senderName: String) {
        self.teamId = teamId
        self.lastMsg = msg
        self.lastTime = time
        self.lastSenderName = senderName
    }
}
