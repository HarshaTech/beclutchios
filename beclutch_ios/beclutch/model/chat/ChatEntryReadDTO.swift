//
//  ChatEntryReadDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import FirebaseFirestore

class ChatEntryReadDTO {
    var lastSenderName: String?
    var lastSenderId: String?
    var lastSendMsg: String?

    var lastSendDate: Date?

    var readEntries: [Int] = []
    var memberIds: [Int] = []
    var type: Int?

    var teamId: String?

    var conversationDisplayName: String {
        if AppDataInstance.instance.me?.DEFAULT_MEMBER_LOG == lastSenderId && lastSenderId != nil {
            return "You"
        } else {
            return self.lastSenderName ?? ""
        }
    }

    init() {
    }

    init(dict: [String: Any]) {
        if let name = dict["lastSenderName"] as? String {
            lastSenderName = name
        }

        if let senderId = dict["lastSenderId"] as? String {
            lastSenderId = senderId
        }

        if let message = dict["lastSendMsg"] as? String {
            lastSendMsg = message
        }

        if let date = dict["lastSendTime"] as? Timestamp {
            lastSendDate = date.dateValue()
        }

        if let entries = dict["readEntries"] as? [Int] {
            readEntries = entries
        }
        
        if let entries = dict["memberIds"] as? [Int] {
            memberIds = entries
        }
        
        if let teamId = dict["teamId"] as? String {
            self.teamId = teamId
        }
    }

    init(message: Message, allIds: [Int] = []) {
        lastSenderName = message.sender.displayName
        lastSenderId = message.sender.id
        lastSendMsg = message.content
        lastSendDate = message.sentDate
        teamId = message.teamId
        self.memberIds = allIds

        if let id = Int(message.sender.id) {
            readEntries = [id]
        }

        type = message.type
    }

    func firebaseDict() -> [String: Any] {
        guard let msg = lastSendMsg, let name = lastSenderName, let date = lastSendDate, let teamId = teamId else {
            return [:]
        }

        let definedMsg = type == 1 ? "[Image]" : msg

        var result: [String: Any] = ["lastSendMsg": definedMsg, "lastSenderName": name, "lastSendTime": Timestamp(date: date), "readEntries": readEntries, "teamId": teamId, "memberIds": memberIds]

        if let senderId = self.lastSenderId {
            result["lastSenderId"] = senderId
        }

        return result
    }
}
