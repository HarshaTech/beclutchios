//
//  ChatRes.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class ChatServiceBaseRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }

}

class ReminderLoadRes {
    var result: Bool?
    var msg: String?
    var reminders: [ReminderEntryDTO]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.reminders = []
            if let dicReminders = val!["extra"] as? [NSDictionary] {
                for itemDic in dicReminders {
                    let reminder = ReminderEntryDTO(val: itemDic)
                    self.reminders?.append(reminder)
                }
            }
        } else {
            self.result = false
            self.msg = "Database error"
            self.reminders = []
        }
    }
}

class ConversationEntryLoadRes {
    var result: Bool?
    var msg: String?
    var conversation_entries: [ConversationEntryDTO]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.conversation_entries = []
            if let dicReminders = val!["extra"] as? [NSDictionary] {
                for itemDic in dicReminders {
                    let converse = ConversationEntryDTO(val: itemDic)
                    self.conversation_entries?.append(converse)
                }
            }
        } else {
            self.result = false
            self.msg = "Database error"
            self.conversation_entries = []
        }
    }
}

class NewConversationAddRes {
    var result: Bool?
    var msg: String?

    var conversation: ConversationEntryDTO?

    init(val: NSDictionary?) {
        if let value = val {
            self.result = (value["result"] as! Bool)
            self.msg = (value["msg"] as! String)

            if let dict = value["extra"] as? NSDictionary {
                self.conversation = ConversationEntryDTO(val: dict)
            }
        }
    }
}

class UploadImageMediaChatRes {
    var result: Bool?
    var msg: String?

    var senderRosterFirstName: String?
    var senderRosterLastName: String?
    var teamId: String?
    var senderRosterId: String?

    var urlOnServer: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)

            if let extra = val?["extra"] as? NSDictionary {
                if let firstName = extra["senderRosterFirstName"] as? String {
                    self.senderRosterFirstName = firstName
                }

                if let lastName = extra["senderRosterLastName"] as? String {
                    self.senderRosterLastName = lastName
                }

                if let teamId = extra["teamId"] as? String {
                    self.teamId = teamId
                }

                if let url = extra["urlOnServer"] as? String {
                    self.urlOnServer = url
                }
            }
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }

}
