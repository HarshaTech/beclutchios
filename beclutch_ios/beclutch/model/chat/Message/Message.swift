//
//  Message.swift
//  beclutch
//
//  Created by SM on 2/13/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

import Firebase
import MessageKit
import FirebaseFirestore

struct Message: MessageType {

  let id: String?
  let content: String
  let sentDate: Date
  let sender: Sender
    let type: Int
    let teamId: String

  var data: MessageData {
    if let image = image {
      return .photo(image)
    } else {
      return .text(content)
    }
  }

  var messageId: String {
    return id ?? UUID().uuidString
  }

  var image: UIImage?
  var downloadURL: URL?

    init(roster: RosterInfo, content: String, type: Int = 0) {
        sender = Sender(id: roster.ID, displayName: roster.displayName)
        self.content = content
        sentDate = Date()
        id = nil
        self.type = type
        self.teamId = roster.teamOrClubId
    }

    init(roster: RosterInfo, image: UIImage, downloadUrl: String) {
        sender = Sender(id: roster.ID, displayName: roster.displayName)
        self.image = image
        content = downloadUrl
        sentDate = Date()
        id = nil
        type = 1
        teamId = roster.teamOrClubId
      }

  init?(document: QueryDocumentSnapshot) {
    let data = document.data()

    guard let sentDate = data["created"] as? Timestamp else {
      return nil
    }
    guard let senderID = data["senderID"] as? Int else {
      return nil
    }
    guard let senderName = data["senderName"] as? String else {
      return nil
    }
    guard let type = data["type"] as? Int else {
      return nil
    }

    if let teamId = data["teamId"] as? String {
        self.teamId = teamId
    } else {
        self.teamId = ""
    }

    id = document.documentID

    self.type = type

    self.sentDate = sentDate.dateValue()
    sender = Sender(id: String(senderID), displayName: senderName)

    if let content = data["content"] as? String {
      self.content = content
      downloadURL = nil
    } else if let urlString = data["url"] as? String, let url = URL(string: urlString) {
      downloadURL = url
      content = ""
    } else {
      return nil
    }
  }

}

extension Message: DatabaseRepresentation {

  var representation: [String: Any] {
    var rep: [String: Any] = [
      "created": sentDate,
      "senderID": Int(sender.id) ?? 0,
      "senderName": sender.displayName,
      "type": type,
      "teamId": teamId
    ]

    if let url = downloadURL {
      rep["content"] = url.absoluteString
    } else {
      rep["content"] = content
    }

    return rep
  }

}

extension Message: Comparable {

  static func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.id == rhs.id
  }

  static func < (lhs: Message, rhs: Message) -> Bool {
    return lhs.sentDate < rhs.sentDate
  }

}
