//
//  CalendarRes.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class CreatePracticeRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class CreateGameRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class GetCalendarByTeamIdRes {
    var result: Bool?
    var message: String?
    var items: [CalendarFromBackendDTO] = []
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            self.items = []
            if let dic_items = val!["items"] as? [NSDictionary] {
                for item in dic_items {
                    let backendDTO = CalendarFromBackendDTO(val: item)
                    self.items.append(backendDTO)
                }
            }
        } else {
            self.result = false
            self.message = "Database Error"
            self.items = []
        }
    }
}

class CalendarContentRes {
    var result: Bool = false
    var dtos: [CalendarListDTO] = []
}

class LoadRosterInCalendarRes {
    var result: Bool?
    var message: String?
    var rosters: [RosterForEvent]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            self.rosters  = []
            if let dics = val!["extra"] as? [NSDictionary] {
                for item in dics {
                    let roster = RosterForEvent(val: item)
                    self.rosters?.append(roster)
                }
            }
        } else {
            self.result = false
            self.message = "Database Error"
            self.rosters = []
        }
    }
}

class LoadRosterInCalendarToUI {
    var result: Bool?
    var rosters: [CalendarRosterDTO]?
}

class CancelGameRes {
    var result: Bool?
    var msg: String?
    var pos: Int?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.pos = -1
        } else {
            self.result = false
            self.msg = "Database Error"
            self.pos = -1
        }
    }
}

class DeleteCalendarItemRes {
    var result: Bool?
    var msg: String?
    var pos: Int?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.pos = -1
        } else {
            self.result = false
            self.msg = "Database Error"
            self.pos = -1
        }
    }
}

class GetHistoryEventsRes {
    var result: Bool?
    var msg: String?
    var practices: [PracticeModel]?
    var games: [GameModel]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.practices  = []
            self.games  = []
            if let dics = val!["extra"] as? NSDictionary {
                if let dics = dics["practices"] as? [NSDictionary] {
                    for item in dics {
                        let practice = PracticeModel(val: item)
                        self.practices?.append(practice)
                    }
                }
                if let dics = dics["games"] as? [NSDictionary] {
                    for item in dics {
                        let game = GameModel(val: item)
                        self.games?.append(game)
                    }
                }
            }
        } else {
            self.result = false
            self.msg = "Database Error"
            self.practices = []
            self.games = []
        }
    }
}

class UpdateAttendanceRes {
    var attendance: Attendance?
    var result: Bool?
    var msg: String?

    var type: CALENDAR_ITEM_TYPE?
    var itemID: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            if let dicAttendance = val!["extra"] as? NSDictionary {
                self.attendance = Attendance(val: dicAttendance)
            } else {
                self.attendance = Attendance(val: nil)
            }
        } else {
            self.result = false
            self.msg = ""
            self.attendance = Attendance(val: nil)
        }
    }
}

class CancelPracticeRes {
    var result: Bool?
    var msg: String?
    var pos: Int?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.pos = -1
        } else {
            self.result = false
            self.msg = "Database Error"
            self.pos = -1
        }
    }
}

class HealthCheckMultiple {
    var id: String?
    var kind: String?
    var gamePracticeId: String?
    var rosterId: String?
    var status: String?
    var updateAt: String?
    var createAt: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.id = (val!["ID"] as! String)
            self.kind = (val!["KIND"] as! String)
            self.gamePracticeId = (val!["GAME_PRACTICE_ID"] as! String)
            self.rosterId = (val!["ROSTER_ID"] as! String)
            self.status = (val!["STATUS"] as! String)
            self.updateAt = (val!["UPDATED_AT"] as! String)
            self.createAt = (val!["CREATED_AT"] as! String)
        } else {
            id = ""
            kind = ""
            gamePracticeId = ""
            rosterId = ""
            status = ""
            updateAt = ""
            createAt = ""
        }
    }
}

class UpdateHealthCheckMultipleRes {
    var result: Bool?
    var msg: String?
    var extra: [HealthCheckMultiple]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.extra = []
            if let dict = val!["extra"] as? [NSDictionary] {
                for item_dict in dict {
                    let model = HealthCheckMultiple(val: item_dict)
                    self.extra?.append(model)
                }
            }
        } else {
            self.result = false
            self.msg = "Database Error"

        }
    }
}

class UpdatePracticeRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
class UpdateGameRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}

class CreateHealthCheckRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
