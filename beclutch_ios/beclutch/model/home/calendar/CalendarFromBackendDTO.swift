//
//  CalendarFromBackendDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class CalendarFromBackendDTO {
    var monthName: String?
    var games: [GameModel] = []
    var practices: [PracticeModel] = []

    init(val: NSDictionary?) {
        if val != nil {
            self.monthName = (val!["month_name"] as! String)
            self.games = []
            self.practices = []
            if let games_dic = val!["games"] as? [NSDictionary] {
                for item in games_dic {
                    let game = GameModel(val: item)
                    self.games.append(game)
                }
            } else {
                self.games = []
            }

            if let practices_dic = val!["practices"] as? [NSDictionary] {
                for item in practices_dic {
                    let practice = PracticeModel(val: item)
                    self.practices.append(practice)
                }
            } else {
                self.practices = []
            }
        } else {
            self.monthName = ""
            self.games = []
            self.practices = []
        }
    }
}
