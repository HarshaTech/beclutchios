//
//  CalendarRosterDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/11.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class CalendarRosterDTO {
    enum CALENDAR_ROSTER_TYPE {
        case ROSTER_MODEL
        case HEADER_MODEL
    }

    var type: CALENDAR_ROSTER_TYPE?
    var title: String?
    var roster: RosterForEvent?
}
