//
//  CalendarReq.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class CreatePracticeReq {
    var token: String?
    var startTimeAmPm: String?
    var arrivalTimeAmPm: String?
    var name: String?
    var memo: String?
    var startTimeHour: String?
    var startTimeMin: String?
    var arrivalTimeHour: String?
    var arrivalTimeMin: String?
    var locationId: String?
    var repeatModel: String?
    var needNotify: Bool?
    var teamId: String?
    var addr: String?
    var dayofWeek: String?
    var year: String?
    var month: String?
    var dayOfMonth: String?
    var durationHour: String?
    var durationMin: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "year": self.year!, "month": self.month!, "day_of_month": self.dayOfMonth!, "start_time_hour": self.startTimeHour!, "start_time_min": self.startTimeMin!, "start_time_ampm": self.startTimeAmPm!,
                "arrival_time_hour": self.arrivalTimeHour!, "arrival_time_min": self.arrivalTimeMin!, "arrival_time_ampm": self.arrivalTimeAmPm!, "repeat": self.repeatModel!, "location_id": self.locationId!, "name": self.name!, "memo": self.memo!, "addr": self.addr!, "notify": self.needNotify! ? "1" : "0", "day_of_week": self.dayofWeek!, "duration_hour": self.durationHour!, "duration_min": self.durationMin!]
    }
}

class CreateNewGameReq {
    var token: String?
    var startTimeAmPm: String?, startTimeHour: String?, startTimeMin: String?
    var arrivalTimeAmPm: String?, arrivalTimeHour: String?, arrivalTimeMin: String?
    var year: String?, month: String?, dayOfMonth: String?, dayOfWeek: String?
    var teamLocationId: String?
    var needNotify: Bool?
    var teamId: String?
    var addr: String?
    var opponentId: String?
    var wearColor: String?
    var durationHour: String?, durationMin: String?
    var isHomeTeam: String?
    var memo: String?
    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "year": self.year!, "month": self.month!, "day_of_month": self.dayOfMonth!, "start_time_hour": self.startTimeHour!, "start_time_min": self.startTimeMin!, "start_time_ampm": self.startTimeAmPm!, "arrival_time_hour": self.arrivalTimeHour!, "arrival_time_min": self.arrivalTimeMin!, "arrival_time_ampm": self.arrivalTimeAmPm!, "location_id": self.teamLocationId!, "uniform_color": self.wearColor!, "is_home": self.isHomeTeam!, "memo": self.memo!, "addr": self.addr!, "notify": self.needNotify! ? "1" : "0", "day_of_week": self.dayOfWeek!, "opponent_id": self.opponentId!, "duration_hour": self.durationHour!, "duration_min": self.durationMin!]
    }
}

class CreateHealthCheckReq {
    var token: String?
    var rosterId: String?
    var eventId: String?
    var eventKind: String?
    var status: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "roster_id": self.rosterId!,
                "related_id": self.eventId!,
                "kind": self.eventKind!,
                "status": self.status!]
    }
}

class GetGameScoreReq {
    var token: String?
    var gameId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "game_id": self.gameId!]
    }
}

class UpdateGameScoreReq {
    var token: String?
    var gameId: String?
    var usScore: String?
    var themScore: String?
    var memo: String?
    var flag: Bool?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "game_id": self.gameId!,
                "us_score": "\(self.usScore!)",
                "them_score": "\(self.themScore!)",
                "memo": self.memo!,
                "flag": self.flag! ? "1" : "0"]
    }
}

class GetCalendarByTeamIdReq {
    var token: String?
    var teamId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "roster_id": self.rosterId!]
    }
}

class LoadRosterInCalendarReq {
    var token: String?
    var kind: String?
    var eventId: String?
    var curRosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "practice_game_id": self.eventId!, "kind": self.kind!, "my_roster_id": self.curRosterId!]
    }
}

class CancelGameReq {
    var token: String?
    var pos: Int?
    var gameId: String?
    var notifyTeam: Bool?

    func getDict() -> [String: String] {
        return ["token": self.token!, "game_id": self.gameId!, "notify": self.notifyTeam! ? "1" : "0"]
    }
}

class DeleteCalendarItemReq {
    var token: String?
    var Id: String?
    var kind: String?
    var pos: Int?
    
    var isApplyAll: Bool = false

    func getDict() -> [String: String] {
        return ["token": self.token!, "id": self.Id!, "kind": self.kind!, "is_apply_all": self.isApplyAll.toServerString()]
    }
}

class GetHistoryEventsReq {
    var token: String?
    var memberId: String?
    var page: String?

    func getDict() -> [String: String] {
        return ["token": self.token ?? "",
                "member_id": self.memberId ?? "",
                "page": self.page ?? ""]
    }
}

class CreateAttendaceReq {
    var token: String?
    var eventId: String?
    var rosterId: String?
    var kind: String?
    var status: String?
    var calendarItemId: String?
    var calendarItemType: CALENDAR_ITEM_TYPE?

    func getDict() -> [String: String] {
        return ["token": self.token!, "related_id": self.eventId!, "roster_id": self.rosterId!, "kind": self.kind!, "status": self.status!]
    }
}

class UpdateAttendanceReq {
    var token: String?
    var attendanceId: String?
    var status: String?
    var calendarItemId: String?
    var calendarItemType: CALENDAR_ITEM_TYPE?

    func getDict() -> [String: String] {
        return ["token": self.token!, "id": self.attendanceId!, "status": self.status!]
    }
}

class UpdateAttendanceMultipleReq {
    var token: String?
    var rosters: [String]?
    var calendarItemId: String?
    var calendarItemKind: String?
    var status: String?
    var rosterId: String? = AppDataInstance.instance.currentSelectedTeam?.ID

    func getDict() -> [String: String] {
        var rostersJson = ""
        if let rosters = rosters, rosters.count > 0 {
            let transform = rosters.map { "{\"roster_id\": \($0)}" }.joined(separator: ",")
            rostersJson = "[" + transform + "]"
        }
        return ["token": self.token ?? "",
        "rosters": rostersJson,
        "status": self.status ?? "",
        "related_id": self.calendarItemId ?? "",
        "kind": self.calendarItemKind ?? "",
        "roster_id": self.rosterId ?? ""]
    }
}

class CancelPracticeReq {
    var token: String?
    var practiceId: String?
    var notify: Bool?
    var isApplyAll: Bool = false

    var pos: Int?
    func getDict() -> [String: String] {
        return ["token": self.token!, "practice_id": self.practiceId!, "notify": self.notify! ? "1" : "0", "is_apply_all": self.isApplyAll.toServerString()]
    }
}

class UpdatePracticeReq {
    var originId: String?
    var token: String?
    var startTimeAmPm: String?
    var arrivalTimeAmPm: String?
    var memo: String?
    var startTimeHour: String?
    var startTimeMin: String?
    var arrivalTimeHour: String?
    var arrivalTimeMin: String?
    var locationId: String?
    var repeatModel: String?
    var needNotify: Bool?
    var teamId: String?
    var addr: String?
    var dayofWeek: String?
    var year: String?
    var month: String?
    var dayOfMonth: String?
    var durationHour: String?
    var durationMin: String?
    var name: String?
    
    var isApplyAll: Bool = false
    
    func getDict() -> [String: String] {
        return ["token": self.token!, "id": self.originId!, "name": self.name!, "team_id": self.teamId!, "year": self.year!, "month": self.month!, "day_of_month": self.dayOfMonth!, "start_time_hour": self.startTimeHour!, "start_time_min": self.startTimeMin!, "start_time_ampm": self.startTimeAmPm!,
                "arrival_time_hour": self.arrivalTimeHour!, "arrival_time_min": self.arrivalTimeMin!, "arrival_time_ampm": self.arrivalTimeAmPm!, "repeat": self.repeatModel!, "location_id": self.locationId!, "memo": self.memo!, "addr": self.addr!, "notify": self.needNotify! ? "1" : "0", "day_of_week": self.dayofWeek!, "duration_hour": self.durationHour!, "duration_min": self.durationMin!, "is_apply_all": self.isApplyAll.toServerString()]
    }
}

class UpdateGameReq {
    var token: String?
    var startTimeAmPm: String?, startTimeHour: String?, startTimeMin: String?
    var arrivalTimeAmPm: String?, arrivalTimeHour: String?, arrivalTimeMin: String?
    var year: String?, month: String?, dayOfMonth: String?, dayOfWeek: String?
    var teamLocationId: String?
    var needNotify: Bool?
    var teamId: String?
    var addr: String?
    var opponentId: String?
    var wearColor: String?
    var durationHour: String?, durationMin: String?
    var isHomeTeam: String?
    var memo: String?
    var id: String?
    var name: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "id": self.id!, "team_id": self.teamId!, "name": self.name!, "year": self.year!, "month": self.month!, "day_of_month": self.dayOfMonth!, "start_time_hour": self.startTimeHour!, "start_time_min": self.startTimeMin!, "start_time_ampm": self.startTimeAmPm!, "arrival_time_hour": self.arrivalTimeHour!, "arrival_time_min": self.arrivalTimeMin!, "arrival_time_ampm": self.arrivalTimeAmPm!, "location_id": self.teamLocationId!, "uniform_color": self.wearColor!, "is_home": self.isHomeTeam!, "memo": self.memo!, "addr": self.addr!, "notify": self.needNotify! ? "1" : "0", "day_of_week": self.dayOfWeek!, "opponent_id": self.opponentId!, "duration_hour": self.durationHour!, "duration_min": self.durationMin!]
    }
}
