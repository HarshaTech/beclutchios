//
//  CalendarListDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
enum CALENDAR_ITEM_TYPE {
    case GAME_MODEL
    case PRACTICE_MODEL
    case HEADER_MODEL

    func itemKindString() -> String {
        if self == .GAME_MODEL {
            return "1"
        }

        return "0"
    }
}

class CalendarListDTO {
    var game: GameModel?
    var practice: PracticeModel?
    var month: String?
    var type: CALENDAR_ITEM_TYPE?
    var dayOfMonth: String?
    var hoursOfStart: String?
    var ampmStart: String?
    var id: String?
    var isHistory: Bool?
}
