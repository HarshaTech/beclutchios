//
//  HomeRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ClubTreeLoadRes {
    var result: Bool?
    var rosters: [RosterInfo]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.rosters = []

            if let dict = val!["msg"] as? [NSDictionary] {
                for item_dict in dict {
                    let roster = RosterInfo(val: item_dict)
                    self.rosters?.append(roster)
                }
            }
        } else {
            self.result = false
            self.rosters = []
        }
    }
}

class UpdateDefaultTeamRes {
    var result: Bool?
    var labels: [String: String] = [:]

    init(val: NSDictionary?) {
        if let value = val {
            result = true

            if let dict = value["labels"] as? Dictionary<String, String>{
                labels = dict
            }
        } else {
            result = false
        }
    }
}

class LoadMyRosterRes {
    var result: Bool?
    var rosters: [RosterInfo]?
    var message: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            self.rosters = []
            if let dict = val!["extra"] as? [NSDictionary] {
                for item_dict in dict {
                    let roster = RosterInfo(val: item_dict)
                    self.rosters?.append(roster)
                }
            }
        } else {
            result = false
            rosters  = []
            message = "Database Error"
        }
    }
}

class LoadHomeDashboardRes {
    var result: Bool?
    var message: String?
    var game: GameModel?
    var practice: PracticeModel?
    var reminder: ReminderEntryDTO?
    var logo: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)

            if let dict = val?["extra"] as? NSDictionary {
                if let practiceDicts = dict["practices"] as? NSArray {
                    if let practiceDict = practiceDicts.firstObject as? NSDictionary {
                        self.practice = PracticeModel(val: practiceDict)
                    }
                }

                if let gameDicts = dict["games"] as? NSArray {
                    if let gameDict = gameDicts.firstObject as? NSDictionary {
                        self.game = GameModel(val: gameDict)
                    }
                }

                if let reminderDicts = dict["reminders"] as? NSArray {
                    if let reminderDict = reminderDicts.firstObject as? NSDictionary {
                        self.reminder = ReminderEntryDTO(val: reminderDict)
                    }
                }

                if let logoStr = dict["logo"] as? String, logoStr.count > 10 {
                    self.logo = logoStr
                }
            }
        } else {
            result = false
            message = "Database Error"
            game = nil
            practice = nil
            reminder = nil
            logo = nil
        }
    }
}

class CreateInviteRosterToThisTeamRes {
    var result: Bool?
    var wantedEmail: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.wantedEmail = (val!["extra"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.wantedEmail = false
            self.msg = "API Error"
        }
    }
}

class GeneralRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "API Error"
        }
    }
}

class ScoreRes {
    var result: Bool?
    var score: ScoreModel?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)

            if let dict = val?["score"] as? NSDictionary {
                self.score = ScoreModel(dict: dict)
            }

            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "API Error"
        }
    }

}

class GetResponsibleRes {
    var result: Bool?
    var msg: String?
    var rosters: [RosterInfo]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)

            self.rosters = []
            if let dict = val!["extra"] as? [NSDictionary] {
                for item_dict in dict {
                    let roster = RosterInfo(val: item_dict)
                    self.rosters?.append(roster)
                }
            }
        } else {
            self.result = false
            self.msg = "API Error"
            self.rosters = []
        }
    }
}
