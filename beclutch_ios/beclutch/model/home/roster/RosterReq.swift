//
//  RosterReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation

class LoadRosterInfoReq {
    var token: String?
    var memberId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "roster_id": self.memberId!]
    }
}

class UpdateRosterProfileReq {
    var token: String?
    var email: String?
    var fName: String?
    var lName: String?
    var phoneCell: String?
    var phoneWork: String?
    var phoneHome: String?
    var rosterId: String?
    var uploadFile: Data?

    func getDict() -> [String: String] {
        return ["token": self.token!, "roster_id": self.rosterId!, "f_name": self.fName!, "l_name": self.lName!, "email": self.email!, "phone_cell": self.phoneCell!, "phone_home": self.phoneHome!, "phone_work": self.phoneWork!]
    }
}

class PlayerProfileLoadReq {
    var token: String?
    var playerId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "roster_id": self.playerId!]
    }
}

class UpdatePlayerProfileReq {
    var token: String?
    var email: String?
    var fName: String?
    var lName: String?
    var phoneCell: String?
    var phoneWork: String?
    var phoneHome: String?
    var rosterId: String?
    var uploadFile: Data?
    var playerNo: String?
    func getDict() -> [String: String] {
        return ["token": self.token!, "player_roster_id": self.rosterId!, "player_no": self.playerNo!, "f_name": self.fName!, "l_name": self.lName!, "email": self.email!, "phone_cell": self.phoneCell!, "phone_home": self.phoneHome!, "phone_work": self.phoneWork!]
    }
}

class AddPlayerContactReq {
    var token: String?
    var email: String?
    var fName: String?
    var lName: String?
    var phoneCell: String?
    var phoneWork: String?
    var phoneHome: String?
    var playerId: String?
    var uploadFile: Data?
    var role: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "player_roster_id": self.playerId!, "target_roleId": self.role!, "f_name": self.fName!, "l_name": self.lName!, "email": self.email!, "phone_cell": self.phoneCell!, "phone_home": self.phoneHome!, "phone_work": self.phoneWork!]
    }
}

class UpdatePlayerContactReq {
    var token: String?
    var email: String?
    var fName: String?
    var lName: String?
    var phoneCell: String?
    var phoneWork: String?
    var phoneHome: String?
    var rosterId: String?
    var uploadFile: Data?
    var role: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "f_name": self.fName!, "l_name": self.lName!,
                "email": self.email!,
                "phone_cell": self.phoneCell!, "phone_home": self.phoneHome!, "phone_work": self.phoneWork!,
                "role_id": role!,
                "roster_id": self.rosterId!, "target_roleId": self.role!]
    }
}

class RemoveContactGuestReq {
    var token: String?
    var rosterId: String?
    var playerId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "player_roster_id": self.playerId!, "contactor_id": self.rosterId!]
    }
}

class RemoveRosterFromTeamReq {
    var token: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "roster_id": self.rosterId!]
    }
}
