//
//  RosterRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/30.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation

class LoadRosterInfoRes {
    var result: Bool?
    var roster: RosterInfo?
    var message: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            self.roster = RosterInfo(val: nil)
            if let dict = val!["extra"] as? NSDictionary {
                self.roster = RosterInfo(val: dict)
            }
        } else {
            self.result = false
            self.roster = RosterInfo(val: nil)
            self.message = "Database Error"
        }
    }

}

class UpdateRosterProfileRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class PlayerProfileLoadRes {
    var result: Bool?
    var message: String?
    var player_profile: FullRosterPlayer?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            if let extraDic = (val!["extra"] as? NSDictionary) {
                self.player_profile = FullRosterPlayer(val: extraDic)
            } else {
                self.player_profile = FullRosterPlayer(val: nil)
            }
        } else {
            self.result = false
            self.message = "Database Error"
            self.player_profile = FullRosterPlayer(val: nil)
        }
    }
}

class UpdatePlayerProfileRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class AddPlayerContactRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class UpdatePlayerContactRes {
    var result: Bool?
    var message: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
        } else {
            self.result = false
            self.message = "Database Error"
        }
    }
}

class RemoveContactGuestRes {
    var result: Bool?
    var message: String?
    var player_profile: FullRosterPlayer?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.message = (val!["msg"] as! String)
            if let extraDic = (val!["extra"] as? NSDictionary) {
                self.player_profile = FullRosterPlayer(val: extraDic)
            } else {
                self.player_profile = FullRosterPlayer(val: nil)
            }
        } else {
            self.result = false
            self.message = "Database Error"
            self.player_profile = FullRosterPlayer(val: nil)
        }
    }
}
