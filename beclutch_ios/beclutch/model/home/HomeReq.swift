//
//  HomeReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ClubTreeLoadReq {
    var token: String?

    func getDict() -> [String: String] {
        return ["token": self.token!]
    }
}

class UpdateDefaultTeamReq {
    var token: String?
    var memberId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "selectMemberId": self.memberId!]
    }
}

class LoadMyRosterReq {
    var token: String?
    var currentTeamClubMemberId: String?
    var targetLevel: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "selectMemberId": self.currentTeamClubMemberId!, "targetLevelId": self.targetLevel!]
    }
}

class LoadHomeDashboardReq {
    var token: String?
    var memberId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "member_id": self.memberId!]
    }
}

class CreateInviteRosterToThisTeamReq {
    var token: String?
    var fname: String?
    var lname: String?
    var email: String?
    var roleTitle: String?
    var spotTitle: String?
    var teamName: String?
    var roleId: String?
    var sportId: String?
    var clubId: String?
    var teamId: String?
    var sendMail: Bool?
    var isPlayer: Bool = false

    func getDict() -> [String: String] {
        return ["token": self.token!, "fname": self.fname!, "lname": self.lname!, "email": self.email!, "role_id": self.roleId!, "team_name": self.teamName!, "sport": self.sportId!, "sport_title": self.spotTitle!, "club_id": self.clubId!, "team_id": self.teamId!, "send_mail": self.sendMail! ? "1" : "0", "role_title": self.roleTitle!, "is_player": self.isPlayer.toServerString()]
    }
}

class ResendInviteRosterEmailReq {
    var token: String?
    var userId: String?
    var memberId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "user_id": self.userId!, "member_id": self.memberId!]
    }
}

class GetResponsibleReq {
    var token: String?
    var memberId: String?

    func getDict() -> [String: String] {
        return ["token": self.token ?? "",
                "member_id": self.memberId ?? ""]
    }
}
