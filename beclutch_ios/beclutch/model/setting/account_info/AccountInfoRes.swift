//
//  AccountInfoRes.swift
//  beclutch
//
//  Created by FAT_MAC on 7/31/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class AccountInfoRes {
    var result: Bool?
    var updateResult: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
        } else {
            self.result = false
        }
    }
}
