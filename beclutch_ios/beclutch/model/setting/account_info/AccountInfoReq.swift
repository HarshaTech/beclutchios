//
//  AccountInfoReq.swift
//  beclutch
//
//  Created by FAT_MAC on 7/31/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class AccountInfoReq {
    var token: String?
    var fname: String?
    var lname: String?
    var phone: String?
    var addr: String?
    var city: String?
    var state: String?
    var zip: String?

    func getDict() -> [String: String] {
        return ["token": token!, "fname": self.fname!, "lname": self.lname!, "phone": self.phone!, "addr": self.addr!, "city": self.city!, "state": self.state!, "zip": self.zip!]
    }
}
