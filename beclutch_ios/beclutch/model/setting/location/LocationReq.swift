//
//  Req.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LocationLoadReq {
    var token: String?
    func getDict() -> [String: String] {
        return ["token": token!]
    }
}

class LocationAddReq {
    var token: String?
    var name: String?
    var country_idx: String?
    var country: String?
    var state_idx: String?
    var state: String?
    var city: String?
    var street: String?
    var zip: String?
    var link: String?
    var note: String?
    var lat: String?
    var lot: String?

    func getDict() -> [String: String] {
        return ["token": token!, "name": name!, "country_idx": country_idx!, "country": country!, "state_idx": state_idx!, "state": state!, "city": city!, "street": street!, "zip": zip!, "link": link!, "note": note!, "lat": lat!, "lot": lot!]
    }
}

class LocationUpdateReq {
    var token: String?
    var id: String?
    var name: String?
    var country_idx: String?
    var country: String?
    var state_idx: String?
    var state: String?
    var city: String?
    var street: String?
    var zip: String?
    var link: String?
    var note: String?
    var lat: String?
    var lot: String?

    func getDict() -> [String: String] {
        return ["token": token!, "id": id!, "name": name!, "country_idx": country_idx!, "country": country!, "state_idx": state_idx!, "state": state!, "city": city!, "street": street!, "zip": zip!, "link": link!, "note": note!, "lat": lat!, "lot": lot!]
    }

}
