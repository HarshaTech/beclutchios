//
//  Res.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LocationLoadRes {

    var result: Bool?
    var locationList: [TeamLocation]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.locationList = []
            if let oppsDict = val!["msg"] as? [NSDictionary] {
                for oppo in oppsDict {
                    let item = TeamLocation(val: oppo)
                    self.locationList?.append(item)
                }
            }
        } else {
            self.result = false
            self.locationList = []
        }
    }
}

class LocationAddRes {
    var result: Bool?
    var msg: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}

class LocationUpdateRes {
    var result: Bool?
    var msg: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
