//
//  SurveyReq.swift
//  beclutch
//
//  Created by NamViet on 5/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

enum ReminderFrequencyEnum: Int {
    case never = 0
    case daily
    case twoXDaily
    case lastDayHourly

    static func titles() -> [String] {
        return ["Never", "Daily", "2X Daily", "Last Day Hourly"]
    }
}

class SurveyAddReq {
    var token: String?
    var teamId: String?
    var name: String?
    var desc: String?
    var question: String?
    var answers: [String]?

    var isMultiple: Bool?
    var isAnonymous: Bool = false
    var reminderFreq: Int?
    var startDate: Date?
    var endDate: Date?

    var isAllPlayers: Bool = false
    var groupSelectionResult: GroupSelectionResult?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "team_id": self.teamId!,
                "name": self.name!,
                "desc": self.desc!,
                "question": self.question!,
                "answers": [String].toJsonString(array: answers)!,
            "is_multiple": self.isMultiple!.toServerString(),
            "is_anonymous": self.isAnonymous.toServerString(),
            "reminder_freq": String(self.reminderFreq!),
            "start_date": self.startDate!.dateSeverFormat(),
            "end_date": self.endDate!.dateSeverFormat(),
            "to_all_players": self.isAllPlayers ? "1" : "0",
            "to_members": self.groupSelectionResult!.toJsonString()]
    }
}

class SurveySubmitReq {
    var token: String?
    var teamId: String?
    var rosterId: String?

    var surveyId: String?
    var questionId: String?

    var vote_answers: [SurveyAnswerDetail]?

    func getDict() -> [String: String] {
        let voteAnswers = SurveyAnswerDetail.toJson(rosterAnswers: vote_answers)

        return ["token": self.token!,
                "team_id": self.teamId!,
                "survey_id": self.surveyId!,
                "question_id": self.questionId!,
                "roster_id": self.rosterId!,
                "vote_answers": voteAnswers]
    }
}

class SurveyMultipleSubmitReq {
    var token: String?
    var teamId: String?
    var rosters: [String] = []
    
    var surveyId: String?
    var questionId: String?

    var vote_answers: [SurveyAnswerDetail]?

    func getDict() -> [String: String] {
        let voteAnswers = SurveyAnswerDetail.toJson(rosterAnswers: vote_answers)
        
        var rostersJson = ""
        if rosters.count > 0 {
            let transform = rosters.map { "{\"roster_id\": \($0)}" }.joined(separator: ",")
            rostersJson = "[" + transform + "]"
        }

        return ["token": self.token!,
                "team_id": self.teamId!,
                "survey_id": self.surveyId!,
                "question_id": self.questionId!,
                "rosters": rostersJson,
                "vote_answers": voteAnswers]
    }
}


class SurveyAnswerDetail {
    var id: String?
    var text: String?
    var optionalText: String = ""
    var count: Int = 0

    var isSelected = false

    init(val: NSDictionary?) {
        if let dict = val {
            self.id = dict["ID"] as? String
            self.text = dict["TEXT"] as? String
            if let text = dict["OPTIONAL_TEXT"] as? String {
                self.optionalText = text
            }

            if let countStr = dict["Count"] as? String, let count = Int(countStr) {
                self.count = count
            }
        }
    }

    func updateData(rosterAnswer: SurveyAnswerDetail?) {
        if let answer = rosterAnswer {
            self.isSelected = true
            self.optionalText = answer.optionalText
        }
    }

    func getString() -> String {
        return "{\"id\": \(id!), \"optional_text\": \"\(optionalText)\"}"
    }

    static func toJson(rosterAnswers: [SurveyAnswerDetail]?) -> String {
        guard let rosterAnswers = rosterAnswers, rosterAnswers.count > 0 else {
            return ""
        }

        var result = "["

        for i in 0 ..< rosterAnswers.count {
            let answer = rosterAnswers[i]
            result += answer.getString()

            if i != rosterAnswers.count - 1 {
                result += ","
            }
        }

        result += "]"

        return result
    }

    static func parseData(array: [NSDictionary], rosterAnswer: [SurveyAnswerDetail]?) -> [SurveyAnswerDetail] {
        var result: [SurveyAnswerDetail] = []

        for dict in array {
            let item = SurveyAnswerDetail(val: dict)
            let rosterAnswer = rosterAnswer?.first(where: { $0.id == item.id })
            item.updateData(rosterAnswer: rosterAnswer)

            result.append(item)
        }

        return result
    }
}

class SurveyTeamGetReq {
    var token: String?
    var teamId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "team_id": self.teamId!,
                "roster_id": self.rosterId!]
    }
}

class SurveyGetResponseDetailReq {
    var token: String?
    var surveyId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "survey_id": self.surveyId!]
    }
}

class SurveyDeleteReq {
    var token: String?
    var surveyId: String?
    var teamId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "survey_id": self.surveyId!,
        "team_id": self.surveyId!,
        "roster_id": self.surveyId!]
    }
}
