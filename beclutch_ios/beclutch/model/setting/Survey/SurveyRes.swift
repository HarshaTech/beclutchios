//
//  SurveyRes.swift
//  beclutch
//
//  Created by NamViet on 5/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class SurveyTeamGetRes {
    var result: Bool?
    var extraMsg: String?

    var teamSurveys: [TeamSurvey]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let surveyArr = dict["extra"] as? [NSDictionary] {
                self.teamSurveys = TeamSurvey.parseData(array: surveyArr)
            }
        }
    }
}

class TeamSurvey {
    var id: String?
    var name: String?
    var questionId: String?
    var questionText: String?
    var response: Int = 0
    var teamMemberCount: Int = 0

    var startDate: Date?
    var endDate: Date?

    var isAnonymous: Bool = false
    var isMultiple: Bool = false
    var answerDetails: [SurveyAnswerDetail] = []
    var rosterAnswerDetails: [SurveyAnswerDetail] = []

    var isResponse: Bool {
        return rosterAnswerDetails.count > 0
    }

    init(val: NSDictionary?) {
        if let dict = val {
            self.id = dict["ID"] as? String
            self.name = dict["NAME"] as? String
            self.questionId = dict["QUESTION_ID"] as? String
            self.questionText = dict["QUESTION_TEXT"] as? String

            if let responseStr = dict["RESPONSE"] as? String, let responseInt = Int(responseStr) {
                self.response = responseInt
            }

            if let countInt = dict["TEAM_MEMBER_COUNT"] as? Int {
                self.teamMemberCount = countInt
            }

            if let dateStr = dict["START_DATE"] as? String {
                self.startDate = Date.dateServerFromString(dateStr: dateStr)
            }

            if let dateStr = dict["END_DATE"] as? String {
                self.endDate = Date.dateServerFromString(dateStr: dateStr)
            }

            if let anonymous = dict["IS_ANONYMOUS"] as? String {
                self.isAnonymous = Bool.boolFromString(str: anonymous)
            }

            if let multiple = dict["IS_MULTIPLE"] as? String {
                self.isMultiple = Bool.boolFromString(str: multiple)
            }

            if let array = dict["ROSTER_ANSWER_DETAIL"] as? [NSDictionary] {
                self.rosterAnswerDetails = SurveyAnswerDetail.parseData(array: array, rosterAnswer: nil)
            }

            if let array = dict["ANSWER_DETAIL"] as? [NSDictionary] {
                self.answerDetails = SurveyAnswerDetail.parseData(array: array, rosterAnswer: rosterAnswerDetails)
            }
        }
    }

    static func parseData(array: [NSDictionary]) -> [TeamSurvey] {
        var result: [TeamSurvey] = []

        for dict in array {
            let item = TeamSurvey(val: dict)
            result.append(item)
        }

        return result
    }
}

class SurveyGetResponseDetailRes {
    var result: Bool?
    var extraMsg: String?
    var surveyRosterResponses: [SurveyRosterResponse]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let responseArr = dict["extra"] as? [NSDictionary] {
                self.surveyRosterResponses = SurveyRosterResponse.parseData(array: responseArr)
            }
        }
    }
}

class SurveyRosterResponse {
    var answerText: String?
    var answerOptionText: String?
    var rosterFirstName: String?
    var rosterLastName: String?

    var fullName: String {
        return (rosterFirstName ?? "") + " " + (rosterLastName ?? "")
    }

    init(val: NSDictionary?) {
        if let dict = val {
            self.answerText = dict["ANSWER_TEXT"] as? String
            self.answerOptionText = dict["ANSWER_OPTION_TEXT"] as? String
            self.rosterFirstName = dict["ROSTER_FIRST_NAME"] as? String
            self.rosterLastName = dict["ROSTER_LAST_NAME"] as? String
        }
    }

    static func parseData(array: [NSDictionary]) -> [SurveyRosterResponse] {
        var result: [SurveyRosterResponse] = []

        for dict in array {
            let item = SurveyRosterResponse(val: dict)
            result.append(item)
        }

        return result
    }
}
