//
//  FinanceReq.swift
//  beclutch
//
//  Created by Trinh Vu on 2/28/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class FeeAddReq {
    var token: String?
    var rosterId: String?
    var teamId: String?
    var isAllPlayers: Bool = false
    var groupSelectionResult: GroupSelectionResult?
    var dueDate: Date?
    var amount: String?
    var description: String?
    var needNotify: Bool?

    func getDict() -> [String: String] {
        return ["token": self.token!,
                "team_id": self.teamId!,
                "roster_id": self.rosterId!,
                "to_all_players": self.isAllPlayers ? "1" : "0",
                "to_members": self.groupSelectionResult!.toJsonString(),
                "due_date": "\(Int(self.dueDate!.timeIntervalSince1970))",
                "due_amount": amount!,
                "description": description!,
                "notify": self.needNotify! ? "1" : "0"]
    }
}

class LeaderAddReq {
    var token: String?
    var rosterId: String?
    var teamId: String?
    var isAllPlayers: Bool = false
    var groupSelectionResult: GroupSelectionResult?
    var startDate: String?
    var endDate: String?
    var title: String?
    var activityType: String?
    var activityName: String?
    var frequency: String?
    var needNotify: Bool?

    func getDict() -> [String: String] {
        
        if isAllPlayers == true {
            return ["token": self.token!,
                    "team_id": self.teamId!,
                    "roster_id": self.rosterId!,
                    "to_all_players": self.isAllPlayers ? "1" : "0",
                    "end_date": "\(endDate ?? "")",
                    "start_date": "\(startDate ?? "")",
                    "name": title!,
                    "reminder_freq": "\(frequency!)",
                    "edit_all": self.needNotify! ? "1" : "0",
                    "type": "\(activityType!)",
                    "type_colname": "\(activityName!)"]
        } else {
            return ["token": self.token!,
                    "team_id": self.teamId!,
                    "roster_id": self.rosterId!,
                    "to_all_players": self.isAllPlayers ? "1" : "0",
                    "to_members": self.groupSelectionResult!.toJsonString(),
                    "end_date": "\(endDate ?? "")",
                    "start_date": "\(startDate ?? "")",
                    "name": title!,
                    "reminder_freq": "\(frequency!)",
                    "edit_all": self.needNotify! ? "1" : "0",
                    "type": "\(activityType!)",
                    "type_colname": "\(activityName!)"]

        }
        
        

    }
}

class LeaderListReq {
    var token: String?
    var teamId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!]
    }
}

class CompetitionRemoveReq {
    var token: String?
    var leaderboardId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "leaderboard_id": leaderboardId!]
    }
}

class CompetitionTeamReq {
    var token: String?
    var leaderboardId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "leaderboard_id": leaderboardId!]
    }
}

class UpdateScoreReq {
    var token: String?
    var leaderboardid: String?
    var score: String?
    var roasterid: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "leaderboard_id": self.leaderboardid!, "roster_id": roasterid!, "score": score!]
    }
}



class FinanceTeamReq {
    var token: String?
    var teamId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "roster_id": self.rosterId!]
    }
}

class FinanceRosterReq {
    var token: String?
    var teamId: String?
    var rosterId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "roster_id": self.rosterId!]
    }
}

class FinanceFeeRemovesReq {
    var token: String?
    var feeId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "finance_id": feeId!]
    }
}

class FinanceFeeDetailRemoveReq {
    var token: String?
    var feeId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "finance_detail_id": feeId!]
    }
}

class FinancePayAddReq {
    var token: String?
    var dueId: String?
    var amount: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "finance_detail_id": self.dueId!, "amount": self.amount!]
    }
}
