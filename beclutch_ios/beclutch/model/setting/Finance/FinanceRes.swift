//
//  FinanceRes.swift
//  beclutch
//
//  Created by Trinh Vu on 2/28/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class FeeAddRes {
    var result: Bool?
    var extraMsg: String?
    var fees: [FeeInfo]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let feeArr = dict["extra"] as? [NSDictionary] {
                self.fees = FeeInfo.parseData(array: feeArr)
            }
        }
    }
}

class FinanceTeamRes {
    var result: Bool?
    var extraMsg: String?
    var fees: [FeeInfo]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let feeArr = dict["extra"] as? [NSDictionary] {
                self.fees = FeeInfo.parseData(array: feeArr)
            }
        }
    }
}

class FinanceRosterRes {
    var result: Bool?
    var extraMsg: String?
    var fees: [FeeInfo]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let feeArr = dict["extra"] as? [NSDictionary] {
                self.fees = FeeInfo.parseData(array: feeArr)
            }
        }
    }
}

class FinanceFeeRemovesRes {
    var result: Bool?
    var extraMsg: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

        }
    }
}

class FinancePayAddRes {
    var result: Bool?
    var extraMsg: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String
        }
    }
}

class AddCompetitionRes {
    var result: Bool?
    var extraMsg: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String
        }
    }
}

class LeaderListRes {
    var result: Bool?
    var extraMsg: String?
    var fees: [LeaderBoardInfo]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let feeArr = dict["extra"] as? [NSDictionary] {
                self.fees = LeaderBoardInfo.parseData(array: feeArr)
            }
        }
    }
}

class TeamListRes {
    var result: Bool?
    var extraMsg: String?
    var fees: [LeaderBoardTeamListInfo]?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String

            if let feeArr = dict["extra"] as? [NSDictionary] {
                self.fees = LeaderBoardTeamListInfo.parseData(array: feeArr)
            }
        }
    }
}

class UpdateScoreRes {
    var result: Bool?
    var extraMsg: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.result = dict["result"] as? Bool
            self.extraMsg = dict["msg"] as? String
        }
    }
}
