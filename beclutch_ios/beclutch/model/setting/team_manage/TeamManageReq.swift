//
//  TeamManageReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/10.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LoadTimeZoneReq {
    var token: String?
    func getDict() -> [String: String] {
        return ["token": token!]
    }
}

class TeamInfoUpdateReq {
    var token: String?
    var name: String?
    var sport: String?
    var timezone: String?
    var durationHour: String?
    var durationMin: String?
    var arriveHour: String?
    var arriveMin: String?
    var homeColor: String?
    var awayColor: String?
    var alterColor1: String?
    var alterColor2: String?
    var teamId: String?
    var memberId: String?

    var isSelect: String?
    var colorValue: String?
    var colorName: String?
    var colorResourcePrimary: String?
    var colorResourceDark: String?
    var themeResource: String?

    var fileData: Data?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_name": self.name!, "sport": self.sport!, "timezone": self.timezone!, "practice_hour": self.durationHour!, "practice_min": self.durationMin!, "arrive_hour": self.arriveHour!, "arrive_min": self.arriveMin!, "home_color": self.homeColor!, "away_color": self.awayColor!, "alter_color_1": self.alterColor1!, "alter_color_2": self.alterColor2!, "team_id": self.teamId!, "member_id": self.memberId!, "is_select": self.isSelect!, "color_value": self.colorValue!, "color_name": self.colorName!,
            "color_resource_primary": self.colorResourcePrimary!, "color_resource_dark": self.colorResourceDark!, "theme_resource": self.themeResource!]
    }
}

class TeamPlanUpdateReq {
    var token: String?
    var team_id: String?
    var plan: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.team_id!, "plan": self.plan!]
    }
}

class TeamSupportReq {
    var token: String?
    var teamId: String?
    var fName: String?
    var lName: String?
    var subject: String?
    var content: String?
    var version: String = ""

    func getDict() -> [String: String] {
        return ["token": self.token!, "team_id": self.teamId!, "user_fname": self.fName!, "user_lname": self.lName!, "subject": self.subject!, "content": self.content!, "version": self.version]
    }
}
