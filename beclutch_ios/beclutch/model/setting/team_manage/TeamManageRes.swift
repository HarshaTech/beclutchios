//
//  TeamManageRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/10.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LoadTimeZoneRes {
    var result: Bool?
    var timezones: [TimeZoneInfo]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.timezones = []
            if let timezoneDic = val!["msg"] as? [NSDictionary] {
                for zone in timezoneDic {
                    let item = TimeZoneInfo(val: zone)
                    self.timezones?.append(item)
                }
            }
        } else {
            self.result = false
            self.timezones = []
        }
    }
}

class TeamInfoUpdateRes {
    var result: Bool?
    var extraMsg: String?
    var currentTeam: RosterInfo?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.extraMsg = (val!["extra"] as! String)
            if let cur = val!["msg"] as? NSDictionary {
                self.currentTeam = RosterInfo(val: cur)
            } else {
                self.currentTeam = RosterInfo(val: nil)
            }
        } else {
            result = false
            extraMsg = "Database Error"
            currentTeam = RosterInfo(val: nil)
        }
    }
}

class TeamPlanUpdateRes {
    var result: Bool?
    var plan: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            if let planStr = val!["msg"] as? String {
                self.plan = planStr
            } else {
                self.plan = "\(val!["msg"] as! Int)"
            }
        } else {
            result = false
            plan = "-1"
        }
    }
}

class TeamSupportRes {
    var result: Bool?
    var msg: String?
    init(val: NSDictionary?) {
        if val != nil {
            self.result  = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            result = false
            msg = "Database Error"
        }
    }
}
