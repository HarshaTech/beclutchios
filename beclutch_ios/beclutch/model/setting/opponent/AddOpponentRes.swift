//
//  AddOpponentRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
public class AddOpponentRes {
    var result: Bool?
    var opponentResult: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.opponentResult = (val!["msg"] as! String)
        } else {
            self.result = false
            self.opponentResult = "Database Error"
        }
    }
}

public class UpdateOpponentRes {
    var result: Bool?
    var opponentResult: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.opponentResult = (val!["msg"] as! String)
        } else {
            self.result = false
            self.opponentResult = "Database Error"
        }
    }
}
