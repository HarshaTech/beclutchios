//
//  AddOpponentReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class AddOpponentReq {
    var token: String?
    var name: String?
    var contactName: String?
    var contactPhone: String?
    var contactEmail: String?
    var contactNote: String?

    func getDict() -> [String: String] {
        return ["token": token!, "name": self.name!, "contact_name": self.contactName!, "contact_phone": self.contactPhone!, "contact_mail": self.contactEmail!, "note": self.contactNote!]
    }
}

class UpdateOpponentReq {
    var token: String?
    var id: String?
    var name: String?
    var contactName: String?
    var contactPhone: String?
    var contactEmail: String?
    var contactNote: String?

    func getDict() -> [String: String] {
        return ["token": token!, "name": self.name!, "id": id!, "contact_name": self.contactName!, "contact_phone": self.contactPhone!, "contact_mail": self.contactEmail!, "note": self.contactNote!]
    }
}
