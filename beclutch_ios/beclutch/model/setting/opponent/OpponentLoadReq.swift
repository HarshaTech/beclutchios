//
//  OpponentLoadReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class OpponentLoadReq {
    var token: String?
    func getDict() -> [String: String] {
        return ["token": token!]
    }
}
