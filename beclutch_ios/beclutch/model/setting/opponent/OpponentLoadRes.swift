//
//  OpponentLoadRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class OpponentLoadRes {

    var result: Bool?
    var opponentList: [OpponentModel]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.opponentList = []
            if let oppsDict = val!["msg"] as? [NSDictionary] {
                for oppo in oppsDict {
                    let item = OpponentModel(val: oppo)
                    self.opponentList?.append(item)
                }
            }
        } else {
            self.result = false
            self.opponentList = []
        }
    }
}
