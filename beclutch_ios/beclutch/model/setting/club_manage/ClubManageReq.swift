//
//  ClubManageReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ClubInfoUpdateReq {
    var token: String?
    var club_name: String?
    var sport: String?
    var clubId: String?
    var memberId: String?
    var fileData: Data?

    func getDict() -> [String: String] {
        return ["token": self.token!, "club_name": self.club_name!, "sport": self.sport!, "club_id": self.clubId!, "member_id": self.memberId!]
    }
}

class LoadTeamsInClubReq {
    var token: String?
    var clubId: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "club": self.clubId!]
    }
}

class CreateInviteTeamToClubReq {
    var token: String?
    var clubId: String?
    var teamId: String?
    var email: String?
    var fname: String?
    var lname: String?
    var roleId: String?
    var roleTitle: String?
    var sendEMail: Bool?
    var sportId: String?
    var teamName: String?
    var sportTitle: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "fname": self.fname!, "lname": self.lname!, "email": self.email!, "role_id": self.roleId!, "team_name": self.teamName!, "sport": self.sportId!, "sport_title": self.sportTitle!, "club_id": self.clubId!, "team_id": self.teamId!, "send_mail": self.sendEMail! ? "1":"0", "role_title": self.roleTitle!]
    }
}

class LoadMyClubReq {
    var token: String?
    var sport: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "sport_item": self.sport!]
    }
}
