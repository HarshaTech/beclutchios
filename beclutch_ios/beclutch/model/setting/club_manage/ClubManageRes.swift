//
//  ClubManageRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/19.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LoadTeamsInClubRes {
    var result: Bool?
    var extraMsg: String?
    var teams: [RosterInfo]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.extraMsg = (val!["msg"] as! String)
            self.teams = [RosterInfo]()
            if let curTeamsDict = val!["extra"] as? [NSDictionary] {
                for team_item in curTeamsDict {
                    let team = RosterInfo(val: team_item)
                    self.teams!.append(team)
                }
            } else {
                self.teams = [RosterInfo]()
            }
        } else {
            self.result = false
            self.extraMsg = "Database Error"
            self.teams = [RosterInfo]()
        }
    }
}

class CreateInviteTeamToClubRes {
    var result: Bool?
    var msg: String?
    var sendmail: Bool?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.sendmail = (val!["extra"] as! Bool)
        } else {
            self.result = false
            self.msg = ""
            self.sendmail = false
        }
    }
}

class LoadMyClubRes {
    var result: Bool?
    var clubTree: [RosterInfo]?

    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            self.clubTree = []
            if let treeDict = val!["msg"] as? [NSDictionary] {
                for item in treeDict {
                    let team = RosterInfo(val: item)
                    self.clubTree?.append(team)
                }
            }
        } else {
            self.result = false
            self.clubTree = []
        }
    }
}
