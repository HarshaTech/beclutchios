//
//  ThemeColorModel.swift
//  beclutch
//
//  Created by FAT_MAC on 8/1/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import UIKit

public struct ThemeColorModel {
    var isSelect: Bool?
    var colorValue: String?
    var colorName: String?
    var colorResourcePrimary: UIColor?
    var colorResourceDark: UIColor?
    var themeResource: Int?

    init(isSelect: Bool, colorValue: String, colorName: String, colorResourcePrimary: UIColor, colorResourceDark: UIColor, themeResource: Int) {
        if colorValue != "" {
            self.isSelect = isSelect
            self.colorValue = colorValue
            self.colorName = colorName
            self.colorResourcePrimary = colorResourcePrimary
            self.colorResourceDark = colorResourceDark
            self.themeResource = themeResource
        } else {
            self.isSelect = false
            self.colorValue = "beclutch"
            self.colorName = "BeClutch"
            self.colorResourcePrimary = UIColor.COLOR_PRIMARY_BECLUTCH
            self.colorResourceDark = UIColor.COLOR_PRIMARY_DARK_BECLUTCH
            self.themeResource = 0
        }
    }

}
