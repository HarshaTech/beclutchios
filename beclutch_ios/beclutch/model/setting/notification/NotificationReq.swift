//
//  Req.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
public class NotificationLoadReq {
    var token: String?
    func getDict() -> [String: String] {
        return ["token": token!]
    }
}

public class NotificationUpdateReq {
    var token: String?
    var id: String?
    var sch_app: String?
    var sch_mail: String?
    var chat_app: String?
    var chat_mail: String?
    var score_app: String?
    var score_mail: String?
    var reminder_app: String?
    var reminder_mail: String?
    var tour_app: String?
    var tour_mail: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "id": self.id!, "sch_app": self.sch_app!, "sch_mail": self.sch_mail!, "chat_app": self.chat_app!, "chat_mail": self.chat_mail!, "score_app": self.score_app!, "score_mail": self.score_mail!, "reminder_app": self.reminder_app!, "reminder_mail": self.reminder_mail!, "tour_app": self.tour_app!, "tour_mail": self.tour_mail!]
    }
}
