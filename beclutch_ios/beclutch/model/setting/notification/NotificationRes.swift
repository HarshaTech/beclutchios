//
//  Res.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
public class NotificationLoadRes {
    var result: Bool?
    var setting: NotificationSetting?
    init(val: NSDictionary?) {
        if val != nil {
            self.result = (val!["result"] as! Bool)
            if let notify_set = (val!["msg"] as? NSDictionary) {
                self.setting = NotificationSetting(val: notify_set)
            } else {
                self.setting = NotificationSetting(val: nil)
            }
        } else {
            self.result = false
            self.setting = nil
        }
    }
}

public class NotificationUpdateRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result  = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
