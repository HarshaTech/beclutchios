//
//  CreateSectionReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class CreateClubInSettingReq {
    var token: String?
    var club_name: String?
    var sport_id: String?

    func getDict() -> [String: String] {
        return ["token": token!, "club_name": club_name!, "sport_item": sport_id!]
    }
}

class CreateTeamInSettingReq {
    var token: String?
    var team_name: String?
    var sport_id: String?
    var club_id: String?
    var role: String?

    func getDict() -> [String: String] {
        return ["token": token!, "team_name": team_name!, "sport_item": sport_id!, "club_id": club_id!, "role": role!]
    }
}
