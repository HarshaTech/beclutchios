//
//  CreateSectionRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/8.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class CreateClubInSettingRes {
    var result: Bool?
    var msg: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.result  = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
class CreateMyTeamInSettingRes {
    var result: Bool?
    var msg: String?
    var labels: [String: String]?

    init(val: NSDictionary?) {
        if let value = val {
            self.result  = (val!["result"] as! Bool)
            self.msg = (val!["msg"] as! String)
            self.labels = value["labels"] as? [String: String]
        } else {
            self.result = false
            self.msg = "Database Error"
        }
    }
}
