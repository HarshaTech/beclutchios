//
//  ApiConstants.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
struct ApiConstants {
//    static let BASE_URL = "http://chunglv.sierralingo.com/"

    static let register_email_url = ApiConstants.BASE_URL + "api/auth/register"
    static let register_email_verify_submit = ApiConstants.BASE_URL + "api/auth/register_email_verify"
    static let register_send_email = ApiConstants.BASE_URL + "api/auth/send_email"
    static let load_productions = ApiConstants.BASE_URL + "api/auth/productions"
    static let register_create_club = ApiConstants.BASE_URL + "api/auth/create_club_team"
    static let register_invite_accept = ApiConstants.BASE_URL + "api/auth/invite_accept"
    static let register_user_info = ApiConstants.BASE_URL + "api/auth/reg_userinfo"
    static let update_user_info = ApiConstants.BASE_URL + "api/auth/update_userinfo"

    static let forgot_reset_email = ApiConstants.BASE_URL + "api/auth/password_reset_email"
    static let forgot_reset_confirm = ApiConstants.BASE_URL + "api/auth/password_reset_confirm"
    static let forgot_change_password = ApiConstants.BASE_URL + "api/auth/change_password"

    static let login = ApiConstants.BASE_URL + "api/auth/login"
    static let login_by_token = ApiConstants.BASE_URL + "api/auth/login_by_token"

    static let setting_oppo_list = ApiConstants.BASE_URL + "api/setting/get_opponents"
    static let setting_oppo_add = ApiConstants.BASE_URL + "api/setting/create_opponent_info"
    static let setting_oppo_update = ApiConstants.BASE_URL + "api/setting/update_opponent_info"

    static let setting_location_list = ApiConstants.BASE_URL + "api/setting/get_locations"
    static let setting_location_add = ApiConstants.BASE_URL + "api/setting/create_location_info"
    static let setting_location_update = ApiConstants.BASE_URL + "api/setting/update_location_info"

    static let setting_notification_read = ApiConstants.BASE_URL + "api/setting/read_notify_setting"
    static let setting_notification_update = ApiConstants.BASE_URL + "api/setting/insert_update_notify"
    static let setting_load_timezone = ApiConstants.BASE_URL + "api/setting/load_time_zone"
    static let setting_update_team = ApiConstants.BASE_URL + "api/setting/update_team_info"
    static let setting_update_club = ApiConstants.BASE_URL + "api/setting/update_club_info"
    static let setting_update_plan = ApiConstants.BASE_URL + "api/setting/update_plans"
    static let setting_add_support = ApiConstants.BASE_URL + "api/setting/add_support"
    static let setting_load_teams_in_club = ApiConstants.BASE_URL + "api/setting/get_my_teams_inclub"

    static let setting_create_myclub = ApiConstants.BASE_URL + "api/create/create_my_club"
    static let setting_create_myteam = ApiConstants.BASE_URL + "api/create/create_my_team"
    static let setting_create_invite_manager = ApiConstants.BASE_URL + "api/create/create_invite_team"
    static let setting_load_teams_add_club = ApiConstants.BASE_URL + "api/create/load_teams_add_club"

    static let home_load_clubtree = ApiConstants.BASE_URL + "api/create/get_club_trees"
    static let home_update_default_team = ApiConstants.BASE_URL + "api/home/update_default"
    static let home_invite_accept = ApiConstants.BASE_URL + "api/create/accept_invite"
    static let home_invite_roster_to_team = ApiConstants.BASE_URL + "api/create/invite_roster"

    static let home_load_my_roster = ApiConstants.BASE_URL + "api/home/my_roster"
    static let home_load_dashboard = ApiConstants.BASE_URL + "api/home/get_dashboard"

    static let roster_load_common_information = ApiConstants.BASE_URL + "api/rosters/get_common_profile"
    static let roster_load_player_information = ApiConstants.BASE_URL + "api/rosters/get_full_profile"
    static let roster_update_common_profile = ApiConstants.BASE_URL + "api/rosters/update_common_profile"
    static let roster_update_player_profile = ApiConstants.BASE_URL + "api/rosters/update_player_profile"
    static let roster_add_player_contact = ApiConstants.BASE_URL + "api/rosters/add_contact_player"
    static let roster_update_player_contact = ApiConstants.BASE_URL + "api/rosters/update_contact_profile"
    static let roster_remove_player_contact = ApiConstants.BASE_URL + "api/rosters/delete_relation"
    static let roster_remove_from_team      = ApiConstants.BASE_URL + "api/rosters/remove_roster_team"
    static let roster_resend_email_invitation = ApiConstants.BASE_URL + "/api/create/resend_invite"
    static let roster_get_responsible = ApiConstants.BASE_URL + "/api/rosters/get_responsible"

    static let calendar_create_practice = ApiConstants.BASE_URL + "api/calendar/create_new_practice"
    static let calendar_create_game = ApiConstants.BASE_URL + "api/calendar/create_new_game"
    static let calendar_create_health_check = ApiConstants.BASE_URL + "api/calendar/create_healthcheck"

    static let calendar_get_team_id = ApiConstants.BASE_URL + "api/calendar/get_calendar_by_team"
    static let calendar_rosters_for_event = ApiConstants.BASE_URL + "api/calendar/get_rosters_for_event"
    static let calendar_cancel_game = ApiConstants.BASE_URL + "api/calendar/cancel_game"
    static let calendar_update_game = ApiConstants.BASE_URL + "api/calendar/update_game"
    static let calendar_cancel_practice = ApiConstants.BASE_URL + "api/calendar/cancel_practice"
    static let calendar_update_practice = ApiConstants.BASE_URL + "api/calendar/update_practice"
    static let calendar_delete_game_practice = ApiConstants.BASE_URL + "api/calendar/remove_practice_game"
    static let calendar_get_history_events = ApiConstants.BASE_URL + "api/home/get_history_events"

    static let calendar_get_game_score = ApiConstants.BASE_URL + "/api/calendar/get_game_score"
    static let calendar_update_game_score = ApiConstants.BASE_URL + "/api/calendar/update_game_score"

    static let calendar_create_attendance = ApiConstants.BASE_URL + "api/calendar/create_attendance"
    static let calendar_update_attendance = ApiConstants.BASE_URL + "api/calendar/update_attendance"
    static let calendar_update_attendance_multiple = ApiConstants.BASE_URL + "api/calendar/update_attendance_multiple"
    static let calendar_update_healthcheck_multiple = ApiConstants.BASE_URL + "api/calendar/update_healthcheck_multiple"

    static let chat_reminder_send = ApiConstants.BASE_URL + "api/chat/send_reminder"
    static let chat_reminder_load = ApiConstants.BASE_URL + "api/chat/get_reminders"
    static let chat_reminder_remove = ApiConstants.BASE_URL + "api/chat/remove_reminder"
    static let update_reminder_read = ApiConstants.BASE_URL + "api/chat/update_reminder_read"

    static let send_notification_to_team = ApiConstants.BASE_URL + "api/chat/send_notification_to_team"
    static let send_email_to_team = ApiConstants.BASE_URL + "api/chat/send_email_to_team"

    static let chat_conversation_add = ApiConstants.BASE_URL + "api/chat/add_conversation"
    static let chat_conversation_remove = ApiConstants.BASE_URL + "api/chat/remove_conversation"
    static let chat_conversation_update = ApiConstants.BASE_URL + "api/chat/update_conversation_group"
    static let chat_conversation_entry_load = ApiConstants.BASE_URL + "api/chat/load_conversation_entries"
    static let chat_conversation_check_individual = ApiConstants.BASE_URL + "api/chat/check_individual_conversation"

    static let chat_team_image_media_send = ApiConstants.BASE_URL + "api/chat/upload_team_image"

    static let chat_conversation_text_send = ApiConstants.BASE_URL + "api/chat/send_conversation_text"
    static let chat_team_text_send = ApiConstants.BASE_URL + "api/chat/send_team_text"
    static let chat_carpool_text_send = ApiConstants.BASE_URL + "api/chat/send_carpool_text"

    static let finance_add_fee = ApiConstants.BASE_URL + "api/fee/create_finance"
    static let finance_team_get = ApiConstants.BASE_URL + "api/fee/get_finance_team"
    static let finance_roster_get = ApiConstants.BASE_URL + "api/fee/get_finance_team_roster"
    static let finance_remove_finance = ApiConstants.BASE_URL + "api/fee/remove_finance"
    static let finance_remove_finance_detail = ApiConstants.BASE_URL + "api/fee/remove_finance_detail"
    static let finance_add_pay = ApiConstants.BASE_URL + "api/fee/create_finance_paid"
    static let finance_get_pay = ApiConstants.BASE_URL + "api/fee/get_finance_paid"

    static let survey_add_new = ApiConstants.BASE_URL + "api/survey/create_survey"
    static let survey_submit_roster_response = ApiConstants.BASE_URL + "api/survey/submit"
    static let survey_submit_multiple_roster_response = ApiConstants.BASE_URL + "api/survey/submit_multiple"
    static let survey_get_team_survey = ApiConstants.BASE_URL + "api/survey/get_team_survey"
    static let survey_get_detail_response = ApiConstants.BASE_URL + "api/survey/get_survey_response_detail"
    static let survey_delete = ApiConstants.BASE_URL + "api/survey/delete"
    
    static let addCompetition = ApiConstants.BASE_URL + "api/leaderboard/create_leaderboard"
    static let getLeaderboardList = ApiConstants.BASE_URL + "api/leaderboard/get_leaderboard_list"
    static let deleteLeaderBoard = ApiConstants.BASE_URL + "api/leaderboard/delete_leaderboard"
    static let getLeaderBoardTeamList = ApiConstants.BASE_URL + "api/leaderboard/get_team_leaderboard"
    static let updateScoreAPI = ApiConstants.BASE_URL + "api/leaderboard/update_leaderboard_score"
}
