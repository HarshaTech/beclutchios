//
//  ThemeConstants.swift
//  beclutch
//
//  Created by rabbit_mac on 6/11/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
struct ThemeConstants {
    let COLOR_PRIMARY: String = "#7FB522"
    let COLOR_PRIMARY_DARK: String = "#6A9128"
    let COLOR_ACCENT: String = "#c80000"
}
