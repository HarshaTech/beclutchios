//
//  RegisterCostants.swift
//  beclutch
//
//  Created by zeus on 2019/11/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
struct RegisterConstants {
    static let REGISTER_COMPLETE: Int = 10
    static let REGISTER_NOT_START_YET: Int = -1

    static let REGISTER_SETP_START: Int = 0
    static let REGISTER_EMAIL_CODE_VERIFY: Int = 1
    static let REGISTER_REG_USER_INFO: Int = 2
    static let REGISTER_ROLE_CHOOSE: Int = 3
    static let REGISTER_INVITATION_CODE: Int = 4
    static let REGISTER_CLUB_INFO: Int = 5
    static let REGISTER_TEAM_INFO: Int = 6
}

struct UserRoles {
    static let EVERYONE: String = "0"
    static let CLUB_OWNER: String = "1"
    static let TEAM_COACH: String = "2"
    static let TEAM_MANAGER: String = "3"
    static let PLAYER: String = "6"
    static let TEAM_TREASURE: String = "8"
    static let CLUB_TREASURER: String = "9"
    static let PARENT: String = "10"
    static let OTHER: String = "11"
    static let GUEST: String = "12"
}
