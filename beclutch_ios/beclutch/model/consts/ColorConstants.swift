//
//  ColorConstants.swift
//  beclutch
//
//  Created by rabbit_mac on 6/11/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
struct ColorConstants {
    static let COLOR_PRIMARY: String = "#7FB522"
    static let COLOR_PRIMARY_DARK: String = "#6A9128"
    static let COLOR_ACCENT: String = "#c80000"

    static let COLOR_PRIMARY_BECLUTCH: String = "#7FB522"
    static let COLOR_PRIMARY_DARK_BECLUTCH: String = "#6A9128"

    static let COLOR_PRIMARY_BLUE: String = "#2C68CD"
    static let COLOR_PRIMARY_DARK_BLUE: String = "#1D4589"

    static let COLOR_PRIMARY_DARKBLUE: String = "#294DB5"
    static let COLOR_PRIMARY_DARK_DARKBLUE: String = "#142556"

    static let COLOR_PRIMARY_LIGHTBLUE: String = "#C6E3EF"
    static let COLOR_PRIMARY_DARK_LIGHTBLUE: String = "#add8eb"

    static let COLOR_PRIMARY_LIME: String = "#56DF16"
    static let COLOR_PRIMARY_DARK_LIME: String = "#4CBB17"

    static let COLOR_PRIMARY_GREEN: String = "#02BC63"
    static let COLOR_PRIMARY_DARK_GREEN: String = "#00713B"

    static let COLOR_PRIMARY_ORANGE: String = "#FAB83E"
    static let COLOR_PRIMARY_DARK_ORANGE: String = "#FFa500"

    static let COLOR_PRIMARY_BLACK: String = "#222222"
    static let COLOR_PRIMARY_DARK_BLACK: String = "#040404"

    static let COLOR_PRIMARY_DARKRED: String = "#EA2A20"
    static let COLOR_PRIMARY_DARK_DARKRED: String = "#B32119"

    static let COLOR_PRIMARY_RED: String = "#FB4646"
    static let COLOR_PRIMARY_DARK_RED: String = "#FF0000"

    static let COLOR_PRIMARY_MAROON: String = "#D10202"
    static let COLOR_PRIMARY_DARK_MAROON: String = "#800000"

    static let COLOR_PRIMARY_GOLD: String = "#FBC749"
    static let COLOR_PRIMARY_DARK_GOLD: String = "#FBB816"

    static let COLOR_PRIMARY_YELLOW: String = "#FADA3D"
    static let COLOR_PRIMARY_DARK_YELLOW: String = "#FFD500"

    static let COLOR_PRIMARY_DARKGREEN: String = "#02A802"
    static let COLOR_PRIMARY_DARK_DARKGREEN: String = "#006400"

    static let COLOR_PRIMARY_PURPLE: String = "#8D47D3"
    static let COLOR_PRIMARY_DARK_PURPLE: String = "#663399"

    static let COLOR_PRIMARY_GREY: String = "#D1CFCF"
    static let COLOR_PRIMARY_DARK_GREY: String = "#a9a9a9"

    static let COLOR_PRIMARY_DARKGREY: String = "#9A9A9A"
    static let COLOR_PRIMARY_DARK_DARKGREY: String = "#5d5d5d"

    static let COLOR_PRIMARY_MIDNIGHT: String = "#3548B3"
    static let COLOR_PRIMARY_DARK_MIDNIGHT: String = "#192256"

    static let COLOR_PRIMARY_FUSCIA: String = "#FD59FD"
    static let COLOR_PRIMARY_DARK_FUSCIA: String = "#FF00FF"

    static let COLOR_PRIMARY_BROWN: String = "#BC7D3F"
    static let COLOR_PRIMARY_DARK_BROWN: String = "#654321"

}
