//
//  ForgotPasswordResetEmailVerifyResult.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ForgotPasswordResetEmailVerifyResult {
    var result: Bool?
    var msg: String?
    var isActive: Bool?

    init() {
        result = false
        msg = "Network Error"
    }

    init(val: NSDictionary?) {
        if let value = val {
            result = (value["result"] as! Bool)
            msg = (value["msg"] as! String)
            isActive = value["active"] as? Bool
        } else {
            result = false
            msg = "Network Error"
        }
    }
}
