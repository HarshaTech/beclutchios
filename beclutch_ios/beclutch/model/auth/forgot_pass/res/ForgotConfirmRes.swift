//
//  ForgotConfirmRes.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ForgotConfirmRes {
    var result: Bool?
    var msg: String?

    init() {
        result = false
        msg = "Network Error"
    }

    init(val: NSDictionary?) {
        if val != nil {
            result = (val!["result"] as! Bool)
            msg = (val!["msg"] as! String)
        } else {
            result = false
            msg = "Network Error"
        }
    }
}
