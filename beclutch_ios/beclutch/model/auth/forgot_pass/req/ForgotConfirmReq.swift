//
//  ForgotConfirmReq.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class ForgotConfirmReq {
    var pass: String?
    var token: String?

    func getDict() -> [String: String] {
        return ["token": self.token!, "new_pass": self.pass!]
    }
}
