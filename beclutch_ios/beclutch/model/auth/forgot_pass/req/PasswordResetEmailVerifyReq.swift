//
//  PasswordResetEmailVerifyReq.swift
//  beclutch
//
//  Created by zeus on 2019/11/28.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class PasswordResetEmailVerifyReq {
    var email: String?
    func getDict() -> [String: String] {
        return ["email": email!]
    }
}
