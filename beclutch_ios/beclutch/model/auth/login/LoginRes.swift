//
//  LoginRes.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation

class LoginRes {
    var result: Bool?
    var msg: String?
    var isActive: Bool?
    var user: UserInfo?
    var clubs: [RosterInfo]?
    var default_member: RosterInfo?
    var roles: [Role]?
    var labels: [String: String] = [:]

    init(val: NSDictionary?) {
        if let value = val {
            result = (val!["result"] as! Bool)
            msg = (val!["msg"] as! String)
            
            if let active = value["active"] as? Int {
                isActive = active != 0
            }

            if !result! {
                return
            }

            if let user_dict = val!["user"] as? NSDictionary {
                user = UserInfo(val: user_dict)
            }

            if let club_dicts = val!["clubs"] as? [NSDictionary] {
                clubs = []
                for club_item in club_dicts {
                    let club = RosterInfo(val: club_item)
                    clubs?.append(club)
                }
            }

            if let default_member_dic = val!["default_member"] as? NSDictionary {
                default_member = RosterInfo(val: default_member_dic)
            } else {
                default_member = RosterInfo(val: nil)
            }

            if let role_dicts = val!["roles"] as? [NSDictionary] {
                roles = []
                for role_item in role_dicts {
                    let role = Role(val: role_item)
                    roles?.append(role)
                }
            }

            if let labelDict = value["labels"] as? [String: String] {
                self.labels = labelDict
            }
        } else {
            result = false
            msg = "Errors occured on server. Try again."
        }
    }
}
