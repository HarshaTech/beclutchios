//
//  loginReq.swift
//  beclutch
//
//  Created by zeus on 2019/11/16.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class LoginReq {
    var email: String?
    var pwd: String?
    var fbToken: String?

    func getDict() -> [String: String] {
        return ["email": email!, "pwd": pwd!, "fb_token": fbToken!, "platform": CommonUtils.appVersionAndPlatform, "ip": CommonUtils.getWiFiAddress()]
    }
}
