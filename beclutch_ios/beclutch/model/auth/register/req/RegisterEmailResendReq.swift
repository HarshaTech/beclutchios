//
//  RegisterEmailResendReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterEmailResendReq {
    var email: String?
    var token: String?

    func getDict() -> [String: String] {
        return ["email": self.email!, "token": self.token!]
    }
}
