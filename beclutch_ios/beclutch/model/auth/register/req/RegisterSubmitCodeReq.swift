//
//  RegisterSubmitCodeReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterSubmitCodeReq {
    var code: String!=""

    func getDict() -> [String: String] {
        return ["code": self.code!]
    }
}
