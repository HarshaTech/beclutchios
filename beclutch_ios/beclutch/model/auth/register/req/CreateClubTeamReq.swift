//
//  CreateClubTeamReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class CreateClubTeamReq {
    var name: String?
    var sportId: String?
    var isTeam: String?
    var token: String?

    func getDict() -> [String: String] {
        return ["token": token!, "name": name!, "sport": sportId!, "kind": isTeam!]
    }
}
