//
//  RegisterEmailRequest.swift
//  beclutch
//
//  Created by zeus on 2019/11/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterEmailRequest {
    var email: String?
    var pwd: String?
    var firebaseToken: String?

    func getDict() -> [String: String] {
        return ["email": self.email!, "password": self.pwd!, "fb_token": self.firebaseToken!]
    }
}
