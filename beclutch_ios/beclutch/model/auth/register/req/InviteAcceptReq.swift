//
//  InviteAcceptReq.swift
//  beclutch
//
//  Created by zeus on 2019/12/3.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class InviteAcceptReq {
    var token: String?
    var code: String?
    func getDict() -> [String: String] {
        return ["token": token!, "invite_code": code!]
    }
}
