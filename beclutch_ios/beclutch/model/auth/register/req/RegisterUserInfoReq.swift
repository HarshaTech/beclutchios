//
//  RegisterUserInfoReq.swift
//  beclutch
//
//  Created by zeus on 2019/11/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterUserInfoReq {
    var token: String = ""
    var fName: String = ""
    var lName: String = ""
    var addr: String = ""
    var state: String = ""
    var city: String = ""
    var zip: String = ""
    var phone: String = ""

    func getDict() -> [String: String] {
        return ["token": token, "fname": fName, "lname": lName, "phone": phone, "addr": addr, "city": city, "state": state, "zip": zip]
    }
}
