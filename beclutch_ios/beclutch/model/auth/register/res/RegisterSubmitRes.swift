//
//  RegisterSubmitRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterSubmitRes {
    var result: Bool?
    var msg: String?
    var token: String?

    init() {
        result = false
        msg = "Network Error"

    }

    init(val: NSDictionary?) {
        if let value = val {
            result = (val!["result"] as! Bool)
            msg = (val!["msg"] as! String)
            
            if let token = value["token"] as? String {
                self.token = token
            }
        } else {
            result = false
            msg = "Network Error"
        }
    }
}
