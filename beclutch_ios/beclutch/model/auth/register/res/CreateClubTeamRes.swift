//
//  CreateClubTeamRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class CreateClubTeamRes {
    var result: Bool
    var msg: String
    var userInfo: UserInfo
    var clubDtos: [RosterInfo]
    var defaultLevel: RosterInfo
    var roles: [Role]

    init() {
        result = false
        msg = "Network Error"

        userInfo = UserInfo(val: nil)
        clubDtos = []
        self.defaultLevel = RosterInfo(val: nil)
        self.roles = []
    }

    init(val: NSDictionary?) {
        if val != nil {
            self.result = val!["result"] as! Bool
            self.msg = val!["msg"] as! String

            self.userInfo = UserInfo(val: (val!["user"] as! NSDictionary))

            self.clubDtos = []
            if let clubDtos_dic = (val!["clubs"] as? NSDictionary) {
                for i in 0..<clubDtos_dic.count {
                    self.clubDtos.append(RosterInfo(val: (clubDtos_dic[i] as! NSDictionary)))
                }
            }

            self.defaultLevel = RosterInfo(val: (val!["default_member"] as! NSDictionary))

            self.roles = []
            if let roles_dic = (val!["roles"] as? NSDictionary) {
                for i in 0..<roles_dic.count {
                    self.roles.append(Role(val: (roles_dic[i] as! NSDictionary)))
                }
            }
        } else {
            result = false
            msg = "Network Error"

            userInfo = UserInfo(val: nil)
            clubDtos = []
            self.defaultLevel = RosterInfo(val: nil)
            self.roles = []
        }
    }
}
