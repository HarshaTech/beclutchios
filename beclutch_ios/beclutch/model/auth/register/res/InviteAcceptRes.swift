import Foundation
class InviteAccpetRes {
    var result: Bool
    var msg: String
    var userInfo: UserInfo = UserInfo(val: nil)
    var clubDtos: [RosterInfo]
    var defaultLevel: RosterInfo = RosterInfo(val: nil)
    var roles: [Role]
    var teamId: String = ""

    init() {
        result = false
        msg = "Network Error"

        userInfo = UserInfo(val: nil)
        clubDtos = []
        self.defaultLevel = RosterInfo(val: nil)
        self.roles = []
    }

    init(val: NSDictionary?) {
        if let value = val {
            self.result = value["result"] as! Bool
            self.msg = value["msg"] as! String

            if let userValue = value["user"] as? NSDictionary {
                self.userInfo = UserInfo(val: userValue)
            }

            self.clubDtos = []
            if let clubDtos_dic = (value["clubs"] as? NSDictionary) {
                for i in 0..<clubDtos_dic.count {
                    self.clubDtos.append(RosterInfo(val: (clubDtos_dic[i] as! NSDictionary)))
                }
            }

            if let defaultMemberInfo = value["default_member"] as? NSDictionary {
                self.defaultLevel = RosterInfo(val: defaultMemberInfo)
            }

            self.roles = []
            if let roles_dic = (value["roles"] as? NSDictionary) {
                for i in 0..<roles_dic.count {
                    self.roles.append(Role(val: (roles_dic[i] as! NSDictionary)))
                }
            }
            
            if let teamId = value["teamId"] as? String {
                self.teamId = teamId
            }
        } else {
            result = false
            msg = "Network Error"

            userInfo = UserInfo(val: nil)
            clubDtos = []
            self.defaultLevel = RosterInfo(val: nil)
            self.roles = []
        }
    }
}
