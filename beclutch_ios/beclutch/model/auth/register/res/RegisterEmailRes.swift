//
//  RegisterEmailRes.swift
//  beclutch
//
//  Created by zeus on 2019/11/29.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterEmailRes {
    var result: Bool?
    var msg: String?
    var isInvited: String?
    init() {
        result = false
        msg = "Network Error"
        isInvited = "0"
    }

    init(val: NSDictionary?) {
        if val != nil {
            result = (val!["result"] as! Bool)
            msg = (val!["msg"] as! String)
            if let invitedString = val!["extra"] as? String {
                isInvited = invitedString
            } else {
                isInvited = "\(val!["extra"] as! Int)"
            }

        } else {
            result = false
            msg = "Network Error"
            isInvited = "0"
        }
    }
}
