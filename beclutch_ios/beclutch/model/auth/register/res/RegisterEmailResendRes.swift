//
//  RegisterEmailResendRes.swift
//  beclutch
//
//  Created by zeus on 2019/12/2.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class RegisterEmailResendRes {
    var result: Bool?

    init() {
        result = false
    }

    init(val: NSDictionary?) {
        if val != nil {
            result = (val!["result"] as! Bool)
        } else {
            result = false
        }
    }
}
