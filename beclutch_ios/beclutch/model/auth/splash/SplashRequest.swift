//
//  SplashRequest.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class SplashRequest: Encodable {
    init(token: String, fb_token: String) {
        self.token  = token
        self.fb_token = fb_token
    }
    let token: String
    let fb_token: String

    func getDict() -> [String: String] {
        return ["token": self.token, "fb_token": self.fb_token, "platform": CommonUtils.appVersionAndPlatform, "ip": CommonUtils.getWiFiAddress()]
    }
}
