//
//  Attendance.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class Attendance {
    static let ATTENDING_YES: String = "1"
    static let ATTENDING_NO: String = "0"
    static let ATTENDING_MAYBE: String = "2"

    var KIND: String?
    var ATTENDTING_DECISION: String?
    var GAME_PRACTICE_ID: String?
    var ROSTER_ID: String?
    var ID: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.ID = (val!["ID"] as! String)
            self.KIND = (val!["KIND"] as! String)
            self.ATTENDTING_DECISION = (val!["ATTENDING_DECISION"] as! String)
            self.GAME_PRACTICE_ID = (val!["GAME_PRACTICE_ID"] as! String)
            self.ROSTER_ID = (val!["ROSTER_ID"] as! String)
        } else {
            self.ID = ""
            self.ROSTER_ID = ""
            self.ATTENDTING_DECISION  = ""
            self.KIND = ""
        }
    }
}
