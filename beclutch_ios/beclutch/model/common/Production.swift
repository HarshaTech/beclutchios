//
//  Production.swift
//  beclutch
//
//  Created by zeus on 2019/11/16.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
struct Production: Decodable {
    var ID: String = ""
    
    var NAME: String? = nil
    var ROLE_NAME: String? = nil
    
    var displayName: String? {
        return NAME ?? ROLE_NAME
    }
    
    init() {
    }
    
    init(val: NSDictionary?) {
        if val != nil {
            ID = val!["ID"] as! String
            NAME = val!["NAME"] as! String?
        } else {
            ID = ""
            NAME = ""
        }
    }

}
