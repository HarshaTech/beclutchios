//
//  RepeatModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/7.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class RepeatModel {
    var type: String?
    var chooseDate: Date?
    var tillDate: String?
    var dailyInt: [Int] = []
    var formattedTillDate: String {
//        if let components = tillDate?.components(separatedBy: "-"), components.count == 3 {
//            return components[1] + "-" + components[2] + "-" + components[0]
//        }
//
//        return ""
        return tillDate ?? ""
    }

    static let REPEAT_TYPE_NO_REPEAT: String = "0"
    static let REPEAT_TYPE_DAILY_REPEAT: String = "5"
    static let REPEAT_TYPE_WEEKLY_REPEAT: String = "1"
    static let REPEAT_TYPE_MONTHLY_REPEAT: String = "2"
    static let REPEAT_TYPE_YEARLY_REPEAT: String = "3"
    static let REPEAT_TYPE_CUSTOM_REPEAT: String = "4"

    init(val: NSDictionary?) {
        if let value = val {
            if let strType = value["type"] as? String {
                self.type = strType
            } else {
                self.type = "\(value["type"] as! Int)"
            }

            self.tillDate = (value["till_date"] as! String)

            if let daily = value["daily"] as? String {
                dailyInt = [String].fromCommaString(str: daily).map { Int($0) ?? -1 }
            }
        } else {
            self.type = RepeatModel.REPEAT_TYPE_NO_REPEAT
            self.tillDate = ""
        }
    }

    func toJson() -> String {
        var dic = ["type": self.type!, "till_date": self.tillDate!]

        if type == RepeatModel.REPEAT_TYPE_DAILY_REPEAT {
            dic["daily"] = [String].toJsonString(array: dailyInt)
        }

        let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: [])
        guard let jsonText = String(data: jsonData, encoding: .utf8) else { return "{\"type\":\"\",\"till_date\":\"\"}" }
        return jsonText
    }
    
    func isRepeatEvent() -> Bool {
        return type != RepeatModel.REPEAT_TYPE_NO_REPEAT
    }
}
