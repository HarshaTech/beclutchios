//
//  ConversationEntryInListDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class ConverstaionEntryInListDTO {
    var readDTO: ChatEntryReadDTO?
    var entryDTO: ConversationEntryDTO?
}

class ConversationEntryDTO {
    static let CONVERSATION_TYPE_GROUP = "1"
    var id: String?
    var strGroup: String?
    var idArr: String?
    var type: String?
    var rosterIds: [Int]?
    var groupSelection: GroupSelectionResult?
    var lastChatInfo: ChatEntryReadDTO?

    init(val: NSDictionary?) {
        if val != nil {
            self.id = (val!["ID"] as! String)
            self.strGroup  = (val!["USERS_IN_CONVERSE"] as! String)
            self.idArr = (val!["ROSTER_ID_ARR"] as! String)
            self.type = (val!["TYPE"] as! String)
            self.makeRosterIds()

            if self.strGroup!.isEmpty {
                self.groupSelection = GroupSelectionResult(val: nil)
            } else {
                self.groupSelection = GroupSelectionResult(val: nil)
                let data = self.strGroup!.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                        self.groupSelection = GroupSelectionResult(val: jsonArray as NSDictionary)
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
        } else {
            self.id = ""
            self.strGroup = ""
            self.idArr = ""
            self.type = ""
            self.rosterIds  = []
            self.groupSelection = GroupSelectionResult(val: nil)
        }
    }

    func makeRosterIds() {
        if self.rosterIds == nil {
            self.rosterIds = []
        } else {
            let data = self.idArr!.data(using: .utf8)!
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Int] {
                    self.rosterIds = jsonArray
                } else {
                    self.rosterIds = []
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
}

extension ConversationEntryDTO: Comparable {
  static func == (lhs: ConversationEntryDTO, rhs: ConversationEntryDTO) -> Bool {
    return lhs.id == rhs.id
  }

  static func < (lhs: ConversationEntryDTO, rhs: ConversationEntryDTO) -> Bool {
    if let leftId = Int(lhs.id ?? ""), let rightId = Int(rhs.id ?? "") {
        return leftId < rightId
    }
    return false
  }

}

class GroupSelectionResult {
    var dto: [GroupChatUserEntity]?
    var isSelelctAll: Bool?
    var isSelelctPlayers: Bool = false

    init(val: NSDictionary?) {
        if val != nil {
            if let strVal = val!["all_select"] as? String {
                self.isSelelctAll = strVal == "true"
            } else if let boolVal = val!["all_select"] as? Bool {
                self.isSelelctAll = boolVal
            } else {
                self.isSelelctAll = false
            }

            if let strVal = val!["all_players"] as? String {
                self.isSelelctPlayers = strVal == "true"
            }

            self.dto = []
            if let valDic = val!["users"] as? [NSDictionary] {
                for item in valDic {
                    let dtoItem = GroupChatUserEntity(val: item)
                    self.dto?.append(dtoItem)
                }
            }
        } else {
            self.isSelelctPlayers = true
            self.isSelelctAll = false
            self.dto = []
        }
    }

    func toJsonString() -> String {
        var dic: [String: Any] = [:]
        dic["all_select"] = self.isSelelctAll! ? true : false
        dic["all_players"] = self.isSelelctPlayers ? true : false
        var arrDTO: [NSDictionary] = []
        for item in self.dto! {
            arrDTO.append(item.getDict() as NSDictionary)
        }
        dic["users"] = arrDTO

        let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)!
        return json
    }
}

class GroupChatResult {
    var rosters: [GroupChatUserEntity] = []

    init(val: NSDictionary?) {
        if val != nil {
            self.rosters = []
            if let valDic = val?["users"] as? [NSDictionary] {
                for item in valDic {
                    let dtoItem = GroupChatUserEntity(val: item)
                    self.rosters.append(dtoItem)
                }
            }
        }
    }
    
    init(selectionResult: GroupSelectionResult) {
        self.rosters = selectionResult.dto ?? []
    }

    func toJsonString() -> String {
        var dic: [String: Any] = [:]
        
        var arrDTO: [NSDictionary] = []
        
        for item in self.rosters {
            arrDTO.append(item.getDict() as NSDictionary)
        }
        dic["users"] = arrDTO

        let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)!
        return json
    }
}


class GroupChatUserEntity {
    var rosterId: String?
    var rosterName: String?

    var conversationDisplayName: String {
        if AppDataInstance.instance.me?.DEFAULT_MEMBER_LOG == rosterId && rosterId != nil {
            return "You"
        } else {
            return self.rosterName ?? ""
        }
    }

    init(val: NSDictionary?) {
        if val != nil {
            if let id = val!["roster_id"] as? String {
                self.rosterId = id
            } else if let id = val!["roster_id"] as? Int {
                self.rosterId = String(id)
            }

            self.rosterName = (val!["name"] as! String)
        } else {
            self.rosterId  = ""
            self.rosterName = ""
        }
    }

    init(rosterId: String?, rosterName: String?) {
        self.rosterId = rosterId
        self.rosterName = rosterName
    }

    func getDict() -> [String: String] {
        return ["roster_id": self.rosterId!, "name": self.rosterName!]
    }
}
