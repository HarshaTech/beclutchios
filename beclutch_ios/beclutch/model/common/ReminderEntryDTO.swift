//
//  ReminderEntryDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/13.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class ReminderEntryDTO {
    var id: String?
    var strToRosters: String?
    var senderFirstName: String?
    var senderLastName: String?
    var expirationLong: Int?
    var subject: String?
    var content: String?
    var readStatusStr: String?
    var readStatusArr: [Int]?
    var createdAt: Int?

    init(val: NSDictionary?) {
        if val != nil {
            self.id = (val!["ID"] as! String)
            self.strToRosters = (val!["TO_ROSTERS"] as! String)
            self.senderLastName = (val!["SENDER_LAST_NAME"] as! String)
            self.senderFirstName = (val!["SENDER_FIRST_NAME"] as! String)
            if let strExpirationLong = val!["EXPIRATION_DATE"] as? String {
                self.expirationLong = Int(strExpirationLong)
            } else {
                self.expirationLong = (val!["EXPIRATION_DATE"] as! Int)
            }
            self.subject = (val!["SUBJECT"] as! String)
            self.content = (val!["CONTENT"] as! String)
            self.readStatusStr = (val!["READ_STATUS"] as! String)
            self.makeReadStatus()
            if let strCreatedAt = val!["CREATED_AT"] as? String {
                self.createdAt = Int(strCreatedAt)
            } else {
                self.createdAt = (val!["CREATED_AT"] as! Int)
            }
        } else {
            self.id = ""
            self.strToRosters = ""
            self.senderFirstName = ""
            self.senderLastName = ""
            self.expirationLong = 0
            self.subject = ""
            self.content = ""
            self.readStatusStr = ""
            self.readStatusArr = []
            self.createdAt = 0
        }
    }

    func makeReadStatus() {
        if self.readStatusStr!.isEmpty {
            self.readStatusArr = []
        } else {
            let data = self.readStatusStr!.data(using: .utf8)!
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Int] {
                    self.readStatusArr = jsonArray
                } else {
                    self.readStatusArr = []
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
