//
//  TimeZoneInfo.swift
//  beclutch
//
//  Created by zeus on 2019/12/10.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class TimeZoneInfo {
    var id: String?
    var zoneName: String = ""
    var zoneOffset: String = ""

    init(val: NSDictionary?) {
        if val != nil {
            self.id = (val!["ID"] as! String)
            self.zoneName = (val!["TIME_ZONE_NAME"] as! String)
            self.zoneOffset = (val!["TIME_ZONE_OFF_SET"] as! String)
        } else {
            self.id = ""
            self.zoneName = ""
            self.zoneOffset = ""
        }
    }
}
