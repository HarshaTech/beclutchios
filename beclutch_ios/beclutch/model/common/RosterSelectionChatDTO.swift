//
//  RosterSelectionChatDTO.swift
//  beclutch
//
//  Created by zeus on 2020/1/12.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class RosterSelectionChatDTO {
    var roster: RosterInfo?
    var isSel: Bool?
}
