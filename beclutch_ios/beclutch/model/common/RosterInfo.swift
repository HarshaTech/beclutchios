//
//  RosterInfo.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation

enum GroupType {
    case club
    case team
}

struct RosterInfo {
    var ID: String
    var CLUB_OR_TEAM: String
    var CLUB_ID: String
    var TEAM_ID: String
    var USER_ID: String
    var LEVEL: String
    var ROSTER_FIRST_NAME: String
    var ROSTER_LAST_NAME: String
    var CLUB_TEAM_INFO: ClubTeamSimpleDesc
    var PRIMARY_CONTACT: PrimaryContact
    var USER_INFO: UserInfo
    var ROLE_INFO: Role
    var SPORT_INFO: Production
    var IS_ACTIVE: String = "0"
    var IS_PLAYER: String?
    var SUB_TEAMS: String?
    var ROSTER_EMAIL: String
    var ROSTER_WORK_PHONE: String
    var ROSTER_CELL_PHONE: String
    var ROSTER_HOME_PHONE: String
    var ROSTER_PROFILE_PIC: String
    var ADDITION_INFO_ROSTER: String

    var teamOrClubId: String {
        if TEAM_ID != "0" {
            return TEAM_ID
        }

        return CLUB_ID
    }
    
    var rosterIdInt: Int {
        return Int(ID) ?? 0
    }
    
    var teamIdInt: Int {
        return Int(TEAM_ID) ?? 0
    }

    var isRepresented: Bool {
        return IS_PLAYER == "1"
    }

    var displayName: String {
        if isRepresented {
            return PRIMARY_CONTACT.PRIMARY_FIRST_NAME + " " + PRIMARY_CONTACT.PRIMARY_LAST_NAME
        }

        return ROSTER_FIRST_NAME + " " + ROSTER_LAST_NAME
    }

    var teamName: String {
        return CLUB_TEAM_INFO.NAME
    }
    
    var isClub: Bool {
        return CLUB_OR_TEAM == "1"
    }
    
    func getGroupType() -> GroupType {
        return CLUB_OR_TEAM == "0" ? .team : .club
    }

    init(val: NSDictionary?) {

        if val != nil {
            ID = val!["ID"] as! String
            CLUB_OR_TEAM = val!["CLUB_OR_TEAM"] as! String
            CLUB_ID = val!["CLUB_ID"] as! String
            TEAM_ID = val!["TEAM_ID"] as! String
            USER_ID = val!["USER_ID"] as! String
            LEVEL = val!["LEVEL"] as! String
            ROSTER_FIRST_NAME = val!["ROSTER_FIRST_NAME"] as! String
            ROSTER_LAST_NAME = val!["ROSTER_LAST_NAME"] as! String
            CLUB_TEAM_INFO = ClubTeamSimpleDesc(val: val!["CLUB_TEAM_INFO"] as? NSDictionary)
            PRIMARY_CONTACT = PrimaryContact(val: val!["PRIMARY_CONTACT"] as? NSDictionary)
            USER_INFO = UserInfo(val: val!["USER_INFO"] as? NSDictionary)
            ROLE_INFO = Role(val: val!["ROLE_INFO"] as? NSDictionary)
            SPORT_INFO = Production(val: val!["SPORT_INFO"] as? NSDictionary)
            IS_ACTIVE = (val!["IS_ACTIVE"] as! String)
            IS_PLAYER = (val!["IS_PLAYER"] as! String)

            if let subs = val!["SUB_TEAMS"] as? String {
                SUB_TEAMS = (subs)
            } else if let subs = val!["SUB_TEAMS"] as? Int {
                SUB_TEAMS = "\(subs)"
            } else {
                SUB_TEAMS = "0"
            }

            ROSTER_EMAIL = val!["ROSTER_EMAIL"] as! String
            ROSTER_WORK_PHONE = val!["ROSTER_WORK_PHONE"] as! String
            ROSTER_CELL_PHONE = val!["ROSTER_CELL_PHONE"] as! String
            ROSTER_HOME_PHONE = val!["ROSTER_HOME_PHONE"] as! String
            ROSTER_PROFILE_PIC = val!["ROSTER_PROFILE_PIC"] as! String
            ADDITION_INFO_ROSTER = val!["ADDITION_INFO_ROSTER"] as! String
        } else {
            ID = ""
            CLUB_OR_TEAM = ""
            CLUB_ID = ""
            TEAM_ID = ""
            USER_ID = ""
            LEVEL = ""
            ROSTER_FIRST_NAME = ""
            ROSTER_LAST_NAME = ""
            CLUB_TEAM_INFO = ClubTeamSimpleDesc(val: nil)
            PRIMARY_CONTACT = PrimaryContact(val: nil)
            USER_INFO = UserInfo(val: nil)
            ROLE_INFO = Role(val: nil)
            SPORT_INFO = Production(val: nil)
            IS_ACTIVE = "0"
            IS_PLAYER = "0"
            SUB_TEAMS = "0"
            ROSTER_EMAIL = ""
            ROSTER_WORK_PHONE = ""
            ROSTER_CELL_PHONE = ""
            ROSTER_HOME_PHONE = ""
            ROSTER_PROFILE_PIC = ""
            ADDITION_INFO_ROSTER = ""
        }
    }
}
