//
//  UserInfo.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
struct UserInfo {
    var EMAIL: String
    var ACTIVE: String
    var FIRST_LOG_IN: String
    var ID: String
    var FNAME: String
    var LNAME: String
    var DEFAULT_MEMBER_LOG: String

    var PHONE: String
    var ADDR: String
    var CITY: String
    var STATE: String
    var ZIP: String

    init(val: NSDictionary?) {
        if val != nil {
            EMAIL = val!["EMAIL"] as! String
            ACTIVE = val!["ACTIVE"] as! String
            FIRST_LOG_IN = val!["FIRST_LOG_IN"] as! String
            ID = val!["ID"] as! String
            FNAME = val!["FNAME"] as! String
            LNAME = val!["LNAME"] as! String
            DEFAULT_MEMBER_LOG = val!["DEFAULT_MEMBER_LOG"] as! String

            PHONE = val!["PHONE"] as! String
            ADDR = val!["ADDR"] as! String
            CITY = val!["CITY"] as! String
            STATE = val!["STATE"] as! String
            ZIP = val!["ZIP"] as! String
        } else {
            EMAIL = ""
            ACTIVE  = "0"
            FIRST_LOG_IN = "0"
            ID = ""
            FNAME = ""
            LNAME = ""
            DEFAULT_MEMBER_LOG =  ""

            PHONE = ""
            ADDR = ""
            CITY = ""
            STATE = ""
            ZIP = ""
        }
    }
}
