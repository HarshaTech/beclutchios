//
//  FullRosterPlayer.swift
//  beclutch
//
//  Created by zeus on 2020/1/6.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class FullRosterPlayer {
    var player: RosterInfo?
    var contacts: [RosterInfo]?
    var guests: [RosterInfo]?

    init(val: NSDictionary?) {
        if let value = val {
            self.player = RosterInfo(val: (value["player"] as! NSDictionary))
            self.contacts = []
            if let dicContacts = value["contacts"] as? [NSDictionary] {
                for item in dicContacts {
                    let contact = RosterInfo(val: item)
                    self.contacts?.append(contact)
                }
            } else {
                self.contacts = []
            }

            self.guests = []
            if let dicGuests = value["guests"] as? [NSDictionary] {
                for item in dicGuests {
                    let contact = RosterInfo(val: item)
                    self.guests?.append(contact)
                }
            } else {
                self.player = RosterInfo(val: nil)
                self.contacts = []
                self.guests = []
            }
        } else {
            self.player = RosterInfo(val: nil)
            self.contacts = []
            self.guests = []
        }
    }
}
