//
//  Role.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class Role {
    var ID: String = ""
    var ROLE_NAME: String = ""
    var LEVEL: String = ""
    init(val: NSDictionary?) {
        if let dict = val {
            if let roleId = dict["ID"] as? String {
                ID = roleId
            }
            if let roleName = dict["ROLE_NAME"] as? String {
                ROLE_NAME = roleName
            }
            if let level = dict["LEVEL"] as? String {
                LEVEL = level
            }
        }
    }

    static func createEveryoneDefault() -> Role {
        let model: Role = Role(val: nil)
        model.ID = "0"
        model.LEVEL = "0"
        model.ROLE_NAME = "Everyone"
        return model
    }
}
