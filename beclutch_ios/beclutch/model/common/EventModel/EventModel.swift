//
//  EventModel.swift
//  beclutch
//
//  Created by Chung Le on 4/16/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

class EventModel {
    enum EventType: Int {
        case practice = 0
        case game = 1
        case event = 2
    }
    
    var ID: String?
    var NAME: String?
    var type: EventType = .event

    var START_TIME_HOUR: String?
    var START_TIME_MIN: String?
    var START_TIME_AMPM: String?

    var ARRIVAL_TIME_HOUR: String?
    var ARRIVAL_TIME_MIN: String?
    var ARRIVAL_TIME_AMPM: String?

    var DATE_YEAR: String?
    var DATE_MONTH: String?
    var DATE_DAY_OF_MONTH: String?

    var DURATION_HOUR: String?
    var DURATION_MIN: String?
    
    var HR: String?
    var hasVolunteer: Bool = false

    var location_detail: TeamLocation?
    var attendance_detail: Attendance?

    var eventDate: Date?

    var getDurationHour: Int {
        if let hour = DURATION_HOUR, let hourInt = Int(hour), hourInt >= 0 {
            return hourInt
        }

        return 0
    }

    var getDurationMinute: Int {
        if let min = DURATION_MIN, let minInt = Int(min), minInt >= 0 {
            return minInt
        }

        return 0
    }

    var realStartTimeHour: String? {
        if START_TIME_HOUR == "0" {
            return "12"
        }

        return START_TIME_HOUR
    }

    var realArriveTimeHour: String? {
        if ARRIVAL_TIME_HOUR == "0" {
            return "12"
        }

        return ARRIVAL_TIME_HOUR
    }

    var startTimeDisplay: String {
        if let hour = realStartTimeHour, var min = START_TIME_MIN, let ampm = START_TIME_AMPM {
            if min.count == 1 {
                min = "0\(min)"
            }

            return hour + ":" + min + " " + ampm
        }

        return ""
     }

    var slashesDateDisplay: String {
        if let month = DATE_MONTH, let day = DATE_DAY_OF_MONTH, let year = DATE_YEAR {
            return month + "/" + day + "/" + year
        }

        return ""
    }
    
    var fullDate: Date? {
        return Date.dateFromFullString(dateStr: HR)
    }
    
    var title: String {
        switch type {
        case .game:
            return "Game"
        case .practice:
            return "Practice"
        default:
            return "Event"
        }
    }
    
    var getName: String {
        return self.NAME ?? ""
    }
    
    var getDisplayedAddress: String {
        return self.location_detail!.id!.isEmpty ? "No Location" : location_detail?.name ?? ""
    }
    
    var getDisplayedDetailAddress: String {
       return location_detail!.id!.isEmpty ? "No Location yet" : StringCheckUtils.getAddressFromLocation(model: location_detail!)
    }
    
    var getAddressUrl: URL? {
        let addr = StringCheckUtils.getAddressFromLocation(model: location_detail!).replacingOccurrences(of: " ", with: "+")
        let targetAddr = "http://maps.apple.com/?daddr=" + addr
        
        return URL(string: targetAddr)
    }
    
    init(val: NSDictionary?) {
        if let value = val {
            self.HR = value["HR"] as? String
            self.eventDate = Date.dateFromFullString(dateStr: value["HR"] as? String)

            if let hasVolunteer = value["has_volunteer"] as? Bool {
                self.hasVolunteer = hasVolunteer
            }
        }
    }
}
