//
//  PracticeModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class PracticeModel: EventModel {
    static let PRACTICE_ACTIVE = 1
    static let PRACTICE_CANCEL = 0

    var TEAM_ID: String?
    var DAY_OF_WEEK: String?

    var REPEAT_MODEL: String?
    var REPEAT_DETAIL: RepeatModel?

    var LOCATION_ID: String?
    var ADDR: String?
    var MEMO: String?

    var IS_ACTIVE: String?

    var dateDisplay: String {
        if let month = DATE_MONTH, let day = DATE_DAY_OF_MONTH, let year = DATE_YEAR {
            return month + "-" + day + "-" + year
        }

        return ""
    }

    var fullDateInt: Int {
        var month = (DATE_MONTH ?? "")
        while month.count < 2 {
            month = "0" + month
        }

        var day = (DATE_DAY_OF_MONTH ?? "")
        while day.count < 2 {
            day = "0" + day
        }

        return Int((DATE_YEAR ?? "") + month + day) ?? 0
    }

//    var eventDate: Date? {
//        if let hour = Int(START_TIME_HOUR ?? ""), let min = Int(START_TIME_MIN ?? ""), let ampm = START_TIME_AMPM {
//            return Date.makeDate(yearStr: DATE_YEAR, monthStr: DATE_MONTH, dayStr: DATE_DAY_OF_MONTH)?.adjustDate(hour: hour, minute: min, ampm: ampm)
//        }
//
//        return nil
//    }

    var duration: Int {
        if let hour: Int = Int(DURATION_HOUR ?? ""), let min: Int = Int(DURATION_MIN ?? "") {
            return (hour * 60 + min) * 60
        }

        return 600
    }

    override init(val: NSDictionary?) {
        super.init(val: val)
        
        self.type = .practice

        if val != nil {
            self.ID = (val!["ID"] as! String)
            self.TEAM_ID = (val!["TEAM_ID"] as! String)
            self.DAY_OF_WEEK = (val!["DAY_OF_WEEK"] as! String)
            
            self.NAME = (val!["NAME"] as! String)
            if self.NAME != "Practice" {
                self.type = .event
            }
            
            self.DATE_YEAR = (val!["DATE_YEAR"] as! String)
            self.DATE_MONTH = (val!["DATE_MONTH"] as! String)
            self.DATE_DAY_OF_MONTH = (val!["DATE_DAY_OF_MONTH"] as! String)
            self.REPEAT_MODEL = (val!["REPEAT_MODEL"] as! String)
            if self.REPEAT_MODEL!.isEmpty {
                self.REPEAT_DETAIL = RepeatModel(val: nil)
            } else {
                let data = self.REPEAT_MODEL!.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                        self.REPEAT_DETAIL = RepeatModel(val: jsonArray)
                    } else {
                        self.REPEAT_DETAIL = RepeatModel(val: nil)
                    }
                } catch _ as NSError {
                    self.REPEAT_DETAIL = RepeatModel(val: nil)
                }
            }

            self.START_TIME_HOUR = (val!["START_TIME_HOUR"] as! String)
            self.START_TIME_MIN = (val!["START_TIME_MIN"] as! String)
            self.START_TIME_AMPM = (val!["START_TIME_AMPM"] as! String)
            self.ARRIVAL_TIME_HOUR = (val!["ARRIVAL_TIME_HOUR"] as! String)
            self.ARRIVAL_TIME_MIN = (val!["ARRIVAL_TIME_MIN"] as! String)
            self.ARRIVAL_TIME_AMPM = (val!["ARRIVAL_TIME_AMPM"] as! String)
            self.LOCATION_ID = (val!["LOCATION_ID"] as! String)
            if let locDic = val!["location_detail"] as? NSDictionary {
                self.location_detail = TeamLocation(val: locDic)
            } else {
                self.location_detail = TeamLocation(val: nil)
            }

            self.ADDR = (val!["ADDR"] as! String)
            self.MEMO = (val!["MEMO"] as! String)

            self.DURATION_HOUR = (val!["DURATION_HOUR"] as! String)
            self.DURATION_MIN = (val!["DURATION_MIN"] as! String)

            if let locDic = val!["attendance_detail"] as? NSDictionary {
                self.attendance_detail = Attendance(val: locDic)
            } else {
                self.attendance_detail = Attendance(val: nil)
            }
            self.IS_ACTIVE = (val!["IS_ACTIVE"] as! String)
        } else {

            self.DAY_OF_WEEK = ""
            self.NAME = ""
            self.DATE_YEAR = ""
            self.DATE_MONTH = ""
            self.DATE_DAY_OF_MONTH = ""
            self.REPEAT_MODEL = ""
            self.START_TIME_HOUR = ""
            self.START_TIME_MIN = ""
            self.ARRIVAL_TIME_HOUR = ""
            self.ARRIVAL_TIME_MIN = ""
            self.LOCATION_ID = ""
            self.ADDR = ""
            self.MEMO = ""
            self.DURATION_HOUR = ""
            self.DURATION_MIN = ""
            self.IS_ACTIVE = ""
            self.ID = ""
            self.TEAM_ID = ""
            self.location_detail = TeamLocation(val: nil)
            self.attendance_detail = Attendance(val: nil)
            self.REPEAT_DETAIL = RepeatModel(val: nil)
            self.ARRIVAL_TIME_AMPM = ""
            self.START_TIME_AMPM = ""
        }
    }
}
