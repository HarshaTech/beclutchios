//
//  GameModel.swift
//  beclutch
//
//  Created by zeus on 2020/1/10.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class GameModel: EventModel {
    static let GAME_ACTIVE = 1
    static let GAME_CANCEL = 0

    var TEAM_ID: String?
    var opponent_detail: OpponentModel?

    var LOCATION_ID: String?
    var ADDR: String?
    var MEMO: String?
    var UNIFORM_COLOR: Int32?
    var IS_HOME: String?
    var DAY_OF_WEEK: String?

    var IS_ACTIVE: String?

    var dateDisplay: String {
        if let month = DATE_MONTH, let day = DATE_DAY_OF_MONTH, let year = DATE_YEAR {
            return month + "-" + day + "-" + year
        }

        return ""
    }
    
    var arrivalTimeDisplay: String {
        return String(format: "%d:%02d", Int(ARRIVAL_TIME_HOUR!)! > 12 ? Int(ARRIVAL_TIME_HOUR!)! - 12 : Int(realArriveTimeHour!)!, Int(ARRIVAL_TIME_MIN!)!) + " " + ARRIVAL_TIME_AMPM!
    }

    var fullDateInt: Int {
        var month = (DATE_MONTH ?? "")
        while month.count < 2 {
            month = "0" + month
        }

        var day = (DATE_DAY_OF_MONTH ?? "")
        while day.count < 2 {
            day = "0" + day
        }

        return Int((DATE_YEAR ?? "") + month + day) ?? 0
    }

//    var eventDate: Date? {
//        if let hour = Int(START_TIME_HOUR ?? ""), let min = Int(START_TIME_MIN ?? ""), let ampm = START_TIME_AMPM {
//            return Date.makeDate(yearStr: DATE_YEAR, monthStr: DATE_MONTH, dayStr: DATE_DAY_OF_MONTH)?.adjustDate(hour: hour, minute: min, ampm: ampm)
//        }
//
//        return nil
//    }
    
    var getOponentName: String {
        return self.opponent_detail?.OPPONENT_NAME ?? ""
    }

    override init(val: NSDictionary?) {
        super.init(val: val)

        self.type = .game
        
        if let value = val {
            self.ID = (value["ID"] as! String)
            self.TEAM_ID = (value["TEAM_ID"] as! String)
            self.NAME = (value["NAME"] as! String)
            self.DATE_YEAR = (value["DATE_YEAR"] as! String)
            self.DATE_MONTH = (value["DATE_MONTH"] as! String)
            if let oppoDic = value["opponent_detail"] as? NSDictionary {
                self.opponent_detail = OpponentModel(val: oppoDic)
            } else {
                self.opponent_detail = OpponentModel(val: nil)
            }
            self.DATE_DAY_OF_MONTH = (value["DATE_DAY_OF_MONTH"] as! String)
            self.START_TIME_HOUR = (value["START_TIME_HOUR"] as! String)
            self.START_TIME_MIN = (value["START_TIME_MIN"] as! String)
            self.START_TIME_AMPM = (value["START_TIME_AMPM"] as! String)
            self.ARRIVAL_TIME_HOUR = (value["ARRIVAL_TIME_HOUR"] as! String)
            self.ARRIVAL_TIME_MIN = (value["ARRIVAL_TIME_MIN"] as! String)
            self.ARRIVAL_TIME_AMPM = (value["ARRIVAL_TIME_AMPM"] as! String)
            self.LOCATION_ID = (value["LOCATION_ID"] as! String)
            if let locDic = value["location_detail"] as? NSDictionary {
                self.location_detail = TeamLocation(val: locDic)
            } else {
                self.location_detail = TeamLocation(val: nil)
            }
            self.ADDR = (value["ADDR"] as! String)
            self.MEMO = (value["MEMO"] as! String)
            if let intColor = value["UNIFORM_COLOR"] as? Int32 {
                self.UNIFORM_COLOR = intColor
            } else {
                self.UNIFORM_COLOR = Int32(value["UNIFORM_COLOR"] as! String)
            }

            self.IS_HOME = (value["IS_HOME"] as! String)
            self.DAY_OF_WEEK = (value["DAY_OF_WEEK"] as! String)
            self.DURATION_HOUR = (value["DURATION_HOUR"] as! String)
            self.DURATION_MIN = (value["DURATION_MIN"] as! String)
            self.IS_ACTIVE = (value["IS_ACTIVE"] as! String)
            if let attDic = value["attendance_detail"] as? NSDictionary {
                self.attendance_detail = Attendance(val: attDic)
            } else {
                self.attendance_detail = Attendance(val: nil)
            }
        } else {
            self.ID = ""
            self.location_detail = TeamLocation(val: nil)
            self.opponent_detail = OpponentModel(val: nil)
            self.attendance_detail = Attendance(val: nil)
        }
    }
}
