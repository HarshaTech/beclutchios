//
//  ClubTeamSimpleDesc.swift
//  beclutch
//
//  Created by zeus on 2019/11/16.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
struct ClubTeamSimpleDesc {
    var ID: String
    var PRODUCTION: String
    var NAME: String
    var OWNER_ID: String
    var PIC_URL: String
    var TIME_ZONE: String
    var PRACTICE_HOURS: String
    var PRACTICE_MINS: String
    var ARRIVE_HOURS: String
    var ARRIVE_MINS: String
    var HOME_COLOR: String
    var AWAY_COLOR: String
    var ADDITION_COLOR_1: String
    var ADDITION_COLOR_2: String
    var PLAN_LEVEL: String

    var IS_SELECT: String
    var COLOR_VALUE: String
    var COLOR_NAME: String
    var COLOR_RESOURCE_PRIMARY: String
    var COLOR_RESOURCE_DARK: String
    var THEME_RESOURCE: String

    init(val: NSDictionary?) {
        if val != nil {
            ID = val!["ID"] as! String
            PRODUCTION = val!["PRODUCTION"] as! String
            NAME = val!["NAME"] as! String
            OWNER_ID = val!["OWNER_ID"] as! String
            PIC_URL = val!["PIC_URL"] as! String
            if let timezone = val!["TIME_ZONE"] as? String {
                TIME_ZONE = timezone
            } else {
                TIME_ZONE = "0"
            }
            if let pracHour = val!["PRACTICE_HOURS"] as? String {
                PRACTICE_HOURS = pracHour
            } else {
                PRACTICE_HOURS = ""
            }

            if let pracMin = val!["PRACTICE_MINS"] as? String {
                PRACTICE_MINS = pracMin
            } else {
                PRACTICE_MINS = ""
            }

            if let arrHour = val!["ARRIVE_HOURS"] as? String {
                ARRIVE_HOURS = arrHour
            } else {
                ARRIVE_HOURS = ""
            }

            if let arrMin = val!["ARRIVE_MINS"] as? String {
                ARRIVE_MINS = arrMin
            } else {
                ARRIVE_MINS = ""
            }

            if let tmp = val!["HOME_COLOR"] as? String {
                HOME_COLOR = tmp
            } else {
                HOME_COLOR = ""
            }

            if let tmp = val!["AWAY_COLOR"] as? String {
                AWAY_COLOR = tmp
            } else {
                AWAY_COLOR = ""
            }

            if let addition1 = val!["ADDITION_COLOR_1"] as? String {
                ADDITION_COLOR_1 = addition1
            } else {
                ADDITION_COLOR_1 = ""
            }

            if let addition2 = val!["ADDITION_COLOR_2"] as? String {
                ADDITION_COLOR_2 = addition2
            } else {
                ADDITION_COLOR_2 = ""
            }

            if let plan = val!["PLAN_LEVEL"] as? String {
                PLAN_LEVEL = plan
            } else {
                PLAN_LEVEL = ""
            }

            if let isSelect = val!["IS_SELECT"] as? String {
                IS_SELECT = isSelect
            } else {
                IS_SELECT = "0"
            }

            if let colorValue = val!["COLOR_VALUE"] as? String {
                COLOR_VALUE = colorValue
            } else {
                COLOR_VALUE = "beclutch"
            }

            if let colorName = val!["COLOR_NAME"] as? String {
                COLOR_NAME = colorName
            } else {
                COLOR_NAME = "Beclutch"
            }

            if let colorPrimary = val!["COLOR_RESOURCE_PRIMARY"] as? String {
                COLOR_RESOURCE_PRIMARY = colorPrimary
            } else {
                COLOR_RESOURCE_PRIMARY = ""
            }

            if let colorDark = val!["COLOR_RESOURCE_DARK"] as? String {
                COLOR_RESOURCE_DARK = colorDark
            } else {
                COLOR_RESOURCE_DARK = ""
            }

            if let themeResource = val!["THEME_RESOURCE"] as? String {
                THEME_RESOURCE = themeResource
            } else {
                THEME_RESOURCE = "0"
            }

        } else {
            ID = ""
            PRODUCTION = ""
            NAME = ""
            OWNER_ID = ""
            PIC_URL = ""
            TIME_ZONE = ""
            PRACTICE_HOURS = ""
            PRACTICE_MINS = ""
            ARRIVE_HOURS = ""
            ARRIVE_MINS = ""
            HOME_COLOR = ""
            AWAY_COLOR = ""
            ADDITION_COLOR_1 = ""
            ADDITION_COLOR_2 = ""
            PLAN_LEVEL = ""

            IS_SELECT = "0"
            COLOR_VALUE = "beclutch"
            COLOR_NAME = "Beclutch"
            COLOR_RESOURCE_PRIMARY = ""
            COLOR_RESOURCE_DARK = ""
            THEME_RESOURCE = "0"
        }
    }
}
