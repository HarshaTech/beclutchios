//
//  FeeInfo.swift
//  beclutch
//
//  Created by SM on 2/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
import FirebaseFirestore

class FeeInfo {
    var id: String?
    var parentId: String?
    var teamId: String?
    var rosterId: String?
    var description: String?

    var amount: Float?
    var paid: Float?
    var dueDate: Date?
    var createdDate: Date?

    var name: String?
    var canShowPaymentHistory: Bool = false

    var balance: Float {
        if let amount = amount, let paid = paid {
            return amount - paid
        }

        return 0
    }

    var amountStr: String {
        return String(amount ?? 0.0)
    }

    var paidStr: String {
        return String(paid ?? 0.0)
    }

    init(val: NSDictionary?) {
        if let dict = val {
            self.id = dict["FINANCE_DETAIL_ID"] as? String
            self.parentId = dict["FINANCE_ID"] as? String

            self.teamId = dict["TEAM_ID"] as? String
            self.rosterId = dict["ROSTER_ID"] as? String
            self.description = dict["DESCRIPTION"] as? String

            if let firstName = dict["ROSTER_FIRST_NAME"] as? String, let lastName = dict["ROSTER_LAST_NAME"] as? String {
                self.name = firstName + " " + lastName
            }

            if let amount = dict["AMOUNT"] as? String {
                self.amount = Float(amount) ?? 0.0
            }

            if let paid = dict["PAID"] as? String {
                self.paid = Float(paid) ?? 0.0
            }

            if let dateStr = dict["DUE_AT"] as? String {
                if let dateDouble = Double(dateStr) {
                    let date = Date(timeIntervalSince1970: dateDouble)
                    self.dueDate = date
                }
            }

            if let dateStr = dict["CREATED_AT"] as? String {
                if let dateDouble = Double(dateStr) {
                    let date = Date(timeIntervalSince1970: dateDouble)
                    self.createdDate = date
                }
            }
            
            if let isShow = dict["CAN_VIEW_PAYMENT_HISTORY"] as? Bool {
                self.canShowPaymentHistory = isShow
            }
        }
    }

    static func parseData(array: [NSDictionary]) -> [FeeInfo] {
        var result: [FeeInfo] = []

        for dict in array {
            let item = FeeInfo(val: dict)
            result.append(item)
        }

        return result
    }

    static func totalDueAmount(fees: [FeeInfo]) -> Float {
        var totalDue: Float = 0
        for fee in fees {
            totalDue += fee.balance
        }

        return totalDue
    }

    static func totalPaidAmount(fees: [FeeInfo]) -> Float {
        var totalPaid: Float = 0
        for fee in fees {
            totalPaid += fee.paid ?? 0
        }

        return totalPaid
    }
}

class LeaderBoardInfo {
    var id: String?
    var name: String?
    var startDate: String?
    var endDate: String?
    var type: String?

    var colnName: String?
    var editAll: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.id = dict["ID"] as? String
            self.name = dict["NAME"] as? String
            self.startDate = dict["START_DATE"] as? String
            self.endDate = dict["END_DATE"] as? String
            self.type = dict["TYPE"] as? String
            self.colnName = dict["TYPE_COLNAME"] as? String
            self.editAll = dict["EDIT_ALL"] as? String
        }
    }

    static func parseData(array: [NSDictionary]) -> [LeaderBoardInfo] {
        var result: [LeaderBoardInfo] = []

        for dict in array {
            let item = LeaderBoardInfo(val: dict)
            result.append(item)
        }

        return result
    }

}


class LeaderBoardTeamListInfo {
    var id: String?
    var roaster_id: String?
    var firstName: String?
    var lastName: String?
    var score: String?

    init(val: NSDictionary?) {
        if let dict = val {
            self.id = dict["ID"] as? String
            self.roaster_id = dict["ROSTER_ID"] as? String
            self.firstName = dict["ROSTER_FIRST_NAME"] as? String
            self.lastName = dict["ROSTER_LAST_NAME"] as? String
            self.score = dict["SCORE"] as? String
        }
    }

    static func parseData(array: [NSDictionary]) -> [LeaderBoardTeamListInfo] {
        var result: [LeaderBoardTeamListInfo] = []

        for dict in array {
            let item = LeaderBoardTeamListInfo(val: dict)
            result.append(item)
        }

        return result
    }

}
