//
//  ScoreModel.swift
//  beclutch
//
//  Created by Triet Nguyen on 8/21/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
struct ScoreModel {
    var GAME_ID: String = ""
    var TEAM_SCORE: String = ""
    var OPPONENT_SCORE: String = ""
    var MEMO: String = ""
    var FLAG: String = ""
    var UPDATE_AT: String = ""
    var CREATE_AT: String = ""

    init(dict: NSDictionary?) {
        guard let val = dict else {
            return
        }

        GAME_ID = val["GAME_ID"] as! String
        if let TEAM_SCORE = val["TEAM_SCORE"] as? String {
            self.TEAM_SCORE = TEAM_SCORE
        }

        if let OPPONENT_SCORE = val["OPPONENT_SCORE"] as? String {
            self.OPPONENT_SCORE = OPPONENT_SCORE
        }

        if let memo = val["MEMO"] as? String {
            MEMO = memo
        }

        if let flag = val["FLAG"] as? String {
            FLAG = flag
        }

        UPDATE_AT = ""
        CREATE_AT = ""
    }
}
