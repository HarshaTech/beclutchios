//
//  PrimaryContact.swift
//  beclutch
//
//  Created by FAT_MAC on 7/29/20.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation

struct PrimaryContact {
    var PRIMARY_FIRST_NAME: String
    var PRIMARY_LAST_NAME: String
    var PRIMARY_EMAIL: String
    var PRIMARY_CELL_PHONE: String
    var PRIMARY_HOME_PHONE: String
    var PRIMARY_WORK_PHONE: String
    init(val: NSDictionary?) {
        if val != nil {

            if let PRIMARY_FIRST_NAME = val!["PRIMARY_FIRST_NAME"] as? String {
                self.PRIMARY_FIRST_NAME = PRIMARY_FIRST_NAME
            } else {
                self.PRIMARY_FIRST_NAME = ""
            }
            if let PRIMARY_LAST_NAME = val!["PRIMARY_LAST_NAME"] as? String {
                self.PRIMARY_LAST_NAME = PRIMARY_LAST_NAME
            } else {
                self.PRIMARY_LAST_NAME = ""
            }
            if let PRIMARY_EMAIL = val!["PRIMARY_EMAIL"] as? String {
                self.PRIMARY_EMAIL = PRIMARY_EMAIL
            } else {
                self.PRIMARY_EMAIL = ""
            }
            if let PRIMARY_CELL_PHONE = val!["PRIMARY_CELL_PHONE"] as? String {
                self.PRIMARY_CELL_PHONE = PRIMARY_CELL_PHONE
            } else {
                self.PRIMARY_CELL_PHONE = ""
            }
            if let PRIMARY_HOME_PHONE = val!["PRIMARY_HOME_PHONE"] as? String {
                self.PRIMARY_HOME_PHONE = PRIMARY_HOME_PHONE
            } else {
                self.PRIMARY_HOME_PHONE = ""
            }
            if let PRIMARY_WORK_PHONE = val!["PRIMARY_WORK_PHONE"] as? String {
                self.PRIMARY_WORK_PHONE = PRIMARY_WORK_PHONE
            } else {
                self.PRIMARY_WORK_PHONE = ""
            }
        } else {
            self.PRIMARY_FIRST_NAME = ""
            self.PRIMARY_LAST_NAME = ""
            self.PRIMARY_EMAIL = ""
            self.PRIMARY_CELL_PHONE = ""
            self.PRIMARY_HOME_PHONE = ""
            self.PRIMARY_WORK_PHONE = ""
        }
    }
}
