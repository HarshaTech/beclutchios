//
//  OpponentModel.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
public class OpponentModel {
    var ID: String?
    var OPPONENT_NAME: String?
    var CONTACT_NAME: String?
    var CONTACT_PHONE: String?
    var CONTACT_EMAIL: String?
    var CONTACT_NOTE: String?
    var USER_ID: String?

    public init(val: NSDictionary?) {
        if val != nil {
            ID = (val!["ID"] as! String)
            OPPONENT_NAME = (val!["OPPONENT_NAME"] as! String)
            CONTACT_NAME = (val!["CONTACT_NAME"] as! String)
            CONTACT_PHONE = (val!["CONTACT_PHONE"] as! String)
            CONTACT_EMAIL = (val!["CONTACT_EMAIL"] as! String)
            CONTACT_NOTE = (val!["CONTACT_NOTE"] as! String)
            USER_ID = (val!["USER_ID"] as! String)
        } else {
            ID = ""
            OPPONENT_NAME = ""
            CONTACT_NAME = ""
            CONTACT_PHONE = ""
            CONTACT_EMAIL = ""
            CONTACT_NOTE = ""
            USER_ID = ""
        }
    }
}
