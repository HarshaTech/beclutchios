//
//  NotificationSetting.swift
//  beclutch
//
//  Created by zeus on 2019/12/6.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class NotificationSetting {
    var id: String?
    var userId: String?
    var schApp: String?
    var schMail: String?
    var chatApp: String?
    var chatMail: String?
    var scoreApp: String?
    var scoreMail: String?
    var reminderApp: String?
    var reminderMail: String?
    var tourApp: String?
    var tourMail: String?

    init(val: NSDictionary?) {
        if let value = val {
            self.id = (value["ID"] as? String)
            self.userId = (value["USER_ID"] as? String)
            self.schApp = (value["SCHEDULE_APP"] as? String)
            self.schMail = (value["SCHEDULE_MAIL"] as? String)
            self.chatApp = (value["CHAT_APP"] as? String)
            self.chatMail = (value["CHAT_MAIL"] as? String)
            self.scoreApp = (value["SCORE_APP"] as? String)
            self.scoreMail = (value["SCORE_MAIL"] as? String)
            self.reminderApp = (value["REMINDER_APP"] as? String)
            self.reminderMail = (value["REMINDER_MAIL"] as? String)
            self.tourApp = (value["TOUR_APP"] as? String)
            self.tourMail = (value["TOUR_MAIL"] as? String)
        } else {
            self.id = ""
            self.userId = ""
            self.schApp = ""
            self.schMail = ""
            self.chatApp = ""
            self.chatMail = ""
            self.scoreApp = ""
            self.scoreMail = ""
            self.reminderApp = ""
            self.reminderMail = ""
            self.tourApp = ""
            self.tourMail = ""
        }
    }
}
