//
//  RosterForEvent.swift
//  beclutch
//
//  Created by zeus on 2020/1/11.
//  Copyright © 2020 zeus. All rights reserved.
//

import Foundation
class RosterForEvent {
    var attendance: String?
    var firstName: String?
    var secondName: String?
    var isActive: String?
    var rosterId: String?
    var userId: String?
    var userLevel: String?
    var userLevelStr: String?
    var healthStatus: Int = 2

    init(val: NSDictionary?) {
        if let val = val {
            if let strAttendance = val["ATTENDANCE"] as? String {
                self.attendance = strAttendance
            } else {
                 self.attendance = ""
            }
            self.firstName = (val["FIRST_NAME"] as! String)
            self.secondName = (val["LAST_NAME"] as! String)
            self.isActive = (val["IS_ACTIVE"] as! String)
            self.rosterId = (val["ROSTER_ID"] as! String)
            self.userId = (val["USER_ID"] as! String)
            self.userLevel = (val["USER_LEVEL_INT"] as! String)
            self.userLevelStr = (val["USER_LEVEL_STR"] as! String)

            if let healthStatus = val["HEALTH_STATUS"] as? String, let status = Int(healthStatus) {
                self.healthStatus = status
            }
        } else {
            self.attendance = ""
            self.firstName = ""
            self.secondName = ""
            self.isActive = ""
            self.rosterId = ""
            self.userId = ""
            self.userLevel = ""
            self.userLevelStr = ""
        }
    }
}
