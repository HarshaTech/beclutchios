//
//  TeamLocation.swift
//  beclutch
//
//  Created by zeus on 2019/12/5.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
class TeamLocation {
    var id: String?
    var userId: String?
    var country: String?
    var countryIdx: String?
    var state: String?
    var stateIdx: String?
    var city: String?
    var street: String?
    var zip: String?
    var name: String?
    var link: String?
    var note: String?
    var gps_lat: String?
    var gps_lot: String?

    init(val: NSDictionary?) {
        if val != nil {
            self.id = (val!["ID"] as! String)
            self.userId = (val!["USER_ID"] as! String)
            self.country = (val!["LOCATION_COUNTRY"] as! String)
            self.countryIdx = (val!["LOCATION_COUNTRY_IDX"] as! String)
            self.state = (val!["LOCATION_STATE"] as! String)
            self.stateIdx = (val!["LOCATION_STATE_IDX"] as! String)
            self.city = (val!["LOCATION_CITY"] as! String)
            self.street = (val!["LOCATION_STREET"] as! String)
            self.zip = (val!["LOCATION_ZIP"] as! String)
            self.name = (val!["LOCATION_NAME"] as! String)
            self.link = (val!["LOCATION_LINK"] as! String)
            self.note = (val!["LOCATION_NOTE"] as! String)
            self.gps_lat = (val!["GPS_LAT"] as! String)
            self.gps_lot = (val!["GPS_LOT"] as! String)

        } else {
            self.id = ""
            self.userId = ""
            self.country = ""
            self.countryIdx = ""
            self.state = ""
            self.stateIdx = ""
            self.city = ""
            self.street = ""
            self.zip = ""
            self.name = ""
            self.link = ""
            self.note = ""
            self.gps_lat = ""
            self.gps_lot = ""
        }
    }
}
