//
//  globals.swift
//  beclutch
//
//  Created by zeus on 2019/11/15.
//  Copyright © 2019 zeus. All rights reserved.
//

import Foundation
import UIKit
func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

class AppDataInstance {
    static let instance = AppDataInstance()

    private var token: String?
    private var email: String?
    private var firebaseToken: String?
    private(set) var myClubTeamList: [RosterInfo]?
    
    var me: UserInfo?
    var currentSelectedTeam: RosterInfo?
    var roles: [Role]?
    
    var currentTeamId: String {
        return currentSelectedTeam?.teamOrClubId ?? ""
    }
    
    var currentRosterId: Int {
        return Int(currentSelectedTeam?.ID ?? "") ?? 0
    }
    
    // Dot
    private var chatDotList: [ChatDotModel] = []
    private (set) var allTeamIdList: [Int] = []
    private (set) var allRosterIdList: [Int] = []
    
    init() {

    }
    
    var getTeamName: String {
        return currentSelectedTeam?.teamName ?? ""
    }
    
    var getUserName: String {
        guard let team = currentSelectedTeam else {
            return ""
        }
        
        return team.ROSTER_FIRST_NAME + " " + team.ROSTER_LAST_NAME
    }

    func getToken() -> String {
        if self.token == nil || self.token!.isEmpty {
            self.token = PreferenceMgr.readString(keyName: PreferenceMgr.TOKEN)
        }
        return self.token!
    }

    func setToken(token: String?) {
        if let token = token {
            PreferenceMgr.saveString(keyName: PreferenceMgr.TOKEN, stringValue: token)
            self.token = token
        }
    }

    func getEmail() -> String {
        if self.email == nil {
            self.email = PreferenceMgr.readString(keyName: PreferenceMgr.EMAIL)
        }
        return self.email!
    }

    func setEmail(val: String) {
        PreferenceMgr.saveString(keyName: PreferenceMgr.EMAIL, stringValue: val)
        self.email = val
    }

    func getFirebaseToken() -> String {
        if self.firebaseToken == nil {
            self.firebaseToken = PreferenceMgr.readString(keyName: PreferenceMgr.FIREBASE_TOKEN)
        }
        return self.firebaseToken!
    }

    func setFirebaseToken(val: String) {
        PreferenceMgr.saveString(keyName: PreferenceMgr.FIREBASE_TOKEN, stringValue: val)
        self.firebaseToken = val
    }

    func logoutAndResetDatas() {
        let keys = [PreferenceMgr.TOKEN, PreferenceMgr.EMAIL, PreferenceMgr.IS_AUTO_LOGIN]
        PreferenceMgr.deleteString(keyName: keys)
        AppDataInstance.instance.setToken(token: "")
    }
    
    func getMemberIdInTeam(teamId: String) -> Int? {
        if let teams = myClubTeamList {
            for team in teams {
                if team.teamOrClubId == teamId {
                    return team.rosterIdInt
                }
            }
        }
        
        return nil
    }
    
    func setMyClubTeamList(myClubTeamList: [RosterInfo]?) {
        guard let myClubTeamList = myClubTeamList else {
            return
        }
        
        self.myClubTeamList = myClubTeamList
        
        if myClubTeamList.count == 0 {
            self.allTeamIdList = []
            self.allRosterIdList = []
        } else {
            for i in 0 ..< myClubTeamList.count {
                if myClubTeamList[i].CLUB_OR_TEAM == "0" {
                    if !allTeamIdList.contains(myClubTeamList[i].teamIdInt) {
                        allTeamIdList.append(myClubTeamList[i].teamIdInt)
                    }
                    
                    if !allRosterIdList.contains(myClubTeamList[i].rosterIdInt) {
                        allRosterIdList.append(myClubTeamList[i].rosterIdInt)
                    }
                }
            }
        }
    }
    
    func isOnly1PendingTeam() -> Bool {
        if myClubTeamList?.count == 1, let onlyTeam = myClubTeamList?.first {
            if !onlyTeam.IS_ACTIVE.boolValueFromServer() {
                return true
            }
        }
        
        return false
    }
}
